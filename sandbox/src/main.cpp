/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include <iostream>

#include "ObjSandbox.hpp"
#include "PhysSandbox.hpp"
#include "RotationSandbox.hpp"
#include "ThreadSandbox.hpp"
#include "engine.hpp"

void printHelp()
{
    std::cout << "options: \n\tphys\n\tobj\n\trot\n\tthr\n" << std::endl;
}

#ifdef main
#undef main
#endif /* main */
int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printHelp();
    }
    else if (!strcmp(argv[1], "phys"))
    {
        PhysSandbox game = PhysSandbox();
        game.mainLoop(argc, argv);
    }
    else if (!strcmp(argv[1], "obj"))
    {
        ObjSandbox game = ObjSandbox();
        game.mainLoop(argc, argv);
    }
    else if (!strcmp(argv[1], "rot"))
    {
        RotationSandbox game = RotationSandbox();
        game.mainLoop(argc, argv);
    }
    else if (!strcmp(argv[1], "thr"))
    {
        ThreadSandbox().run();
    }

    return 0;
}