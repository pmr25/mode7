#include "ThreadSandbox.hpp"
#include "engine.hpp"

#include <cstdint>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#define NUM_OBJECTS 1'000'000U
#define BATCH_SIZE 1'000U
#define NUM_BATCHES (NUM_OBJECTS / BATCH_SIZE)

class Timer
{
public:
    void tick()
    {
        start = mode7::Clock::millis();
    }

    void tock()
    {
        end = mode7::Clock::millis();
    }

    uint64_t constexpr elapsed() const
    {
        return end - start;
    }

    uint64_t start;
    uint64_t end;
};

class Experiment
{
public:
    Experiment(uint32_t objs, uint32_t batches, uint32_t threads)
        : numObjects(objs)
        , batchSize(batches)
        , numThreads(threads)
    {
    }

    uint32_t constexpr numBatches() const
    {
        return numObjects / batchSize;
    }

    uint32_t numObjects;
    uint32_t batchSize;
    uint32_t numThreads;
};

class BatchUpdateJob : public mode7::Job
{
public:
    BatchUpdateJob(std::vector<mode7::Object*>& objs, uint32_t start, uint32_t end)
        : mode7::Job()
    {
        fill(objs, start, end);
    }

    virtual ~BatchUpdateJob() = default;

    void fill(std::vector<mode7::Object*>& objs, uint32_t start, uint32_t end)
    {
        for (auto i{start}; i < end; ++i)
        {
            objs[i]->setName("JOB[" + std::to_string(getID()) + "] object[" + std::to_string(i) + "]");
            toUpdate.push_back(objs[i]);
            // std::cout << "filling job with " << toUpdate.back()->getName() <<
            // ": " << glm::to_string(objs[i]->getFront()) << " " << objs[i] <<
            // std::endl;
        }
    }

    virtual void run()
    {
        for (auto obj : toUpdate)
        {
            // std::cout << "\tupdating " << obj->getName() << ": " << obj <<
            // std::endl; std::cout << "\tbefore " <<
            // glm::to_string(obj->getFront()) << "\n\t" <<
            // glm::to_string(obj->getWorldMatrix()) << std::endl;
            obj->update();
            // std::cout << "\tafter " << glm::to_string(obj->getFront()) <<
            // std::endl;
        }
    }

    std::vector<mode7::Object*> toUpdate;
};

static void randomizeObject(mode7::Object* obj)
{
    glm::vec3 pos(mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100));
    glm::vec3 rot(mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100));
    glm::vec3 scl(mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100), mode7::Util::randInt(-100, 100));

    obj->setPosition(pos);
    obj->setRotation(rot);
    // obj->setScale(scl);
}

static void randomizeObjects(std::vector<mode7::Object*>& vec)
{
    for (auto& obj : vec)
    {
        randomizeObject(obj);
    }
}

static std::string runExperiment(Experiment& experiment)
{
    // std::cout << "running " << experiment.numThreads << " thread experiment"
    // << std::endl;
    std::stringstream resultsRow;
    Timer experimentTimer;
    std::vector<mode7::Object*> objects;
    std::vector<BatchUpdateJob> jobs;

    objects.reserve(experiment.numObjects);
    for (auto i{0U}; i < experiment.numObjects; ++i)
    {
        objects.push_back(new mode7::Object());
    }
    randomizeObjects(objects);

    jobs.reserve(experiment.numBatches());

    for (auto i{0U}; i < experiment.numBatches(); ++i)
    {
        const uint32_t start = (i + 0) * experiment.batchSize;
        const uint32_t end = (i + 1) * experiment.batchSize - 1;
        assert(start < objects.size());
        assert(end < objects.size());
        jobs.push_back(BatchUpdateJob(objects, start, end));
    }

    experimentTimer.tick();
    mode7::Producer producer(experiment.numThreads);
    for (auto i{0U}; i < jobs.size(); ++i)
    {
        producer.feedJob(&jobs[i]);
    }
    producer.startConsumers();
    producer.distribute();
    experimentTimer.tock();

    // threads,objects,batches,batch_size,start,end,elapsed
    resultsRow << experiment.numThreads << "," << experiment.numObjects << "," << experiment.numBatches() << ","
               << experiment.batchSize << "," << experimentTimer.start << "," << experimentTimer.end << ","
               << experimentTimer.elapsed() << std::endl;

    return resultsRow.str();
}

void ThreadSandbox::run()
{
    std::cout << "ThreadSandbox" << std::endl;

    std::vector<Experiment> experiments;
    experiments.push_back(Experiment(1'000'000, 10, 1));
    experiments.push_back(Experiment(1'000'000, 10, 2));
    experiments.push_back(Experiment(1'000'000, 10, 4));
    experiments.push_back(Experiment(1'000'000, 10, 8));
    experiments.push_back(Experiment(1'000'000, 100, 1));
    experiments.push_back(Experiment(1'000'000, 100, 2));
    experiments.push_back(Experiment(1'000'000, 100, 4));
    experiments.push_back(Experiment(1'000'000, 100, 8));
    experiments.push_back(Experiment(1'000'000, 1000, 1));
    experiments.push_back(Experiment(1'000'000, 1000, 2));
    experiments.push_back(Experiment(1'000'000, 1000, 4));
    experiments.push_back(Experiment(1'000'000, 10000, 8));
    experiments.push_back(Experiment(1'000'000, 10000, 32));

    for (auto& experiment : experiments)
    {
        std::cout << runExperiment(experiment);
    }
}
