/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "PhysSandbox.hpp"

#include <iostream>

PhysSandbox::PhysSandbox()
{
}

PhysSandbox::~PhysSandbox()
{
}

void PhysSandbox::init()
{
    auto assets = mode7::AssetManager::createAndMakeCurrent();
    assets->setBaseDirectory("../assets/sandbox");
    assets->setModelDirectory("models");
    assets->setShaderDirectory("shaders");
    assets->setTextureDirectory("textures");

    std::shared_ptr<mode7::Shader> pass =
        std::make_shared<mode7::Shader>(assets->shaderPath("pass_v.glsl"), assets->shaderPath("pass_f.glsl"));

    std::shared_ptr<mode7::Shader> screen =
        std::make_shared<mode7::Shader>(assets->shaderPath("screen_v.glsl"), assets->shaderPath("screen_f.glsl"));

    mode7::GLScreen::create(1280, 720, 1280, 720, "Sandbox", false, false, false);
    mode7::Camera::createAndMakeCurrent(70.0f, 0.1f, 1500.0f);

    controls.setInput(ORBITCONTROLS_INPUT_MOUSE, 1);
    controls.attachCamera();
    controls.setPitchRange(-90, 90);

    basicShader.open(assets->shaderPath("basic_v.glsl"), assets->shaderPath("basic_f.glsl"));

    ground = mode7::ModelLoader::openMesh(assets->modelPath("ground.dae"), *assets);

    if (!ground)
    {
        LOG(0, LG_ERROR) << "ground was null";
        exit(1);
    }

    ground->setPosition(glm::vec3(0, 0.1, 0));
    // ground->rotate(M_PI / 12.f, 0.f, 0.f);
    ground->update();
    ground->setName("ground");

    auto tmp_msh = mode7::ModelLoader::openMesh(assets->modelPath("cube.dae"), *assets);

    cube = std::make_shared<PhysCar>();
    cube->setModel(tmp_msh);

    if (!cube)
    {
        LOG(0, LG_ERROR) << "cube was null";
        exit(1);
    }

    cube->setPosition(glm::vec3(0, 5, 0));
    cube->setMass(0.1f);
    cube->setName("cube");
    cube->addChild(&controls);

    mode7::Camera::getCurrent().lookAt(glm::vec3(12, 1, 12), glm::vec3(0));

    physics.addObject(ground.get(), "plane");
    ground->getRigidBody()->setFriction(0.0f);
    physics.addObject(cube.get(), "cube");
    cube->physRequestActive(true);
}

void PhysSandbox::update()
{
    cube->input();

    if (mode7::Keyboard::isPressed("g"))
    {
        mode7::Mouse::grab();
    }
    if (mode7::Keyboard::isPressed("r"))
    {
        mode7::Mouse::release();
    }
    if (mode7::Keyboard::isPressed("escape"))
    {
        mode7::Mouse::release();
        destroy();
    }

    controls.updateControls();

    physics.simulate();
    mode7::Camera::getCurrent().updateView();
}

void PhysSandbox::draw(int32_t step)
{
    ground->draw(&basicShader);
    cube->draw(&basicShader);
}
