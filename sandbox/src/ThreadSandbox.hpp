#ifndef THREADSANDBOX_HPP
#define THREADSANDBOX_HPP

class ThreadSandbox
{
public:
    ThreadSandbox()
    {
    }
    ~ThreadSandbox() = default;

    void run();
};

#endif /* THREADSANDBOX_HPP */
