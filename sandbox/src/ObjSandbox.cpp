/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "ObjSandbox.hpp"
#include "DirectPipeline.hpp"

#include <iostream>

ObjSandbox::ObjSandbox()
    : toggleRotate(false)
    , toggleCam(false)
{
}

void ObjSandbox::init()
{
    auto assets = mode7::AssetManager::createAndMakeCurrent();
    assets->setBaseDirectory("../assets");
    assets->setModelDirectory("models");
    assets->setShaderDirectory("shaders");
    assets->setTextureDirectory("textures");

    mode7::GLScreen::create(1600, 900, 1600, 900, "Sandbox", false, false, true);
    mode7::GLScreen::setPipeline(std::make_shared<mode7::DirectPipeline>());
    // mode7::GLScreen::setPipeline(std::make_shared<mode7::Pipeline>(assets->assetPath("shaders/sandbox/sandbox.shpl")));
    mode7::Camera::createAndMakeCurrent(70.0f, 0.1f, 1500.0f);

    std::shared_ptr<mode7::Shader> pass =
        std::make_shared<mode7::Shader>(assets->shaderPath("pass_v.glsl"), assets->shaderPath("pass_f.glsl"));

    std::shared_ptr<mode7::Shader> screen =
        std::make_shared<mode7::Shader>(assets->shaderPath("screen_v.glsl"), assets->shaderPath("screen_f.glsl"));

    /*controls.setInput(INPUT_MOUSE, 1);
    controls.attachCamera();
    controls.setPitchRange(-90, 90);*/

    basicShader.open(assets->assetPath("sandbox/shaders/basic_v.glsl"), assets->assetPath("sandbox/shaders/basic_f.glsl"));

    billboardShader.open(assets->assetPath("sandbox/shaders/billboard_v.glsl"),
                         assets->assetPath("sandbox/shaders/billboard_f.glsl"));

    cube = mode7::Primitives::cube(1.f);
    cube->setPosition(0.f, 0.f, 0.f);
    cube->update();

    cube2 = mode7::Primitives::cube(1.f);
    cube2->setPosition(3.f, 0.f, 0.f);
    cube2->update();

    cube->addChild(cube2.get());

    mode7::Camera::getCurrent().lookAt(glm::vec3(5.f), cube->getPosition());
    std::cout << "before: " << glm::to_string(mode7::Camera::getCurrent().getObject().getPosition()) << std::endl;
    anchor.addChild(&mode7::Camera::getCurrent().getObject());
    anchor.update();
    std::cout << "after: " << glm::to_string(mode7::Camera::getCurrent().getObject().getPosition()) << std::endl;

    bboard = std::make_shared<mode7::Billboard>();
    bboard->create();
    bboard->setPosition(0.f, 0.f, -3.f);
    billboardMat = std::make_shared<mode7::Material>();
    bboard->setMaterial(billboardMat);

    bboard->addChild(&anchor);
}

void ObjSandbox::update()
{
    cube->update();

    if (mode7::Keyboard::isPressed("escape"))
    {
        destroy();
    }

    if (mode7::Keyboard::isPressed("r"))
    {
        toggleRotate = !toggleRotate;
    }

    if (mode7::Keyboard::isPressed("c"))
    {
        toggleCam = !toggleCam;
    }

    if (toggleRotate)
    {
        cube->rotate(0.f, 0.f, 1.f);
        cube2->rotate(1.f, 0.f, 1.f);
        // std::cout << glm::to_string(cube->getRotation()) << std::endl;
    }

    if (toggleCam)
    {
        anchor.rotate(0.f, 1.f, 0.f);
        // mode7::Camera::getCurrent().getObject().rotate(0.f, 1.f, 0.f);
    }

    bboard->update();
    anchor.update();
    mode7::Camera::getCurrent().updateView();
}

void ObjSandbox::drawShader(mode7::Shader* shader)
{
    cube->draw(&basicShader);
    cube2->draw(&basicShader);
    bboard->draw(&billboardShader);
    // bboard->draw(basicShader);
}
