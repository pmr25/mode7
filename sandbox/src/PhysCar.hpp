#ifndef PHYSCAR_HPP
#define PHYSCAR_HPP

#include <cassert>
#include <engine.hpp>

class PhysCar : public mode7::Object
{
public:
    PhysCar();
    ~PhysCar();

    void input();
    virtual void update();

    inline void setModel(std::shared_ptr<mode7::Mesh> sp)
    {
        m_model = sp;
        addChild(m_model.get());
        assert(m_children.size() > 0);
        assert(m_model->getParent() == this);
    }

    inline void draw(mode7::Shader* sh)
    {
        m_model->draw(sh);
    }

private:
    std::shared_ptr<mode7::Mesh> m_model;
};

#endif /* PHYSCAR_HPP */