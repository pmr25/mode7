#ifndef ROTATIONSANDBOX_HPP
#define ROTATIONSANDBOX_HPP

#include "engine.hpp"

class RotationSandbox : public mode7::Game
{
public:
    RotationSandbox()
        : Game()
    {
    }

    virtual ~RotationSandbox() = default;
};

#endif /* ROTATIONSANDBOX_HPP */
