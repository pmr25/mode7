/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef PHYSSANDBOX_HPP
#define PHYSSANDBOX_HPP

#include "PhysCar.hpp"

#include "engine.hpp"
#include <cstdint>
#include <memory>

class PhysSandbox : public mode7::Game
{
public:
    PhysSandbox();
    virtual ~PhysSandbox();

    virtual void init();
    virtual void update();
    virtual void draw(int32_t);

private:
    std::shared_ptr<mode7::Mesh> ground;
    std::shared_ptr<PhysCar> cube;
    std::shared_ptr<mode7::Shader> screenShader;
    mode7::Shader basicShader;
    mode7::Physics physics;
    mode7::AssetManager assets;
    mode7::OrbitControls controls;
};

#endif /* PHYSSANDBOX_HPP */