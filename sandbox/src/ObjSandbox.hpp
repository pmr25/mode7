#ifndef OBJSANDBOX_HPP
#define OBJSANDBOX_HPP

#include "engine.hpp"

class ObjSandbox : public mode7::Game
{
public:
    ObjSandbox();
    virtual ~ObjSandbox() = default;

    virtual void init();
    virtual void update();
    virtual void drawShader(mode7::Shader*);

private:
    std::shared_ptr<mode7::Shader> screenShader;
    std::shared_ptr<mode7::Mesh> cube;
    std::shared_ptr<mode7::Mesh> cube2;
    std::shared_ptr<mode7::Billboard> bboard;
    mode7::Object anchor;
    std::shared_ptr<mode7::Material> billboardMat;
    mode7::Shader basicShader;
    mode7::Shader billboardShader;
    mode7::OrbitControls controls;

    bool toggleRotate;
    bool toggleCam;
};

#endif /* OBJSANDBOX_HPP */