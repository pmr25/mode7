#include "PhysCar.hpp"

#include <iostream>

PhysCar::PhysCar()
    : Object()
{
}

PhysCar::~PhysCar()
{
}

void PhysCar::input()
{
    float torque = 0.f;
    // float torqueDamping = 0.f;
    float downforce = 0.f;
    float thrust = 0.f;
    float brake = 0.f;
    // float speed = 0.f;
    float cd = 0.4 * 0.1;

    btVector3 drag;
    btVector3 friction;
    btVector3 vel = getRigidBody()->getLinearVelocity();
    // speed = vel.length();
    btVector3 rvel = mode7::Util::toModelSpace(getRigidBody(), vel);
    // btVector3 theta = getRigidBody()->getAngularVelocity();
    btVector3 btv(0.f, 0.f, 0.f);

    if (mode7::Keyboard::isDown("w"))
    {
        thrust += 1.f;
    }
    if (mode7::Keyboard::isDown("s"))
    {
        brake += 1.f;
    }
    if (mode7::Keyboard::isDown("a"))
    {
        torque += 3.f;
    }
    if (mode7::Keyboard::isDown("d"))
    {
        torque += -3.f;
    }

    std::cout << mode7::Util::btVector3ToStr(rvel) << std::endl;

    drag = -vel * cd;
    friction = btVector3(-vel.x(), 0.f, 0.f) * 0.2f;
    // friction = btVector3(torque, 0.f, 0.f) * 0.2f;
    downforce = fabsf(rvel.z()) * 0.5f;

    btv = btVector3(0.f, torque, 0.f);
    // btv = mode7::Util::toWorldSpace(getRigidBody(), btv);
    // getRigidBody()->applyTorque(btv);
    getRigidBody()->setAngularVelocity(btv);

    btv = btVector3(0.1f, -downforce, -thrust + brake);
    // btv = mode7::Util::toWorldSpace(getRigidBody(), btv);
    btv = mode7::Util::toModelSpace(getRigidBody(), btv);
    btv += drag;
    btv += mode7::Util::toModelSpace(getRigidBody(), friction);
    getRigidBody()->applyCentralForce(btv);
}

void PhysCar::update()
{
    Object::update();
}
