#include "SoundSys.hpp"

#include <AL/al.h>
#include <AL/alc.h>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <stack>
#include <vector>

#define DEFAULT_DOPPLER_FACTOR 20.0f

namespace sound
{
static ALCdevice* device;
static ALCcontext* context;
static std::stack<Listener*> listeners;
static Listener defaultListener;
static uint16_t masterVolume;
static uint16_t musicVolume;
static uint16_t sampleVolume;
static std::vector<Source*> mRegisteredSources;

void Init()
{
    device = alcOpenDevice(nullptr);
    if (!device)
    {
        return;
    }

    context = alcCreateContext(device, nullptr);
    if (context == nullptr)
    {
        std::cout << "could not create context!" << std::endl;
        return;
    }

    if (!alcMakeContextCurrent(context))
    {
        std::cout << "could not make context current!" << std::endl;
        return;
    }

    // make default listener
    PushListener(&defaultListener);

    alDopplerFactor(DEFAULT_DOPPLER_FACTOR);

    masterVolume = UINT16_MAX;
    musicVolume = UINT16_MAX;
    sampleVolume = UINT16_MAX;
}

void Stop()
{
    UnregisterAllSources();

    alcMakeContextCurrent(nullptr);
    alcDestroyContext(context);
    alcCloseDevice(device);
}

void PushListener(Listener* l)
{
    listeners.push(l);
    GetCurrentListener()->update();
}

void PopListener()
{
    listeners.pop();
    GetCurrentListener()->update();
}

Listener* GetCurrentListener()
{
    assert(listeners.size() >= 1);

    return listeners.top();
}

void Update()
{
    GetCurrentListener()->update();
}

void SetMasterVolume(uint16_t val)
{
    masterVolume = val;
}

void SetMasterVolumeDouble(double val)
{
    SetMasterVolume((uint16_t)round(val * (double)UINT16_MAX));
}

uint16_t GetMasterVolume()
{
    return masterVolume;
}

double GetMasterVolumeDouble()
{
    return GetMasterVolume() / (double)UINT16_MAX;
}

void SetMusicVolume(uint16_t val)
{
    musicVolume = val;
}

void SetMusicVolumeDouble(double val)
{
    SetMusicVolume((uint16_t)round(val * (double)UINT16_MAX));
}

uint16_t GetMusicVolume()
{
    return musicVolume;
}

double GetMusicVolumeDouble()
{
    return GetMusicVolume() / (double)UINT16_MAX;
}

void SetSampleVolume(uint16_t val)
{
    sampleVolume = val;
}

void SetSampleVolumeDouble(double val)
{
    SetSampleVolume((uint16_t)round(val * (double)UINT16_MAX));
}

uint16_t GetSampleVolume()
{
    return sampleVolume;
}

double GetSampleVolumeDouble()
{
    return GetSampleVolume() / (double)UINT16_MAX;
}

int RegisterSource(Source* source)
{
    mRegisteredSources.push_back(source);
    return mRegisteredSources.size() - 1;
}

int UnregisterSource(uint32_t index)
{
    if (index >= mRegisteredSources.size())
    {
        return 1;
    }

    mRegisteredSources[index]->destroy();

    mRegisteredSources.erase(mRegisteredSources.begin() + index);
    return 0;
}

void UnregisterAllSources()
{
    for (auto& e : mRegisteredSources)
    {
        e->destroy();
    }
    mRegisteredSources.erase(mRegisteredSources.begin(), mRegisteredSources.end());
}
} // namespace sound
