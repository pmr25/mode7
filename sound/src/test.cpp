#include "SoundSys.hpp"
#include "Source.hpp"

#include <iostream>

int main()
{
    std::cout << "hi" << std::endl;

    sound::Init();

    sound::Source engine;
    engine.load("../assets/sound/samples/engine.ogg");
    engine.test();

    sound::Stop();

    return 0;
}
