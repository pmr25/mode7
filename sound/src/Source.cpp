#include "Source.hpp"
#include "SoundSys.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#ifdef _WIN32
#include <fcntl.h>
#include <io.h>
#endif /* _WIN32 */

namespace sound
{
typedef struct _PCM
{
    char* data;
    size_t len;
    int nchans;
    long rate;
    long nsamples;
    long depth;
} PCM;

static void printALError(ALenum err)
{
    if (err == AL_NO_ERROR)
    {
        return;
    }
    else if (err == AL_INVALID_NAME)
    {
        std::cout << "al invalid name" << std::endl;
    }
    else if (err == AL_INVALID_ENUM)
    {
        std::cout << "al invalid enum" << std::endl;
    }
    else if (err == AL_INVALID_VALUE)
    {
        std::cout << "al invalid value" << std::endl;
    }
    else if (err == AL_INVALID_OPERATION)
    {
        std::cout << "al invalid operation" << std::endl;
    }
    else if (err == AL_OUT_OF_MEMORY)
    {
        std::cout << "al oom" << std::endl;
    }
}

static PCM ogg_to_pcm(const std::string& filename)
{
    PCM pcm;
    FILE* fp;
    OggVorbis_File vf;
    int eof = 0;
    int cur_section;
    int err;

    memset(&pcm, 0, sizeof(PCM));

    fp = fopen(filename.c_str(), "rb");

    err = ov_open_callbacks(fp, &vf, nullptr, 0, OV_CALLBACKS_NOCLOSE);
    if (err < 0)
    {
        std::cout << "error" << std::endl;
        return pcm;
    }

    // extract info
    vorbis_info* vi = ov_info(&vf, -1);
    pcm.nchans = vi->channels;
    pcm.rate = vi->rate;
    pcm.nsamples = (long)ov_pcm_total(&vf, -1);
    pcm.depth = vi->bitrate_nominal / pcm.rate / pcm.nsamples;

    // read PCM
    const size_t bufsize = 4096;
    char buffer[bufsize];

    size_t outsize = 32 * 1024;
    size_t outused = 0;
    char* output = (char*)malloc(outsize);
    if (!output)
    {
        return pcm;
    }

    while (!eof)
    {
        const long ret = ov_read(&vf, buffer, bufsize, 0, 2, 1, &cur_section);
        if (ret == 0)
        {
            eof = 1;
        }
        else if (ret < 0)
        {
            // error
        }
        else
        {
            // copy chunk into output buffer
            memcpy(output + outused, buffer, ret);
            outused += ret;
            if ((double)outused / (double)outsize > 0.7)
            {
                outsize *= 2;
                output = (char*)realloc(output, outsize);
            }
        }
    }

    // shrink output buffer
    output = (char*)realloc(output, outused);
    pcm.data = output;
    pcm.len = outused;

    ov_clear(&vf);

    return pcm;
}

int Source::load(const std::string& filename)
{
    PCM pcm;
    ALenum err;

    pcm = ogg_to_pcm(filename);

    err = alGetError(); // reset errors
    printALError(err);

    alGenBuffers(1, &mBuffer);
    err = alGetError();
    printALError(err);

    ALenum format;
    if (pcm.nchans == 1)
    {
        format = AL_FORMAT_MONO16;
    }
    else if (pcm.nchans == 2)
    {
        format = AL_FORMAT_STEREO16;
    }
    else
    {
        return 1;
    }

    alBufferData(mBuffer, format, pcm.data, pcm.len, pcm.rate);

    err = alGetError();
    printALError(err);

    free(pcm.data);

    alGenSources(1, &mSource);
    alSourcei(mSource, AL_BUFFER, mBuffer);
    alSourcei(mSource, AL_SOURCE_RELATIVE, AL_FALSE);
    alSourcef(mSource, AL_REFERENCE_DISTANCE, 30.0f);
    update();

    mReady = true;

    sound::RegisterSource(this);

    return 0;
}

void Source::update()
{
    assert(mSource > 0);
    assert(mBuffer > 0);

    float adjGain = mGain * GetMasterVolumeDouble() * GetSampleVolumeDouble();

    alSourcef(mSource, AL_PITCH, mPitch);
    alSourcef(mSource, AL_GAIN, adjGain);
    alSourcei(mSource, AL_LOOPING, mLooping);
    alSourcefv(mSource, AL_POSITION, &mPosition[0]);
    alSourcefv(mSource, AL_VELOCITY, &mVelocity[0]);
    alSourcefv(mSource, AL_DIRECTION, &mDirection[0]);
}

void Source::play(bool loop)
{
    mLooping = loop;
    update();
    alSourcePlay(mSource);
}

void Source::pause()
{
    alSourcePause(mSource);
}

void Source::stop()
{
    alSourceStop(mSource);
}

void Source::test()
{
    alSourcePlay(mSource);
    ALint state = AL_PLAYING;
    while (state == AL_PLAYING)
    {
        alGetSourcei(mSource, AL_SOURCE_STATE, &state);
    }
}

void Source::destroy()
{
    if (mReady)
    {
        stop();
        alSourcei(mSource, AL_BUFFER, 0); // unbind
        alDeleteBuffers(1, &mBuffer);
        alDeleteSources(1, &mSource);
        mReady = false;
    }
}
} // namespace sound
