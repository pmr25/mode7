#include "Listener.hpp"

#include <AL/al.h>

namespace sound
{
void Listener::update()
{
    alListenerfv(AL_POSITION, &mPosition[0]);
    alListenerfv(AL_VELOCITY, &mVelocity[0]);
    alListenerfv(AL_ORIENTATION, &mOrientation[0][0]);
}
} // namespace sound
