#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <AL/al.h>
#include <cmath>
#include <glm/glm.hpp>
#include <iostream>
#include <string>

namespace sound
{
class Source
{
public:
    Source()
        : mBuffer(0)
        , mSource(0)
        , mPitch(1.0f)
        , mGain(1.0f)
        , mLooping(0)
        , mIndex(-1)
        , mPosition(0.0f)
        , mVelocity(0.0f)
        , mDirection(0.0f, 0.0f, -1.0f)
        , mReady(false)
    {
    }

    ~Source()
    {
        destroy();
    }

    inline void setPosition(glm::vec3 pos)
    {
        mPosition = pos;
    }

    inline void setVelocity(glm::vec3 vel)
    {
        mVelocity = vel;
    }

    inline void setDirection(glm::vec3 dir)
    {
        mDirection = dir;
    }

    inline void setPitch(float val)
    {
        mPitch = fmaxf(val, 0.0f);
    }

    inline void setGain(float val)
    {
        mGain = fmaxf(val, 0.0f);
    }

    int load(const std::string&);
    void update();
    void play(bool loop = false);
    void pause();
    void stop();
    void test();
    void destroy();

private:
    ALuint mBuffer;
    ALuint mSource;
    ALfloat mPitch;
    ALfloat mGain;
    ALint mLooping;
    int mIndex;
    glm::vec3 mPosition;
    glm::vec3 mVelocity;
    glm::vec3 mDirection;
    bool mReady;
};
} // namespace sound

#endif /* SOURCE_HPP */
