#ifndef LISTENER_HPP
#define LISTENER_HPP

#include <glm/glm.hpp>

namespace sound
{
class Listener
{
public:
    Listener()
        : mPosition(0.0f)
        , mVelocity(0.0f)
        , mOrientation(0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f)
    {
    }

    ~Listener() = default;

    inline void setPosition(glm::vec3 pos)
    {
        mPosition = pos;
    }

    inline void setVelocity(glm::vec3 vel)
    {
        mVelocity = vel;
    }

    inline glm::vec3 getVelocity()
    {
        return mVelocity;
    }

    inline void setOrientation(glm::vec3 front, glm::vec3 up)
    {
        mOrientation = glm::mat2x3(front, up);
    }

    void update();

private:
    glm::vec3 mPosition;
    glm::vec3 mVelocity;
    glm::mat2x3 mOrientation;
};
} // namespace sound

#endif /* LISTENER_HPP */
