#ifndef SOUNDSYS_HPP
#define SOUNDSYS_HPP

#include "Listener.hpp"
#include "Source.hpp"
#include <cstdint>

namespace sound
{
void Init();
void Stop();
void PushListener(Listener*);
void PopListener();
Listener* GetCurrentListener();
void Update();
void SetMasterVolume(uint16_t);
void SetMasterVolumeDouble(double);
uint16_t GetMasterVolume();
double GetMasterVolumeDouble();
void SetMusicVolume(uint16_t);
void SetMusicVolumeDouble(double);
uint16_t GetMusicVolume();
double GetMusicVolumeDouble();
void SetSampleVolume(uint16_t);
void SetSampleVolumeDouble(double);
uint16_t GetSampleVolume();
double GetSampleVolumeDouble();
int RegisterSource(Source*);
int UnregisterSource(int);
void UnregisterAllSources();
} // namespace sound

#endif /* SOUNDSYS_HPP */
