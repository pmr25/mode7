/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef TRACKDATA_HPP
#define TRACKDATA_HPP

#include "HeightData.hpp"
#include "Line.hpp"
#include "Plane.hpp"
#include "Quad.hpp"
#include "TrackZone.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/vector.hpp>
#include <glm/glm.hpp>
#include <ostream>

#define DEFAULT_VERSION 2

namespace mode7
{
class TrackData
{
public:
    TrackData(int32_t formatVer=DEFAULT_VERSION)
        : m_reversed(false)
        , m_startFinishZone(0)
        , mDataFormatVer(formatVer)
    {
    }

    explicit TrackData(const std::string& fn)
        : TrackData()
    {
        openTD2(fn);
    }

    int openTDAT(const std::string&, bool = false);
    int openTD2(const std::string&);
    int open(const std::string&);
    void selfHeal();
    void setVersion(int);
    int transform(const glm::mat4&);
    void genTrackZones();
    std::vector<glm::vec2> getCenterLinePts() const;
    std::vector<glm::vec2> getTrackBoundPts() const;
    std::vector<glm::vec2> getRunoffBoundPts() const;
    std::vector<glm::vec2> getWallBoundPts() const;
    void setReversed(bool);
    bool isReversed() const;

    int32_t getStartFinishZone() const;

    void autoGenerateSectors(int);
    int getSectorOfZone(int) const;

    const std::vector<Line<2>>& getCenterLines() const
    {
        return m_centerLines;
    }

    const std::vector<Quad>& getTrackBounds() const
    {
        return m_trackBounds;
    }

    const std::vector<Quad>& getRunoffBounds() const
    {
        return m_runoffBounds;
    }

    const std::vector<Quad>& getWallBounds() const
    {
        return m_wallBounds;
    }

    const std::vector<HeightData>& getHeightData() const
    {
        return mHeightData;
    }

    uint32_t getNumZones() const
    {
        return (uint32_t)m_centerLines.size();
    }

    uint32_t getNumSectors() const
    {
        return (uint32_t)(m_sectors.size() - 1);
    }

    std::vector<TrackZone>& getTrackZones()
    {
        return m_zones;
    }

    const std::vector<TrackZone>& getLapZones() const
    {
        return m_lapZones;
    }

    const std::vector<geom::Plane>& getDiscreteWalls() const
    {
        return m_discreteWalls;
    }

    void addSegment(Line<2>, Quad, float, float);
    void addSegment(Line<2>, Quad, float, float, HeightData);
    void addHeightData(HeightData);
    void addWall(geom::Plane);

    void saveData(const std::string&);
    void csvDump(const std::string&);
    void gnuplotDump(const std::string&);

    template <class Archive>
    void serialize(Archive& archive)
    {
        if (mDataFormatVer == 2)
        {
            archive(CEREAL_NVP(m_centerLines), CEREAL_NVP(m_trackBounds),
                    CEREAL_NVP(m_runoffBounds), CEREAL_NVP(m_wallBounds),
                    CEREAL_NVP(m_discreteWalls));
        }
        else if (mDataFormatVer == 3)
        {
            archive(CEREAL_NVP(m_centerLines), CEREAL_NVP(m_trackBounds),
                    CEREAL_NVP(m_runoffBounds), CEREAL_NVP(m_wallBounds),
                    CEREAL_NVP(m_discreteWalls), CEREAL_NVP(mHeightData));
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const TrackData& data);

private:
    std::vector<int> m_sectors;
    std::vector<Line<2>> m_centerLines;
    std::vector<HeightData> mHeightData;
    std::vector<Quad> m_trackBounds;
    std::vector<Quad> m_runoffBounds;
    std::vector<Quad> m_wallBounds;
    std::vector<TrackZone> m_zones;
    std::vector<TrackZone> m_lapZones;
    // std::vector<std::unique_ptr<float[]>> m_discreteWalls;
    std::vector<geom::Plane> m_discreteWalls;
    bool m_reversed;
    int32_t m_startFinishZone;
    int32_t mDataFormatVer;
};
} // namespace mode7

#endif /* TRACKDATA_HPP */