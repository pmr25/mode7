#ifndef TRACKZONE_HPP
#define TRACKZONE_HPP

#include "Line.hpp"
#include "Quad.hpp"
#include <cereal/archives/binary.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

namespace mode7
{
class TrackZone
{
public:
    template <class Archive> void serialize(Archive& archive)
    {
        archive(m_trackRect, m_runoffRect, m_wallRect, m_centerline);
    }

    inline bool onTrack(glm::vec2 v) const
    {
        return m_trackRect.checkIntersect(v);
    }

    inline bool onRunoff(glm::vec2 v) const
    {
        return m_runoffRect.checkIntersect(v);
    }

    inline bool onWall(glm::vec2 v) const
    {
        return m_wallRect.checkIntersect(v);
    }

    inline Quad getTrackRect() const
    {
        return m_trackRect;
    }

    inline Quad getRunoffRect() const
    {
        return m_runoffRect;
    }

    inline Quad getWallRect() const
    {
        return m_wallRect;
    }

    inline const Line<2> getCenterLine() const
    {
        return m_centerline;
    }

private:
    Quad m_trackRect;
    Quad m_runoffRect;
    Quad m_wallRect;
    Line<2> m_centerline;

    friend class TrackData;
};
} // namespace mode7

#endif /* TRACKZONE_HPP */
