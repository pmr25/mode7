#ifndef MODE7_HEIGHTDATA_HPP
#define MODE7_HEIGHTDATA_HPP

#include <vector>

namespace mode7
{

class HeightData
{
public:
    HeightData(float a=0, float b=0, float c=0, float d=0)
    {
        mHeights.push_back(a);
        mHeights.push_back(b);
        mHeights.push_back(c);
        mHeights.push_back(d);
    }

    auto operator[](int index) -> float&
    {
        [[unlikely]]
        if (std::size_t(index) >= mHeights.size())
        {
            return mHeights[0];
        }

        return mHeights[index];
    }

    [[nodiscard]] auto at(int index) const -> float
    {
        return mHeights.at(index);
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(mHeights);
    }

private:
    std::vector<float> mHeights;
};

}

#endif // MODE7_HEIGHTDATA_HPP
