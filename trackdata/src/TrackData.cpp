/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "TrackData.hpp"

#include <cassert>
#include <cstdint>
#include <cstring>
#include <fstream>

#define TDAT_OUTPUT_ARCHIVE_TYPE cereal::XMLOutputArchive
#define TDAT_INPUT_ARCHIVE_TYPE cereal::XMLInputArchive

namespace mode7
{
void TrackData::addSegment(Line<2> line, Quad quad, float runoffWidth, float wallWidth)
{
    Quad runoff;
    Quad wall;
    m_centerLines.push_back(line);
    m_trackBounds.push_back(quad);

    runoff.copy(quad);
    runoff.stretchSide(0, runoffWidth);
    runoff.stretchSide(2, runoffWidth);
    m_runoffBounds.push_back(runoff);

    wall.copy(runoff);
    wall.stretchSide(0, wallWidth);
    wall.stretchSide(2, wallWidth);
    m_wallBounds.push_back(wall);
}

void TrackData::addWall(geom::Plane plane)
{
    m_discreteWalls.push_back(plane);
}

int TrackData::openTDAT(const std::string& filename, bool dump)
{
    (void)filename;
    (void)dump;
    return 0;
//    std::ifstream in;
//    in.open(filename, std::ios::binary);
//
//    size_t length;
//    in.seekg(0, std::ios::end);
//    length = in.tellg();
//    in.seekg(0, std::ios::beg);
//
//    if (length > 1024 * 1024 * 1024)
//    {
//        // too big
//        return 1;
//    }
//
//    char buffer[1024];
//    memset(buffer, 0, 1024);
//
//    // check magic word
//    in.read(buffer, 4);
//    if (strcmp(buffer, "TDAT") != 0)
//    {
//        return 2;
//    }
//
//    // read num segments
//    int32_t n_centerline_pts;
//    int32_t n_discrete_walls;
////    int32_t n_track_pts;
////    int32_t n_runoff_pts;
////    int32_t n_wall_pts;
//
//    in.read(buffer, 2 * sizeof(int32_t));
//    n_centerline_pts = *(int32_t*)(buffer + (0 * sizeof(int32_t)));
//    n_discrete_walls = *(int32_t*)(buffer + (1 * sizeof(int32_t)));
//    // n_track_pts         = *(int32_t*)(buffer + (1 * sizeof(int32_t)));
//    // n_runoff_pts        = *(int32_t*)(buffer + (2 * sizeof(int32_t)));
//    // n_wall_pts          = *(int32_t*)(buffer + (3 * sizeof(int32_t)));
//
//    // read starting locations
//    uint32_t start;
//    uint32_t walls_start;
//    uint32_t stride;
//
//    in.read(buffer, 3 * sizeof(uint32_t));
//    start = *(uint32_t*)(buffer + (0 * sizeof(uint32_t)));
//    walls_start = *(uint32_t*)(buffer + (1 * sizeof(uint32_t)));
//    stride = *(uint32_t*)(buffer + (2 * sizeof(uint32_t)));
//
//    if (stride != 28 * sizeof(float))
//    {
//        // bad format
//        // return 3;
//    }
//
//    // move head to start
//    // in.seekg(start, std::ios::beg);
//
//    m_centerLines.reserve(n_centerline_pts);
//    m_trackBounds.reserve(n_centerline_pts);
//    m_runoffBounds.reserve(n_centerline_pts);
//
//    // parse data
//    float qbuf[9];
//    Line<2> ln;
//    Quad qd;
//    for (int i = 0; i < n_centerline_pts; ++i)
//    {
//        // read centerline
//        in.read((char*)qbuf, 4 * sizeof(float));
//        // line_from_buffer(&ln, qbuf);
//        ln.connectPoints(glm::vec2(qbuf[0], qbuf[1]), glm::vec2(qbuf[2], qbuf[3]));
//        m_centerLines.push_back(ln);
//
//        // read track bounds
//        in.read((char*)qbuf, 8 * sizeof(float));
//        // quad_from_buffer(&qd, qbuf);
//        qd.connectPoints(glm::vec2(qbuf[0], qbuf[1]), glm::vec2(qbuf[2], qbuf[3]), glm::vec2(qbuf[4], qbuf[5]),
//                         glm::vec2(qbuf[6], qbuf[7]));
//        m_trackBounds.push_back(qd);
//
//        // read runoff bounds
//        in.read((char*)qbuf, 8 * sizeof(float));
//        // quad_from_buffer(&qd, qbuf);
//        qd.connectPoints(glm::vec2(qbuf[0], qbuf[1]), glm::vec2(qbuf[2], qbuf[3]), glm::vec2(qbuf[4], qbuf[5]),
//                         glm::vec2(qbuf[6], qbuf[7]));
//        m_runoffBounds.push_back(qd);
//
//        // read wall bounds
//        in.read((char*)qbuf, 8 * sizeof(float));
//        // quad_from_buffer(&qd, qbuf);
//        qd.connectPoints(glm::vec2(qbuf[0], qbuf[1]), glm::vec2(qbuf[2], qbuf[3]), glm::vec2(qbuf[4], qbuf[5]),
//                         glm::vec2(qbuf[6], qbuf[7]));
//        m_wallBounds.push_back(qd);
//    }
//
//    // ssize_t n_read = 1;
//    for (int i = 0; i < n_discrete_walls; ++i)
//    {
//        in.read((char*)qbuf, 9 * sizeof(float));
//        // std::cout << qbuf[0] << "," << qbuf[1] << "," << qbuf[2] << "," <<
//        // std::endl; std::unique_ptr<float[]> data(new float[9]);
//        // memcpy(data.get(), qbuf, 9 * sizeof(float));
//        // m_discreteWalls.push_back(std::move(data));
//
//        glm::vec3 a(qbuf[0], qbuf[1], qbuf[2]);
//        glm::vec3 b(qbuf[3], qbuf[4], qbuf[5]);
//        glm::vec3 c(qbuf[6], qbuf[7], qbuf[8]);
//
//        geom::Plane pl;
//        pl.definePoints(a, b, c);
//
//        m_discreteWalls.push_back(pl);
//    }
//
//    if (dump)
//    {
//        saveData("dump.td2");
//    }
//
//    return 0;
}

auto TrackData::openTD2(const std::string& filename) -> int
{
    std::ifstream is(filename, std::ios_base::binary);
    if (!is)
    {
        return 1;
    }

    TDAT_INPUT_ARCHIVE_TYPE archive(is);
    archive(*this);

    return 0;
}

auto TrackData::open(const std::string& filename) -> int
{
    const auto split{filename.rfind('.')};
    if (split == std::string::npos)
    {
        return 2; // cannot understand file format
    }
    const auto extension{filename.substr(split+1)};

    if (extension == "td2")
    {
        mDataFormatVer = 2;
    }

    if (extension == "td3")
    {
        mDataFormatVer = 3;
    }

    int err = openTD2(filename);
    selfHeal();
    return err;
}

auto TrackData::transform(const glm::mat4& matrix) -> int
{
    assert(m_centerLines.size() > 0);

    int n_processed = 0;

    Line<2>* cur_line;
    // quad* cur_quad;
    Quad* cur_quad;
    glm::vec3 a;
    glm::vec3 b;
    glm::vec3 c;
    glm::vec3 d;
    for (uint32_t i = 0; i < m_centerLines.size(); ++i)
    {
        cur_line = &m_centerLines[i];
        a = glm::vec3(cur_line->getPoint().x, 0, cur_line->getPoint().y);
        b = glm::vec3(cur_line->getEndpoint().x, 0, cur_line->getEndpoint().y);
        a = glm::vec3(matrix * glm::vec4(a, 1));
        b = glm::vec3(matrix * glm::vec4(b, 1));
        cur_line->connectPoints(glm::vec2(a.x, a.z), glm::vec2(b.x, b.z));

        cur_quad = &m_trackBounds[i];
        a = glm::vec3(cur_quad->getPoint(0).x, 0, cur_quad->getPoint(0).y);
        b = glm::vec3(cur_quad->getPoint(1).x, 0, cur_quad->getPoint(1).y);
        c = glm::vec3(cur_quad->getPoint(2).x, 0, cur_quad->getPoint(2).y);
        d = glm::vec3(cur_quad->getPoint(3).x, 0, cur_quad->getPoint(3).y);
        a = glm::vec3(matrix * glm::vec4(a, 1));
        b = glm::vec3(matrix * glm::vec4(b, 1));
        c = glm::vec3(matrix * glm::vec4(c, 1));
        d = glm::vec3(matrix * glm::vec4(d, 1));
        cur_quad->connectPoints(glm::vec2(a.x, a.z), glm::vec2(b.x, b.z), glm::vec2(c.x, c.z), glm::vec2(d.x, d.z));

        cur_quad = &m_runoffBounds[i];
        a = glm::vec3(cur_quad->getPoint(0).x, 0, cur_quad->getPoint(0).y);
        b = glm::vec3(cur_quad->getPoint(1).x, 0, cur_quad->getPoint(1).y);
        c = glm::vec3(cur_quad->getPoint(2).x, 0, cur_quad->getPoint(2).y);
        d = glm::vec3(cur_quad->getPoint(3).x, 0, cur_quad->getPoint(3).y);
        a = glm::vec3(matrix * glm::vec4(a, 1));
        b = glm::vec3(matrix * glm::vec4(b, 1));
        c = glm::vec3(matrix * glm::vec4(c, 1));
        d = glm::vec3(matrix * glm::vec4(d, 1));
        cur_quad->connectPoints(glm::vec2(a.x, a.z), glm::vec2(b.x, b.z), glm::vec2(c.x, c.z), glm::vec2(d.x, d.z));

        cur_quad = &m_wallBounds[i];
        a = glm::vec3(cur_quad->getPoint(0).x, 0, cur_quad->getPoint(0).y);
        b = glm::vec3(cur_quad->getPoint(1).x, 0, cur_quad->getPoint(1).y);
        c = glm::vec3(cur_quad->getPoint(2).x, 0, cur_quad->getPoint(2).y);
        d = glm::vec3(cur_quad->getPoint(3).x, 0, cur_quad->getPoint(3).y);
        a = glm::vec3(matrix * glm::vec4(a, 1));
        b = glm::vec3(matrix * glm::vec4(b, 1));
        c = glm::vec3(matrix * glm::vec4(c, 1));
        d = glm::vec3(matrix * glm::vec4(d, 1));
        cur_quad->connectPoints(glm::vec2(a.x, a.z), glm::vec2(b.x, b.z), glm::vec2(c.x, c.z), glm::vec2(d.x, d.z));
        // quad_connect_raw(
        //     cur_quad,
        //     &(glm::vec2(a.x, a.z))[0],
        //     &(glm::vec2(b.x, b.z))[0],
        //     &(glm::vec2(c.x, c.z))[0],
        //     &(glm::vec2(d.x, d.z))[0]
        // );
    }

    for (size_t i{0}; i < m_discreteWalls.size(); ++i)
    {
        // float* cur = m_discreteWalls[i].get();
        // a = glm::vec3(cur[0], cur[1], cur[2]);
        // b = glm::vec3(cur[3], cur[4], cur[5]);
        // c = glm::vec3(cur[6], cur[7], cur[8]);
        // a = glm::vec3(matrix * glm::vec4(a, 1));
        // b = glm::vec3(matrix * glm::vec4(b, 1));
        // c = glm::vec3(matrix * glm::vec4(c, 1));

        // cur[0] = a.x;
        // cur[1] = a.y;
        // cur[2] = a.z;
        // cur[3] = b.x;
        // cur[4] = b.y;
        // cur[5] = b.z;
        // cur[6] = c.x;
        // cur[7] = c.y;
        // cur[8] = c.z;

        m_discreteWalls[i].transform(matrix);
    }

    // csvDump("trans.csv");

    return n_processed;
}

void TrackData::genTrackZones()
{
    // generate track zones
    // todo change, this is inefficient

    assert(getNumZones() > 0);

    m_zones.reserve(getNumZones());
    for (uint32_t i = 0; i < getNumZones(); ++i)
    {
        TrackZone tz;

        tz.m_trackRect = getTrackBounds()[i];
        tz.m_runoffRect = getRunoffBounds()[i];
        tz.m_wallRect = getWallBounds()[i];
        tz.m_centerline = getCenterLines()[i];

        m_zones.push_back(tz);
    }

    // generate lap zones
    // todo change, this is inefficient
    uint32_t n_lapZones = 100;
    uint32_t stride = uint32_t(m_centerLines.size()) / n_lapZones;
    uint64_t ia;
    uint64_t ib;
    for (uint64_t i = 0; i < (uint64_t)n_lapZones + stride; ++i)
    {
        Quad zone;
        TrackZone tz;

        ia = ((i + 0) * stride) % m_wallBounds.size();
        ib = ((i + 1) * stride) % m_wallBounds.size();

        auto qa{m_trackBounds[ia]};
        auto qb{m_trackBounds[ib]};

        zone.connectPoints(qa.getPoint(0), qa.getPoint(1), qb.getPoint(0), qb.getPoint(1));
        zone.stretchSide(0, 20.0f);
        zone.stretchSide(2, 20.0f);
        tz.m_trackRect = zone;

        qa = m_runoffBounds[ia];
        qb = m_runoffBounds[ib];
        zone.connectPoints(qa.getPoint(0), qa.getPoint(1), qb.getPoint(0), qb.getPoint(1));
        zone.stretchSide(0, 20.0f);
        zone.stretchSide(2, 20.0f);
        tz.m_trackRect = zone;

        qa = m_wallBounds[ia];
        qb = m_wallBounds[ib];
        zone.connectPoints(qa.getPoint(0), qa.getPoint(1), qb.getPoint(0), qb.getPoint(1));
        zone.stretchSide(0, 20.0f);
        zone.stretchSide(2, 20.0f);
        tz.m_trackRect = zone;

        m_lapZones.push_back(tz);
    }
}

auto TrackData::getCenterLinePts() const -> std::vector<glm::vec2>
{
    std::vector<glm::vec2> points;

    for (const auto& e : m_centerLines)
    {
        points.push_back(e.getPoint());
        points.push_back(e.getEndpoint());
    }

    return points;
}

auto TrackData::getTrackBoundPts() const -> std::vector<glm::vec2>
{
    std::vector<glm::vec2> points;

    for (const auto& e : m_trackBounds)
    {
        points.push_back(e.getPoint(0));
        points.push_back(e.getPoint(1));
        points.push_back(e.getPoint(2));
        points.push_back(e.getPoint(3));
    }

    return points;
}

auto TrackData::getRunoffBoundPts() const -> std::vector<glm::vec2>
{
    std::vector<glm::vec2> points;

    for (const auto& e : m_runoffBounds)
    {
        points.push_back(e.getPoint(0));
        points.push_back(e.getPoint(1));
    }

    return points;
}

auto TrackData::getWallBoundPts() const -> std::vector<glm::vec2>
{
    std::vector<glm::vec2> points;

    for (const auto& e : m_wallBounds)
    {
        points.push_back(e.getPoint(0));
        points.push_back(e.getPoint(1));
    }

    return points;
}

void TrackData::autoGenerateSectors(int num_sectors)
{
    int num_zones = int(m_centerLines.size());
    if (num_zones < 1)
    {
        return;
    }

    m_sectors.push_back(0);
    int div = num_zones / num_sectors;
    int rem = num_zones % num_sectors;

    for (int i = 0; i < num_sectors; ++i)
    {
        m_sectors.push_back(m_sectors.back() + div);
    }

    if (rem > 0)
    {
        m_sectors.insert(m_sectors.begin() + 1, rem);
    }
}

auto TrackData::getSectorOfZone(int zone) const -> int
{
    assert(m_centerLines.size() > 0);
    assert(m_sectors.size() > 0);

    if (uint32_t(zone) >= m_centerLines.size())
    {
        return -1;
    }

    int start;
    int mid;
    int end;

    start = 0;
    mid = int(m_sectors.size() / 2);
    end = int(m_sectors.size() - 1);

    do
    {
        mid = (end - start) / 2 + start;

        assert(mid >= 0);
        assert(uint32_t(mid) < m_sectors.size());

        if (zone <= m_sectors[mid])
        {
            end = mid;
        }
        else if (zone >= m_sectors[mid])
        {
            start = mid;
        }
    } while (end - start > 1);

    if (m_reversed)
    {
        return getNumSectors() - start;
    }

    return start;
}

void TrackData::setReversed(bool val)
{
    m_reversed = val;
}

auto TrackData::isReversed() const -> bool
{
    return m_reversed;
}

auto TrackData::getStartFinishZone() const -> int32_t
{
    return m_startFinishZone;
}

void TrackData::saveData(const std::string& filename)
{
    std::ofstream os(filename, std::ios_base::binary);
    TDAT_OUTPUT_ARCHIVE_TYPE archive(os);
    archive(*this);
}

void TrackData::csvDump(const std::string& filename)
{
    std::ofstream os(filename);

    for (auto& e : getTrackBoundPts())
    {
        os << e.x << "," << e.y << std::endl;
    }
}

std::ostream& operator<<(std::ostream& os, const TrackData& data)
{
    os << "m_sectors: " << data.m_sectors.size() << " m_centerLines: " << data.m_centerLines.size() << " m_trackBounds: " << data.m_trackBounds.size()
       << " m_runoffBounds: " << data.m_runoffBounds.size() << " m_wallBounds: " << data.m_wallBounds.size() << " m_zones: " << data.m_zones.size()
       << " m_lapZones: " << data.m_lapZones.size() << " m_discreteWalls: " << data.m_discreteWalls.size() << " m_reversed: " << data.m_reversed
       << " m_startFinishZone: " << data.m_startFinishZone;
    return os;
}

void TrackData::gnuplotDump(const std::string& filename)
{
    std::ofstream os(filename + ".dat");
    std::ofstream os2 = std::ofstream(filename + ".plt");

    os2 << "set style line  1 lt 1 lc rgb '#0c0887' # blue\n"
           "set style line  2 lt 1 lc rgb '#4b03a1' # purple-blue\n"
           "set style line  3 lt 1 lc rgb '#7d03a8' # purple\n"
           "set style line  4 lt 1 lc rgb '#a82296' # purple\n"
           "set style line  5 lt 1 lc rgb '#cb4679' # magenta\n"
           "set style line  6 lt 1 lc rgb '#e56b5d' # red\n"
           "set style line  7 lt 1 lc rgb '#f89441' # orange\n"
           "set style line  8 lt 1 lc rgb '#fdc328' # orange\n"
           "set style line  9 lt 1 lc rgb '#f0f921' # yellow" << std::endl;

    int label = 0;
    for (const auto& quad : getTrackBounds())
    {
        const auto a{quad.getPoint(0)};
        const auto b{quad.getPoint(1)};
        const auto c{quad.getPoint(2)};
        const auto d{quad.getPoint(3)};

        os2 << "set label \"" << label << "\" at " << -a.x << "," << a.y << std::endl;
        os << -a.x << " " << a.y << " 0" << std::endl;
        os << -b.x << " " << b.y << " 1" << std::endl;
        os << -c.x << " " << c.y << " 2" << std::endl;
        os << -c.x << " " << c.y << " 3" << std::endl;
        os << -d.x << " " << d.y << " 4" << std::endl;
        os << -a.x << " " << a.y << " 5" << std::endl;
        os << std::endl; // end rectangle

        ++label;
    }

    os2 << "plot \'" << filename + ".dat" << "\' using 1:2:3 with lines palette\npause -1 \"Hit any key to continue\"" << std::endl;
}
void TrackData::setVersion(int ver)
{
    mDataFormatVer = ver;
}
void TrackData::addHeightData(HeightData data)
{
    mHeightData.push_back(std::move(data));
}
void TrackData::addSegment(Line<2> line, Quad quad, float runoff, float wall, HeightData height)
{
    addSegment(line, quad, runoff, wall);
    addHeightData(height);
}
void TrackData::selfHeal()
{
    if (m_centerLines.size() != mHeightData.size())
    {
        mHeightData.resize(m_centerLines.size());
    }
}

} // namespace mode7
