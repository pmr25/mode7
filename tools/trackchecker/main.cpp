#include <iostream>
#include <cstdio>

#include "TrackData.hpp"

#define ARG_INPUT 1
#define ARG_OUTPUT 2
#define NUM_REQ_ARGS 3

static auto printUsage() -> void
{
    std::cout << "usage: ./trackchecker <input.td2> <output>" << std::endl;
}

auto main(int argc, char** argv) -> int
{
    if (argc < NUM_REQ_ARGS)
    {
        printUsage();
        return 1;
    }

    // init flags
    bool quiet{false};
    bool noPreserve{false};
    // parse flags
    if (argc > NUM_REQ_ARGS)
    {
        for (int i{NUM_REQ_ARGS}; i < argc; ++i)
        {
            if (std::string(argv[i]) == "--quiet")
            {
                quiet = true;
            }
            else if (std::string(argv[i]) == "--no-preserve")
            {
                noPreserve = true;
            }
        }
    }

    // create plot data and gnuplot script
    mode7::TrackData data(argv[ARG_INPUT]);
    glm::mat4 matrix(1.0F);
    auto matrix2 = glm::scale(matrix, glm::vec3(3.0F));
    auto matrix3 = glm::translate(matrix2, glm::vec3(0, -3, 0));
    data.transform(matrix3);
    data.genTrackZones();
    data.gnuplotDump(argv[ARG_OUTPUT]);
    if (quiet)
    {
        return 0;
    }

    // generate plot

    if (noPreserve)
    {
        // remove generated files
        // TODO
    }

    return 0;
}