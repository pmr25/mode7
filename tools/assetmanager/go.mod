module assetmanager

        go 1.16

        require (
        github.com/aws/aws-sdk-go-v2 v1.7.0
        github.com/aws/aws-sdk-go-v2/config v1.4.1
        github.com/aws/aws-sdk-go-v2/service/s3 v1.11.0
        github.com/google/btree v1.0.0 // indirect
        github.com/spf13/cobra v1.2.1
        github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
        golang.org/x/exp v0.0.0-20200224162631-6cc2880d07d6 // indirect
        )
