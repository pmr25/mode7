#!/usr/bin/python3

import glob
import heapq
import json
import ntpath
import sys
from PIL import Image

MAX_DIMENSION = 8192
VERBOSE = True

progress = True


def print_usage():
    print("usage: atlas <textures> <output>")


def ffdh(textures, atlas, padding=0):
    atlas_width, atlas_height = atlas.size

    if progress:
        print("packing ", end="")

    img = None
    bands = []

    metadata = []

    # place first image
    img = textures[0][2]

    if progress:
        print(".", end="")
        sys.stdout.flush()
    elif VERBOSE:
        print(img.filename)

    width, height = img.size
    heapq.heappop(textures)
    bands.append((width + padding, height + padding, 0))
    atlas.paste(img, (0, 0))
    metadata.append({"name": ntpath.basename(img.filename), "x": 0, "y": 0, "w": width, "h": height})

    while len(textures) > 0:
        img = textures[0][2]

        if progress:
            print(".", end="")
            sys.stdout.flush()
        elif VERBOSE:
            print(img.filename)

        width, height = img.size

        packed = False
        for i in range(0, len(bands)):
            b = bands[i]
            if b[0] + width <= atlas_width:
                atlas.paste(img, (b[0], b[2]))
                metadata.append({"name": ntpath.basename(img.filename), "x": b[0], "y": b[2], "w": width, "h": height})
                bands[i] = (b[0] + width + padding, b[1], b[2])
                packed = True
                break

        if not packed:
            bands.append((width + padding, height + padding, bands[-1][1]))
            atlas.paste(img, (0, bands[-1][2]))
            metadata.append({"name": ntpath.basename(img.filename), "x": 0, "y": bands[-1][2], "w": width, "h": height})

        heapq.heappop(textures)

    if progress:
        print(" done!")

    return metadata


def main(argc, argv):
    if argc < 3:
        print_usage()
        return

    input_files = argv[1:-1]
    output_file = argv[-1]

    if "*" in input_files[0]:
        input_files = glob.glob(input_files[0])

    if len(input_files) < 1:
        print("ERROR: no input files")
        if VERBOSE:
            print("input", argv[1:-1])
            print("output", argv[-1])
        return 1

    textures = []
    atlas = None

    ctr = 0
    for fn in input_files:
        if fn == output_file:
            continue
        img = Image.open(fn)
        w, h = img.size
        # used for maxheap comparison
        # ctr allows for comparison between images of the same height
        heapq.heappush(textures, (-h, ctr, img))
        ctr += 1

    atlas = Image.new("RGBA", (MAX_DIMENSION, MAX_DIMENSION))
    atlas.paste((255, 0, 255), [0, 0, atlas.size[0], atlas.size[1]])

    img_metadata = ffdh(textures, atlas)

    # crop
    # atlas_bbox = atlas.getbbox()
    # px = math.ceil(math.log(atlas_bbox[2] - atlas_bbox[0]) / math.log(2))
    # py = math.ceil(math.log(atlas_bbox[3] - atlas_bbox[1]) / math.log(2))
    # pwr2 = int(max(px, py))
    # atlas = atlas.crop(box=(0, 0, 2 ** pwr2, 2 ** pwr2))

    print("saving  " + '.' * len(img_metadata), end="")
    sys.stdout.flush()

    atlas.save(output_file)

    atlas_metadata = {"num_images": len(img_metadata), "metadata": img_metadata}
    fp = open(output_file + ".atlas", "w")
    fp.write(json.dumps(atlas_metadata, indent=4))
    fp.close()

    print(" done!")


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
