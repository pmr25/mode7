#!/usr/bin/env python3

import json
import sys
from PIL import Image


def print_help():
    print("usage: fillreducer.py <image>")


def compute_content_rect(pixels, w, h):
    xmin = sys.maxsize
    xmax = 0
    ymin = sys.maxsize
    ymax = 0

    for y in range(0, h):
        for x in range(0, w):
            if pixels[y * w + x][3] > 0:
                xmin = min(xmin, x)
                xmax = max(xmax, x)
                ymin = min(ymin, y)
                ymax = max(ymax, y)

    # normalize
    bbox = (xmin / w, (xmax + 1) / w, ymin / h, (ymax + 1) / h)
    # flip y
    bbox = (bbox[0], bbox[1], 1 - bbox[2], 1 - bbox[3])
    # change 0,1 to -1,1
    bbox = (bbox[0] * 2 - 1, bbox[1] * 2 - 1, bbox[2] * 2 - 1, bbox[3] * 2 - 1)
    # recenter
    centerx = (bbox[1] + bbox[0]) / 2
    centery = (bbox[3] + bbox[2]) / 2
    bbox = (bbox[0] - centerx, bbox[1] - centerx, bbox[2] - centery, bbox[3] - centery)

    ul = (bbox[0], bbox[2], 0)
    ur = (bbox[1], bbox[2], 0)
    bl = (bbox[0], bbox[3], 0)
    br = (bbox[1], bbox[3], 0)

    # return (ul, bl, br, ur)
    return (bl, br, ur, ul)


def compute_content_uvs(pixels, w, h):
    xmin = sys.maxsize
    xmax = 0
    ymin = sys.maxsize
    ymax = 0

    for y in range(0, h):
        for x in range(0, w):
            if pixels[y * w + x][3] > 0:
                xmin = min(xmin, x)
                xmax = max(xmax, x)
                ymin = min(ymin, y)
                ymax = max(ymax, y)

    # normalize
    bbox = (xmin / w, (xmax + 1) / w, ymin / h, (ymax + 1) / h)

    uvs = bbox

    sx = 1 / (bbox[1] - bbox[0])
    sy = 1 / (bbox[3] - bbox[2])
    tx = -bbox[0] * sx
    ty = -bbox[2] * sy

    ul = (-tx / sx, -ty / sy)
    ur = ((1 - tx) / sx, -ty / sy)
    bl = (-tx / sx, (1 - ty) / sy)
    br = ((1 - tx) / sx, (1 - ty) / sy)

    # return (ul, bl, br, ur)
    return bl, br, ur, ul


def to_vertex(pos, norm, uv):
    return [pos[0], pos[1], pos[2], norm[0], norm[1], norm[2], uv[0], uv[1]]


def main(argc, argv):
    if argc < 2:
        print_help()
        return

    fn = argv[1]
    outfn = fn + ".quad"

    if argc >= 3:
        outfn = argv[2]

    with Image.open(fn) as img:
        width, height = img.size
        pixels = list(img.getdata())

        rect = compute_content_rect(pixels, width, height)
        uvs = compute_content_uvs(pixels, width, height)

        normal = (0, 0, 1)
        buffer = []

        bl = to_vertex(rect[0], normal, uvs[0])
        br = to_vertex(rect[1], normal, uvs[1])
        ur = to_vertex(rect[2], normal, uvs[2])
        ul = to_vertex(rect[3], normal, uvs[3])

        buffer = bl + br + ur + ur + ul + bl

        obj = {
            "size": len(buffer),
            "data": buffer,
        }

        with open(outfn, "w") as fp:
            json.dump(obj, fp)


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
