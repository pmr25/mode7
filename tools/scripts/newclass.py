#!/usr/bin/python3

import os
import sys

CLASS_HEADER = """#ifndef {1}_HPP
#define {1}_HPP

class {2}
{{
{0}
}};

#endif /* {1}_HPP */
"""

CLASS_SOURCE = """#include \"{1}.hpp\"

"""

CLASS_NAMESP_HEADER = """#ifndef {1}_HPP
#define {1}_HPP

namespace {3}
{{
{0}class {2}
{0}{{
{0}{0} 
{0}}};
}}

#endif /* {1}_HPP */
"""

CLASS_NAMESP_SOURCE = """#include \"{1}.hpp\"

namespace {2}
{{
{0}
}}

"""


def print_help():
    print("usage: <source.cpp> <header.hpp> [flags]")


def main(argc, argv):
    if argc < 3:
        print_help()
        return

    tabstyle = "    "
    cpp = ""
    hpp = ""
    pwd = ""
    namesp = ""
    flags = []

    for i in range(1, argc):
        if argv[i].startswith("-"):
            # process flag
            if argv[i].startswith("-C"):
                pwd = argv[i][2:]  # remove flag start, spaces, etc
            elif argv[i].startswith("-N"):
                namesp = argv[i][2:]
        else:
            # process file
            if argv[i].endswith("cpp"):
                cpp = argv[i]
            elif argv[i].endswith("hpp"):
                hpp = argv[i]

    cpp = os.path.join(pwd, cpp)
    hpp = os.path.join(pwd, hpp)
    classname = os.path.splitext(os.path.basename(hpp))[0]

    print("creating files {}, {}".format(cpp, hpp))

    with open(hpp, "w") as fp:
        if len(namesp) > 0:
            fp.write(CLASS_NAMESP_HEADER.format(tabstyle, classname.upper(), classname, namesp))
        else:
            fp.write(CLASS_HEADER.format(tabstyle, classname.upper(), classname))

    with open(cpp, "w") as fp:
        if len(namesp) > 0:
            fp.write(CLASS_NAMESP_SOURCE.format(tabstyle, classname, namesp))
        else:
            fp.write(CLASS_SOURCE.format(tabstyle, classname))


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
