@echo off

SET mypath=%~dp0
SET repo=%mypath:~0,-1%\..\..
SET fn=dependencies.zip

@REM powershell -Command "wget https://mode7-assets.s3.us-east-2.amazonaws.com/%fn% -O %repo%\%fn%"
curl https://mode7-assets.s3.us-east-2.amazonaws.com/%fn% --output "%repo%\%fn%"
tar -xf "%repo%\\%fn%" -C "%repo%\\"
del "%repo%\\%fn%"
