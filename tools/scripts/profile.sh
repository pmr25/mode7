#!/usr/bin/env bash

gprof $1 gmon.out > analysis.txt
python3 -m gprof2dot analysis.txt | dot -Tpng -o profile_graph.png
rm analysis.txt
shotwell profile_graph.png & disown
