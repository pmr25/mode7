#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SOURCE_DIR=$SCRIPT_DIR/../..

BUILD_DIR=$1
AI_NAME=$2
ARCH=$3
AI_EXEC=$4

rm -rf $BUILD_DIR/
mkdir -p $BUILD_DIR
linuxdeploy-x86_64.AppImage --appdir=$BUILD_DIR --icon-file=$SOURCE_DIR/game/metadata/game.png --desktop-file=$SOURCE_DIR/game/metadata/game.desktop --executable=$AI_EXEC
cp -r $SOURCE_DIR/assets $BUILD_DIR/usr/
cp -r $SOURCE_DIR/game/scripts $BUILD_DIR/usr/assets/
mkdir -p $BUILD_DIR/usr/assets/shaders/v2
cp -r $SOURCE_DIR/game/shaders/* $BUILD_DIR/usr/assets/shaders/v2
rm -f $BUILD_DIR/$AI_NAME-$ARCH.AppImage
ARCH=$ARCH appimagetool-x86_64.AppImage $BUILD_DIR $BUILD_DIR/$AI_NAME-$ARCH.AppImage $5
rm -rf $BUILD_DIR/usr
