#!/usr/bin/python3

import os
import shutil
import sys

IGNORED = shutil.ignore_patterns("*.blend*", "*.pxo", "*.xcf", "*.template.*", "__pycache__", "../../assets/blender", "gimp",
                                 "pixelorama", "templates", "install.py", "assets.zip")


def print_usage():
    print("usage: install.py <source dir> <destination dir>")


def copy_tree(src, dst):
    shutil.copytree(src, dst, ignore=IGNORED, dirs_exist_ok=True)


def main(argc, argv):
    if argc < 3:
        print_usage()
        return

    source_dir = os.path.abspath(argv[1])
    build_dir = os.path.abspath(argv[2])
    assets_dir = build_dir
    preserve_dest = False

    if argc >= 3:
        for arg in argv[3:]:
            if arg.startswith("--preserve"):
                preserve_dest = True

    print("installing assets at {} to {}".format(source_dir, assets_dir))

    if not os.path.exists(source_dir):
        print("cannot open directory {}".format(source_dir))
        return

    if source_dir in os.path.commonprefix([source_dir, assets_dir]):
        print("cannot install {} into source directory {}".format(assets_dir, source_dir))
        return

    if not preserve_dest and os.path.exists(assets_dir):
        shutil.rmtree(assets_dir)

    copy_tree(source_dir, assets_dir)


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
