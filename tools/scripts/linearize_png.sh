#!/usr/bin/env bash

convert $1 -define png:color-type=6 -alpha on -gamma .45455 $1
