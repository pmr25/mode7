#!/usr/bin/env bash

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
