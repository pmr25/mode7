#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
FN=assets.zip

wget https://mode7-assets.s3.us-east-2.amazonaws.com/$FN -O$SCRIPT_DIR/$FN
unzip -o $SCRIPT_DIR/$FN -d $SCRIPT_DIR/../../assets
rm $SCRIPT_DIR/$FN
