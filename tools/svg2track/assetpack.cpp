/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "assetpack.h"
#include "Loader.hpp"
#include "mesh.h"
#include "wavefront.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>

static bool endswith(const char* str, const char* pat)
{
    uint32_t str_len = strlen(str);
    uint32_t pat_len = strlen(pat);

    if (pat_len > str_len)
    {
        return false;
    }

    const char* start = str + (str_len - pat_len);

    return strcmp(start, pat) == 0;
}

static bool extract_obj_file_to_newmesh(zip_t* archive, uint64_t obj_file_indx, std::shared_ptr<NewMesh>& mesh)
{
    int err = 0;
    zip_file_t* obj_file;

    obj_file = zip_fopen_index(archive, obj_file_indx, ZIP_FL_UNCHANGED);
    if (!obj_file)
    {
        fprintf(stderr, "zip error: slice.obj does not exist in the archive\n");
        zip_close(archive);
        return 1;
    }

    zip_stat_t obj_file_stat;
    err = zip_stat_index(archive, obj_file_indx, 0, &obj_file_stat);
    int64_t obj_file_size = obj_file_stat.size;

    if (err)
    {
        fprintf(stderr, "fatal error\n");
        return 1;
    }
    if (obj_file_size == 0)
    {
        fprintf(stderr, "obj file is empty\n");
        return 1;
    }
    if (obj_file_size < 0)
    {
        fprintf(stderr, "fatal error\n");
        return 1;
    }

    FILE* tmp = fopen(".transfer.obj", "wb");
    if (!tmp)
    {
        fprintf(stderr, "io error: cannot open temporary file for writing\n");
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    size_t buf_size = 1024;
    char* buf = (char*)malloc(buf_size);
    if (!buf)
    {
        fprintf(stderr, "memory error\n");
        fclose(tmp);
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    uint32_t iterations = obj_file_size / buf_size + ((obj_file_size % buf_size) > 0);
    if (iterations < 1)
    {
        fprintf(stderr, "unable to process obj file\n");
        fclose(tmp);
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    uint64_t n_read;
    for (uint32_t i = 0; i <= iterations; ++i)
    {
        n_read = zip_fread(obj_file, buf, buf_size);
        fwrite(buf, sizeof(char), n_read, tmp);
    }

    fclose(tmp);
    zip_fclose(obj_file);

    // wavefront_load(dest, ".transfer.obj.tmp");

    mesh = Loader(".transfer.obj").getModel();

    remove(".transfer.obj");
    return true;
}

static bool extract_obj_file(zip_t* archive, uint64_t obj_file_indx, mesh** dest)
{
    int err = 0;
    zip_file_t* obj_file;

    obj_file = zip_fopen_index(archive, obj_file_indx, ZIP_FL_UNCHANGED);
    if (!obj_file)
    {
        fprintf(stderr, "zip error: slice.obj does not exist in the archive\n");
        zip_close(archive);
        return 1;
    }

    zip_stat_t obj_file_stat;
    err = zip_stat_index(archive, obj_file_indx, 0, &obj_file_stat);
    int64_t obj_file_size = obj_file_stat.size;

    if (err)
    {
        fprintf(stderr, "fatal error\n");
        return 1;
    }
    if (obj_file_size == 0)
    {
        fprintf(stderr, "obj file is empty\n");
        return 1;
    }
    if (obj_file_size < 0)
    {
        fprintf(stderr, "fatal error\n");
        return 1;
    }

    FILE* tmp = fopen(".transfer.obj", "wb");
    if (!tmp)
    {
        fprintf(stderr, "io error: cannot open temporary file for writing\n");
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    size_t buf_size = 1024;
    char* buf = (char*)malloc(buf_size);
    if (!buf)
    {
        fprintf(stderr, "memory error\n");
        fclose(tmp);
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    uint32_t iterations = obj_file_size / buf_size + ((obj_file_size % buf_size) > 0);
    if (iterations < 1)
    {
        fprintf(stderr, "unable to process obj file\n");
        fclose(tmp);
        zip_fclose(obj_file);
        zip_close(archive);
        return 1;
    }

    uint64_t n_read;
    for (uint32_t i = 0; i <= iterations; ++i)
    {
        n_read = zip_fread(obj_file, buf, buf_size);
        fwrite(buf, sizeof(char), n_read, tmp);
    }

    fclose(tmp);
    zip_fclose(obj_file);

    wavefront_load(dest, ".transfer.obj");

    Loader(".transfer.obj").getModel();

    remove(".transfer.obj");
    return true;
}

int assetpack_open(assetpack* ap, const char* filename)
{
    zip_error_t ze;
    int err = 0;
    zip_t* archive;

    ap->msh = NULL;
    ap->name = NULL;

    archive = zip_open(filename, ZIP_RDONLY, &err);
    if (err)
    {
        zip_error_init_with_code(&ze, err);
        fprintf(stderr, "zip error: %s\n", zip_error_strerror(&ze));
        return 1;
    }

    int n_entries = zip_get_num_entries(archive, 0);
    if (n_entries < 1)
    {
        fprintf(stderr, "zip error: no files in archive\n");
        zip_close(archive);
        return 1;
    }

    const char* indx_name;
    uint64_t slice_file_indx = UINT64_MAX;
    uint64_t wall_file_indx = UINT64_MAX;
    for (uint64_t i = 0; i < n_entries; ++i)
    {
        indx_name = zip_get_name(archive, i, ZIP_FL_ENC_GUESS); // guess encoding
        if (endswith(indx_name, "default.obj"))
        {
            slice_file_indx = i;
        }
        else if (endswith(indx_name, "wall.obj"))
        {
            wall_file_indx = i;
        }
    }

    // read .obj files
    // extract_obj_file(archive, slice_file_indx, &ap->msh);
    // extract_obj_file(archive, wall_file_indx, &ap->wall);

    extract_obj_file_to_newmesh(archive, slice_file_indx, ap->newMesh);
    extract_obj_file_to_newmesh(archive, wall_file_indx, ap->newWall);

    if (ap->newMesh == nullptr)
    {
        return 1;
    }

    if (ap->newMesh == nullptr)
    {
        return 2;
    }

    // strcpy(ap->msh->material, "Material.001");
    // strcpy(ap->wall[0].material, "Material.002");
    // strcpy(ap->wall[1].material, "Material.002");

    zip_close(archive);

    return 0;
}

void assetpack_free(assetpack* ap)
{
    // TODO
    // if (ap->name)
    // {
    //     free(ap->name);
    //     ap->name = NULL;
    // }
    // if (ap->msh)
    // {
    //     mesh_destroy(ap->msh);
    //     ap->msh = NULL;
    // }
}