#ifndef TRACKBUILDER_HPP
#define TRACKBUILDER_HPP

#include "Bezier.hpp"
#include "Line.hpp"
#include "assetpack.h"
#include "util.h"
#include "EquationCurve.hpp"
#include <cstdint>
#include <string>
#include <vector>

class TrackBuilder
{
public:
    void addLine(Line<2>&);
    void addBezier(Bezier<2>&);
    void addWall(Line<2>&);
    auto addHeightMap(geom::EquationCurve) -> void;
    void createMesh(assetpack*, const std::string&, float, float, float,
                    bool genPreview = false);
    auto centerTrack(bool val) -> void
    {
        mShouldCenter = val;
    }

private:
    std::vector<Line<2>> mLines;
    std::vector<Bezier<2>> mBeziers;
    std::vector<Line<2>> mWalls;
    std::vector<std::pair<segment_t, uint32_t>> mSegments;
    geom::EquationCurve mHeightMap;
    bool mShouldCenter{false};
};

#endif /* TRACKBUILDER_HPP */
