# svg2track

## Usage

`svg2track <track.svg> <assetpack.zip> <output-file.obj> [flags]`

### Flags

```
--track-width       float   width of the track
--runoff-width      float   width of the runoff
--wall-width        float   width of the wall (not used)
--curvature-weight  float   (not used)
```

## Input SVG Format

### Valid Components

- Line segments
- Bezier Curves

### Track

- Must be closed loop
- Line segments or bezier curves
- Stroke color #000000

### Walls

- Closed or open loop
- Line segments only
- Stroke color #FF0000

## Examples

Examples .svg files are located in `assets/track_data/reference`
