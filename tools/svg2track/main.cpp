/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Exporter.hpp"
#include "Loader.hpp"
#include "assetpack.h"
#include "mesh.h"
#include "preview.h"
#include "svg.hpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <iostream>
#include <string>

namespace fs = std::filesystem;

static void print_usage()
{
    std::cout << "usage: svg2track <.svg input> <.zip asset pack> <.obj output> [flags]" << std::endl;
    std::cout << "flags:\n\t--track-width\t\t<float>\n\t--runoff-width\t\t<float>\n\t--wall-width\t\t<float>\n\t--curvature-weight\t<float>\n\t--workdir\t<directory>\n\tmore" << std::endl;
}

static void self_test()
{
    std::cout << "running self test..." << std::endl;
    std::shared_ptr<NewMesh> mesh = Loader("../../../assets/svg2track/cube.ply").getModel();
    const std::string& err = Exporter(*mesh, "./out.ply", EXPORTER_TRIANGULATE).getError();
    if (err != "")
    {
        std::cout << "error: " + err << std::endl;
        return;
    }

    std::shared_ptr<NewMesh> def{Loader("../../../assets/track_data/reference/default/default.obj").getModel()};
    def->setName("default");
    auto err2{Exporter(*def, "./out.obj").getError()};
    if (err2 != "")
    {
        std::cout << "error: " << err << std::endl;
        return;
    }
}

static bool startswith(const char* str, const char* prefix)
{
    uint32_t prefix_len = strlen(prefix);
    if (prefix_len > strlen(str))
    {
        return false;
    }

    return strncmp(str, prefix, prefix_len) == 0;
}

int main(int argc, char** argv)
{
    int err;

    std::string workDir;

    if (argc < 3)
    {
        print_usage();
        self_test();
        return 1;
    }

    // parse flags
    float track_width = 10.0;
    float runoff_width = 20.0;
    float wall_width = 1.0;
    bool should_center{true};
    char* flag;
    char* val;
    for (int i = 4; i < argc; ++i)
    {
        flag = argv[i];
        if (i + 1 >= argc)
        {
            break;
        }

        val = argv[i + 1];

        std::cout << "flag,val = " << flag << "," << val << std::endl;

        if (startswith(flag, "--track-width"))
        {
            track_width = atof(val);
        }
        else if (startswith(flag, "--runoff-width"))
        {
            runoff_width = atof(val);
        }
        else if (startswith(flag, "--wall-width"))
        {
            wall_width = atof(val);
        }
        else if (startswith(flag, "--workdir"))
        {
            workDir = val;
        }
        else if (startswith(flag, "--no-center"))
        {
            should_center = atoi(val) != 0;
        }
        else
        {
            printf("ignoring unknown flag %s\n", flag);
        }
    }

    // track cur_track;
    TrackBuilder builder;
    assetpack ap;

    builder.centerTrack(should_center);

    fs::path assetPackPath = fs::path(workDir) / fs::path(argv[2]);
    printf("open assetpack %s\n", assetPackPath.string().c_str());
    err = assetpack_open(&ap, assetPackPath.string().c_str());
    if (err)
    {
        return 1;
    }

    std::cout << "construct track from svg" << std::endl;
    fs::path svgPath = fs::path(workDir) / fs::path(argv[1]);
    svg_construct_track(&builder, svgPath.string());

    geom::EquationCurve heightMap([](float t) -> float
                          {
                              const auto A{10.0F};
                              const auto B{M_PI};
                              const auto C {0.0F};
                              const auto s{A * powf(sinf(B*t), 2.0F) + C};
                              return s;
                          });

    fs::path meshPath = fs::path(workDir) / fs::path(argv[3]);
    std::cout << "create mesh" << std::endl;
    builder.addHeightMap(heightMap);
    builder.createMesh(&ap, meshPath.string(), track_width, runoff_width, wall_width);
    assetpack_free(&ap);

    return 0;
}
