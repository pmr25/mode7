#include "TrackBuilder.hpp"
#include "Exporter.hpp"
#include "NewMesh.hpp"
#include "TrackData.hpp"
#include "preview.h"
#include "wavefront.h"

#include <cassert>
#include <iostream>
#include <memory>

#define STEPSIZE 1.0f

void TrackBuilder::addLine(Line<2>& ln)
{
    mLines.push_back(ln);
    mSegments.push_back(
        std::pair<segment_t, uint32_t>(SEG_LINE, mLines.size() - 1));
}

void TrackBuilder::addBezier(Bezier<2>& bz)
{
    mBeziers.push_back(bz);
    mSegments.push_back(
        std::pair<segment_t, uint32_t>(SEG_BEZIER, mBeziers.size() - 1));
}

void TrackBuilder::addWall(Line<2>& ln)
{
    mWalls.push_back(ln);
}

void TrackBuilder::createMesh(assetpack* assets, const std::string& fn,
                              float trackWidth, float runoffWidth,
                              float wallWidth, bool genPreview)
{
    mode7::TrackData trackData;
    std::vector<glm::vec2> points;
    std::vector<float> curvature;

    /**
     * accumulate interpolated points
     * compute curvature
     */
    for (auto& segmentTypeIndex : mSegments)
    {
        const segment_t type = segmentTypeIndex.first;
        const uint32_t idx = segmentTypeIndex.second;
        const ParametricCurve<2>* curve;

        if (type == SEG_LINE)
        {
            assert(idx < mLines.size());
            curve = &mLines[idx];
        }
        else if (type == SEG_BEZIER)
        {
            assert(idx < mBeziers.size());
            curve = &mBeziers[idx];
        }
        else
        {
            std::cout << "unknown curve type" << std::endl;
            return;
        }

        assert(curve != nullptr);

        std::vector<glm::vec2> tmp = curve->interpolate(STEPSIZE);
        std::cout << "interpolated " << tmp.size() << " points" << std::endl;

        std::cout << tmp.size() << "," << curve->numPoints(STEPSIZE)
                  << std::endl;

        const float averageCurvature = curve->getAverageCurvature(STEPSIZE);

        for (size_t i{0}; i < tmp.size() - 1; ++i)
        {
            //            const float t = (float)i / (float)tmp.size();
            //            const float k = curve->curvature(t);
            const float k = averageCurvature;
            curvature.push_back(k);
        }

        points.insert(points.end(), tmp.begin(), tmp.end() - 1);
    }

    uint32_t numLines = points.size();
    assert(numLines > 0);

    std::cout << "NUM LINES = " << numLines << std::endl;

    // allocate memory
    std::vector<Line<2>> centerLines;
    std::vector<Line<2>> orthogLines;
    centerLines.reserve(numLines);
    orthogLines.reserve(numLines);

    /**
     * for each pair of points, compute its line and orthogonal line
     */
    uint32_t ia;
    uint32_t ib;
    Line<2> curLine;
    Line<2> orthLine;
    for (uint32_t i = 0; i < numLines; ++i)
    {
        // compute indices
        ia = ((i + 0) % numLines);
        ib = ((i + 1) % numLines);

        assert(ia < points.size());
        assert(ib < points.size());

        // compute center points
        glm::vec2& pointA = points[ia];
        glm::vec2& pointB = points[ib];

        // compute center line
        curLine.connectPoints(pointA, pointB);
        centerLines.push_back(curLine);

        // compute orthogonal lines
        orthLine.definePointVector(pointA, curLine.computeNormal());
        orthogLines.push_back(orthLine);
    }

    // create materials
    std::vector<std::shared_ptr<Material>> materials;
    materials.push_back(std::make_shared<Material>("track"));
    materials.push_back(std::make_shared<Material>("wall"));

    // allocate meshes
    std::vector<NewMesh> meshes;
    meshes.resize(numLines);

    /**
     * create meshes for each segment
     * segments are defined by the two orthogonal lines for each centerline
     */
    // --------------------------- orth 1
    //              |
    //              |
    //              | centerline
    //              |
    // --------------------------- orth 2
    const auto heightMapStep{1.0F / float(numLines)};
    for (uint32_t i = 0; i < numLines; ++i)
    {
        // compute track segment
        const Line<2>* frontLine = &orthogLines[(i + 0) % numLines];
        const Line<2>* backLine = &orthogLines[(i + 1) % numLines];

        const auto frontHeight = mHeightMap.solve(float(i + 0) * heightMapStep);
        const auto backHeight = mHeightMap.solve(float(i + 1) * heightMapStep);

        NewMesh* curMesh{&meshes[i]};
        assert(assets->newMesh != nullptr);
        *curMesh = *(assets->newMesh);
        curMesh->setName("track_segment_" + std::to_string(i));
        curMesh->setMaterial(materials[0]);

        const double halfTrackWidth = trackWidth / 2.0;
        // const double curvatureFactor1{10.0f * fabsf(curvature[i])};
        // const double curvatureFactor2{10.0f * fabsf(curvature[i + 1])};
        const double curvatureFactor1{0.0};
        const double curvatureFactor2{0.0};

        Quad quad;
        quad.connectPoints(frontLine->solve(halfTrackWidth - curvatureFactor1),
                           frontLine->solve(-halfTrackWidth + curvatureFactor1),
                           backLine->solve(-halfTrackWidth + curvatureFactor2),
                           backLine->solve(halfTrackWidth - curvatureFactor2));

        trackData.addSegment(
            centerLines[i], quad, runoffWidth, wallWidth,
            {frontHeight, frontHeight, backHeight, backHeight});

        // stretch mesh to fit track segment
        // const float halfWidth = curMesh->getLength().z / 2.0F;
        for (auto j{0U}; j < curMesh->getNumVertices(); ++j)
        {
            Vertex& v = curMesh->getVertices().at(j);
            const float t = v.position.z / curMesh->getLength().z + 0.5F;

            glm::vec2 pt;
            if (v.position.x == 0.0F)
            {
                pt = backLine->solve(t * trackWidth);
                v.position.y += backHeight;
            }
            else if (v.position.x == 1.0F)
            {
                pt = frontLine->solve(t * trackWidth);
                v.position.y += frontHeight;
            }

            v.position.x = pt.x;
            v.position.z = pt.y;
        }
    }

    /********** build walls **********/
    std::vector<NewMesh> wallMeshes;
    glm::vec2 maxDim(-FLT_MAX);
    glm::vec2 minDim(FLT_MAX);

    // fit wall meshes to computed walls
    for (auto& line : mWalls)
    {
        geom::Plane plane;

        glm::vec3 a(line.getPoint().x, 0.0F, line.getPoint().y);
        glm::vec3 b(line.getEndpoint().x, 2.0F, line.getEndpoint().y);
        glm::vec3 c(line.getEndpoint().x, 0.0F, line.getEndpoint().y);
        plane.definePoints(a, b, c);

        maxDim.x = fmaxf(maxDim.x, fmaxf(a.x, c.x));
        maxDim.y = fmaxf(maxDim.y, fmaxf(a.z, c.z));
        minDim.x = fminf(minDim.x, fminf(a.x, c.x));
        minDim.y = fminf(minDim.y, fminf(a.z, c.z));

        trackData.addWall(plane);

        float desired_len = 8.0F;
        glm::vec2 diff = line.getPoint() - line.getEndpoint();
        float wall_len = glm::length(diff);
        float len_div = wall_len / desired_len;
        float extra = (len_div - floorf(len_div)) / floorf(len_div);
        float acutal_len = desired_len + (desired_len * extra);
        uint32_t num_segments = floorf(len_div);
        glm::vec2 dir = -glm::normalize(diff) * acutal_len;

        for (auto i{0U}; i < num_segments; ++i)
        {
            wallMeshes.emplace_back();
            NewMesh* curWallMesh{&wallMeshes[wallMeshes.size() - 1]};
            *curWallMesh = *assets->newWall;
            curWallMesh->setName("wall_" + std::to_string(wallMeshes.size()));
            curWallMesh->setMaterial(materials[1]);

            for (auto j{0U}; j < curWallMesh->getNumVertices(); ++j)
            {
                glm::vec3& pos = curWallMesh->getVertices()[j].position;

                if (pos.z == -4.0F)
                {
                    pos.x = line.getPoint().x + dir.x * float(i);
                    pos.z = line.getPoint().y + dir.y * float(i);
                }
                else if (pos.z == 4.0F)
                {
                    pos.x = line.getPoint().x + dir.x * float(i + 1);
                    pos.z = line.getPoint().y + dir.y * float(i + 1);
                }

                assert(pos == curWallMesh->getVertices()[j].position);
            }
        }
    }

    meshes.insert(meshes.end(), wallMeshes.begin(), wallMeshes.end());

    glm::vec2 dim = maxDim - minDim;
    glm::vec3 center(0.0F);
    if (mShouldCenter)
    {
        std::cout << "centering the track" << std::endl;
        center = glm::vec3(-(minDim.x + dim.x / 2.0F), 0, -(minDim.y + dim.y / 2.0F));
        for (auto& mesh : meshes)
        {
            mesh.translateVertices(center);
        }
        trackData.transform(glm::translate(glm::mat4(1.0F), center));
    }

    // write out files
    auto err{Exporter(meshes, materials, fn).getError()};
    if (err != "")
    {
        std::cout << err << std::endl;
        assert(1 == 0); // TODO CHANGE
    }

    trackData.setVersion(3);
    trackData.saveData(fn + ".td3");

    if (genPreview)
    {
        preview_generate_png(centerLines, orthogLines,
                             trackData.getTrackBounds(),
                             trackData.getRunoffBounds());
    }
}

auto TrackBuilder::addHeightMap(geom::EquationCurve equation) -> void
{
    mHeightMap = equation;
}
