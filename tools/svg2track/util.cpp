#include "util.h"

#include <math.h>

int util_curve_orientation(float x1, float y1, float x2, float y2)
{
    float a[3] = {x2 - x1, 0.0f, y2 - y1};
    float b[3] = {-a[2], 0.0f, a[1]};

    float cross = (a[2] * b[1] - a[1] * b[2]);

    return (int)copysignf(1.0f, cross);
}