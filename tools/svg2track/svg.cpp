/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "svg.hpp"
#include "Bezier.hpp"
#include "Line.hpp"

#include "msvg.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>

static TrackBuilder* curBuilder = nullptr;

static void path_to_track(TrackBuilder* builder, MsvgElement* el)
{
    MsvgPathAttributes* pa = el->ppathattr;
    MsvgSubPath* sp = pa->sp;
    MsvgSubPathPoint* pt;

    glm::vec2 head;
    char cur_cmd;
    Bezier<2> bzCurve;
    Line<2> curLine;
    int repeat_counter = 0;

    bool saveLine = false;
    bool saveCurve = false;

    bool origin_set = false;
    float ox = 0.f;
    float oy = 0.f;
    glm::vec2 origin(0.0f);

    for (int i = 0; i < sp->npoints; ++i)
    {
        pt = &(sp->spp[i]);
        switch (pt->cmd)
        {
        case 'M': // move to
            // head[0] = pt->x;
            // head[1] = pt->y;
            head = glm::vec2(pt->x, pt->y);
            if (!origin_set)
            {
                origin_set = true;
                // ox = pt->x;
                // oy = pt->y;
                // printf("origin: %f,%f\n", ox, oy);
            }
            break;

        case 'L': // linear
            cur_cmd = pt->cmd;
            if (saveLine)
            {
                builder->addLine(curLine);
                saveLine = false;
                // track_add_line(builder, cur_line);
                // cur_line = NULL;
            }

            // assert(cur_line == NULL);
            // cur_line = (line*)malloc(sizeof(line));
            // if (cur_line == NULL)
            // {
            //     printf("fatal\n");
            //     exit(1);
            //     // todo improve
            // }

            if (saveCurve) // save curve
            {
                builder->addBezier(bzCurve);
                saveCurve = false;
                // track_add_bezier(tr, cur_curve);
                // cur_curve = NULL;
            }

            assert(origin_set);
            // line_connect(cur_line, head[0] - ox, head[1] - oy, pt->x - ox,
            // pt->y - oy);
            curLine.connectPoints(glm::vec2(head[0] - ox, head[1] - oy), glm::vec2(pt->x - ox, pt->y - oy));
            saveLine = true;

            // move head
            head[0] = pt->x;
            head[1] = pt->y;

            break;

        case 'C': // cubic bezier
            repeat_counter = 0;
            cur_cmd = pt->cmd;
            if (saveLine)
            {
                builder->addLine(curLine);
                saveLine = false;
                // track_add_line(tr, cur_line);
                // cur_line = NULL;
            }
            if (saveCurve) // save curve
            {
                builder->addBezier(bzCurve);
                saveCurve = false;
                // track_add_bezier(tr, cur_curve);
                // cur_curve = NULL;
            }

            // assert(cur_curve == NULL);
            // cur_curve = (bezier*)malloc(sizeof(bezier));
            // if (cur_curve == NULL)
            // {
            //     exit(1); // todo improve
            // }
            // bezier_init(cur_curve, 100);

            bzCurve.setStartPoint(head - origin);
            bzCurve.addControlPoint(glm::vec2(pt->x, pt->y) - origin);
            saveCurve = true;
            // bezier_set_endpt(cur_curve, 0, head[0] - ox, head[1] - oy);
            // bezier_set_ctrlpt(cur_curve, 0, pt->x - ox, pt->y - oy);
            break;

        case ' ': // extra points
            if (cur_cmd == 'C')
            {
                // assert(cur_curve != NULL);
                ++repeat_counter;
                if (repeat_counter == 1)
                {
                    // bezier_set_ctrlpt(cur_curve, 1, pt->x - ox, pt->y - oy);
                    bzCurve.addControlPoint(glm::vec2(pt->x, pt->y) - origin);
                }
                else if (repeat_counter == 2)
                {
                    // bezier_set_endpt(cur_curve, 1, pt->x - ox, pt->y - oy);
                    // head[0] = pt->x;
                    // head[1] = pt->y;

                    bzCurve.setEndPoint(glm::vec2(pt->x, pt->y) - origin);
                    head = glm::vec2(pt->x, pt->y);
                }
                else
                {
                    exit(1); // todo improve
                }
            }
            break;

        default:

            break;
        }
    }
    if (saveCurve) // check again if curve needs to be saved
    {
        builder->addBezier(bzCurve);
        saveCurve = false;
        // track_add_bezier(tr, cur_curve);
        //  cur_curve = NULL;
    }

    if (saveLine)
    {
        builder->addLine(curLine);
        saveLine = false;
        // track_add_line(tr, cur_line);
        //  cur_line = NULL;
    }
}

static void path_to_wall(TrackBuilder* builder, MsvgElement* el)
{
    (void)builder;

    MsvgPathAttributes* pa = el->ppathattr;
    MsvgSubPath* sp = pa->sp;
    MsvgSubPathPoint* pt;
    glm::vec2 head;
    glm::vec2 origin;
    char cur_cmd;
    Line<2> curWall;
    bool pending_save = false;
    // int repeat_counter = 0;

    bool origin_set = false;

    for (int i = 0; i <= sp->npoints; ++i)
    {
        pt = &(sp->spp[i]);
        switch (pt->cmd)
        {
        case 'M': // move to
            head.x = pt->x;
            head.y = pt->y;
            if (!origin_set)
            {
                origin.x = pt->x;
                origin.y = pt->y;
                origin_set = true;
            }

            break;

        case 'L': // linear
            cur_cmd = pt->cmd;

            if (pending_save)
            {
                // track_add_wall(tr, cur_wall[0], cur_wall[1], cur_wall[2],
                // cur_wall[3]);
                curBuilder->addWall(curWall);
                pending_save = false;
            }

            curWall.connectPoints(head, glm::vec2(pt->x, pt->y));
            pending_save = true;

            // move head
            head.x = pt->x;
            head.y = pt->y;

            break;

        case 'C': // cubic bezier
            cur_cmd = pt->cmd;
            printf("WARNING: cubic bezier should not happen\n");
            break;
        case ' ': // extra points
            printf("WARNING: extra points should not happen\n");
            if (cur_cmd == 'C')
            {
            }
            break;

        default:
            if (pending_save) // check again if curve needs to be saved
            {
                // track_add_wall(tr, cur_wall[0], cur_wall[1], cur_wall[2],
                // cur_wall[3]);
                curBuilder->addWall(curWall);
                pending_save = false;
            }
            break;
        }
    }
    if (pending_save) // check again if curve needs to be saved
    {
        // track_add_wall(tr, cur_wall[0], cur_wall[1], cur_wall[2],
        // cur_wall[3]);
        curBuilder->addWall(curWall);
        pending_save = false;
    }
    if (sp->closed)
    {
        curWall.connectPoints(curWall.getEndpoint(), origin);
        curBuilder->addWall(curWall);
        // track_add_wall(tr, cur_wall[2], cur_wall[3], ox, oy);
    }
}

static void parse_path(TrackBuilder* builder, MsvgElement* el)
{
    // assert(tr != NULL);
    assert(builder != nullptr);

    MsvgPaintCtx* pctx = el->pctx;
    // rgbcolor fill = pctx.fill;
    rgbcolor stroke = pctx->stroke;

    if (stroke == BLACK_COLOR)
    {
        printf("parsing track\n");
        path_to_track(builder, el);
    }
    else if (stroke == RED_COLOR)
    {
        printf("parsing wall\n");
        path_to_wall(builder, el);
    }
    else
    {
        printf("WARNING: unknown section color %x\n", stroke);
    }
}

static void my_parser(MsvgElement* el, MsvgPaintCtx* pctx, void* udata)
{
    (void)udata;

    MsvgElement* newel;

    newel = MsvgTransformCookedElement(el, pctx);
    if (newel == nullptr)
    {
        return;
    }

    switch (newel->eid)
    {
    case EID_RECT:
        // YourDrawRectElement(newel);
        break;

    case EID_CIRCLE:
        // YourDrawCircleElement(newel);
        break;

    case EID_ELLIPSE:
        // YourDrawEllipseElement(newel);
        break;

    case EID_LINE:
        // YourDrawLineElement(newel);
        break;

    case EID_POLYLINE:
        // YourDrawPolylineElement(newel);
        break;

    case EID_POLYGON:
        // YourDrawPolygonElement(newel);
        break;

    case EID_PATH:
        parse_path(curBuilder, newel);
        break;

    default:
        printf("unknown element %s\n", MsvgFindElementName(newel->eid));
        break;
    }
}

auto svg_construct_track(TrackBuilder* builder, const std::string& filename) -> int
{
    MsvgElement* root;
    int err;
    int result;

    root = MsvgReadSvgFile(filename.c_str(), &err);
    if (root == nullptr)
    {
        fprintf(stderr, "error %d reading %s\n", err, filename.c_str());
        return 1;
    }

    result = MsvgRaw2CookedTree(root);
    if (result < 1)
    {
        fprintf(stderr, "parsing err\n");
        return 1;
    }

    curBuilder = builder;
    MsvgSerCookedTree(root, my_parser, nullptr);
    curBuilder = nullptr;

    return 0;
}
