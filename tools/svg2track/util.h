#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C"
{
#endif

    typedef enum
    {
        SEG_LINE = 0,
        SEG_BEZIER = 1
    } segment_t;

    int util_curve_orientation(float, float, float, float);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_H */