/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "preview.h"
#include "Line.hpp"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#if defined _WIN32
#include <cairo/cairo.h>
#elif defined _VCPKG
#include <cairo/cairo.h>
#elif defined __linux__
// #include <cairo.h>
#endif
#include <cairo/cairo.h>

#define WIDTH_A4_600PPI 7016
#define HEIGHT_A4_600PPI 4960
#define PREVIEW_FN "./preview.png"

#define X(v) ((v * scale) + offX)
#define Y(v) ((v * scale) + offY)
#define S(v) (v * scale)

int preview_generate_png(const std::vector<Line<2>>& lines, const std::vector<Line<2>>& orth, const std::vector<Quad>& trackBounds,
                         const std::vector<Quad>& runoffBounds)
{
    double scale = 20.0;
    double offX = 0;
    double offY = 0;
    // double x = 0;
    // double y = 0;

    uint32_t width = WIDTH_A4_600PPI;
    uint32_t height = HEIGHT_A4_600PPI;

    cairo_t* cr;
    cairo_surface_t* surf;

    // create drawing stuff
    surf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(surf);

    cairo_set_source_rgb(cr, 255, 255, 255);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    cairo_set_line_width(cr, 4);

    // cairo_set_source_rgba(cr, 0.1, 0.1, 1.0, 1.0);
    // for (auto& e : orth)
    // {
    //     glm::vec2 a = e.solve(-1.0f);
    //     glm::vec2 b = e.solve( 1.0f);
    //     cairo_move_to(cr, X(a.x), Y(a.y));
    //     cairo_line_to(cr, X(b.x), Y(b.y));
    // }
    // cairo_stroke(cr);

    uint32_t ctr = 0;
    uint32_t ctr2 = 0;
    for (const Quad& e : runoffBounds)
    {
        assert(e.getNumSides() == 4);
        ctr2 = 0;
        for (const Line<2>& f : e.getSides())
        {
            cairo_set_source_rgba(cr, 0.5, (double)(ctr2) / 4.0, 0.0, 1.0);
            ++ctr2;
            cairo_move_to(cr, X(f.getPoint().x), Y(f.getPoint().y));
            cairo_line_to(cr, X(f.getEndpoint().x), Y(f.getEndpoint().y));
            cairo_stroke(cr);

            // glm::vec2 a = f.solve(0.5);
            // glm::vec2 b = f.solve(0.5) + f.computeNormal();
            // cairo_move_to(cr, X(a.x), Y(a.y));
            // cairo_line_to(cr, X(b.x), Y(b.y));
        }
    }
    cairo_stroke(cr);

    cairo_set_source_rgba(cr, 0.3, 0.3, 0.3, 1.0);
    for (const Quad& e : trackBounds)
    {
        assert(e.getNumSides() == 4);
        for (const Line<2>& f : e.getSides())
        {
            cairo_move_to(cr, X(f.getPoint().x), Y(f.getPoint().y));
            cairo_line_to(cr, X(f.getEndpoint().x), Y(f.getEndpoint().y));

            glm::vec2 a = f.solve(0.5);
            glm::vec2 b = f.solve(0.5) + f.computeNormal();
            cairo_move_to(cr, X(a.x), Y(a.y));
            cairo_line_to(cr, X(b.x), Y(b.y));
        }
    }
    cairo_stroke(cr);

    cairo_set_source_rgba(cr, 1.0, 0.1, 0.1, 1.0);
    uint32_t index = 0;
    for (auto& e : lines)
    {
        if (index == 0)
        {
            cairo_set_source_rgba(cr, 1.0, 0.1, 1.0, 1.0);
        }
        else if (index == 1)
        {
            cairo_set_source_rgba(cr, 0.0, 1.0, 1.0, 1.0);
        }
        else
        {
            cairo_set_source_rgba(cr, 1.0, 0.1, 0.1, 1.0);
        }
        cairo_move_to(cr, X(e.getPoint().x), Y(e.getPoint().y));
        cairo_line_to(cr, X(e.getEndpoint().x), Y(e.getEndpoint().y));
        ++index;
        cairo_stroke(cr);
    }
    //    cairo_stroke(cr);

    // save png
    cairo_surface_write_to_png(surf, PREVIEW_FN);

    // cleanup
    cairo_destroy(cr);
    cairo_surface_destroy(surf);

    return 0;
}

// int preview_track_to_png(track* trk)
// {
//     if (trk->tdata.n_segments < 1)
//     {
//         fprintf(stderr, "bad track\n");
//         return 1;
//     }
//     int len = trk->tdata.clp;
//     printf("draw %d segments\n", len);

//     cairo_t* cr;
//     cairo_surface_t* surf;

//     uint32_t width = WIDTH_A4_600PPI;
//     uint32_t height = HEIGHT_A4_600PPI;

//     double scale = 20.0;
//     double offset_x = 0.0;
//     double offset_y = 0.0;
//     double x;
//     double y;

//     surf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
//     cr = cairo_create(surf);

//     // draw centerline
//     cairo_set_line_width(cr, 12);
//     cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
//     for (uint32_t i = 0; i < len; ++i)
//     {
//         line* cur = trk->tdata.centerline + i;
//         x = X(cur->p1[0]);
//         y = Y(cur->p1[1]);
//         cairo_move_to(cr, x, y);
//         //printf("move to %f,%f\n", x, y);
//         x = X(cur->p2[0]);
//         y = Y(cur->p2[1]);
//         cairo_line_to(cr, x, y);
//     }
//     cairo_stroke(cr);

//     cairo_set_line_width(cr, 4);

//     // draw wall bounds
//     cairo_set_source_rgba(cr, 1.0, 0.1, 0.1, 1.0);
//     for (uint32_t i = 0; i < len; ++i)
//     {
//         quad* cur = trk->tdata.walls_bounds + i;
//         cairo_move_to(cr, X(cur->p[0].x), Y(cur->p[0].y));
//         cairo_line_to(cr, X(cur->p[1].x), Y(cur->p[1].y));
//         cairo_line_to(cr, X(cur->p[2].x), Y(cur->p[2].y));
//         cairo_line_to(cr, X(cur->p[3].x), Y(cur->p[3].y));
//         cairo_close_path(cr);
//     }
//     cairo_stroke(cr);

//     // draw runoff bounds
//     cairo_set_source_rgba(cr, 1.0, 1.0, 0.2, 1.0);
//     for (uint32_t i = 0; i < len; ++i)
//     {
//         quad* cur = trk->tdata.runoff_bounds + i;
//         //printf("runoff %f,%f\n", X(cur->p[0].x), Y(cur->p[0].y));
//         cairo_move_to(cr, X(cur->p[0].x), Y(cur->p[0].y));
//         cairo_line_to(cr, X(cur->p[1].x), Y(cur->p[1].y));
//         cairo_line_to(cr, X(cur->p[2].x), Y(cur->p[2].y));
//         cairo_line_to(cr, X(cur->p[3].x), Y(cur->p[3].y));
//         cairo_close_path(cr);
//     }
//     cairo_stroke(cr);

//     // draw track bounds
//     cairo_set_line_width(cr, 12);
//     cairo_set_source_rgba(cr, 0.5, 1.0, 0.5, 1.0);
//     for (uint32_t i = 0; i < len; ++i)
//     {
//         quad* cur = trk->tdata.track_bounds + i;
//         cairo_move_to(cr, X(cur->p[0].x), Y(cur->p[0].y));
//         cairo_line_to(cr, X(cur->p[1].x), Y(cur->p[1].y));
//         cairo_line_to(cr, X(cur->p[2].x), Y(cur->p[2].y));
//         cairo_line_to(cr, X(cur->p[3].x), Y(cur->p[3].y));
//         cairo_close_path(cr);
//     }
//     cairo_stroke(cr);

//     // draw walls
//     cairo_set_line_width(cr, 12);
//     cairo_set_source_rgba(cr, 1.0, 0.0, 1.0, 1.0);
//     for (uint32_t i = 0; i < trk->n_discrete_walls; ++i)
//     {
//         float* cur = trk->discrete_walls + (i * 4);
//         cairo_move_to(cr, X(cur[0]), Y(cur[1]));
//         cairo_line_to(cr, X(cur[2]), Y(cur[3]));
//     }
//     cairo_stroke(cr);

//     cairo_surface_write_to_png(surf, "preview.png");

//     cairo_destroy(cr);
//     cairo_surface_destroy(surf);

//     return 0;
// }
