#!/usr/bin/python3

import re
import sys

RESERVED = [
    "if",
    "else",
    "for",
    "while",
    "do",
    "vec2",
    "vec3",
    "vec4",
    "int",
    "float",
    "uniform",
    "varying",
    "location",
    "main",
    "in",
    "out",
]


class SymbolTable:
    def __init__(self):
        self.table = {}
        self.defs = []

    def declare(self, name):
        if not name in self.table.keys:
            self.table[name] = -1

    def define(self, name, contents):
        if not name in self.table.keys:
            self.declare(name)

        index = len(self.defs)
        self.table[name] = index
        self.defs.append(contents)

    def get_resolution(self, name):
        if not name in self.table.keys:
            return "SYMTABLE_UNDECLARED_SYMBOL"

        index = self.table[name]

        if index < 0 or index >= len(self.defs):
            return "SYMTABLE_UNDEFINED_SYMBOL"

        return "SYMTABLE_INDEX(" + str(index) + ")"

    def get_definition(self, index):
        return self.defs[index]

    def replace_symbols(self, line):
        dirty = False
        delim = r"[\[\]\+\*\?\-\^\&\~\|\.\ ]+"

        parts = re.split(delim, line)  # extract possible symbol names
        for part in parts:
            res = self.get_resolution(part)
            if not res == "SYMTABLE_UNDECLARED_SYMBOL":
                dirty = True
                index = int(res.split("(")[1][:-1])  # extract index from SYMTABLE_INDEX(index)
                line.replace(part, self.get_definition(self, self.get_definition(index))

        return (line, dirty)


class Source:
    def __init__(self, symtable):
        self.raw = []
        self.cooked = []
        self.filename = ""
        self.symtable = symtable
        self.symbol_locations = []

    def open(self, filename):
        self.filename = filename
        fp = open(filename, "r")
        lctr = 1
        self.raw = fp.readlines()

        for line in self.raw:
            if line.startswith("#"):
                self.symbol_locations.append(lctr)

                if line.startswith("#include"):
                    name = line.split(" ")[1][1:-1]  # extract filename, strip leading and characters
                    self.symtable.declare(name)

                elif line.startswith("#define"):
                    name = line.split(" ")[1]  # extract name
                    value = line.split(" ")[2]  # extract value
                    self.symtable.define(name, value)

            lctr += 1

    def resolve(self):
        # mark defines as removed, replace includes with symbol tags
        for index in self.symbol_locations:
            pass

        # replace symbol references with symbol tags
        for i in range(len(self.raw)):
            (line, modified) = self.symtable.replace_symbols(self.raw[i])
            if modified:
                self.raw[i] = line
                self.symbol_locations.append(i)

    def cook(self):
        for index in self.symbol_locations:
            pass

    def save(self, filename):
        fp = open(filename, "w")


def main(argc, argv):
    pass


if __name__ == "__main__":
    main(len(sys.argv), argv)
