#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <SDL.h>

namespace trackeditor
{
class Button
{
public:
    Button()
    {
    }
    ~Button()
    {
    }

    void update();
    bool isClicked();
    void ackClick();

private:
    SDL_Texture* mTexture;
};
} // namespace trackeditor

#endif /* BUTTON_HPP */
