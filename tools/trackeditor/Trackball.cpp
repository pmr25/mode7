#include "Trackball.hpp"

#include <cairo/cairo.h>
#include <cassert>
#include <cmath>
#include <iostream>

namespace trackeditor
{
void Trackball::setPosition(float x, float y)
{
    mX = x;
    mY = y;
}

void Trackball::createTexture(const Screen& screen)
{
    const uint32_t flags = 0;
    const uint32_t width = 128;
    const uint32_t height = 128;
    const uint32_t rmask = 0x00FF0000;
    const uint32_t gmask = 0x0000FF00;
    const uint32_t bmask = 0x000000FF;
    const uint32_t amask = 0xFF000000;

    mWidth = width;
    mHeight = height;

    SDL_Surface* surf = SDL_CreateRGBSurface(flags, width, height, 32, rmask, gmask, bmask, amask);
    if (!surf)
    {
        std::cout << "cannot create surface" << std::endl;
        return;
    }

    if (SDL_LockSurface(surf))
    {
        std::cout << "cannot lock surface" << std::endl;
        return;
    }

    cairo_surface_t* cairoSurf =
        cairo_image_surface_create_for_data((unsigned char*)surf->pixels, CAIRO_FORMAT_ARGB32, surf->w, surf->h, surf->pitch);

    cairo_t* cr = cairo_create(cairoSurf);

    // draw interface
    cairo_set_source_rgb(cr, 255, 255, 255);
    cairo_arc(cr, mWidth / 2, mHeight / 2, width / 2, 0, 360);
    cairo_fill(cr);

    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_set_line_width(cr, 2);
    cairo_arc(cr, mWidth / 2, mHeight / 2, mWidth / 3, 0, 360);
    cairo_stroke(cr);

    cairo_destroy(cr);
    cairo_surface_destroy(cairoSurf);
    if (SDL_LockSurface(surf))
    {
        std::cout << "cannot unlock surface" << std::endl;
        return;
    }
    mTexture = SDL_CreateTextureFromSurface(screen.getRenderer(), surf);
    if (!mTexture)
    {
        std::cout << "cannot create texture" << std::endl;
        return;
    }
    SDL_FreeSurface(surf);
}

void Trackball::handleEvent(Screen& screen, const SDL_Event* event)
{
    if (event->type == SDL_MOUSEBUTTONDOWN)
    {
        if (!(event->button.x > mX && event->button.x < mX + mWidth && event->button.y > mY && event->button.y < mY + mHeight))
        {
            return;
        }

        SDL_SetRelativeMouseMode(SDL_TRUE);
        const SDL_Rect rect = {(int)mX + (int)(mWidth / 2.0f), (int)mY + (int)(mHeight / 2.0f), 1, 1};
        SDL_SetWindowMouseRect(screen.getWindow(), &rect);
        mIsMouseGrabbed = true;
    }
    else if (event->type == SDL_MOUSEBUTTONUP)
    {
        SDL_SetWindowMouseRect(screen.getWindow(), nullptr);
        SDL_SetRelativeMouseMode(SDL_FALSE);
        mIsMouseGrabbed = false;
    }
    else if (event->type == SDL_MOUSEMOTION)
    {
        if (mIsMouseGrabbed)
        {
            const int xPos = event->motion.x;
            const int yPos = event->motion.y;
            const int xRel = event->motion.xrel;
            const int yRel = event->motion.yrel;

            screen.translateViewport(xRel, yRel);
        }
    }
}

void Trackball::draw(const Screen& screen) const
{
    assert(mTexture != nullptr);

    const SDL_Rect dst = {(int)roundf(mX), (int)roundf(mY), (int)roundf(mWidth), (int)roundf(mHeight)};
    SDL_RenderCopy(screen.getRenderer(), mTexture, nullptr, &dst);
}
} // namespace trackeditor
