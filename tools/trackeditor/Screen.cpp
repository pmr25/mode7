#include "Screen.hpp"

#include <SDL2_gfxPrimitives.h>
#include <cmath>
#include <iostream>

namespace trackeditor
{
void Screen::create(uint32_t w, uint32_t h)
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);

    uint32_t flags;

    mWindowWidth = w;
    mWindowHeight = h;

    flags = 0;
    mWindow = SDL_CreateWindow("Track Editor", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, flags);
    if (!mWindow)
    {
        return;
    }

    flags = SDL_RENDERER_PRESENTVSYNC;
    mRenderer = SDL_CreateRenderer(mWindow, -1, flags);
    if (!mRenderer)
    {
        return;
    }

    setViewport(0, 0, mWindowWidth, mWindowHeight);
}

void Screen::destroy()
{
    if (mRenderer)
    {
        SDL_DestroyRenderer(mRenderer);
        mRenderer = nullptr;
    }

    if (mWindow)
    {
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
    }
}

void Screen::setViewport(float ox, float oy, float w, float h)
{
    mOriginX = ox;
    mOriginY = oy;
    mViewWidth = w;
    mViewHeight = h;
}

void Screen::translateViewport(float dx, float dy)
{
    mOriginX += dx;
    mOriginY += dy;
}

void Screen::zoom(float dir)
{
    mScale += mScale * dir;
    //    mOriginX += 100 * dir;
    //    mOriginY += 100 * dir;
}

void Screen::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
    mDrawColor = (a << 24) | (b << 16) | (g << 8) | (r << 0);
    SDL_SetRenderDrawColor(mRenderer, r, g, b, a);
}

void Screen::drawLine(int32_t x0, int32_t y0, int32_t x1, int32_t y1) const
{
    const SDL_Rect bbox = {X(x0), Y(y0), X(x1) - X(x0), Y(y1) - Y(y0)};
    if (!isVisible(&bbox))
    {
        return;
    }
    thickLineColor(mRenderer, X(x0), Y(y0), X(x1), Y(y1), 2, mDrawColor);
}

void Screen::drawQuad(int32_t x0, int32_t y0, int32_t x1, int32_t y1) const
{
    drawLine(x0, y0, x1, y0);
    drawLine(x1, y0, x1, y1);
    drawLine(x1, y1, x0, y1);
    drawLine(x0, y1, x0, y0);
}

void Screen::clear()
{
    setColor(0, 0, 0, 255);
    SDL_RenderClear(mRenderer);
}

void Screen::flip() const
{
    SDL_RenderPresent(mRenderer);
}

uint32_t Screen::getViewWidth() const
{
    return mWindowWidth / mScale;
}

uint32_t Screen::getViewHeight() const
{
    return mWindowHeight / mScale;
}

uint32_t Screen::getWidth() const
{
    return mWindowWidth;
}

uint32_t Screen::getHeight() const
{
    return mWindowHeight;
}

float Screen::getScale() const
{
    return mScale;
}

std::pair<float, float> Screen::getOrigin() const
{
    return std::pair<float, float>(mOriginX, mOriginY);
}

SDL_Renderer* Screen::getRenderer() const
{
    return mRenderer;
}

SDL_Window* Screen::getWindow() const
{
    return mWindow;
}

bool Screen::isVisible(const SDL_Rect* item) const
{
    SDL_Rect screen = {0, 0, getWidth(), getHeight()};

    return item->x < screen.x + screen.w && item->x + item->w > screen.x && item->y < screen.y + screen.h &&
           item->y + item->h > screen.y;
}

int32_t Screen::X(int32_t x) const
{
    return (int32_t)roundf((float)(x - mOriginX) * ((float)mViewWidth / (float)mWindowWidth) * mScale);
}

int32_t Screen::Y(int32_t y) const
{
    return (int32_t)roundf((float)(y - mOriginY) * ((float)mViewHeight / (float)mWindowHeight) * mScale);
}
} // namespace trackeditor
