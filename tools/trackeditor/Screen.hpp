#ifndef SCREEN_HPP
#define SCREEN_HPP

#include <SDL.h>
#include <cstdint>
#include <utility>

namespace trackeditor
{
class Screen
{
public:
    Screen()
        : mOriginX(0)
        , mOriginY(0)
        , mViewWidth(0)
        , mViewHeight(0)
        , mWindowWidth(0)
        , mWindowHeight(0)
        , mDrawColor(0xFFFFFFFF)
        , mScale(1.0f)
    {
    }

    ~Screen()
    {
        destroy();
    }

    void create(uint32_t, uint32_t);
    void destroy();
    void setViewport(float, float, float, float);
    void translateViewport(float, float);
    void zoom(float);
    void setColor(uint8_t, uint8_t, uint8_t, uint8_t);
    void drawLine(int32_t, int32_t, int32_t, int32_t) const;
    void drawQuad(int32_t, int32_t, int32_t, int32_t) const;
    void clear();
    void flip() const;
    uint32_t getViewWidth() const;
    uint32_t getViewHeight() const;
    uint32_t getWidth() const;
    uint32_t getHeight() const;
    float getScale() const;
    std::pair<float, float> getOrigin() const;
    SDL_Renderer* getRenderer() const;
    SDL_Window* getWindow() const;
    bool isVisible(const SDL_Rect*) const;

private:
    int32_t X(int32_t) const;
    int32_t Y(int32_t) const;

    float mOriginX;
    float mOriginY;
    float mViewWidth;
    float mViewHeight;
    float mWindowWidth;
    float mWindowHeight;
    float mDrawColor;
    float mScale;
    SDL_Window* mWindow;
    SDL_Renderer* mRenderer;
};
} // namespace trackeditor

#endif /* SCREEN_HPP */
