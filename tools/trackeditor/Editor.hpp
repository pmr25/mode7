#ifndef EDITOR_HPP
#define EDITOR_HPP

#include "Screen.hpp"
#include "TrackData.hpp"

#include <SDL.h>
#include <string>

namespace trackeditor
{
class Editor
{
public:
    Editor(const std::string& fn)
        : mSurface(nullptr)
        , mTexture(nullptr)
        , mReady(false)
    {
        open(fn);
    }
    ~Editor()
    {
    }

    void open(const std::string&);
    void save();

    void update();
    void draw(Screen&);

private:
    SDL_Surface* mSurface;
    SDL_Texture* mTexture;
    bool mReady;
    std::string mName;
    std::string mFileName;
    mode7::TrackData mData;
};
} // namespace trackeditor

#endif /* EDITOR_HPP */
