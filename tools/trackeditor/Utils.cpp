#include "Utils.hpp"
#include <filesystem>

#ifdef __linux__
#include <linux/limits.h>
#include <unistd.h>
#endif /* __linux__ */

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif /* _WIN32 */

namespace fs = std::filesystem;

namespace trackeditor
{
auto Utils::getInstallDir() -> const std::string
{
#ifdef __linux__
    ssize_t r;

    char buf[PATH_MAX];
    r = readlink("/proc/self/exe", buf, PATH_MAX - 1);
    if (r != -1)
    {
        buf[r] = '\0';
        return fs::path(std::string(buf)).parent_path().string();
    }

    return "";
#endif
#ifdef _WIN32
    TCHAR buf[MAX_PATH];
    if (!GetModuleFileName(NULL, buf, MAX_PATH))
    {
        return "";
    }

    return fs::path(std::string(buf)).parent_path().string();
#endif
}
} // namespace trackeditor
