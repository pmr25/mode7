#ifndef MODE7_TRACKBALL_HPP
#define MODE7_TRACKBALL_HPP

#include "Screen.hpp"
#include <SDL.h>

namespace trackeditor
{
class Trackball
{
public:
    Trackball()
        : mIsMouseGrabbed(false)
        , mIsWarping(false)
        , mTexture(nullptr)
        , mX(0)
        , mY(0)
        , mWidth(1)
        , mHeight(1)
    {
    }
    ~Trackball()
    {
        if (mTexture)
        {
            SDL_DestroyTexture(mTexture);
            mTexture = nullptr;
        }
    }

    void setPosition(float, float);
    void createTexture(const Screen&);
    void handleEvent(Screen&, const SDL_Event*);
    void draw(const Screen&) const;

private:
    bool mIsMouseGrabbed;
    bool mIsWarping;
    SDL_Texture* mTexture;
    float mX;
    float mY;
    float mWidth;
    float mHeight;
};
} // namespace trackeditor

#endif // MODE7_TRACKBALL_HPP
