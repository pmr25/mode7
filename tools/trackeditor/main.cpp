#include "Desk.hpp"
#include "Screen.hpp"
#include "Trackball.hpp"
#include "Utils.hpp"

#include <SDL.h>
#include <iostream>
#include <tinyfiledialogs.h>

#define ZOOM_FACTOR 0.01f

auto main(int argc, char** argv) -> int
{
    trackeditor::Desk desk;
    trackeditor::Screen screen;
    trackeditor::Trackball trackball;
    bool running = true;
    SDL_Event event;

    screen.create(1280, 720);

    trackball.createTexture(screen);
    trackball.setPosition(screen.getWidth() - 128, screen.getHeight() - 128);

    bool zoomOut = false;
    bool zoomIn = false;
    while (running)
    {
        while (SDL_PollEvent(&event))
        {
            trackball.handleEvent(screen, &event);
            if (event.type == SDL_QUIT)
            {
                running = false;
            }
            else if (event.type == SDL_KEYUP)
            {
                char const* lFilterPatterns[1]{"*.td2"};
                char const* fn;
                switch (event.key.keysym.sym)
                {
                case SDLK_o:
                    fn = tinyfd_openFileDialog("Open Track", trackeditor::Utils::getInstallDir().c_str(), 1, lFilterPatterns,
                                               "TrackData Version 2 File Format", 0);
                    if (fn == nullptr)
                    {
                        break;
                    }

                    std::cout << "opening " << fn << std::endl;
                    desk.openEditor(fn);
                    break;

                case SDLK_q: // zoom out
                    zoomOut = false;
                    break;

                case SDLK_e: // zoom in
                    zoomIn = false;
                    break;
                }
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_q: // zoom out
                    zoomOut = true;
                    break;

                case SDLK_e: // zoom in
                    zoomIn = true;
                    break;
                }
            }
        }

        if (zoomIn)
        {
            screen.zoom(ZOOM_FACTOR);
        }

        if (zoomOut)
        {
            screen.zoom(-ZOOM_FACTOR);
        }

        desk.update();

        screen.clear();

        desk.draw(screen);
        trackball.draw(screen);

        screen.flip();
    }

    return 0;
}
