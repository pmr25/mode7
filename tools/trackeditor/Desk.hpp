#ifndef DESK_HPP
#define DESK_HPP

#include "Editor.hpp"
#include "Screen.hpp"

#include <cstdint>
#include <string>
#include <vector>

namespace trackeditor
{
class Desk
{
public:
    Desk()
    {
    }
    ~Desk()
    {
    }

    void openEditor(const std::string&);
    void saveEditor(uint32_t);
    void closeEditor(uint32_t);
    Editor* getEditor(uint32_t);

    void update();
    void draw(Screen&);

private:
    void drawGrid(Screen&);

    std::vector<Editor> mEditors;
};
} // namespace trackeditor

#endif /* DESK_HPP */
