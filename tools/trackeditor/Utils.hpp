//
// Created by Patrick on 8/6/2022.
//

#ifndef MODE7_UTILS_HPP
#define MODE7_UTILS_HPP

#include <string>

namespace trackeditor
{

class Utils
{
public:
    static auto getInstallDir() -> const std::string;
};

} // namespace trackeditor

#endif // MODE7_UTILS_HPP
