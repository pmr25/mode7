#include "Desk.hpp"

#define GRID_DIM 64

namespace trackeditor
{
void Desk::openEditor(const std::string& fn)
{
    mEditors.push_back(Editor(fn));
}

void Desk::saveEditor(uint32_t index)
{
    mEditors[index].save();
}

void Desk::closeEditor(uint32_t index)
{
    mEditors.erase(mEditors.begin() + index);
}

Editor* Desk::getEditor(uint32_t index)
{
    return &mEditors[index];
}

void Desk::update()
{
    for (auto& editor : mEditors)
    {
        editor.update();
    }
}

void Desk::draw(Screen& screen)
{
    drawGrid(screen);

    for (auto& editor : mEditors)
    {
        editor.draw(screen);
    }
}

void Desk::drawGrid(Screen& screen)
{
    const uint32_t cols = screen.getViewWidth() / GRID_DIM + 1;
    const uint32_t rows = screen.getViewHeight() / GRID_DIM + 1;
    //        const std::pair<float, float> origin = screen.getOrigin();
    const std::pair<float, float> origin(0, 0);
    screen.setColor(20, 20, 20, 255);

    for (uint32_t i = 0; i < rows; ++i)
    {
        const uint32_t y = i * GRID_DIM;
        screen.drawLine(origin.first, y + origin.second, screen.getViewWidth() + origin.first, y + origin.second);
    }

    for (uint32_t i = 0; i < cols; ++i)
    {
        const uint32_t x = i * GRID_DIM;
        screen.drawLine(x + origin.first, origin.second, x + origin.first, screen.getViewHeight() + origin.second);
    }
}
} // namespace trackeditor
