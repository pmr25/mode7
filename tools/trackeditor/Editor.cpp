#include "Editor.hpp"

#include <cassert>
#include <filesystem>

namespace fs = std::filesystem;

namespace trackeditor
{
void Editor::open(const std::string& fn)
{
    mFileName = fn;
    mName = fs::path(fn).filename().string();
    mData.open(fn);
    mReady = true;
}

void Editor::save()
{
    mData.saveData(mFileName);
}

void Editor::update()
{
}

void Editor::draw(Screen& screen)
{
    if (!mReady)
    {
        return;
    }

    screen.setColor(255, 0, 255, 255);

    const float scale{10.0f};

    for (auto& e : mData.getTrackBounds())
    {
        assert(e.getNumSides() == 4);
        for (auto& side : e.getSides())
        {
            screen.drawLine(scale * side.getPoint().x, scale * side.getPoint().y, scale * side.getEndpoint().x,
                            scale * side.getEndpoint().y);
        }
    }
}
} // namespace trackeditor
