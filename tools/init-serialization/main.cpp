#include <cereal/archives/json.hpp>
#include <fstream>
#include <iostream>

#include "Properties.hpp"
#include "Settings.hpp"

int main()
{
    CarProperties car_p;
    GlobalCarProperties car_g;
    Settings settings;

    std::ofstream car_p_of("./car_properties.template.json");
    cereal::JSONOutputArchive car_p_oarchive(car_p_of);
    car_p_oarchive(car_p);

    std::ofstream car_g_of("./global_properties.template.json");
    cereal::JSONOutputArchive car_g_oarchive(car_g_of);
    car_g_oarchive(car_g);

    std::ofstream settings_of("./settings.template.json");
    cereal::JSONOutputArchive settings_oarchive(settings_of);
    settings_oarchive(settings);

    return 0;
}