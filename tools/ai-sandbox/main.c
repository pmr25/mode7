#include "car.h"
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
#include "screen.h"
#include "track.h"
#include "util.h"
#include <dpiaware.h>
#include <stdio.h>

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        printf("usage: ai-sandbox <script.lua> [-track=<track.track>] "
               "[-tdat=<track.tdat>]\n");
        return 1;
    }

    lua_State* L = luaL_newstate();
    luaL_openlibs(L);

    screen_create(1280, 720);
    car_load_icon();

    int err = 0;
    if (startswith(argv[2], "-track="))
    {
        printf("open track file\n");
        err = track_open(argv[2] + 7);
        if (err)
        {
            printf("failed to open with error code %d\n", err);
            exit(err);
        }
    }
    else if (startswith(argv[2], "-tdat="))
    {
        printf("open tdat file\n");
        err = track_open_tdat(argv[2] + 6);
        if (err)
        {
            printf("failed to open with error code %d\n", err);
            exit(err);
        }
    }
    else
    {
        printf("TODO help\n");
        return 1;
    }

    Car* car = car_new(L, argv[1]);
    track_place_car(car);

    cairo_t* cr;

    cr = screen_lock();
    cairo_set_source_rgb(cr, 0.2, 0.2, 0.2);
    cairo_rectangle(cr, 0, 0, 8192, 8192);
    cairo_fill(cr);
    track_draw(cr);
    screen_unlock(cr);

    while (screen_is_running())
    {
        // control
        car_update(car);

        // events
        screen_update();

        /** Cairo calls **/

        screen_draw_canvas();

        /** SDL calls **/
        car_draw(car);

        /** Show **/
        screen_flip();
    }

    lua_close(L);

    free(car);

    track_free();

    return 0;
}
