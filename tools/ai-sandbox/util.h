#ifndef UTIL_H
#define UTIL_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool startswith(const char* str, const char* prefix);
char* strip(char* str);
size_t charat(char* str, char ch);
#ifdef _WIN32
size_t getline(char**, size_t*, FILE*);
#endif

#endif /* UTIL_H */
