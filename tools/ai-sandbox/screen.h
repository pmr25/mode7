#ifndef SCREEN_H
#define SCREEN_H

#include "vec2.h"
#include <SDL.h>
#include <cairo.h>
#include <stdbool.h>

void screen_create(int, int);
void screen_flip(void);
void screen_update(void);
bool screen_is_running(void);
int screen_get_offset_x(void);
int screen_get_offset_y(void);
void screen_set_focus(vec2);
void screen_draw_canvas(void);
cairo_t* screen_lock(void);
void screen_unlock(cairo_t*);
SDL_Surface* screen_get_surface(void);
float screen_get_scale(void);

#endif /* SCREEN_H */