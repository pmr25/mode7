#ifndef CAR_H
#define CAR_H

#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>

#include "vec2.h"

typedef struct _Car
{
    vec2 position;
    vec2 velocity;
    vec2 front;
    vec2 right;
    float speed;
    int last_zone;

    lua_State* L;

    char name[100];
    char lua_func_name[100];
} Car;

void car_load_icon(void);
Car* car_new(lua_State*, const char*);
void car_update(Car*);
void car_draw(Car*);
void car_accel(Car*);
void car_brake(Car*);
void car_turn(Car*, int);

#endif /* CAR_H */