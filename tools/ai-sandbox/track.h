#ifndef TRACK_H
#define TRACK_H

#include "vec2.h"
#include <cairo.h>

#include "car_forward.h"
#include "line_forward.h"
#include "quad_forward.h"

int track_open(const char*);
int track_open_tdat(const char*);
void track_free(void);
void track_build(float*);
void track_draw(cairo_t*);
void track_place_car(Car*);
int track_get_cur_zone(vec2, int);
line* track_get_line(int);
quad* track_get_quad(int);
float track_get_width(void);
float* track_get_offset(void);

#endif /* TRACK_H */
