#include "line.h"
#include <stdio.h>

void line_connect(line* ln, vec2 a, vec2 b)
{
    COPY(ln->point, a);
    COPY(ln->end, b);
    vec2_sub(ln->dir, b, a);
    ln->length = vec2_mag(ln->dir);
    vec2_norm(ln->dir, ln->dir);
    line_calc_normal(ln);
    ln->is_segment = true;
}

void line_calc_normal(line* ln)
{
    X(ln->normal) = -Y(ln->dir);
    Y(ln->normal) = X(ln->dir);
}

void line_create(line* ln, vec2 point, vec2 dir)
{
    COPY(ln->point, point);
    COPY(ln->dir, dir);
    vec2_norm(ln->dir, ln->dir);
    line_calc_normal(ln);
    ln->is_segment = false;
    ln->length = 0.0f;
}

void line_extrapolate(vec2 dst, line* ln, float t)
{
    vec2 diff;
    vec2_scale(diff, ln->dir, t);
    vec2_add(dst, ln->point, diff);
}

void line_draw(line* ln, cairo_t* cr)
{
    vec2 off;
    ZERO(off);
    line_draw_offset(ln, off, cr);
}

void line_draw_offset(line* ln, vec2 off, cairo_t* cr)
{
    double len = 20.0;
    double pos = 0.5 * ln->length;
    // draw line
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_set_line_width(cr, 6.0);
    cairo_move_to(cr, X(off) + X(ln->point), Y(off) + Y(ln->point));
    cairo_line_to(cr, X(off) + X(ln->end), Y(off) + Y(ln->end));
    cairo_stroke(cr);

    // draw normal
    cairo_set_source_rgb(cr, 1.0, 0.0, 0.0);
    cairo_set_line_width(cr, 2.0);
    cairo_move_to(cr, X(off) + X(ln->point) + X(ln->dir) * pos, Y(off) + Y(ln->point) + Y(ln->dir) * pos);
    cairo_line_to(cr, X(off) + X(ln->point) + X(ln->dir) * pos + X(ln->normal) * len,
                  Y(off) + Y(ln->point) + Y(ln->dir) * pos + Y(ln->normal) * len);
    cairo_stroke(cr);

    // draw dir
    cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    cairo_set_line_width(cr, 2.0);
    cairo_move_to(cr, X(off) + X(ln->point) + X(ln->dir) * pos, Y(off) + Y(ln->point) + Y(ln->dir) * pos);
    cairo_line_to(cr, X(off) + X(ln->point) + X(ln->dir) * (pos + len), Y(off) + Y(ln->point) + Y(ln->dir) * (pos + len));
    cairo_stroke(cr);
}

float line_dist_to(line* ln, vec2 pt)
{
    vec2 diff;
    vec2_sub(diff, pt, ln->point);
    return vec2_dot(diff, ln->normal);
}

void line_print(line* ln)
{
    if (ln->is_segment)
    {
        printf("Line[start=(%f,%f), end=(%f,%f)]\n", X(ln->point), Y(ln->point), X(ln->end), Y(ln->end));
    }
    else
    {
        printf("Line[point=(%f,%f), dir=(%f,%f)]\n", X(ln->point), Y(ln->point), X(ln->dir), Y(ln->dir));
    }
}
