#ifndef VEC2_H
#define VEC2_H

#define ZERO(v) memset(v, 0, sizeof(vec2));
#define SET(v, x, y)                                                                                                               \
    v[0] = x;                                                                                                                      \
    v[1] = y
#define COPY(dst, src) memcpy(dst, src, sizeof(vec2))

#define X(v) (v[0])
#define Y(v) (v[1])

#include <cairo.h>
#include <string.h>

typedef float vec2[2];

void vec2_add(vec2, vec2, vec2);
void vec2_sub(vec2, vec2, vec2);
void vec2_scale(vec2, vec2, float);
float vec2_dot(vec2, vec2);
float vec2_mag(vec2);
int vec2_norm(vec2, vec2);
void vec2_rot(vec2, vec2, float);
void vec2_draw(vec2, cairo_t*);
void vec2_print(vec2);

#endif /* VEC2_H */
