#ifndef LINE_H
#define LINE_H

#include "vec2.h"
#include <cairo.h>
#include <stdbool.h>

typedef struct _line
{
    vec2 point;
    vec2 end;
    vec2 dir;
    vec2 normal;
    bool is_segment;
    float length;
} line;

void line_connect(line*, vec2, vec2);
void line_calc_normal(line*);
void line_create(line*, vec2, vec2);
void line_extrapolate(vec2, line*, float);
void line_draw(line*, cairo_t*);
void line_draw_offset(line*, vec2, cairo_t*);
float line_dist_to(line*, vec2);
void line_print(line*);

#endif /* LINE_H */
