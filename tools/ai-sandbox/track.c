#include "track.h"
#include "car.h"
#include "line.h"
#include "quad.h"
#include "util.h"
#include <SDL.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static line* segments = NULL;
static quad* rects = NULL;
static float width;
static size_t num_segments;
static bool line_strip;
static bool close_path;
static float scale = 20.0f;
static vec2 offset;
static vec2 offset2;

#ifdef _WIN32
#define ssize_t long long
#endif

int track_open(const char* filename)
{
    ZERO(offset);
    // file ops
    char* line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE* fp;

    // general parsing
    char* param1;
    char* param2;
    char conv_buf[100];

    // for parsing points
    float x;
    float y;
    size_t slen;

    // buffers
    float* point_buffer = NULL;
    float* bp = NULL;

    fp = fopen(filename, "r");
    if (!fp)
    {
        printf("error: cannot open file %s for reading\n", filename);
        return 1;
    }

    while ((read = getline(&line, &len, fp)) != -1)
    {
        line[strlen(line) - 1] = 0; // remove newline

        if (startswith(line, "v"))
        {
            param1 = strip(line + 1);
            param2 = strip(param1 + charat(param1, ' '));
            if (startswith(param1, "width"))
            {
                width = atof(param2) * scale;
                // printf("width=%f\n", width);
            }
            else if (startswith(param1, "numseg"))
            {
                num_segments = atof(param2);
                point_buffer = (float*)malloc(num_segments * 2 * sizeof(float));
                if (!point_buffer)
                {
                    // TODO fail
                }
                bp = point_buffer;
            }
            else if (startswith(param1, "strip"))
            {
                line_strip = atoi(param2) == 1;
            }
            else if (startswith(param1, "close"))
            {
                close_path = atoi(param2) == 1;
            }
        }
        else if (startswith(line, "o"))
        {
            param1 = strip(line + 1);
            slen = charat(param1, ',');
            strncpy(conv_buf, param1, slen);
            conv_buf[slen] = 0;
            x = atof(conv_buf);

            param2 = strip(param1 + slen + 1);
            slen = strlen(param2);
            strncpy(conv_buf, param2, slen);
            conv_buf[slen] = 0;
            y = atof(conv_buf);

            SET(offset, x * scale, y * scale);
        }
        else if (startswith(line, "p"))
        {
            assert(bp);

            param1 = strip(line + 1);
            slen = charat(param1, ',');
            strncpy(conv_buf, param1, slen);
            conv_buf[slen] = 0;
            x = atof(conv_buf);

            param2 = strip(param1 + slen + 1);
            slen = strlen(param2);
            strncpy(conv_buf, param2, slen);
            conv_buf[slen] = 0;
            y = atof(conv_buf);

            assert(bp - point_buffer < num_segments * 2);
            *(bp + 0) = x * scale + X(offset);
            *(bp + 1) = y * scale + Y(offset);
            bp += 2;
        }
    }

    fclose(fp);
    if (line)
    {
        free(line);
    }

    track_build(point_buffer);
    free(point_buffer);

    return 0;
}

int track_open_tdat(const char* filename)
{
    ZERO(offset2);
    scale = 10.0;
    width = 80.0;

    float* point_buffer = NULL;
    FILE* fp = NULL;
    char buffer[1024];
    float qbuf[9];

    fp = fopen(filename, "rb");
    if (!fp)
    {
        return 1;
    }

    fread(buffer, sizeof(char), 4, fp);
    if (strncmp(buffer, "TDAT", 4) != 0)
    {
        fclose(fp);
        return 2;
    }

    uint32_t num_lines;
    uint32_t num_walls;
    uint32_t start;
    uint32_t wall_start;
    uint32_t stride;

    fread(&num_lines, sizeof(uint32_t), 1, fp);
    fread(&num_walls, sizeof(uint32_t), 1, fp);
    fread(&start, sizeof(uint32_t), 1, fp);
    fread(&wall_start, sizeof(uint32_t), 1, fp);
    fread(&stride, sizeof(uint32_t), 1, fp);

    point_buffer = (float*)malloc(num_lines * 4 * sizeof(float));
    if (!point_buffer)
    {
        fclose(fp);
        return 3;
    }

    uint32_t i;
    for (i = 0; i < num_lines; ++i)
    {
        // we only care about reading centerlines
        fread(qbuf, sizeof(float), 4, fp);
        fseek(fp, 24 * sizeof(float), SEEK_CUR);

        for (int j = 0; j < 4; ++j)
        {
            qbuf[j] *= scale;
        }

        printf("found points %f,%f,%f,%f\n", qbuf[0], qbuf[1], qbuf[2], qbuf[3]);

        assert(i * 4 < num_lines * 4);
        memcpy(point_buffer + (i * 4), qbuf, 4 * sizeof(float));

        assert((point_buffer + (i * 4))[0] == qbuf[0]);
        assert((point_buffer + (i * 4))[1] == qbuf[1]);
        assert((point_buffer + (i * 4))[2] == qbuf[2]);
        assert((point_buffer + (i * 4))[3] == qbuf[3]);
    }

    fclose(fp);

    num_segments = num_lines;
    track_build(point_buffer);
    free(point_buffer);

    return 0;
}

void track_build(float* points)
{
    assert(num_segments > 0);
    assert(segments == NULL);
    assert(sizeof(line) > 0);

    segments = (line*)malloc(num_segments * sizeof(line));
    if (!segments)
    {
        printf("cannot alloc memory!\n");
        exit(1);
    }

    line* cur_segment = segments;
    line* next_segment = NULL;

    vec2 point;
    vec2 last;
    if (line_strip)
    {
        vec2 min;
        vec2 max;

        X(last) = points[0];
        Y(last) = points[1];

        X(min) = X(last);
        Y(min) = Y(last);
        X(max) = X(last);
        Y(max) = Y(last);

        for (int i = 1; i < num_segments; ++i)
        {
            X(point) = points[i * 2 + 0];
            Y(point) = points[i * 2 + 1];

            line_connect(cur_segment, last, point);
            line_print(cur_segment);
            ++cur_segment;

            X(last) = X(point);
            Y(last) = Y(point);

            // update mins, maxes
            X(min) = fminf(X(min), X(last));
            Y(min) = fminf(Y(min), Y(last));
            X(max) = fmaxf(X(max), X(last));
            Y(max) = fmaxf(X(max), Y(last));
        }

        // set offsets
        // X(offset2) = X(min);
        // Y(offset2) = Y(min);

        if (close_path)
        {
            X(point) = points[0];
            Y(point) = points[1];

            line_connect(cur_segment, last, point);
            ++cur_segment;
        }
    }
    else
    {
        for (int i = 0; i < num_segments; ++i)
        {
            X(last) = points[i * 4 + 0];
            Y(last) = points[i * 4 + 1];
            X(point) = points[i * 4 + 2];
            Y(point) = points[i * 4 + 3];

            line_connect(cur_segment, last, point);
            line_print(cur_segment);
            ++cur_segment;
        }
    }

    line near;
    line far;
    vec2 a;
    vec2 b;
    vec2 c;
    vec2 d;

    assert(num_segments > 0);
    rects = (quad*)malloc(num_segments * sizeof(quad));
    if (!rects)
    {
        printf("cannot alloc memory!\n");
        free(segments);
        segments = NULL;
        exit(1);
    }

    for (int i = 0; i < num_segments; ++i)
    {
        cur_segment = &segments[i];
        next_segment = &segments[(i + 1) % num_segments];

        line_create(&near, cur_segment->point, cur_segment->normal);
        // line_create(&far, cur_segment->end, cur_segment->normal);
        line_create(&far, next_segment->point, next_segment->normal);

        line_extrapolate(a, &near, -width);
        line_extrapolate(b, &far, -width);
        line_extrapolate(c, &far, width);
        line_extrapolate(d, &near, width);

        quad_create_from_pts(rects + i, a, b, c, d);
        quad_print(rects + i);
    }
}

void track_draw(cairo_t* cr)
{
    for (int i = 0; i < num_segments; ++i)
    {
        quad_draw_offset(rects + i, offset2, cr);
        // line_draw_offset(segments + i, offset2, cr);
    }
}

void track_place_car(Car* c)
{
    int start_segment = 0;

    assert(segments);
    X(c->position) = X(segments[start_segment].point);
    Y(c->position) = Y(segments[start_segment].point);
}

int track_get_cur_zone(vec2 point, int nearby)
{
    vec2 adj;
    // vec2_sub(adj, point, offset2);
    COPY(adj, point);

    int zone = -1;

    if (nearby < 0)
    {
        // brute force
        for (int i = 0; i < num_segments; ++i)
        {
            // printf("zone %d\n", i);
            // quad_print(rects + i);
            // vec2_print(point);
            if (quad_intersects(rects + i, adj))
            {
                // printf("intersect %d\n", i);
                zone = i;
                break;
            }
        }
    }
    else
    {
        int spread = 5;
        int end = nearby + spread;
        int beg = nearby - spread;

        if (end >= num_segments)
        {
            end = end % num_segments;
        }

        if (beg < 0)
        {
            beg = num_segments - beg - 1;
        }

        for (int i = beg; i != end; ++i)
        {
            if (i >= num_segments)
            {
                i = 0;
            }

            if (quad_intersects(rects + i, adj))
            {
                // printf("intersect %d\n", i);
                zone = i;
                break;
            }
        }
    }

    return zone;
}

line* track_get_line(int index)
{
    // assert(index < num_segments);
    return &segments[index];
}

quad* track_get_quad(int index)
{
    // assert(index < num_segments);
    return &rects[index];
}

float track_get_width(void)
{
    return width;
}

void track_free(void)
{
    if (segments)
    {
        free(segments);
        segments = NULL;
    }

    if (rects)
    {
        free(rects);
        rects = NULL;
    }
}

float* track_get_offset(void)
{
    return offset2;
}
