#include "car.h"
#include "line.h"
#include "quad.h"
#include "screen.h"
#include "track.h"
#include <SDL.h>
#include <SDL_image.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CAR_MAX_SPEED 20.0f
#define ACCEL_RATE 0.1f
#define BRAKE_RATE -0.1f

static SDL_Surface* car_surf;

void car_load_icon(void)
{
    car_surf = IMG_Load("../assets/scripts/car.png");
    if (!car_surf)
    {
        printf("cannot load car.png\n");
    }
    else
    {
        return;
    }

    car_surf = IMG_Load("../../assets/scripts/car.png");
    if (!car_surf)
    {
        printf("cannot load car.png\n");
        exit(1);
    }
}

Car* car_new(lua_State* L, const char* filename)
{
    Car* car = (Car*)malloc(sizeof(Car));
    if (!car)
    {
        return car;
    }

    car->last_zone = -1;
    car->L = L;

    ZERO(car->position);
    ZERO(car->velocity);
    SET(car->front, 1, 0);
    SET(car->right, 0, -1);
    car->speed = 0;

    luaL_dofile(L, filename);

    return car;
}

void car_update(Car* c)
{
    // get current paramters
    int cur_zone = track_get_cur_zone(c->position, c->last_zone);
    c->last_zone = cur_zone; // uncomment to boost efficiency

    float dir = -1.0;
    line* cur_line = track_get_line(cur_zone);
    // quad* cur_quad = track_get_quad(cur_zone);
    float dist = dir * line_dist_to(cur_line, c->position);
    float dot = dir * vec2_dot(c->front, cur_line->dir);
    float dot_right = dir * vec2_dot(c->right, cur_line->dir);

    /** Run script **/
    // function name
    lua_getglobal(c->L, "Command");

    // function arguments
    lua_pushnumber(c->L, X(c->position));                       // position x
    lua_pushnumber(c->L, Y(c->position));                       // position y
    lua_pushnumber(c->L, X(c->velocity));                       // velocity x
    lua_pushnumber(c->L, Y(c->velocity));                       // velocity y
    lua_pushnumber(c->L, X(cur_line->normal));                  // centerline normal x
    lua_pushnumber(c->L, Y(cur_line->normal));                  // centerline normal y
    lua_pushnumber(c->L, fabs(dist));                           // dist to centerline
    lua_pushnumber(c->L, copysign(1.0, dist));                  // side of centerline
    lua_pushnumber(c->L, 0.5 * track_get_width() - fabs(dist)); // dist to wall
    lua_pushnumber(c->L, dot);                                  // dot
    lua_pushnumber(c->L, dot_right);                            // dot right

    lua_call(c->L, 11, 3);

    int should_accel;
    int should_brake;
    int turn_dir;

    should_accel = (int)lua_tointeger(c->L, -3);
    should_brake = (int)lua_tointeger(c->L, -2);
    turn_dir = (int)lua_tointeger(c->L, -1);

    lua_pop(c->L, 3);

    /** Command car **/
    if (should_accel)
    {
        car_accel(c);
    }
    else
    {
        c->speed -= 0.1f;
        if (c->speed < 0)
        {
            c->speed = 0.0f;
        }
    }

    if (should_brake)
    {
        car_brake(c);
    }

    car_turn(c, turn_dir);

    COPY(c->velocity, c->front);
    vec2_scale(c->velocity, c->velocity, c->speed);
    vec2_add(c->position, c->position, c->velocity);

    screen_set_focus(c->position);
}

void car_draw(Car* c)
{
    assert(car_surf);

    SDL_Rect dst;
    dst.x = X(c->position) * screen_get_scale() + screen_get_offset_x();
    dst.y = Y(c->position) * screen_get_scale() + screen_get_offset_y();
    dst.w = 32 * screen_get_scale();
    dst.h = 32 * screen_get_scale();

    SDL_BlitScaled(car_surf, NULL, screen_get_surface(), &dst);
}

void car_accel(Car* c)
{
    c->speed += ACCEL_RATE;
    if (c->speed > CAR_MAX_SPEED)
    {
        c->speed = CAR_MAX_SPEED;
    }
}

void car_brake(Car* c)
{
    c->speed -= BRAKE_RATE;
    if (c->speed < 0.0f)
    {
        c->speed = 0.0f;
    }
}

void car_turn(Car* c, int dir)
{
    float rate;
    float dtheta;

    if (dir == 0)
    {
        return;
    }

    rate = 0.1f;
    dtheta = copysignf(rate, (float)dir);

    vec2_rot(c->front, c->front, dtheta);
    vec2_rot(c->right, c->right, dtheta);

    vec2_norm(c->front, c->front);
    vec2_norm(c->right, c->right);
}
