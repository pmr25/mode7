#include "quad.h"
#include <stdio.h>

void quad_create_from_pts(quad* q, vec2 a, vec2 b, vec2 c, vec2 d)
{
    // assume a,b,c,d go ccw
    line_connect(&q->lines[0], a, b);
    line_connect(&q->lines[1], b, c);
    line_connect(&q->lines[2], c, d);
    line_connect(&q->lines[3], d, a);

    for (int i = 0; i < 4; ++i)
    {
        q->points[i] = q->lines[i].point;
    }
}

bool quad_intersects(quad* q, vec2 p)
{
    float dist;
    bool result = true;

    for (int i = 0; i < 4; ++i)
    {
        dist = line_dist_to(q->lines + i, p);
        // printf("\tdist=%f\n", dist);
        if (dist < 0)
        {
            result = false;
            break;
        }
    }

    return result;
}

void quad_draw(quad* q, cairo_t* cr)
{
    vec2 off;
    ZERO(off);
    quad_draw_offset(q, off, cr);
}

void quad_draw_offset(quad* q, vec2 off, cairo_t* cr)
{
    // cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
    // cairo_set_line_width(cr, 4.0);

    // cairo_move_to(cr, X(off) + X((q->points[0])), Y(off) +
    // Y((q->points[0])));

    // for (int i = 1; i < 4; ++i)
    // {
    //     cairo_line_to(cr, X(off) + X((q->points[i])), Y(off) +
    //     Y((q->points[i])));
    // }

    // cairo_close_path(cr);
    // cairo_stroke(cr);

    for (int i = 0; i < 4; ++i)
    {
        line_draw_offset(&q->lines[i], off, cr);
    }
}

void quad_print(quad* q)
{
    printf("Quad[\n");
    for (int i = 0; i < 4; ++i)
    {
        printf("\t");
        vec2_print(q->points[i]);
    }
    printf("\n");
}
