#include "vec2.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

void vec2_add(vec2 dst, vec2 a, vec2 b)
{
    vec2 cpy;
    X(cpy) = X(a) + X(b);
    Y(cpy) = Y(a) + Y(b);
    COPY(dst, cpy);
}

void vec2_sub(vec2 dst, vec2 a, vec2 b)
{
    vec2 cpy;
    X(cpy) = X(a) - X(b);
    Y(cpy) = Y(a) - Y(b);
    COPY(dst, cpy);
}

void vec2_scale(vec2 dst, vec2 src, float c)
{
    vec2 cpy;
    X(cpy) = X(src) * c;
    Y(cpy) = Y(src) * c;
    COPY(dst, cpy);
}

float vec2_dot(vec2 a, vec2 b)
{
    return X(a) * X(b) + Y(a) * Y(b);
}

float vec2_mag(vec2 v)
{
    return sqrtf(X(v) * X(v) + Y(v) * Y(v));
}

int vec2_norm(vec2 dst, vec2 src)
{
    float mag = vec2_mag(src);
    if (mag == 0.0f)
    {
        return 1;
    }

    X(dst) = X(src) / mag;
    Y(dst) = Y(src) / mag;

    return 0;
}

void vec2_rot(vec2 dst, vec2 src, float theta)
{
    vec2 cpy;
    float cs;
    float sn;

    cs = cosf(theta);
    sn = sinf(theta);
    memcpy(cpy, src, sizeof(vec2));

    X(dst) = X(cpy) * cs - Y(cpy) * sn;
    Y(dst) = X(cpy) * sn + Y(cpy) * cs;
}

void vec2_draw(vec2 v, cairo_t* cr)
{
}

void vec2_print(vec2 v)
{
    printf("Vec2[%f,%f]\n", X(v), Y(v));
}
