#ifndef QUAD_H
#define QUAD_H

#include "line.h"
#include "vec2.h"
#include <cairo.h>
#include <stdbool.h>

typedef struct _quad
{
    line lines[4];
    float* points[4];
} quad;

void quad_create_from_pts(quad*, vec2, vec2, vec2, vec2);
bool quad_intersects(quad*, vec2);
void quad_draw(quad*, cairo_t*);
void quad_draw_offset(quad*, vec2, cairo_t*);
void quad_print(quad*);

#endif /* QUAD_H */
