#include "screen.h"
#include <SDL_image.h>
#include <dpiaware.h>
#include <stdint.h>

#define CANVAS_WIDTH 8192
#define CANVAS_HEIGHT 8192
#define RMASK 0x00FF0000
#define GMASK 0x0000FF00
#define BMASK 0x000000FF
#define AMASK 0x0

#define MAX_DELAY 20

static int offset_x = 0;
static int offset_y = 0;
static int width;
static int height;
static SDL_Window* window;
static SDL_Surface* surface;
static SDL_Surface* canvas;
static cairo_surface_t* cairo_surface;
static SDL_Event event;
static bool running;
static float scale = 1.0f;
static int32_t delay_amt = 10;

void screen_create(int w, int h)
{
    dpi_gather_metrics();
    dpi_set_proc_aware();

    uint32_t flags;
    running = true;
    width = w;
    height = h;
    scale = dpi_get_monitor_scale();

    SDL_Init(SDL_INIT_VIDEO);

    flags = 0;
    window = SDL_CreateWindow("AI Sandbox", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width * scale, height * scale, flags);

    surface = SDL_GetWindowSurface(window);

    flags = 0;
    canvas = SDL_CreateRGBSurface(flags, CANVAS_WIDTH, CANVAS_HEIGHT, 32, RMASK, GMASK, BMASK, AMASK);

    IMG_Init(IMG_INIT_PNG);
}

void screen_update(void)
{
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            running = false;
        }

        if (event.type == SDL_KEYDOWN)
        {
            if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
            {
                running = false;
            }

            if (event.key.keysym.scancode == SDL_SCANCODE_LEFTBRACKET)
            {
                --delay_amt;
                if (delay_amt < 0)
                {
                    delay_amt = 0;
                }
            }
            else if (event.key.keysym.scancode == SDL_SCANCODE_RIGHTBRACKET)
            {
                ++delay_amt;
                if (delay_amt > MAX_DELAY)
                {
                    delay_amt = MAX_DELAY;
                }
            }
        }
    }
}

bool screen_is_running(void)
{
    return running;
}

void screen_draw_canvas(void)
{
    int err;
    SDL_Rect src;
    SDL_Rect dst;

    dst.x = offset_x * scale;
    dst.y = offset_y * scale;
    dst.w = CANVAS_HEIGHT * scale;
    dst.h = CANVAS_WIDTH * scale;

    src.x = 0;
    src.y = 0;
    src.w = CANVAS_WIDTH;
    src.h = CANVAS_HEIGHT;
    err = SDL_BlitScaled(canvas, &src, surface, &dst);
    if (err)
    {
        printf("%s\n", SDL_GetError());
    }
}

void screen_flip(void)
{
    int err;

    err = SDL_UpdateWindowSurface(window);
    if (err)
    {
        printf("%s\n", SDL_GetError());
    }

    SDL_Delay(delay_amt);
}

int screen_get_offset_x(void)
{
    return offset_x * scale;
}

int screen_get_offset_y(void)
{
    return offset_y * scale;
}

void screen_set_focus(vec2 ctr)
{
    offset_x = -X(ctr) + (width / 2);
    offset_y = -Y(ctr) + (height / 2);
}

cairo_t* screen_lock(void)
{
    SDL_LockSurface(canvas);

    cairo_surface = cairo_image_surface_create_for_data(canvas->pixels, CAIRO_FORMAT_RGB24, canvas->w, canvas->h, canvas->pitch);

    return cairo_create(cairo_surface);
}

void screen_unlock(cairo_t* cr)
{
    cairo_destroy(cr);
    cairo_surface_destroy(cairo_surface);

    SDL_UnlockSurface(canvas);
}

SDL_Surface* screen_get_surface(void)
{
    return surface;
}

float screen_get_scale(void)
{
    return scale;
}
