#include <cmath>
#include <matplot/matplot.h>
#include <vector>

#include "DampedScalar.hpp"
#include "RestoringScalar.hpp"

using namespace matplot;
using namespace mode7;

#define TIMESTEP (0.1F)

auto dampedScalarPlot() -> void
{
    DampedScalar value(0.0F, 0.99F, TIMESTEP);
    const size_t start = 0;
    const size_t end = 100;
    const size_t numSteps = size_t(float(end - start) / TIMESTEP);
    std::vector<double> x = linspace(start, end, numSteps);
    std::vector<double> y;

    bool osc = true;
    value.setTarget(float(osc));
    for (size_t i{0}; i < numSteps; ++i)
    {
        if (i % 100 == 0)
        {
            osc = !osc;
            //value.setTarget(float(osc));
        }
        y.push_back(value.getPosition());
        value.update();
    }

    plot(x, y)->color({0.f, 0.7f, 0.9f});
    title("2-D Line Plot");
    xlabel("time");
    ylabel("position");

    show();
}

auto restoringScalarPlot() -> void
{
    RestoringScalar value(1.0F, 10.0F, TIMESTEP);
    const size_t start = 0;
    const size_t end = 100;
    const auto numSteps = size_t(float(end - start) / TIMESTEP);

    std::vector<double> x = linspace(start, end, numSteps);
    std::vector<double> y;

    for (size_t i{0}; i < numSteps; ++i)
    {
        value.applyForce(1.0F);
        y.push_back(value.getPosition());
        value.update();
    }

    plot(x, y)->color({0.f, 0.7f, 0.9f});
    title("2-D Line Plot");
    xlabel("time");
    ylabel("position");

    show();
}

auto main(int argc, char** argv) -> int
{
//    dampedScalarPlot();
    restoringScalarPlot();

    return 0;
}
