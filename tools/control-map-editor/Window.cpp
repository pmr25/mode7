#include "Window.hpp"

#include <iostream>

Window::Window(uint32_t w, uint32_t h)
    : m_width(w)
    , m_height(h)
    , m_window(nullptr)
{
    create(w, h);
}

Window::~Window()
{
    SDL_DestroyWindow(m_window);
}

void Window::create(uint32_t w, uint32_t h)
{
    m_width = w;
    m_height = h;

    m_projection = glm::ortho((float)w / -2.0f, (float)w / 2.0f, (float)h / -2.0f, (float)h / 2.0f, -10000.0f, 10000.0f);
    // m_projection = glm::mat4(1.f);
    // m_projection = glm::perspective(70.f, 16.f / 9.f, 0.1f, 1000.f);
    m_view = /*glm::inverse*/ (glm::lookAt(glm::vec3(10, 8, 10), glm::vec3(0, 2, 0), glm::vec3(0, 1, 0)));

    int err;

    err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    if (err < 0)
    {
        std::cout << SDL_GetError() << std::endl;
        return;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    // SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    uint32_t flags = 0;
    m_window = SDL_CreateWindow("Control Map Editor", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height,
                                SDL_WINDOW_OPENGL | flags);
    m_ctx = SDL_GL_CreateContext(m_window);

    err = SDL_GL_SetSwapInterval(1);
    if (err < 0)
    {
        SLOG(LG_ERROR) << "SDL error: " << SDL_GetError();
        exit(1);
    }

    glewExperimental = GL_TRUE;
    const GLenum glerr = glewInit();
    if (glerr != GLEW_OK)
    {
        SLOG(LG_ERROR) << "glew error: " << glewGetErrorString(glerr);
        exit(1);
    }

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDepthFunc(GL_LESS);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glViewport(0, 0, m_width, m_height);
}
