#ifndef MESH_HPP
#define MESH_HPP

#include "CSV.hpp"
#include "glincludes.hpp"
#include <memory>
#include <string>
#include <vector>

class Mesh
{
public:
    Mesh(std::shared_ptr<CSV>);

    void create();
    void createTriangle();
    void orbit(float, float);
    void update();
    void draw();

    inline GLuint getShader()
    {
        return m_program;
    }

private:
    void genMeshDataFromCSV();
    void loadShader(const std::string&, const std::string&);

    GLuint m_program;
    GLuint m_modelLoc;
    GLuint m_vao;
    GLuint m_vbo;
    glm::mat4 m_matrix;
    glm::mat4 m_inherent;
    glm::mat4 m_shaderMatrix;
    std::shared_ptr<CSV> m_csv;
    std::vector<glm::vec3> m_vertices;
    std::vector<uint32_t> m_indices;
};

#endif /* MESH_HPP */