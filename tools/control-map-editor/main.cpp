#include <iostream>
#include <memory>

#include "CSV.hpp"
#include "Mesh.hpp"
#include "Mouse.hpp"
#include "Window.hpp"

std::shared_ptr<Window> window;
std::shared_ptr<Mesh> mesh;
std::shared_ptr<CSV> csv;

bool running;
SDL_Event e;

Mouse mouse;

static void printUsage()
{
    std::cout << "usage: control-map-editor <.csv file>" << std::endl;
}

void update()
{
    SDL_PumpEvents();
    while (SDL_PollEvent(&e))
    {
        if (e.type == SDL_QUIT)
        {
            running = false;
            break;
        }
        else if (e.type == SDL_KEYDOWN)
        {
            if (e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
            {
                running = false;
                break;
            }
        }

        mouse.processEvents(window.get(), &e);
    }

    mouse.update(window.get());

    float sensitivity = 0.01;
    float pitch = (float)mouse.getLeftDrag().second * sensitivity;
    float yaw = (float)mouse.getLeftDrag().first * sensitivity;

    mesh->orbit(pitch, yaw);
    mesh->update();
}

void draw()
{
    glUseProgram(mesh->getShader());
    window->sendMatrices();
    mesh->draw();
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        printUsage();
        return 0;
    }

    int err;
    csv = std::make_shared<CSV>();

    err = csv->open(argv[1]);
    if (err == 1)
    {
        std::cout << "cannot open " << argv[1] << std::endl;
        return 1;
    }
    else if (err == 2)
    {
        std::cout << "bad format" << std::endl;
        return 1;
    }
    csv->trimFirstRowCol();

    window = std::make_shared<Window>(1280, 720);
    mesh = std::make_shared<Mesh>(csv);
    mesh->create();

    window->setShader(mesh->getShader());

    running = true;
    while (running)
    {
        update();
        window->clear();
        draw();
        window->flip();
    }

    return 0;
}