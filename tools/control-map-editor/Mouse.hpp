#ifndef MOUSE_HPP
#define MOUSE_HPP

#include "Window.hpp"

#include <SDL.h>
#include <cstdint>

class Mouse
{
public:
    Mouse();

    void update(Window*);
    void processEvents(Window*, SDL_Event*);

    inline std::pair<int32_t, int32_t> getLeftDrag()
    {
        return std::pair<int32_t, int32_t>(m_leftDrag[0], m_leftDrag[1]);
    }

private:
    bool m_buttonState[3];
    int32_t m_leftDragStart[2];
    int32_t m_leftDrag[2];
};

#endif /* MOUSE_HPP */