#version 330 core

layout (location = 0) in vec3 position;

out vec3 vcolor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    float scale = 5.0;
    float color_scale = 0.4;
    gl_Position = projection * view * model * (vec4(position.x, position.y, position.z * scale, 1));

    vec3 np = normalize(position);
    vcolor = vec3(0.1);
    vcolor += color_scale * vec3(0.7, 1, 0.7) * np.x;
    vcolor += color_scale * vec3(0.7, 0.7, 1) * np.y;
    vcolor += color_scale * vec3(1, 0.7, 1) * -position.z;

    vcolor = vec3(1, 0, 0) * (-position.z);
    vcolor += vec3(0.2);
}