#include "CSV.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

CSV::CSV()
    : m_delim(',')
    , m_rows(0)
    , m_cols(0)
{
}

int CSV::open(const std::string& filename)
{
    return open(filename, ',');
}

int CSV::open(const std::string& filename, char delim)
{
    std::ifstream in(filename);
    if (!in)
    {
        return 1;
    }

    std::string line;
    std::string buffer;
    uint32_t ctr;
    float ent;
    while (std::getline(in, line))
    {
        buffer = "";
        ctr = 0;
        for (auto& c : line)
        {
            if (c == delim)
            {
                ++ctr;
                ent = 0.f;
                if (buffer.length() > 0)
                {
                    ent = atof(buffer.c_str());
                    buffer = "";
                }
                m_cells.push_back(ent);
            }
            else
            {
                buffer += c;
            }
        }
        /* add last item */
        ++ctr;
        ent = 0.f;
        if (buffer.length() > 0)
        {
            ent = atof(buffer.c_str());
            buffer = "";
        }
        m_cells.push_back(ent);
        if (m_cols == 0)
        {
            // init number of cols
            m_cols = ctr;
        }
        else
        {
            // make sure number of cols is consistent
            if (ctr != m_cols)
            {
                return 2;
            }
        }
        ++m_rows;
    }

    return 0;
}

void CSV::trimFirstRowCol()
{
    uint32_t r;
    uint32_t c;
    std::vector<float> trimmed;
    for (r = 0; r < m_rows; ++r)
    {
        for (c = 0; c < m_cols; ++c)
        {
            if (r != 0 && c != 0)
            {
                trimmed.push_back(m_cells[toIndex(r, c)]);
            }
        }
    }

    --m_rows;
    --m_cols;
    m_cells = trimmed;
}

float CSV::get(uint32_t row, uint32_t col)
{
    float cell = 0.f;
    uint32_t index = toIndex(row, col);
    if (index < m_cells.size())
    {
        cell = m_cells[index];
    }

    return cell;
}

int CSV::put(float val, uint32_t row, uint32_t col)
{
    uint32_t index = toIndex(row, col);
    if (index < m_cells.size())
    {
        return 1;
    }

    m_cells[index] = val;

    return 0;
}

int CSV::save(const std::string& filename)
{
    return 0;
}

uint32_t CSV::toIndex(uint32_t r, uint32_t c)
{
    return r * m_cols + c;
}

void CSV::summarize()
{
    std::cout << "rows: " << m_rows << "\tcols: " << m_cols << "\n";
    uint32_t r;
    uint32_t c;
    for (r = 0; r < m_rows; ++r)
    {
        for (c = 0; c < m_cols; ++c)
        {
            std::cout << m_cells[toIndex(r, c)];
            if (c < m_cols - 1)
            {
                std::cout << ", ";
            }
        }
        std::cout << "\n";
    }
    std::cout << std::endl;
}