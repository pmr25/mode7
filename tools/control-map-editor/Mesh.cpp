#include "Mesh.hpp"
#include "glincludes.hpp"

#include <fstream>
#include <iostream>
#include <sstream>

static std::string openFile(const std::string& filename)
{
    std::ifstream fp(filename);
    if (!fp)
    {
        return "";
    }

    std::stringstream code;
    code << fp.rdbuf();
    fp.close();

    return code.str();
}

static int compile(GLuint id, const std::string& code)
{
    GLint res = 0;
    int loglen;

    const char* ptr = code.c_str();
    glShaderSource(id, 1, &ptr, nullptr);
    glCompileShader(id);

    glGetShaderiv(id, GL_COMPILE_STATUS, &res);
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 0)
    {
        std::vector<char> errMsg;
        errMsg.reserve(loglen + 1);
        glGetShaderInfoLog(id, loglen, NULL, &errMsg[0]);
        printf("%s\n", &errMsg[0]);
    }

    return res;
}

Mesh::Mesh(std::shared_ptr<CSV> csv)
    : m_program(0)
    , m_modelLoc(0)
    , m_vao(0)
    , m_vbo(0)
    , m_matrix(1.f)
    , m_inherent(1.f)
    , m_shaderMatrix(1.f)
    , m_csv(csv)
{
}

void Mesh::genMeshDataFromCSV()
{
    uint32_t r;
    uint32_t c;
    glm::vec3 pt;

    glm::vec3 vmax;
    glm::vec3 vmin;

    for (r = 0; r < m_csv->getNumRows(); ++r)
    {
        for (c = 0; c < m_csv->getNumCols(); ++c)
        {
            // center the entire mesh
            // pt.x = c - ((float)(m_csv->getNumCols() + 0) / 2.f);
            // pt.y = r - ((float)(m_csv->getNumRows() + 0) / 2.f);
            // pt.x /= 10.0f;
            // pt.y /= 10.0f;

            pt.x = c;
            pt.y = r;
            // leave height unscaled for now
            pt.z = -m_csv->get(r, c);
            // pt.z = (float)rand() / (float)(RAND_MAX / (0.1f - -0.1f)) +
            // -0.1f; pt.z = -1.f; pt.z = 0.f;

            vmax.x = fmax(vmax.x, c);
            vmax.y = fmax(vmax.y, r);
            vmax.z = fmax(vmax.z, pt.z);
            vmin.x = fmin(vmin.x, c);
            vmin.y = fmin(vmin.y, r);
            vmin.z = fmin(vmin.z, pt.z);

            // add vertex
            m_vertices.push_back(pt);

            // calculate indices while we're here
            if (r < m_csv->getNumRows() - 1 && c < m_csv->getNumCols() - 1)
            {
                m_indices.push_back(m_csv->toIndex(r + 0, c + 0));
                m_indices.push_back(m_csv->toIndex(r + 1, c + 0));
                m_indices.push_back(m_csv->toIndex(r + 0, c + 1));

                m_indices.push_back(m_csv->toIndex(r + 0, c + 1));
                m_indices.push_back(m_csv->toIndex(r + 1, c + 1));
                m_indices.push_back(m_csv->toIndex(r + 1, c + 0));
            }
        }
    }

    for (auto& e : m_indices)
    {
        assert(e < m_csv->getNumRows() * m_csv->getNumCols());
    }

    assert(m_vertices.size() == m_csv->getNumRows() * m_csv->getNumCols());
    assert(m_indices.size() == (m_csv->getNumRows() - 1) * (m_csv->getNumCols() - 1) * 6);
}

void Mesh::loadShader(const std::string& vfilename, const std::string& ffilename)
{
    GLuint vsid = glCreateShader(GL_VERTEX_SHADER);
    GLuint fsid = glCreateShader(GL_FRAGMENT_SHADER);

    std::string vs_src = openFile(vfilename);
    std::string fs_src = openFile(ffilename);
    if (!vs_src.size() || !fs_src.size())
    {
        return;
    }

    compile(vsid, vs_src);
    compile(fsid, fs_src);

    m_program = glCreateProgram();
    glAttachShader(m_program, vsid);
    glAttachShader(m_program, fsid);
    glLinkProgram(m_program);

    GLint res;
    int loglen;

    glGetProgramiv(m_program, GL_LINK_STATUS, &res);
    glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 0)
    {
        std::vector<char> errMsg;
        errMsg.reserve(loglen + 1);
        glGetShaderInfoLog(m_program, loglen, NULL, &errMsg[0]);
        printf("%s\n", &errMsg[0]);
    }

    glDetachShader(m_program, vsid);
    glDetachShader(m_program, fsid);
    glDeleteShader(vsid);
    glDeleteShader(fsid);

    glUseProgram(m_program);

    m_modelLoc = glGetUniformLocation(m_program, "model");
}

void Mesh::createTriangle()
{
    m_matrix = glm::mat4(1.0);
    loadShader("vshader.glsl", "fshader.glsl");

    m_vertices.push_back(glm::vec3(0, 0.5, -1));
    m_vertices.push_back(glm::vec3(0.5, -0.5, -1));
    m_vertices.push_back(glm::vec3(-0.5, -0.5, -1));

    m_indices.push_back(2);
    m_indices.push_back(1);
    m_indices.push_back(0);

    GLuint ibo;

    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_vbo);
    glGenBuffers(1, &ibo);

    glBindVertexArray(m_vao);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * 3 * sizeof(float), &m_vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(uint32_t), &m_indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    assert(m_vertices.size() == 3);
}

void Mesh::create()
{
    m_matrix = glm::mat4(1.0);
    genMeshDataFromCSV();
    loadShader("vshader.glsl", "fshader.glsl");

    GLuint ibo;

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * 3 * sizeof(float), &m_vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(uint32_t), &m_indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    m_inherent = glm::scale(m_inherent, glm::vec3(50.0f));
    float d = 5.5f;
    m_inherent = glm::rotate(m_inherent, (float)M_PI / 2.f, glm::vec3(1, 0, 0));
    m_inherent = glm::translate(m_inherent, glm::vec3(-d, -d, 1.f));
    m_matrix = glm::translate(m_matrix, glm::vec3(0, 0, -0.f));
}

void Mesh::orbit(float pitch, float yaw)
{
    // pitch = 0.f;
    glm::vec3 pitch_axis = glm::cross(glm::vec3(10, 8, 10), glm::vec3(0, 1, 0));
    pitch_axis = glm::inverse(m_matrix) * glm::vec4(pitch_axis.x, pitch_axis.y, pitch_axis.z, 0);
    m_matrix = glm::rotate(m_matrix, -pitch, pitch_axis);
    m_matrix = glm::rotate(m_matrix, yaw, glm::vec3(0, 1, 0));
}

void Mesh::update()
{
    m_shaderMatrix = m_matrix * m_inherent;
    // m_shaderMatrix = m_inherent * m_matrix;
}

void Mesh::draw()
{
    assert(m_vertices.size() > 0);
    // glUseProgram(m_program);
    glUniformMatrix4fv(m_modelLoc, 1, GL_FALSE, &m_shaderMatrix[0][0]);

    glBindVertexArray(m_vao);
    glEnableVertexAttribArray(0);
    glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
    glDisableVertexAttribArray(0);
    glBindVertexArray(0);
}