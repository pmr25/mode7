#include "Mouse.hpp"

Mouse::Mouse()
    : m_buttonState{false, false, false}
    , m_leftDragStart{0, 0}
    , m_leftDrag{0, 0}
{
}

void Mouse::update(Window* window)
{
    int32_t x;
    int32_t y;
    SDL_GetMouseState(&x, &y);

    if (m_buttonState[0])
    {
        m_leftDrag[0] = x - m_leftDragStart[0];
        m_leftDrag[1] = y - m_leftDragStart[1];
        m_leftDragStart[0] = x;
        m_leftDragStart[1] = y;

        // SDL_WarpMouseInWindow(nullptr, 50 , 50);
    }
}

void Mouse::processEvents(Window* window, SDL_Event* e)
{
    if (e->type == SDL_MOUSEBUTTONDOWN)
    {
        switch (e->button.button)
        {
        case SDL_BUTTON_LEFT:
            m_buttonState[0] = true;
            // SDL_SetWindowGrab(window->getSDLWindow(),
            // (SDL_bool)m_buttonState[0]); SDL_SetRelativeMouseMode(SDL_TRUE);
            m_leftDragStart[0] = e->motion.x;
            m_leftDragStart[1] = e->motion.y;
            break;

        case SDL_BUTTON_MIDDLE:
            m_buttonState[1] = true;
            break;

        case SDL_BUTTON_RIGHT:
            m_buttonState[2] = true;
            break;
        }
    }
    else if (e->type == SDL_MOUSEBUTTONUP)
    {
        switch (e->button.button)
        {
        case SDL_BUTTON_LEFT:
            m_buttonState[0] = false;
            // SDL_SetWindowGrab(window->getSDLWindow(),
            // (SDL_bool)m_buttonState[0]); SDL_SetRelativeMouseMode(SDL_FALSE);
            m_leftDrag[0] = 0;
            m_leftDrag[1] = 0;
            break;

        case SDL_BUTTON_MIDDLE:
            m_buttonState[1] = false;
            break;

        case SDL_BUTTON_RIGHT:
            m_buttonState[2] = false;
            break;
        }
    }
    // else if (e->type == SDL_MOUSEMOTION)
    // {
    //     if (m_buttonState[0])
    //     {
    //         m_leftDrag[0] = e->motion.x - m_leftDragStart[0];
    //         m_leftDrag[1] = e->motion.y - m_leftDragStart[1];
    //         m_leftDragStart[0] = e->motion.x;
    //         m_leftDragStart[1] = e->motion.y;
    //     }
    // }
}