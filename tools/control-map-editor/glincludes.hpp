#ifndef GLINCLUDES_HPP
#define GLINCLUDES_HPP

#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/vector_angle.hpp>

#ifdef _WIN32
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#include <windows.h>
#include <gl/gl.h>
#include <gl/glew.h>
#else
#include <GL/gl.h>
#include <GL/glew.h>
#endif
#include <iostream>

inline void gl_err_check()
{
    GLuint err = glGetError();
    if (err)
    {
        std::cout << "[GL ERROR] " << gluErrorString(err) << std::endl;
    }
}

// TODO add debug check
#define glec gl_err_check()

#endif /* GLINCLUDES_HPP */