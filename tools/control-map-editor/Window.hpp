#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "glincludes.hpp"
#include <SDL.h>
#include <cstdint>

class Window
{
public:
    Window(uint32_t, uint32_t);
    ~Window();

    void create(uint32_t, uint32_t);

    inline void clear()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    inline void flip()
    {
        SDL_GL_SwapWindow(m_window);
    }

    inline void sendMatrices()
    {
        glUniformMatrix4fv(m_projectionLoc, 1, GL_FALSE, &m_projection[0][0]);
        glUniformMatrix4fv(m_viewLoc, 1, GL_FALSE, &m_view[0][0]);
    }

    inline void setShader(uint32_t program)
    {
        m_program = program;
        m_projectionLoc = glGetUniformLocation(m_program, "projection");
        m_viewLoc = glGetUniformLocation(m_program, "view");
    }

    inline SDL_Window* getSDLWindow()
    {
        return m_window;
    }

private:
    uint32_t m_width;
    uint32_t m_height;

    glm::mat4 m_projection;
    uint32_t m_projectionLoc;
    glm::mat4 m_view;
    uint32_t m_viewLoc;
    uint32_t m_program;

    SDL_Window* m_window;
    SDL_GLContext m_ctx;
    SDL_Event m_event;
};

#endif /* WINDOW_HPP */