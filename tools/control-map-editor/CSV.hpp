#ifndef CSV_HPP
#define CSV_HPP

#include <cstdint>
#include <string>
#include <vector>

class CSV
{
public:
    CSV();

    int open(const std::string&);
    int open(const std::string&, char);
    void trimFirstRowCol();
    float get(uint32_t, uint32_t);
    int put(float, uint32_t, uint32_t);
    int save(const std::string&);
    void summarize();

    inline uint32_t getNumRows()
    {
        return m_rows;
    }

    inline uint32_t getNumCols()
    {
        return m_cols;
    }

    uint32_t toIndex(uint32_t, uint32_t);

private:
    char m_delim;
    uint32_t m_rows;
    uint32_t m_cols;
    std::vector<float> m_cells;
};

#endif /* CSV_HPP */