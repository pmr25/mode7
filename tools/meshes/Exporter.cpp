//
// Created by Patrick on 8/18/2022.
//

#include "Exporter.hpp"
#include <fstream>
#include <map>

void Exporter::saveOBJ(const NewMesh& mesh)
{
    std::ofstream out(mFilename);
    if (!out)
    {
        // TODO set error message
        return;
    }

    std::vector<NewMesh> oneMesh;
    oneMesh.push_back(mesh);

    bulkSaveOBJ(oneMesh);
}

void Exporter::savePLY(const NewMesh& mesh)
{
    std::ofstream out(mFilename, std::ios::binary);
    if (!out)
    {
        mErrorMessage = "cannot open " + mFilename + " for writing";
        return;
    }

    uint32_t vertsPerFace = 4;
    if (mShouldTriangulate)
    {
        vertsPerFace = 3;
    }
    const uint32_t numFaces = mesh.getVerticesRO().size() / vertsPerFace;

    // write header
    out << "ply\nformat binary_little_endian 1.0\n";
    out << "element vertex " << mesh.getVerticesRO().size() << "\n";
    out << "property float x\nproperty float y\nproperty float z\nproperty "
           "float nx\nproperty float ny\nproperty float nz\nproperty float "
           "s\nproperty float t\n";
    out << "element face " << numFaces << "\n";
    out << "property list uchar uint vertex_indices\n";
    out << "end_header\n";

    char buffer[100];
    for (auto& vertex : mesh.getVerticesRO())
    {
        *(float*)(buffer + 0 * sizeof(float)) = vertex.position.x;
        *(float*)(buffer + 1 * sizeof(float)) = vertex.position.y;
        *(float*)(buffer + 2 * sizeof(float)) = vertex.position.z;
        *(float*)(buffer + 3 * sizeof(float)) = vertex.normal.x;
        *(float*)(buffer + 4 * sizeof(float)) = vertex.normal.y;
        *(float*)(buffer + 5 * sizeof(float)) = vertex.normal.z;
        *(float*)(buffer + 6 * sizeof(float)) = vertex.uv.x;
        *(float*)(buffer + 7 * sizeof(float)) = vertex.uv.y;

        out.write(buffer, 8 * sizeof(float));
    }

    if (mShouldTriangulate)
    {
        for (int i{0}; i < numFaces; ++i)
        {
            buffer[0] = vertsPerFace;
            *(unsigned int*)(buffer + 1 + 0 * sizeof(unsigned int)) = i * 3 + 0;
            *(unsigned int*)(buffer + 1 + 1 * sizeof(unsigned int)) = i * 3 + 1;
            *(unsigned int*)(buffer + 1 + 2 * sizeof(unsigned int)) = i * 3 + 2;
            out.write(buffer, 1 + 3 * sizeof(unsigned int));
        }
    }
    else
    {
        for (int i{0}; i < numFaces; ++i)
        {
            buffer[0] = vertsPerFace;
            *(unsigned int*)(buffer + 1 + 0 * sizeof(unsigned int)) = i * 3 + 0;
            *(unsigned int*)(buffer + 1 + 1 * sizeof(unsigned int)) = i * 3 + 1;
            *(unsigned int*)(buffer + 1 + 2 * sizeof(unsigned int)) = i * 3 + 2;
            *(unsigned int*)(buffer + 1 + 3 * sizeof(unsigned int)) = i * 3 + 2;
            out.write(buffer, 1 + 4 * sizeof(unsigned int));
        }
    }
}

void Exporter::aggregateAndSaveMaterials(const std::vector<std::shared_ptr<Material>>& materials)
{
    const std::string fn = mFilename + ".mtl";
    std::ofstream out(fn);
    if (!out)
    {
        return;
    }

    for (auto& material : materials)
    {
        out << material->saveMTL();
    }
}

void Exporter::bulkSaveOBJ(const std::vector<NewMesh>& meshes)
{
    std::cout << "bulkSaveOBJ" << std::endl;
    std::cout << mFilename << std::endl;

    std::ofstream out(mFilename);
    if (!out)
    {
        // TODO set error message
        return;
    }

    uint32_t lastIndex = 1;
    for (auto& mesh : meshes)
    {
        out << "o " << mesh.getName() << std::endl;

        for (auto& p : mesh.getVertexPositions())
        {
            out << "v " << p.x << " " << p.y << " " << p.z << std::endl;
        }
        for (auto& t : mesh.getVertexUVs())
        {
            out << "vt " << t.x << " " << t.y << std::endl;
        }
        for (auto& n : mesh.getVertexNormals())
        {
            out << "vn " << n.x << " " << n.y << " " << n.z << std::endl;
        }

        if (mesh.getMaterial())
        {
            out << "usemtl " << mesh.getMaterial()->getName() << std::endl;
        }

        out << "s off" << std::endl;

        auto geom{4u};
        auto i{0u};
        for (; i < mesh.getNumVertices() / geom; ++i)
        {
            const auto a{i * geom + 0 + lastIndex};
            const auto b{i * geom + 1 + lastIndex};
            const auto c{i * geom + 2 + lastIndex};
            const auto d{i * geom + 3 + lastIndex};
            //            const auto d{i * 4 + 3 + lastIndex};

            out << "f " << a << "/" << a << "/" << a << " " << b << "/" << b << "/" << b << " " << c << "/" << c << "/" << c
//                << std::endl;
                  << " " << d << "/" << d << "/" << d << std::endl;
        }

        lastIndex = i * geom + lastIndex;
    }
}

const std::string& Exporter::getError() const
{
    return mErrorMessage;
}
