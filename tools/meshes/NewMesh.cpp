//
// Created by Patrick on 8/13/2022.
//

#include "NewMesh.hpp"
#include <limits.h>

void NewMesh::addVertex(glm::vec3 p, glm::vec3 n, glm::vec2 t)
{
    addVertex(Vertex(p, n, t));
}

void NewMesh::addVertex(Vertex v)
{
    mVertices.push_back(v);
}

void NewMesh::computeBoundingBox()
{
    mBoxMin = glm::vec3(std::numeric_limits<float>::max());
    mBoxMax = glm::vec3(std::numeric_limits<float>::lowest());

    for (auto& v : mVertices)
    {
        mBoxMax.x = fmaxf(mBoxMax.x, v.position.x);
        mBoxMax.y = fmaxf(mBoxMax.y, v.position.y);
        mBoxMax.z = fmaxf(mBoxMax.z, v.position.z);

        mBoxMin.x = fminf(mBoxMin.x, v.position.x);
        mBoxMin.y = fminf(mBoxMin.y, v.position.y);
        mBoxMin.z = fminf(mBoxMin.z, v.position.z);
    }
}

std::vector<Vertex>& NewMesh::getVertices()
{
    return mVertices;
}

size_t NewMesh::getNumVertices() const
{
    return mVertices.size();
}

glm::vec3 NewMesh::getLength() const
{
    return mBoxMax - mBoxMin;
}
std::vector<glm::vec3> NewMesh::getVertexNormals() const
{
    std::vector<glm::vec3> out;

    for (auto& vertex : mVertices)
    {
        out.push_back(vertex.normal);
    }

    return out;
}
std::vector<glm::vec2> NewMesh::getVertexUVs() const
{
    std::vector<glm::vec2> out;

    for (auto& vertex : mVertices)
    {
        out.push_back(vertex.uv);
    }

    return out;
}
std::vector<glm::vec3> NewMesh::getVertexPositions() const
{
    std::vector<glm::vec3> out;

    for (auto& vertex : mVertices)
    {
        out.push_back(vertex.position);
    }

    return out;
}
void NewMesh::setMaterial(std::shared_ptr<Material>& material)
{
    mMaterial = material;
}
void NewMesh::translateVertices(glm::vec3 amt)
{
    for (auto& vertex : mVertices)
    {
        vertex.position += amt;
    }
}

std::ostream& operator<<(std::ostream& os, const Vertex& vertex)
{
    return os << "<" << glm::to_string(vertex.position) << ", " << glm::to_string(vertex.normal) << ", "
              << glm::to_string(vertex.uv) << ">";
}

std::ostream& operator<<(std::ostream& os, const NewMesh& mesh)
{
    for (const auto& vertex : mesh.getVerticesRO())
    {
        os << " " << vertex;
    }

    return os;
}
