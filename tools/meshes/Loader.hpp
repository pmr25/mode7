//
// Created by Patrick on 8/13/2022.
//

#ifndef MODE7_LOADER_HPP
#define MODE7_LOADER_HPP

#include "NewMesh.hpp"

#include <glm/glm.hpp>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

bool startswith(const std::string&, const std::string&);
std::vector<int> parseFace(const std::string&);

template <int D> glm::vec<D, float> extractVector(const std::string& str, const std::string& delim)
{
    glm::vec<D, float> out;
    size_t pos{0};
    size_t idx = 0;
    std::string buf{""};

    while (pos < str.length())
    {
        // std::cout << str.substr(pos) << std::endl;
        if (str.substr(pos, delim.length()) == delim)
        {
            pos += delim.length();

            // std::cout << idx << ":" << buf << std::endl;

            assert(idx < out.length());
            out[idx] = atof(buf.c_str());
            ++idx;
            buf = "";
        }
        else
        {
            buf += str[pos];
            ++pos;
        }
    }

    if (buf.length() > 0)
    {
        assert(idx < out.length());
        // std::cout << idx << ":" << buf << std::endl;
        out[idx] = atof(buf.c_str());
        ++idx;
    }

    return out;
}

class Loader
{
public:
    Loader(const std::string& fn)
    {
        const std::string suffix = fn.substr(fn.rfind("."));
        mMesh = std::make_shared<NewMesh>();
        if (suffix == ".obj")
        {
            loadOBJ(fn);
            mMesh->computeBoundingBox();
            std::cout << "loaded " << mMesh->getVertices().size() << " vertices" << std::endl;
        }
        else if (suffix == ".ply")
        {
            loadPLY(fn);
            mMesh->computeBoundingBox();
        }
        else
        {
            std::cout << "unknown file type " << suffix << std::endl;
        }
    }

    std::shared_ptr<NewMesh> getModel()
    {
        return mMesh;
    }

    void loadOBJ(const std::string&);
    void loadPLY(const std::string&);

private:
    std::shared_ptr<NewMesh> mMesh;
};

#endif // MODE7_LOADER_HPP
