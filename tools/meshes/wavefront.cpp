/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "wavefront.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#ifdef _WIN32
#define ssize_t long long
#endif

#ifdef _WIN32
/* This code is public domain -- Will Hartung 4/9/09 */
size_t getline(char** lineptr, size_t* n, FILE* stream)
{
    char* bufptr = NULL;
    char* p = bufptr;
    size_t size;
    int c;

    if (lineptr == NULL)
    {
        return -1;
    }
    if (stream == NULL)
    {
        return -1;
    }
    if (n == NULL)
    {
        return -1;
    }
    bufptr = *lineptr;
    size = *n;

    c = fgetc(stream);
    if (c == EOF)
    {
        return -1;
    }
    if (bufptr == NULL)
    {
        bufptr = (char*)malloc(128);
        if (bufptr == NULL)
        {
            return -1;
        }
        size = 128;
    }
    p = bufptr;
    while (c != EOF)
    {
        if ((p - bufptr) > (size - 1))
        {
            size = size + 128;
            bufptr = (char*)realloc(bufptr, size);
            if (bufptr == NULL)
            {
                return -1;
            }
        }
        *p++ = c;
        if (c == '\n')
        {
            break;
        }
        c = fgetc(stream);
    }

    *p++ = '\0';
    *lineptr = bufptr;
    *n = size;

    return p - bufptr - 1;
}
#endif /* _WIN32 */

static int extract_faces(uint32_t* dest, const char* str, const char* delim, int n_to_extract)
{
    char buf[100];
    memset(buf, 0, 100);
    char* bp = buf;
    int n_extracted = 0;

    char last = *str;

    while (last != 0 && n_extracted <= n_to_extract)
    {
        if (*str == delim[0] || *str == delim[1] || *(str) == 0)
        {
            if (strlen(buf) > 0)
            {
                *dest = atoi(buf);
                bp = buf;
                *bp = 0;
                ++dest; // move to next float in array
                ++n_extracted;
            }
        }
        else
        {
            *bp = *str;
            ++bp;
            *bp = 0;
        }
        last = *str;
        ++str;
    }

    return n_extracted;
}
static int extract_floats(float* dest, const char* str, char delim, int n_to_extract)
{
    char buf[100];
    memset(buf, 0, 100);
    char* bp = buf;
    int n_extracted = 0;
    char last = *str;

    while (last != 0 && n_extracted <= n_to_extract)
    {

        if (*str == delim || *(str) == 0)
        {
            if (strlen(buf) > 0)
            {
                *dest = atof(buf);
                bp = buf;
                *bp = 0;
                ++dest; // move to next float in array
                ++n_extracted;
            }
        }
        else
        {
            *bp = *str;
            ++bp;
            *bp = 0;
        }
        last = *str;
        ++str;
    }

    return n_extracted;
}

int wavefront_load(mesh** m, const char* filename)
{
    FILE* fp = fopen(filename, "r");
    if (fp == NULL)
    {
        return 0;
    }

    char* line = NULL;
    size_t len = 0;
    ssize_t nread;
    float fbuf[3];
    uint32_t ubuf[9];

    int cur_indx = -1;
    mesh* cur = NULL;

    *m = (mesh*)malloc(10 * sizeof(mesh));
    if (*m == NULL)
    {
        return 0;
    }

    while ((nread = getline(&line, &len, fp)) != -1)
    {
        line[strlen(line) - 1] = 0; // strip newline
        if (strncmp(line, "o ", 2) == 0)
        {
            if (cur)
            {
                mesh_finish_building(cur);
            }
            ++cur_indx;
            cur = *m + cur_indx;
            mesh_init(cur);
            strcpy(cur->name, line + 2);
            mesh_begin_building(cur);
        }
        if (strncmp(line, "v ", 2) == 0)
        {
            int ne = extract_floats(fbuf, line + 2, ' ', 3);
            if (ne < 3)
            {
                printf("bad vertex!\n");
                continue;
            }
            mesh_add_vertex(cur, fbuf);
        }
        else if (strncmp(line, "vt ", 3) == 0)
        {
            int ne = extract_floats(fbuf, line + 3, ' ', 2);
            if (ne < 2)
            {
                printf("bad uv!\n");
                continue;
            }
            mesh_add_uv(cur, fbuf);
        }
        else if (strncmp(line, "vn ", 3) == 0)
        {
            int ne = extract_floats(fbuf, line + 3, ' ', 3);
            if (ne < 2)
            {
                printf("bad normal!\n");
                continue;
            }
            mesh_add_normal(cur, fbuf);
        }
        else if (strncmp(line, "f ", 2) == 0)
        {
            int ne = extract_faces(ubuf, line + 2, " /", 9);
            if (ne < 9)
            {
                printf("bad face!\n");
                continue;
            }
            mesh_add_index(cur, ubuf[0], ubuf[1], ubuf[2]);
            mesh_add_index(cur, ubuf[3], ubuf[4], ubuf[5]);
            mesh_add_index(cur, ubuf[6], ubuf[7], ubuf[8]);
        }
    }

    if (cur)
    {
        mesh_finish_building(cur);
    }

    if (line != NULL)
    {
        free(line);
    }

    fclose(fp);

    return cur_indx + 1;
}

void wavefront_save(mesh* m, const char* filename, int n_meshes)
{
    if (n_meshes < 1)
    {
        std::cout << "error: trying to save " << n_meshes << " meshes" << std::endl;
        return;
    }

    std::cout << "wavefront save " << n_meshes << " meshes to " << filename << std::endl;
    FILE* fp = fopen(filename, "w");
    if (!fp)
    {
        std::cout << "cannot open " << filename << std::endl;
        return;
    }

    uint32_t vstart = 0;
    uint32_t nstart = 0;
    uint32_t tstart = 0;

    mesh* cur;
    for (int i = 0; i < n_meshes; ++i)
    {
        cur = m + i;
        fprintf(fp, "o %s.%d\n", cur->name, i);
        for (int n = 0; n < cur->n_vertices; ++n)
        {
            int st = n * 3;
            fprintf(fp, "v %f %f %f\n", cur->vertices[st + 0], cur->vertices[st + 1], cur->vertices[st + 2]);
        }

        for (int n = 0; n < cur->n_uvs; ++n)
        {
            int st = n * 2;
            fprintf(fp, "vt %f %f\n", cur->uvs[st + 0], cur->uvs[st + 1]);
        }

        for (int n = 0; n < cur->n_normals; ++n)
        {
            int st = n * 3;
            fprintf(fp, "vn %f %f %f\n", cur->normals[st + 0], cur->normals[st + 1], cur->normals[st + 2]);
        }

        fprintf(fp, "usemtl %s\ns off\n", cur->material);

        for (int n = 0; n < cur->n_indices; ++n)
        {
            if (n % 3 == 0)
            {
                fprintf(fp, "f ");
            }
            int st = n * 3;
            fprintf(fp, "%d/%d/%d", cur->indices[st + 0] + vstart, cur->indices[st + 1] + tstart, cur->indices[st + 2] + nstart);

            if ((n + 1) % 3 == 0)
            {
                fprintf(fp, "\n");
            }
            else
            {
                fprintf(fp, " ");
            }
        }
        fprintf(fp, "\n");

        vstart += cur->n_vertices;
        nstart += cur->n_normals;
        tstart += cur->n_uvs;
    }

    fclose(fp);
}
