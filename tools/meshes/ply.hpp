//
// Created by Patrick on 8/9/2022.
//

#ifndef MODE7_PLY_HPP
#define MODE7_PLY_HPP

#include <string>
#include <vector>

class PLY
{
public:
    PLY(){};
    ~PLY(){};

    int open(const std::string&);
    int save(const std::string&);

private:
};

#endif // MODE7_PLY_HPP
