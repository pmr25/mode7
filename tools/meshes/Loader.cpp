//
// Created by Patrick on 8/13/2022.
//

#include "Loader.hpp"

#include <cassert>
#include <cstdint>
#include <fstream>
#include <sstream>

bool startswith(const std::string& str, const std::string& prefix)
{
    return str.substr(0, prefix.length()) == prefix;
}

std::vector<int> parseFace(const std::string& str)
{
    std::vector<int> out;
    std::stringstream ss;

    for (auto& c : str)
    {
        if (c == '/' || c == ' ' || c == '\0')
        {
            out.push_back(atoi(ss.str().c_str()));
            ss.str("");
        }
        else
        {
            ss << c;
        }
    }

    if (ss.str().length() > 0)
    {
        out.push_back(atoi(ss.str().c_str()));
    }

    return out;
}

void Loader::loadOBJ(const std::string& fn)
{
    std::ifstream in(fn);
    if (!in)
    {
        std::cout << "cannot open " << fn << std::endl;
        return;
    }

    std::string line;
    uint32_t lineNum = 1;
    size_t numVerts;
    size_t numFaces;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    while (std::getline(in, line, '\n'))
    {
        if (startswith(line, "#"))
        {
            // TODO handle comment
        }
        else if (startswith(line, "mtllib "))
        {
            // TODO handle material
        }
        else if (startswith(line, "o "))
        {
            // TODO handle new object
        }
        else if (startswith(line, "v "))
        {
            vertices.push_back(extractVector<3>(line.substr(2), " "));
        }
        else if (startswith(line, "vt "))
        {
            texcoords.push_back(extractVector<2>(line.substr(3), " "));
        }
        else if (startswith(line, "vn "))
        {
            normals.push_back(extractVector<3>(line.substr(3), " "));
        }
        else if (startswith(line, "f "))
        {
            const std::vector<int> faceIndices = parseFace(line.substr(2));
            const auto numElements{3u};
            const auto numVertices{faceIndices.size() / numElements};
            mMesh->setFaceType((face_t)numVertices);

            for (auto i{0u}; i < faceIndices.size(); i += numElements)
            {
                const int vi{faceIndices[i + 0] - 1};
                const int ti{faceIndices[i + 1] - 1};
                const int ni{faceIndices[i + 2] - 1};

                mMesh->addVertex(vertices[vi], normals[ni], texcoords[ti]);
            }
        }
    }
}

void Loader::loadPLY(const std::string& fn)
{
    std::ifstream in(fn);
    if (!in)
    {
        std::cout << "cannot open " << fn << std::endl;
        return;
    }

    std::string line;
    uint32_t lineNum = 1;
    size_t numVerts = 0;
    size_t numFaces = 0;

    while (std::getline(in, line, '\n'))
    {
        if (lineNum == 1)
        {
            if (line != "ply")
            {
                std::cout << "bad header " << line << std::endl;
                return;
            }
        }
        else if (line.compare(0, 7, "format ") == 0)
        {
            const std::string formatVersion = line.substr(7);
            const std::string format = formatVersion.substr(0, formatVersion.find(' '));
            const std::string version = formatVersion.substr(formatVersion.find(' '));

            if (format != "binary_little_endian")
            {
                std::cout << "bad format " << format << std::endl;
                return;
            }

            continue;
        }
        else if (line.compare(0, 8, "element ") == 0)
        {
            const std::string typeCount = line.substr(8);
            const std::string type = typeCount.substr(0, typeCount.find(' '));
            const std::string count = typeCount.substr(typeCount.find(' '));

            if (type == "vertex")
            {
                numVerts = atoi(count.c_str());
            }
            else if (type == "face")
            {
                numFaces = atoi(count.c_str());
            }
        }
        else if (line == "end_header")
        {
            break;
        }

        ++lineNum;
    }

    char buffer[100];
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texture;
    std::vector<Vertex> vertices;
    while (numVerts > 0)
    {
        in.read(buffer, 8 * sizeof(float));

        position.x = *(float*)(buffer + 0 * sizeof(float));
        position.y = *(float*)(buffer + 1 * sizeof(float));
        position.z = *(float*)(buffer + 2 * sizeof(float));
        normal.x = *(float*)(buffer + 3 * sizeof(float));
        normal.y = *(float*)(buffer + 4 * sizeof(float));
        normal.z = *(float*)(buffer + 5 * sizeof(float));
        texture.x = *(float*)(buffer + 6 * sizeof(float));
        texture.y = *(float*)(buffer + 7 * sizeof(float));

        vertices.push_back(Vertex(position, normal, texture));

        --numVerts;
    }

    uint8_t length;
    uint32_t indices[100];
    std::vector<uint32_t> faces;
    while (numFaces > 0)
    {
        // read first element
        in.read(buffer, sizeof(uint8_t));
        length = *(uint8_t*)(buffer);

        // read the face indices
        in.read(buffer, length * sizeof(uint32_t));
        faces.clear();
        faces.resize(length);
        for (int i = 0; i < length; ++i)
        {
            const int index = *(uint32_t*)(buffer + i * sizeof(uint32_t));
            faces[i] = index;
        }

        if (length == 3)
        {
            mMesh->addVertex(vertices[faces[0]]);
            mMesh->addVertex(vertices[faces[1]]);
            mMesh->addVertex(vertices[faces[2]]);
        }
        else if (length == 4) // create 2 triangles
        {
            mMesh->addVertex(vertices[faces[0]]);
            mMesh->addVertex(vertices[faces[1]]);
            mMesh->addVertex(vertices[faces[2]]);
            mMesh->addVertex(vertices[faces[2]]);
            mMesh->addVertex(vertices[faces[3]]);
            mMesh->addVertex(vertices[faces[0]]);
        }

        --numFaces;
    }
}
