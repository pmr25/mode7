//
// Created by Patrick on 8/13/2022.
//

#ifndef MODE7_NEWMESH_HPP
#define MODE7_NEWMESH_HPP

#include <cstdlib>
#include <memory>
#include <ostream>
#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

#include "Material.hpp"

enum face_t
{
    TRIANGLES = 3,
    QUADS = 4
};

class Vertex
{
public:
    Vertex(const glm::vec3 p, const glm::vec3 n, const glm::vec2 t)
        : position(p)
        , normal(n)
        , uv(t)
    {
    }

    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
};

class NewMesh
{
public:
    NewMesh()
    {
    }

    void addVertex(glm::vec3, glm::vec3, glm::vec2);
    void addVertex(Vertex);
    void computeBoundingBox();

    void setFaceType(face_t type)
    {
        mFaceType = type;
    }

    void setName(const std::string& name)
    {
        mName = name;
    }

    const std::string& getName() const
    {
        return mName;
    }

    std::vector<Vertex>& getVertices();
    const std::vector<Vertex>& getVerticesRO() const
    {
        return mVertices;
    }
    std::vector<glm::vec3> getVertexPositions() const;
    std::vector<glm::vec3> getVertexNormals() const;
    std::vector<glm::vec2> getVertexUVs() const;
    size_t getNumVertices() const;
    glm::vec3 getLength() const;

    std::shared_ptr<Material> getMaterial() const
    {
        return mMaterial.lock();
    }

    void translateVertices(glm::vec3);

    void setMaterial(std::shared_ptr<Material>& material);

private:
    std::vector<Vertex> mVertices;
    face_t mFaceType;
    glm::vec3 mBoxMin;
    glm::vec3 mBoxMax;
    std::string mName;
    std::weak_ptr<Material> mMaterial;
};

std::ostream& operator<<(std::ostream&, const Vertex&);
std::ostream& operator<<(std::ostream&, const NewMesh&);

#endif // MODE7_NEWMESH_HPP
