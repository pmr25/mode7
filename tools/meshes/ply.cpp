//
// Created by Patrick on 8/9/2022.
//

#include "ply.hpp"
#include <cstdint>
#include <fstream>
#include <iostream>

int PLY::open(const std::string& fn)
{
    std::ifstream in(fn);
    if (!in)
    {
        return 1;
    }

    std::string line, lineWithEnd;
    uint32_t lineNum = 1;
    uint32_t numVerts;
    uint32_t numFaces;

    bool headerMode = true;

    while (std::getline(in, lineWithEnd, '\n'))
    {
        line = lineWithEnd.substr(lineWithEnd.length() - 1);
        if (lineNum == 1)
        {
            if (line != "ply")
            {
                return 2;
            }
        }

        if (line.compare(0, 7, "format "))
        {
            const std::string formatVersion = line.substr(7);
            const std::string format = formatVersion.substr(0, formatVersion.find(' '));
            const std::string version = formatVersion.substr(formatVersion.find(' '));

            if (format != "binary")
            {
                return 3;
            }

            continue;
        }
        else if (line.compare(0, 8, "element "))
        {
            const std::string typeCount = line.substr(8);
            const std::string type = typeCount.substr(0, typeCount.find(' '));
            const std::string count = typeCount.substr(typeCount.find(' '));

            if (type == "vertex")
            {
                numVerts = atoi(count.c_str());
            }
            else if (type == "face")
            {
                numFaces = atoi(count.c_str());
            }
        }
        else if (line == "end_header")
        {
            break;
        }

        ++lineNum;
    }

    std::cout << "input stream position: " << in.tellg() << std::endl;

    return 0;
}

int PLY::save(const std::string& fn)
{
    return 0;
}
