//
// Created by Patrick on 11/5/2022.
//

#include "Material.hpp"

#include <sstream>

static int mtlCount = 0;

std::string Material::saveMTL()
{
    std::stringstream stream;

    stream << "newmtl " << getName() << std::endl;
    stream << "d 1.00000" << std::endl;
    stream << std::endl;

    return stream.str();
}

std::string Material::getName()
{
    if (mName.length() == 0)
    {
        mName = "Material." + std::to_string(++mtlCount);
    }

    return mName;
}
