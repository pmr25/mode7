//
// Created by Patrick on 8/18/2022.
//

#ifndef MODE7_EXPORTER_HPP
#define MODE7_EXPORTER_HPP

#include "NewMesh.hpp"
#include <cstdint>
#include <iostream>
#include <string>

#define EXPORTER_TRIANGULATE 0x1

class Exporter
{
public:
    Exporter(const NewMesh& mesh, const std::string& fn, uint32_t flags = 0)
        : mFilename(fn)
    {
        mShouldTriangulate = flags & EXPORTER_TRIANGULATE;
        const std::string suffix = fn.substr(fn.rfind("."));
        if (suffix == ".obj")
        {
            saveOBJ(mesh);
        }
        else if (suffix == ".ply")
        {
            savePLY(mesh);
        }
        else
        {
            std::cout << "unknown file type " << suffix << std::endl;
        }
    }

    Exporter(const std::vector<NewMesh>& meshes, const std::string& fn, uint32_t flags = 0)
        : mFilename(fn)
    {
        mShouldTriangulate = flags & EXPORTER_TRIANGULATE;
        const std::string suffix = fn.substr(fn.rfind("."));
        if (suffix == ".obj")
        {
            bulkSaveOBJ(meshes);
        }
        else if (suffix == ".ply")
        {
            // TODO .ply bulk export
        }
        else
        {
            std::cout << "unknown file type " << suffix << std::endl;
        }
    }

    Exporter(const std::vector<NewMesh>& meshes, const std::vector<std::shared_ptr<Material>>& materials, const std::string& fn,
             uint32_t flags = 0)
        : mFilename(fn)
    {
        mShouldTriangulate = flags & EXPORTER_TRIANGULATE;
        std::cout << "will exporter triangulate? "  << mShouldTriangulate << std::endl;
        const std::string suffix = fn.substr(fn.rfind("."));
        if (suffix == ".obj")
        {
            aggregateAndSaveMaterials(materials);
            bulkSaveOBJ(meshes);
        }
        else if (suffix == ".ply")
        {
            // TODO .ply bulk export
        }
        else
        {
            std::cout << "unknown file type " << suffix << std::endl;
        }
    }

    void saveOBJ(const NewMesh&);
    void savePLY(const NewMesh&);
    void bulkSaveOBJ(const std::vector<NewMesh>&);
    void aggregateAndSaveMaterials(const std::vector<std::shared_ptr<Material>>&);
    const std::string& getError() const;

private:
    bool mShouldTriangulate;
    std::string mErrorMessage;
    std::string mFilename;
};

#endif // MODE7_EXPORTER_HPP
