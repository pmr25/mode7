//
// Created by Patrick on 11/5/2022.
//

#ifndef MODE7_MATERIAL_HPP
#define MODE7_MATERIAL_HPP

#include <string>

class Material
{
public:
    Material(const std::string& name = "")
//        : mID(boost::uuids::random_generator()())
        : mID(newID())
        , mName(name)
    {
    }

    ~Material() = default;

    static uint32_t newID()
    {
        static uint32_t id = 0;
        return id++;
    }

    bool operator==(const Material& rhs) const
    {
        return mID == rhs.mID;
    }
    bool operator!=(const Material& rhs) const
    {
        return !(rhs == *this);
    }

    std::string saveMTL();
    std::string getName();

private:
//    boost::uuids::uuid mID;
    uint32_t mID;
    std::string mName;
};

#endif // MODE7_MATERIAL_HPP
