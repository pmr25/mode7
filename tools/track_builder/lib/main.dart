import 'package:desktop_drop/desktop_drop.dart';
import 'package:flutter/material.dart';
import 'package:cross_file/cross_file.dart';
import 'package:toast/toast.dart';
import 'dart:io';
import 'package:window_size/window_size.dart';
import 'package:toast/toast.dart';

void showToast() {
  Toast.show("hi", duration: Toast.lengthShort, gravity: Toast.bottom);
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    setWindowTitle('Track Builder');
    setWindowMinSize(const Size(900, 500));
    setWindowMaxSize(Size.infinite);
  }
  MediaQuery app = MediaQuery(data: MediaQueryData(), child: MyApp());
  runApp(app);
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    ToastContext().init(context);
  }

  @override
  Widget build(BuildContext context) {
    const Color primaryColor = Color.fromARGB(255, 226, 170, 233);

    Widget buttonSelection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildButtonColumn(primaryColor, Icons.build, 'BUILD'),
        _buildButtonColumn(primaryColor, Icons.settings, 'SETTINGS'),
        _buildButtonColumn(primaryColor, Icons.restart_alt, 'RESET'),
      ],
    );

    Widget dropSelection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildDropColumn(SvgDragTarget()),
        _buildDropColumn(ZipDragTarget()),
        _buildDropColumn(ObjDragTarget()),
      ],
    );
    return MaterialApp(
      theme: ThemeData(primaryColor: primaryColor),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Track Builder'),
          backgroundColor: primaryColor,
        ),
        body: Wrap(
          direction: Axis.horizontal,
          runSpacing: 8,
          spacing: 8,
          children: [
            dropSelection,
            buttonSelection,
          ],
        ),
      ),
    );
  }

  Padding _buildDropColumn(ExmapleDragTarget target) {
    return Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [target],
        ));
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(icon, color: color),
          onPressed: () => {},
        ),
        Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(label,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: color,
                )))
      ],
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class ZipDragTarget extends ExmapleDragTarget {
  ZipDragTarget({super.key}) : super(lbl: '.zip', filter: '.zip');
}

class SvgDragTarget extends ExmapleDragTarget {
  SvgDragTarget({super.key}) : super(lbl: '.svg', filter: '.svg');
}

class ObjDragTarget extends ExmapleDragTarget {
  ObjDragTarget({super.key}) : super(lbl: '.obj', filter: '.obj');
}

class ExmapleDragTarget extends StatefulWidget {
  String label = '';
  String filter = '';

  ExmapleDragTarget({Key? key, required String lbl, required String filter})
      : super(key: key) {
    label = lbl;
    this.filter = filter;
  }

  @override
  _ExmapleDragTargetState createState() => _ExmapleDragTargetState();
}

class _ExmapleDragTargetState extends State<ExmapleDragTarget> {
  List<XFile> _list = [];

  bool _dragging = false;

  Offset? offset;

  @override
  Widget build(BuildContext context) {
    return DropTarget(
      onDragDone: (detail) async {
        setState(() {
          if (_list.isNotEmpty) {
            _list = []; // clear list
          }
          for (final file in detail.files) {
            if (super.widget.filter.isNotEmpty &&
                !file.name.endsWith(super.widget.filter)) {
              continue;
            }

            debugPrint('filter: ${super.widget.filter}');

            _list.add(file);
            break; // only allow one file
          }
        });

        debugPrint('onDragDone:');
        for (final file in _list) {
          debugPrint('  ${file.path} ${file.name}'
              '  ${await file.lastModified()}'
              '  ${await file.length()}'
              '  ${file.mimeType}');
        }

        showToast();
      },
      onDragUpdated: (details) {
        setState(() {
          offset = details.localPosition;
        });
      },
      onDragEntered: (detail) {
        setState(() {
          _dragging = true;
          offset = detail.localPosition;
        });
      },
      onDragExited: (detail) {
        setState(() {
          _dragging = false;
          offset = null;
        });
      },
      child: Container(
        height: 200,
        width: 200,
        color: _dragging ? Colors.blue.withOpacity(0.4) : Colors.black26,
        child: Stack(
          children: [
            if (_list.isEmpty)
              Center(child: Text(super.widget.label))
            else
              Text(_list.map((e) => e.path).join("\n")),
            if (offset != null)
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  '$offset',
                  style: Theme.of(context).textTheme.caption,
                ),
              )
          ],
        ),
      ),
    );
  }
}
