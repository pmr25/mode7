if (MSVC AND "${USE_PREBUILT_DEPENDENCIES}")
    set(PREBUILT_INCLUDE_DIRS
            ${CMAKE_SOURCE_DIR}/dependencies/include
            ${CMAKE_SOURCE_DIR}/dependencies/include/SDL2
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk/harfbuzz
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk/glib-2.0
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk/pango-1.0
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk/freetype2
            ${CMAKE_SOURCE_DIR}/dependencies/include/gtk/cairo
            )
    set(PREBUILT_LIBRARY_DIRS ${CMAKE_SOURCE_DIR}/dependencies/lib)
    set(PREBUILT_LIBRARIES
            assimp-vc143-mt.lib
            Bullet3Common_RelWithDebugInfo.lib
            Bullet3Collision_RelWithDebugInfo.lib
            BulletDynamics_RelWithDebugInfo.lib
            BulletSoftBody_RelWithDebugInfo.lib
            BulletCollision_RelWithDebugInfo.lib
            cairo.lib
            glew32.lib
            LinearMath_RelWithDebugInfo.lib
            OpenAL32.lib
            SDL2.lib
            SDL2_image.lib
            SDL2main.lib
            ogg.lib
            vorbis.lib
            opengl32.lib
            gdiplus.lib
            )
elseif (UNIX AND NOT "${USE_PREBUILT_DEPENDENCIES}")
    find_package(PkgConfig)

    pkg_check_modules(SDL2 REQUIRED sdl2)
    pkg_check_modules(SDL2_IMAGE REQUIRED SDL2_image)
    pkg_check_modules(CAIRO REQUIRED cairo)
    pkg_check_modules(GLEW REQUIRED glew)
    pkg_check_modules(BULLET REQUIRED bullet)
    pkg_check_modules(ASSIMP REQUIRED assimp)
    pkg_check_modules(FREETYPE REQUIRED freetype2)
    pkg_check_modules(FONTCONFIG REQUIRED fontconfig)
    pkg_check_modules(PANGO REQUIRED pango)
    pkg_check_modules(PANGOCAIRO REQUIRED pangocairo)

    set(PKGCONFIG_INCLUDE
        ${incl}
        ${SDL2_INCLUDE_DIRS}
        ${SDL2_IMAGE_INCLUDE_DIRS}
        ${CMAKE_SOURCE_DIR}/3rdparty/SDL_image
        ${CMAKE_SOURCE_DIR}/3rdparty/SDL_ttf
        ${CAIRO_INCLUDE_DIRS}
        ${GLEW_INCLUDE_DIRS}
        ${NLOHMANN_JSON_INCL}
        ${BULLET_INCLUDE_DIRS}
        ${LUA_INCL}
        ${FREETYPE_INCLUDE_DIRS}
        ${FONTCONFIG_INCLUDE_DIRS}
        ${PANGO_INCLUDE_DIRS}
        ${PANGOCAIRO_INCLUDE_DIRS}
        )

    set(PKGCONFIG_LIBRARIES
        ${libs}
        ${SDL_LIBRARIES}
        ${SDL2_IMAGE_LIBRARIES}
        ${GLEW_LIBRARIES}
        ${CAIRO_LIBRARIES}
        ${BULLET_LIBRARIES}
        ${OPENSSL_LIBRARIES}
        ${LUA_LIBS}
        ${FREETYPE_LIBRARIES}
        ${FONTCONFIG_LIBRARIES}
        ${PANGO_LIBRARIES}
        ${PANGOCAIRO_LIBRARIES}
        m
        pthread
        stdc++fs
        dl
        assimp
        )
    if (${USE_DEVIL})
        set(flags
            ${flags}
            -D__DEVIL__
        )
        pkg_check_modules(DEVIL REQUIRED IL)
        set(PKGCONFIG_INCLUDE
            ${incl}
            ${DEVIL_INCLUDE_DIRS}
            )
        set(PKGCONFIG_LIBRARIES
            ${libs}
            ${DEVIL_LIBRARIES}
            )
    endif ()
endif ()