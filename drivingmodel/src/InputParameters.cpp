//
// Created by patrick on 9/2/23.
//

#include "InputParameters.hpp"
float InputParameters::getMedialVelocity() const
{
    return mMedialVelocity;
}
float InputParameters::getLateralVelocity() const
{
    return mLateralVelocity;
}
float InputParameters::getGasInput() const
{
    return mGasInput;
}
float InputParameters::getBrakeInput() const
{
    return mBrakeInput;
}
float InputParameters::getSteerInput() const
{
    return mSteerInput;
}
float InputParameters::getMedialIncline() const
{
    return mMedialIncline;
}
float InputParameters::getSurfaceSlip() const
{
    return mSurfaceSlip;
}
float InputParameters::getLateralIncline() const
{
    return mLateralIncline;
}
