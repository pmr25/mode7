//
// Created by patrick on 9/2/23.
//

#include "OutputParameters.hpp"
auto OutputParameters::setActorProperties(float medialImpulse,
                                          float lateralImpulse, float omega)
    -> bool
{
    mMedialImpulse = medialImpulse;
    mLateralImpulse = lateralImpulse;
    mOmega = omega;

    return true;
}
