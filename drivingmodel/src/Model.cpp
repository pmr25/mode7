//
// Created by patrick on 9/2/23.
//

#include "Model.hpp"

#include <cmath>

auto Model::tick(const InputParameters& inputParameters) const
    -> std::expected<OutputParameters, ModelError>
{
    const float medialFriction =
        friction_dv(inputParameters.getMedialVelocity(), 0.1F);
    const float lateralFriction =
        friction_dv(inputParameters.getLateralVelocity(), 0.9F);
    const float medialDrag =
        drag_dv(inputParameters.getMedialVelocity(), 0.01F);
    const float medialPush = inputParameters.getGasInput();
    const float lateralPush = inputParameters.getSteerInput();
    const float medialBrake = -direction(inputParameters.getMedialVelocity()) *
                              inputParameters.getBrakeInput();

    const float medialImpulse = medialPush + medialBrake + medialDrag + medialFriction;
    const float lateralImpulse = lateralPush + lateralFriction;
    const float omega = inputParameters.getSteerInput();

    OutputParameters outputParameters;
    outputParameters.setActorProperties(medialImpulse, lateralImpulse, omega);

    return outputParameters;
}

float Model::friction_dv(float velocity, float constant) const
{
    return -velocity * constant;
}

float Model::drag_dv(float velocity, float constant) const
{
    return -velocity * fabsf(velocity) * constant;
}
auto Model::direction(float val) const -> float
{
    return copysignf(1.0F, val);
}
void Model::setVehicleProperties(const VehicleProperties& mVehicleProperties)
{
    Model::mVehicleProperties = mVehicleProperties;
}
void Model::setTrackProperties(const TrackProperties& mTrackProperties)
{
    Model::mTrackProperties = mTrackProperties;
}
