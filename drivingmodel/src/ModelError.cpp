//
// Created by patrick on 9/4/23.
//

#include "ModelError.hpp"

auto ModelError::getReason() const -> const std::string&
{
    return mReason;
}
