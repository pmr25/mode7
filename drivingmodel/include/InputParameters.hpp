//
// Created by patrick on 9/2/23.
//

#ifndef INPUTPARAMETERS_HPP
#define INPUTPARAMETERS_HPP

class InputParameters
{
public:
    InputParameters() = default;
    InputParameters(const InputParameters& other) = default;
    ~InputParameters() = default;

    [[nodiscard]] auto getMedialVelocity() const -> float;
    [[nodiscard]] auto getLateralVelocity() const -> float;
    [[nodiscard]] auto getGasInput() const -> float;
    [[nodiscard]] auto getBrakeInput() const -> float;
    [[nodiscard]] auto getSteerInput() const -> float;
    [[nodiscard]] auto getMedialIncline() const -> float;
    [[nodiscard]] auto getLateralIncline() const -> float;
    [[nodiscard]] auto getSurfaceSlip() const -> float;

private:
    // actor properties
    float mMedialVelocity{0.0F};
    float mLateralVelocity{0.0F};
    // control properties
    float mGasInput{0.0F};
    float mBrakeInput{0.0F};
    float mSteerInput{0.0F};
    // track properties
    float mMedialIncline{0.0F};
    float mLateralIncline{0.0F};
    float mSurfaceSlip{0.0F};
};

#endif // INPUTPARAMETERS_HPP
