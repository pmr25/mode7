//
// Created by patrick on 9/10/23.
//

#ifndef MESON_BUILD_TRACKPROPERTIES_HPP
#define MESON_BUILD_TRACKPROPERTIES_HPP

class TrackProperties
{
public:
    TrackProperties() = default;
};

#endif // MESON_BUILD_TRACKPROPERTIES_HPP
