//
// Created by patrick on 9/2/23.
//

#ifndef OUTPUTPARAMETERS_HPP
#define OUTPUTPARAMETERS_HPP

class OutputParameters
{
public:
    OutputParameters() = default;
    OutputParameters(const OutputParameters& other) = default;
    ~OutputParameters() = default;

    auto setActorProperties(float, float, float) -> bool;

private:
    // actor properties
    float mMedialImpulse{0.0F};
    float mLateralImpulse{0.0F};
    float mOmega{0.0F};
};

#endif // OUTPUTPARAMETERS_HPP
