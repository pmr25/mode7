//
// Created by patrick on 9/4/23.
//

#ifndef MESON_BUILD_MODELERROR_HPP
#define MESON_BUILD_MODELERROR_HPP

#include <ostream>
#include <string>
#include <utility>

class ModelError
{
public:
    explicit ModelError(std::string reason)
        : mReason(std::move(reason))
    {
    }

    ~ModelError() = default;

    [[nodiscard]] auto getReason() const -> const std::string&;

private:
    std::string mReason;
};

inline auto operator<<(std::ostream& os, const ModelError& obj) -> std::ostream&
{
    os << obj.getReason();
    return os;
}

#endif // MESON_BUILD_MODELERROR_HPP
