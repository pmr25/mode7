//
// Created by patrick on 9/2/23.
//

#ifndef MODEL_HPP
#define MODEL_HPP

#include "InputParameters.hpp"
#include "ModelError.hpp"
#include "OutputParameters.hpp"
#include "VehicleProperties.hpp"
#include "TrackProperties.hpp"

#include <expected>

class Model
{
public:
    Model() = default;
    ~Model() = default;

    [[nodiscard]] auto tick(const InputParameters&) const
        -> std::expected<OutputParameters, ModelError>;

    void setVehicleProperties(const VehicleProperties& mVehicleProperties);
    void setTrackProperties(const TrackProperties& mTrackProperties);

private:
    [[nodiscard]] auto friction_dv(float velocity, float constant) const -> float;
    [[nodiscard]] auto drag_dv(float velocity, float constant) const -> float;
    [[nodiscard]] auto direction(float val) const -> float;

    VehicleProperties mVehicleProperties;
    TrackProperties mTrackProperties;
};

#endif // MODEL_HPP
