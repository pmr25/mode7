//
// Created by patrick on 9/10/23.
//

#ifndef MESON_BUILD_EDITOR_HPP
#define MESON_BUILD_EDITOR_HPP

#include <functional>
#include <string>
#include <utility>
#include <vector>

#define NameValuePair std::pair<std::string, float>
#define NameValueList std::vector<NameValuePair>

namespace debug
{

class Editor
{
public:
    Editor(std::function<NameValueList()> download, std::function<void(NameValueList)> upload)
        : mDownloadFunc(std::move(download))
        , mUploadFunc(std::move(upload))
    {
        pull();
    }
    ~Editor() = default;

    inline void pull()
    {
        mSliders = std::move(mDownloadFunc());
    }

    inline void push()
    {
        mUploadFunc(mSliders);
    }

private:
    std::vector<std::pair<std::string, float>> mSliders;
    std::function<NameValueList()> mDownloadFunc;
    std::function<void(const NameValueList&)> mUploadFunc;
};

}

#endif // MESON_BUILD_EDITOR_HPP
