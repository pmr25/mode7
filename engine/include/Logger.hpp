
/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef LOGGER_HPP
#define LOGGER_HPP

#define LOGURU_WITH_STREAMS 1
#ifdef __linux__
#include "loguru.hpp"
#endif
#ifdef _WIN32
#include <loguru/loguru.hpp>
#endif
#include <cstdint>
#include <string>

#define LOG(unused, level) DLOG_S(level)
#define SLOG(level) DLOG_S(level)

#define LG_INFO INFO
#define LG_WARN WARNING
#define LG_ERROR ERROR
#define LG_DEBUG MAX

namespace mode7
{
class Logger
{
public:
    static void setLogDir(const std::string&);
    static void setOutputFile(const std::string&);
    static void logToTerminal();
    static void init(int, char**);
};
} // namespace mode7

#endif /* LOGGER_HPP */
