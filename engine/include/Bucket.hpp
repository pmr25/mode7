#ifndef MODE7_BUCKET_HPP
#define MODE7_BUCKET_HPP

#include "Entity.hpp"

#include <glm/glm.hpp>

#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <vector>

namespace mode7
{

template <int D>
class Bucket
{
public:
    Bucket(uint32_t capacity, uint32_t depthLimit, uint32_t level,
           glm::vec<D, float> origin, glm::vec<D, float> size)
        : mCapacity(capacity)
        , mDepthLimit(depthLimit)
        , mLevel(level)
        , mOrigin(origin)
        , mDimensions(size)
    {
        mBranches.reserve(D);
        mLeaves.reserve(mCapacity);
    }
    auto contains(glm::vec<D, float> position, glm::vec<D, float> dimension)
    {
        auto inside{true};

        for (auto i{0}; i < D; ++i)
        {
            inside &= position[i] <= mOrigin[i] + mDimensions[i] &&
                      position[i] + dimension[i] >= mOrigin[i];
        }

        return inside;
    }
    auto contains(std::shared_ptr<Entity<2>> entity) -> bool
    {
        return contains(entity->getPosition(), entity->getDimensions());
    }
    auto isFull() -> bool
    {
        return mLeaves.size() >= mCapacity && mLevel < mDepthLimit;
    }
    auto isBranch() -> bool
    {
        assert(mLeaves.empty());
        return !mBranches.empty();
    }
    auto isLeaf() -> bool
    {
        return mBranches.empty();
    }
    auto insert(std::weak_ptr<Entity<D>> entity) -> bool
    {
        auto inserted{false};

        if (!contains(entity.lock()))
        {
            return inserted;
        }

        if (!mBranches.empty()) // not a leaf
        {
            for (auto& branch : mBranches)
            {
                inserted |= branch.insert(entity);
            }
        }
        else // is a leaf
        {
            mLeaves.push_back(std::weak_ptr<Entity<D>>(entity));
            if (isFull())
            {
                subdivide();
            }
            inserted = true;
        }

        mInsertCounter += int(inserted);

        return inserted;
    }
    auto search(std::vector<std::weak_ptr<Entity<D>>>& results,
                glm::vec<D, float> position, glm::vec<D, float> dimension)
        -> int
    {
        if (!contains(position, dimension))
        {
            return 0;
        }

        auto found{0};
        if (isLeaf())
        {
            found += mLeaves.size();
            results.insert(results.end(), mLeaves.begin(), mLeaves.end());
        }
        else
        {
            for (auto& branch : mBranches)
            {
                branch.search(results, position, dimension);
            }
        }

        return found;
    }
    auto searchUnique(std::map<std::string, std::weak_ptr<Entity<D>>>& results,
                      glm::vec<D, float> position, glm::vec<D, float> dimension)
        -> int
    {
        if (!contains(position, dimension))
        {
            return 0;
        }

        auto found{0};
        if (isLeaf())
        {
            found += mLeaves.size();
            for (auto& entity : mLeaves)
            {
                auto&& ptr{entity.lock()};
                results[ptr->getUUID()] = entity;
            }
        }
        else
        {
            for (auto& branch : mBranches)
            {
                branch.searchUnique(results, position, dimension);
            }
        }

        return found;
    }
    auto subdivide() -> void
    {
        assert(mBranches.empty());
        assert(mLevel < mDepthLimit);

        auto subDimensions{mDimensions / 2.0F};

        for (auto i{0}; i < int(pow(2.0, double(D))); ++i)
        {
            auto origin{computeOrigin(i, mOrigin, subDimensions)};

            mBranches.push_back(Bucket<D>(mCapacity, mDepthLimit, mLevel + 1,
                                          origin, subDimensions));
        }

        assert(!mBranches.empty());

        for (auto& entity : mLeaves)
        {
            auto inserted{false};
            for (auto& branch : mBranches)
            {
                inserted |= branch.insert(entity);
            }
            assert(inserted);
        }
    }

    auto prune() -> std::vector<std::weak_ptr<Entity<D>>>
    {
        std::vector<std::weak_ptr<Entity<D>>> results;
        prune(results);
        return results;
    }

    auto prune(std::vector<std::weak_ptr<Entity<D>>>& results) -> void
    {
        if (isBranch())
        {
            for (auto& branch : mBranches)
            {
                branch.prune(results);
            }
        }
        else
        {
            for (auto i{mLeaves.size() - 1}; i >= 0; --i)
            {
                auto& entity{mLeaves[i]};
                //                auto& lock{entity.lock()};
                if (!contains(entity))
                {
                    results.push_back(entity);
                    mLeaves.erase(mLeaves.begin() + i);
                }
            }
        }
    }

    auto getBranches() const -> const std::vector<Bucket<D>>&
    {
        return mBranches;
    }

    auto getOrigin() const -> const glm::vec<D, float>
    {
        return mOrigin;
    }

    auto getDimensions() const -> const glm::vec<D, float>
    {
        return mDimensions;
    }

    auto getNumInserts() const -> int
    {
        return mInsertCounter;
    }

private:
    auto computeOrigin(uint32_t index, glm::vec<D, float> offset,
                       glm::vec<D, float> dimension) -> glm::vec<D, float>
    {
        glm::vec<D, float> multiplier;
        for (auto i{0}; i < D; ++i)
        {
            multiplier[i] = (index / (int)pow(2, i)) % 2;
        }
        return offset + (multiplier * dimension);
    }

    glm::vec<D, float> mOrigin;
    glm::vec<D, float> mDimensions;
    std::vector<Bucket<D>> mBranches;
    std::vector<std::weak_ptr<Entity<D>>> mLeaves;
    uint32_t mCapacity;
    uint32_t mDepthLimit;
    int32_t mLevel;
    uint32_t mInsertCounter{0};
};

} // namespace mode7

#endif // MODE7_BUCKET_HPP
