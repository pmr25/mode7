#ifndef MODE7_FONTSBACKENDGDI_HPP
#define MODE7_FONTSBACKENDGDI_HPP

#ifdef _WIN32

#include "Fonts.hpp"

// clang-format off
#include <Windows.h>
#include <gdiplus.h>
#include <objidl.h>
#pragma comment (lib,"Gdiplus.lib")
// clang-format on
#ifdef _WIN32
#include <cairo/cairo.h>
#elif defined __linux__
#include <cairo.h>
#endif
#include <memory>
#include <string>
#include <vector>

namespace mode7
{

class FontsBackendGDI : public Fonts
{
public:
    static auto getInstance() -> FontsBackendGDI*
    {
        static FontsBackendGDI instance;
        return &instance;
    }

    auto drawText(cairo_t*, const std::string&, const std::string&, double,
                  double) -> void override;
    auto addFontFileToSystem(const std::string&) -> bool override;

private:
    FontsBackendGDI() = default;
    ~FontsBackendGDI()
    {
        cleanup();
    }
    auto cleanup() -> void;

    std::vector<std::string> installedFonts;
    std::unique_ptr<Gdiplus::PrivateFontCollection> mPrivateFontCollection;
};

} // namespace mode7

#endif

#endif // MODE7_FONTSBACKENDGDI_HPP
