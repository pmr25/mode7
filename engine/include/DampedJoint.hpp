#ifndef MODE7_DAMPEDJOINT_HPP
#define MODE7_DAMPEDJOINT_HPP

#include "RestoringScalar.hpp"

#include <glm/glm.hpp>

namespace mode7
{

class DampedJoint
{
public:
    explicit DampedJoint(float spring = 0.0F, float damp = 0.0F)
        : DampedJoint(glm::vec3(spring), glm::vec3(damp))
    {
    }
    explicit DampedJoint(glm::vec3 spring = glm::vec3(0.0F),
                         glm::vec3 damp = glm::vec3(0.0F))
        : mXAxis(spring.x, damp.x)
        , mYAxis(spring.y, damp.y)
        , mZAxis(spring.z, damp.z)
    {
    }
    ~DampedJoint() = default;

    auto applyForce(glm::vec3) -> void;
    auto update() -> void;
    auto getPosition() -> glm::vec3;

private:
    RestoringScalar mXAxis;
    RestoringScalar mYAxis;
    RestoringScalar mZAxis;
};

} // namespace mode7

#endif // MODE7_DAMPEDJOINT_HPP
