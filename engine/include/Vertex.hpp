/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef VERTEX_HPP
#define VERTEX_HPP

#define VERTEX_POSITION 0x01
#define VERTEX_NORMAL 0x02
#define VERTEX_UV 0x04
#define VERTEX_TANGENT 0x08

#define SHOULD_INCLUDE(flag, mask) (bool((flag & mask)))

#include "gl.hpp"
#include "glmincludes.hpp"
#include <vector>

namespace mode7
{
class Vertex
{
public:
    constexpr Vertex(glm::vec3 p = glm::vec3(0.0F),
                     glm::vec3 n = glm::vec3(0.0f),
                     glm::vec2 u = glm::vec3(0.0F),
                     glm::vec3 t = glm::vec3(0.0f))
        : position(p)
        , normal(n)
        , uv(u)
        , tangent(t)
    {
    }

    constexpr ~Vertex() = default;

    auto interleave(std::vector<float>&, uint8_t) -> void;
    auto getStride(uint8_t) -> size_t;

    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
    glm::vec3 tangent;
};
} // namespace mode7

#endif /* VERTEX_HPP */
