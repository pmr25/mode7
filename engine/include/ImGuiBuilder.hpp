#ifndef IMGUIBUILDER_HPP
#define IMGUIBUILDER_HPP

#ifdef _DEBUG

namespace mode7
{
class ImGuiBuilder
{
public:
    ImGuiBuilder() = default;
    virtual ~ImGuiBuilder() = default;

    virtual void show();
    void render();
};
} // namespace mode7

#endif /* _DEBUG */

#endif /* IMGUIBUILDER_HPP */
