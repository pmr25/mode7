#ifndef HUDBUTTONWIDGET_HPP
#define HUDBUTTONWIDGET_HPP

#include "HUDCairoElement.hpp"

#include <cstdint>

#define DEFAULT_WIDTH 200
#define DEFAULT_HEIGHT 60

namespace mode7
{
class HUDButtonWidget : public HUDCairoElement
{
public:
    HUDButtonWidget(uint32_t w = DEFAULT_WIDTH, uint32_t h = DEFAULT_HEIGHT)
        : HUDCairoElement(w, h)
        , mCallback(nullptr)
        , mCallbackData(nullptr)
        , mIsPressed(false)
        , mIsActive(true)
        , mText("")
    {
    }

    virtual ~HUDButtonWidget() = default;

    virtual void buildWidget(cairo_t*) override;
    virtual void buildDefaultState(cairo_t*);
    virtual void buildPressedState(cairo_t*);
    virtual void buildInactiveState(cairo_t*);
    virtual void press();
    virtual void cancel();
    virtual void release();
    virtual bool listen();
    virtual void handleClick(int32_t, int32_t) override;

    void setCallback(void (*)(void*), void*);
    void setText(const std::string&);
    void setActive(bool val)
    {
        mIsActive = val;
    }

    const std::string& getText()
    {
        return mText;
    }

private:
    void (*mCallback)(void*);
    void* mCallbackData;
    bool mIsPressed;
    bool mIsActive;
    std::string mText;
};
} // namespace mode7

#endif /* HUDBUTTONWIDGET_HPP */