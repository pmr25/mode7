#ifndef UPDATABLE_HPP
#define UPDATABLE_HPP

#include "gl.hpp"
#include <glm/gtx/quaternion.hpp>

namespace mode7
{
class Updatable
{
public:
    virtual void preUpdate()
    {
    }
    virtual void postUpdate()
    {
    }
    virtual void update() = 0;
    virtual void update(const glm::mat4&, const glm::quat&) = 0;
    virtual void update(const glm::mat4&, const glm::quat&, bool) = 0;
};
} // namespace mode7

#endif /* UPDATABLE_HPP */
