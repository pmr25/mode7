/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef SCREEN_HPP
#define SCREEN_HPP

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <cstdint>
#include <memory>
#include <string>

#include "Framebuffer.hpp"
#include "Game.hpp"
#include "GameState.hpp"
#include "ImGuiBuilder.hpp"
#include "Pipeline.hpp"

namespace mode7
{
class GLScreen
{
public:
    static void create(int, int, int, int, const std::string&, bool, bool,
                       bool);
    static void setPipeline(std::shared_ptr<Pipeline>);
    static void clear();
    static void runPipeline(Game*);
    static void runStatePipeline(std::shared_ptr<GameState>);
#ifdef _DEBUG
    static void setImGuiBuilder(std::shared_ptr<ImGuiBuilder>);
#endif /* _DEBUG */
    static void postRender();
    static void flip();
    static void beginRender();
    static void destroy();
    static SDL_Window* getWindow();
    static uint32_t getWidth();
    static uint32_t getHeight();
    static uint32_t getResolutionWidth();
    static uint32_t getResolutionHeight();
    static uint32_t getScale();
    static bool isReady();
    static glm::vec2 pixelToUnit(int, int);

    static void hideWindow();
};
} // namespace mode7

#endif /* SCREEN_HPP */
