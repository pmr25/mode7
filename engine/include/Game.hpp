/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef GAME_HPP
#define GAME_HPP

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <cstdint>

namespace mode7
{
class Shader;

class Game
{
public:
    Game() = default;
    virtual ~Game() = default;

    virtual void init()
    {
    }
    virtual void update()
    {
    }
    virtual void draw(int32_t)
    {
    }
    virtual void drawShader(Shader*)
    {
    }

    virtual void destroy();

    void setup();
    void pollInput();
    void mainLoop(int, char**);

    inline void stop()
    {
        m_running = false;
    }

private:
    SDL_Event m_sdlEvent;
    bool m_running;
};
} // namespace mode7

#endif /* GAME_HPP */
