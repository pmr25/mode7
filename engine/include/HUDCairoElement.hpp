#ifndef HUDCAIROELEMENT_HPP
#define HUDCAIROELEMENT_HPP

#include "HUDElement.hpp"

#include <cstdint>
#ifdef _WIN32
#include <cairo/cairo.h>
#else
#include <cairo.h>
#endif

namespace mode7
{
class HUDCairoElement : public HUDElement
{
public:
    HUDCairoElement(uint32_t w, uint32_t h, const std::string& name = "")
        : HUDElement(w, h, name)
        , isDirty(true)
        , mSurface(nullptr)
    {
    }

    virtual ~HUDCairoElement()
    {
        if (mSurface)
        {
            cairo_surface_destroy(mSurface);
            mSurface = nullptr;
        }
    }

    virtual void buildWidget(cairo_t*);
    virtual void init();
    virtual void show();

    void clearSurface(cairo_t*);

    inline void markDirty()
    {
        isDirty = true;
    }

    inline void markClean()
    {
        isDirty = false;
    }

protected:
    bool isDirty;
    cairo_surface_t* mSurface;
};
} // namespace mode7

#endif /* HUDCAIROELEMENT_HPP */
