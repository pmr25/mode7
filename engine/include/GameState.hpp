#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

namespace mode7
{
class Shader;

class StateBasedGame;

class GameState
{
public:
    GameState()
        : mManager(nullptr)
    {
    }

    ~GameState() = default;

    virtual void init() = 0;

    virtual void onEnter() = 0;

    virtual void onLeave() = 0;

    virtual void destroy() = 0;

    virtual void update() = 0;

    virtual void draw() = 0;

    virtual void drawShader(Shader*) = 0;
    virtual void postRender(){};

    inline StateBasedGame* getManager() const
    {
        return mManager;
    }

private:
    StateBasedGame* mManager;

    friend class StateBasedGame;
};
} // namespace mode7

#endif /* GAMESTATE_HPP */
