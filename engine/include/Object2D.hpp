#ifndef MODE7_OBJECT2D_HPP
#define MODE7_OBJECT2D_HPP

#include <glm/glm.hpp>
#include <string>
#include <vector>

namespace mode7
{

class Object2D
{
public:
    Object2D(glm::vec2 position = {0, 0}, glm::vec2 front = {1, 0})
    {
        setPosition(position);
        setFront(front);
        update();
    }
    void setPosition(glm::vec2 vec);
    void setRotation(float theta);
    void setScale(glm::vec2 vec);
    void setFront(glm::vec2 vec);
    void setVelocity(glm::vec2 vec);

    void translate(glm::vec2 vec);
    void rotate(float theta);
    void scale(glm::vec2 vec);
    void impulse(glm::vec2 vec);

    void update();
    void update(const glm::mat2&, const glm::vec2);

    [[nodiscard]] auto transform(glm::vec2 point) const -> glm::vec2;

    [[nodiscard]] auto getWorldPosition() const -> glm::vec2;
    [[nodiscard]] auto getWorldFront() const -> glm::vec2;

    [[nodiscard]] auto getPosition() const -> glm::vec2;
    [[nodiscard]] auto getFront() const -> glm::vec2;

    [[nodiscard]] auto getTheta() const -> float;

    void addChild(Object2D* child);
    void setParent(Object2D* parent);

    void setName(const std::string& name);
    auto getName() const -> std::string;

protected:
    void dirty();
    void clean();

private:
    bool mShouldUpdate{false};
    glm::vec2 mPosition{0};
    float mTheta{0.0F};
    glm::vec2 mScale{1.0F};

    glm::vec2 mFront{1, 0};
    glm::vec2 mRight{0, -1};
    glm::vec2 mVelocity{0.0F};

    glm::mat2 mTranslationMatrix{1.0F};
    glm::mat2 mRotationMatrix{1.0F};
    glm::mat2 mScaleMatrix{1.0F};
    glm::mat2 mMatrix{1.0F};
    glm::mat2 mWorldMatrix{1.0F};
    glm::vec2 mWorldPosition{0};

    Object2D* mParent;
    std::vector<Object2D*> mChildren;

    std::string mName{"unnamed object2d"};
};

} // namespace mode7

#endif // MODE7_OBJECT2D_HPP
