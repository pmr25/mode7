/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MODELLOADER_HPP
#define MODELLOADER_HPP

#include "AssetManager.hpp"
#include "Mesh.hpp"
#include "Scene.hpp"
#include "TextureImage.hpp"

#include <assimp/scene.h>
#include <memory>
#include <string>
#include <vector>

namespace mode7
{
class ModelLoader
{
public:
    static auto openUnique(const std::string&, const AssetManager&)
        -> std::unique_ptr<Scene>;

    static auto openShared(const std::string&, const AssetManager&)
        -> std::shared_ptr<Scene>;

    static auto openSharedFromMem(const std::string&, const AssetManager&)
        -> std::shared_ptr<Scene>;

    static auto openMesh(const std::string&, const AssetManager&)
        -> std::shared_ptr<Mesh>;

private:
    static void processNode(std::vector<std::shared_ptr<Mesh>>&, aiNode*,
                            const aiScene*, aiMatrix4x4, const AssetManager&);

    static auto processMesh(const std::string&, aiMesh*, const aiScene*,
                            aiMatrix4x4, const AssetManager&)
        -> std::shared_ptr<Mesh>;

    static auto loadTextures(aiMaterial*, aiTextureType, const std::string&,
                             const AssetManager&) -> std::vector<Texture*>;

    static auto openFile(Scene*, const std::string&, const AssetManager&)
        -> int;
};
} // namespace mode7

#endif /* MODELLOADER_HPP */
