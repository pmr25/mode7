#ifndef MODE7_ROLLINGAVERAGE_HPP
#define MODE7_ROLLINGAVERAGE_HPP

#include <deque>
#include <initializer_list>
#include <vector>

namespace mode7
{

class RollingAverage
{
public:
    RollingAverage(std::size_t numSlots)
    {
        mBuffer.resize(numSlots);
        mWeights = std::vector<float>(numSlots, 1.0F / numSlots);
    }

    RollingAverage(std::initializer_list<float> weights)
        : mWeights(weights)
    {
        mBuffer.resize(mWeights.size());
    }

    auto ingest(float) -> void;
    [[nodiscard]] auto compute() const -> float;

private:
    std::deque<float> mBuffer;
    std::vector<float> mWeights;
};

} // namespace mode7

#endif // MODE7_ROLLINGAVERAGE_HPP
