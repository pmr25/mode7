#ifndef MODE7_RESTORINGSCALAR_HPP
#define MODE7_RESTORINGSCALAR_HPP

#include "Clock.hpp"

#include <chrono>

namespace mode7
{

class RestoringScalar
{
public:
    RestoringScalar(
        float spring = 0.0F, float damp = 0.0F,
        float step = std::chrono::duration<float>(
                         std::chrono::milliseconds(Clock::getInterval()))
                         .count())
        : mStep(step)
        , mDampingCoefficient(damp)
        , mSpringCoefficient(spring)
    {
    }
    ~RestoringScalar() = default;

    auto applyForce(float) -> void;
    auto moveTo(float) -> void;
    auto update() -> void;

    auto getPosition() const -> float;

private:
    float mStep;
    float mDampingCoefficient;
    float mSpringCoefficient;
    float mForce{0.0F};
    float mPosition{0.0F};
    float mVelocity{0.0F};
};

} // namespace mode7

#endif // MODE7_RESTORINGSCALAR_HPP
