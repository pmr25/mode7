#ifndef DRAWABLE_HPP
#define DRAWABLE_HPP

#include "Shader.hpp"

namespace mode7
{
class Drawable
{
public:
    virtual void draw(Shader*) = 0;
};
} // namespace mode7

#endif /* DRAWABLE_HPP */
