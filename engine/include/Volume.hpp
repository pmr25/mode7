#ifndef VOLUME_HPP
#define VOLUME_HPP

#include "Plane.hpp"
#include "Shader.hpp"
#include <glm/glm.hpp>
#include <memory>

namespace mode7
{
class Volume
{
public:
    Volume()
        : m_position(0.0f)
        , m_internalPosition(0.0f)
    {
    }

    Volume(const Volume& other)
        : m_position(other.m_position)
        , m_internalPosition(other.m_internalPosition)
    {
    }

    virtual ~Volume() = default;

    virtual bool intersects(const geom::Plane*) const = 0;
    virtual bool intersects(const glm::vec3) const = 0;
    virtual std::unique_ptr<Volume> clone() const = 0;
    virtual void setPosition(glm::vec3);
    virtual void setInternalPosition(glm::vec3);
    virtual glm::vec3 getPosition() const;
    void drawDebugMesh(Shader*);

    static void setDebugMesh(const std::string&);
    static void setDebugShader(std::shared_ptr<Shader>);

protected:
    glm::vec3 m_position;
    glm::vec3 m_internalPosition;

private:
};
} // namespace mode7

#endif /* VOLUME_HPP */
