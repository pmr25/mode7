#ifndef MODE7_FONTS_HPP
#define MODE7_FONTS_HPP

#include "AssetManager.hpp"
#include "Logger.hpp"
#include <nlohmann/json.hpp>

#include <iostream>

#include <cairo/cairo.h>
#include <fstream>
#include <string>
#include <vector>

using json = nlohmann::json;

namespace mode7
{

class Fonts
{
public:
    Fonts(const Fonts& other) = delete;
    auto operator=(const Fonts& other) -> Fonts& = delete;

    virtual auto drawText(cairo_t*, const std::string&, const std::string&,
                          double, double) -> void = 0;
    virtual auto addFontFileToSystem(const std::string&) -> bool = 0;

    [[nodiscard]] auto getFontName(const std::string& nick) const -> std::string
    {
        const auto loc = mFontNameLookup.find(nick);
        if (loc == mFontNameLookup.end())
        {
            return nick;
        }

        const auto idx{(*loc).second};
        return mInstalledFonts[idx];
    }

    auto loadAllFontsJSON(const std::string& filename) -> bool
    {
        json object;

        std::ifstream fileStream(filename);
        if (!fileStream)
        {
            return false;
        }
        fileStream >> object;

        const auto& assets{AssetManager::getCurrent()};
        for (auto& fontFile : object["fonts"])
        {
            const std::string& file{fontFile["file"].get<std::string>()};
            const std::string& nick{fontFile["nick"].get<std::string>()};
            const std::string& name{fontFile["name"].get<std::string>()};

            const bool ok{addFontFileToSystem(assets.assetPath(file))};
            if (!ok)
            {
                SLOG(LG_WARN) << "cannot open font " << file;
                return false;
            }

            if (mFontNameLookup.find(nick) != mFontNameLookup.end())
            {
                SLOG(LG_WARN) << "duplicate font " << nick << " at " << file;
            }
            mFontNameLookup[nick] = mInstalledFonts.size();
            mInstalledFonts.push_back(name);
        }

        return true;
    }

    [[nodiscard]] auto getFonts() const -> const std::vector<std::string>&
    {
        return mInstalledFonts;
    }

protected:
    Fonts() = default;
    ~Fonts() = default;

    [[nodiscard]] auto formatString(const std::string& style) const
        -> std::string
    {
        // extract font nickname, replace with real name
        const auto firstSpace{style.find(' ')};
        const std::string fontName{style.substr(0, firstSpace)};
        const std::string styleInfo{style.substr(firstSpace)};
        const std::string formatted{Fonts::getFontName(fontName) + " " +
                                    styleInfo};

        return formatted;
    }

private:
    std::vector<std::string> mInstalledFonts;
    std::map<std::string, size_t> mFontNameLookup;
};

} // namespace mode7

#endif // MODE7_FONTS_HPP
