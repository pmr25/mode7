#ifndef PIPELINE_HPP
#define PIPELINE_HPP

#include "Framebuffer.hpp"
#include "Game.hpp"
#include "GameState.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

namespace mode7
{
class Pipeline
{
public:
    Pipeline(const std::string& fn = "")
        : mScreenOutput(-1)
    {
        if (fn.length() > 0)
        {
            loadPipeline(fn);
        }
    }

    ~Pipeline() = default;

    int32_t loadPipeline(const std::string&);
    uint32_t addFramebuffer(std::shared_ptr<Framebuffer>, bool = false);
    int32_t addInput(uint32_t, uint32_t);
    int32_t addTarget(uint32_t, uint32_t);
    int32_t addFlow(char, uint32_t);
    int32_t computeFlow();
    virtual bool run(Game*) const;
    virtual bool stateRun(std::shared_ptr<GameState>) const;
    void summarize() const;

protected:
    int32_t mScreenOutput;
    std::vector<std::shared_ptr<Framebuffer>> mFrameBuffers;
    std::vector<std::unique_ptr<std::vector<uint32_t>>> mInputs;
    std::vector<std::unique_ptr<std::vector<uint32_t>>> mTargets;
    std::vector<std::pair<uint32_t, char>> mFlow;
};
} // namespace mode7

#endif /* PIPELINE_HPP */
