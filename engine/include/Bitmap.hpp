#ifndef MODE7_BITMAP_HPP
#define MODE7_BITMAP_HPP

#include <cstdint>
#include <string>
#include <vector>

#include <cairo/cairo.h>

namespace mode7
{

class Bitmap
{
public:
    Bitmap() = default;
    Bitmap(const std::string& fn)
    {
        open(fn);
    }
    Bitmap(const Bitmap& other)
    {
        mData = std::move(other.mData);
        mWidth = other.mWidth;
        mHeight = other.mHeight;
        mStride = other.mStride;
    }
    ~Bitmap()
    {
        if (mCairoSurface != nullptr)
        {
            cairo_surface_destroy(mCairoSurface);
            mCairoSurface = nullptr;
        }
    }

    auto create(uint32_t, uint32_t, uint32_t) -> void;
    auto createWithData(void*, uint32_t, uint32_t, uint32_t) -> void;
    auto open(const std::string&) -> bool;
    auto debugSavePNG(const std::string&) -> cairo_status_t;
    auto getBitsPerPixel() -> uint32_t;
    auto getBytesPerPixel() const -> uint32_t;
    auto getWidth() -> uint32_t;
    auto getHeight() -> uint32_t;
    auto shrink(uint32_t, uint32_t) -> void;

    auto createCairoSurface() -> cairo_surface_t*;
    auto halveImage() -> void;
    auto bilinearResize(uint32_t, uint32_t) -> void;

private:
    auto reformat(std::vector<uint8_t>::iterator, std::vector<int>) -> void;
    std::vector<uint8_t> mData;
    std::vector<uint8_t> formattedData;
    uint32_t mWidth{0};
    uint32_t mHeight{0};
    uint32_t mStride{0};
    cairo_surface_t* mCairoSurface{nullptr};
};

} // namespace mode7

#endif // MODE7_BITMAP_HPP
