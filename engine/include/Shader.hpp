/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef SHADER_HPP
#define SHADER_HPP

#include "Material.hpp"
#include "gl.hpp"
#include <cstdint>
#include <string>
#include <vector>

#define MAT(x) &(x[0][0])
#define VEC(x) &(x[0])

namespace mode7
{
typedef enum _uniformtype_t
{
    MATRICES = 0x1,
    TEXTUREMAPS = 0x2,
    TEXTURECOORDS = 0x4
} uniformtype_t;
typedef enum _particletype_t
{
    NONE = 0,
    BILLBOARD = 1,
    PARTICLE = 2
} particletype_t;
class Object;
class Camera;

class Shader
{
public:
    Shader()
        : id(0)
        , projection(0)
        , view(0)
        , model(0)
        , diffuseTexture(0)
        , uv_tile(0)
        , uvTransform(0)
        , mAmbientLoc(0)
        , mDiffuseLoc(0)
        , mEmissionLoc(0)
        , mSpecularLoc(0)
        , mShininessLoc(0)
        , mFresnelLoc(0)
        , m_name("Unnamed Shader")
    {
    }

    Shader(const std::string& vfn, const std::string& ffn)
        : Shader()
    {
        int err;
        err = open(vfn, ffn);
        if (err > 0)
        {
            exit(1);
        }
    }

    ~Shader() = default;

    auto open(const std::string&, const std::string&) -> int;
    auto open(const std::string&, const std::string&, const std::string&) -> int;
    void cacheCameraMatrices();
    void cacheLocations();
    void cacheLocations(uint32_t);

    void use() const;
    void onlyUse() const;
    void setMaterial(const Material&);
    void setModel(Object*);
    void setVec3(const std::string&, float*);

    void setCameraParams(Camera*);
    void setAlwaysUp(GLint);
    void setParticleType(particletype_t);
    void setBlurAmount(float);
    void setAlphaThreshold(float);

    inline GLuint pid() const
    {
        return id;
    }

    inline bool isReady() const
    {
        return id > 0;
    }

    inline void setName(const std::string& str)
    {
        m_name = str;
    }

    inline const std::string& getName() const
    {
        return m_name;
    }

    inline GLuint getUniformLocation(const std::string& name) const
    {
        return glGetUniformLocation(id, name.c_str());
    }

private:
    GLint id;
    GLint projection;
    GLint view;
    GLint model;
    GLint diffuseTexture;
    GLint uv_tile;
    GLint uvTransform;
    GLint camera_params_loc;
    GLint always_up_loc;
    GLint particle_type_loc;
    GLint blur_amt_loc;
    GLint alpha_threshold_loc;
    GLint particle_color_loc;

    GLint mAmbientLoc;
    GLint mDiffuseLoc;
    GLint mEmissionLoc;
    GLint mSpecularLoc;
    GLint mShininessLoc;
    GLint mFresnelLoc;
    GLint mDiffuseFloorLoc;

    std::string m_name;

    std::vector<GLint> m_diffuseMaps;
    std::vector<GLint> m_normalMaps;
    std::vector<GLint> m_bumpMaps;
    std::vector<GLint> m_specularMaps;
};
} // namespace mode7

#endif /* SHADER_HPP */
