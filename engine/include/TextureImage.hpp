/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef TEXTUREIMAGE_HPP
#define TEXTUREIMAGE_HPP

#include "Texture.hpp"
#include "gl.hpp"
#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <cstdint>
#include <string>
#include <vector>

namespace mode7
{
class TextureImage : public Texture
{
public:
    TextureImage();
    virtual ~TextureImage() = default;

    virtual void open(const std::string&, TexType, bool preserveData = false);
    int open(const std::string&);
    void loadIntoMemory(const std::string&);
    void fill(void*, int, int);
    void fill(void*, GLenum, int, int);
    void update(void*, GLint, GLint, GLsizei, GLsizei, GLenum);
    void addSrcRect(int32_t, int32_t, int32_t, int32_t);
    void useSrcRect(uint32_t);
    uint32_t getId() const;
    TexType getType() const;
    void destroy();

    const uint8_t* getData() const override
    {
        if (m_pixels == nullptr)
        {
            return nullptr;
        }
        return (const uint8_t*)m_pixels->pixels;
    }

private:
    SDL_Surface* m_pixels;
    std::vector<SDL_Rect> m_srcRects;
};

} // namespace mode7

#endif /* TEXTUREIMAGE_HPP */
