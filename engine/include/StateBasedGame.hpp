#ifndef STATEBASEDGAME_HPP
#define STATEBASEDGAME_HPP

#include "GameState.hpp"

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace mode7
{
class StateBasedGame
{
public:
    StateBasedGame()
        : mIsRunning(true)
    {
    }

    ~StateBasedGame() = default;

    void addState(const std::string&, std::shared_ptr<GameState>);
    void setCurrentState(const std::string& key);
    std::shared_ptr<GameState> getState(const std::string& key);
    void quit();

    void pollInput();
    void mainLoop(int, char**);

private:
    bool mIsRunning;
    std::map<std::string, uint32_t> mStateLookup;
    std::vector<std::shared_ptr<GameState>> mStates;
    std::shared_ptr<GameState> mCurrentState;
    SDL_Event mSdlEvent;
};
} // namespace mode7

#endif /* STATEBASEDGAME_HPP */
