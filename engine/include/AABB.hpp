#ifndef AABB_HPP
#define AABB_HPP

#include "Plane.hpp"
#include "Vertex.hpp"
#include "Volume.hpp"

#include <vector>

namespace mode7
{
class AABB : public Volume
{
public:
    AABB()
        : Volume()
        , m_length(0)
        , m_width(0)
        , m_height(0)
        , m_faces(nullptr)
    {
    }

    AABB(const AABB& other)
        : Volume(other)
        , m_length(other.m_length)
        , m_width(other.m_width)
        , m_height(other.m_height)
    {
        m_faces = new geom::Plane[6];
        for (int i = 0; i < 6; ++i)
        {
            m_faces[i] = other.m_faces[i];
        }
    }

    ~AABB()
    {
        if (m_faces != nullptr)
        {
            delete[] m_faces;
            m_faces = nullptr;
        }
    }

    void fitVertices(const std::vector<Vertex>&);

    void setDimensions(float, float, float);

    virtual bool intersects(const geom::Plane*) const;
    virtual bool intersects(const glm::vec3) const;

    virtual std::unique_ptr<Volume> clone() const
    {
        return std::make_unique<AABB>(*this);
    }

private:
    float m_length;
    float m_width;
    float m_height;
    geom::Plane* m_faces;
};
} // namespace mode7

#endif /* AABB_HPP */