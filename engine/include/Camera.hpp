/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Object.hpp"
#include "Plane.hpp"

#include <iostream>
#include <memory>
#include <vector>

namespace mode7
{
typedef enum _frustumplane_t
{
    PLANE_TOP = 0,
    PLANE_BOTTOM = 1,
    PLANE_LEFT = 2,
    PLANE_RIGHT = 3,
    PLANE_NEAR = 4,
    PLANE_FAR = 5
} frustumplane_t;

class Volume;

class Camera
{
public:
    Camera()
        : m_width(0.0f)
        , m_height(0.0f)
        , m_near(0.0f)
        , m_far(0.0f)
        , m_fov(70.0f)
        , m_viewMatrix(1.0f)
        , m_projMatrix(1.0f)
        , m_object()
    {
    }

    void create(float, float, float);
    void create(float, float, float, float, float);
    void lookAt(glm::vec3 eye, glm::vec3 target);
    void defineFrustum(glm::vec3, glm::vec3, glm::vec3);
    void updateFrustum();
    bool isInsideFrustum(const Volume*) const;
    float getNear() const;
    float getFar() const;
    float getFov() const;

    inline Object& getObject()
    {
        return m_object;
    }

    inline Object* getObjectPtr()
    {
        return &m_object;
    }

    inline glm::mat4 getView() const
    {
        return m_viewMatrix * glm::inverse(m_object.getWorldMatrix());
    }

    void updateView()
    {
        // m_viewMatrix = glm::inverse(m_object.getWorldMatrix());
    }

    inline glm::mat4 getProjection() const
    {
        return m_projMatrix;
    }

    inline void setFOV(float fov)
    {
        if (m_height == 0.0F)
        {
            return;
        }

        const float aspect{m_width / m_height};
        assert(aspect != 0.0F);

        m_fov = fov;
        m_projMatrix =
            glm::perspective(glm::radians(fov), aspect, m_near, m_far);
    }

    static inline void setCurrent(std::shared_ptr<Camera> ptr)
    {
        m_current = ptr;
    }

    static inline Camera& getCurrent()
    {
        return *m_current;
    }

    static inline std::shared_ptr<Camera> getCurrentShPtr()
    {
        return m_current;
    }

    static inline std::shared_ptr<Camera>
    createAndMakeCurrent(float fov, float nr, float fr)
    {
        std::shared_ptr<Camera> cam = std::make_shared<Camera>();
        cam->create(fov, nr, fr);
        setCurrent(cam);
        return cam;
    }

private:
    float m_width;
    float m_height;
    float m_near;
    float m_far;
    float m_fov;
    glm::mat4 m_viewMatrix;
    glm::mat4 m_projMatrix;
    Object m_object;
    std::vector<geom::Plane> m_frustum;

    static std::shared_ptr<Camera> m_current;
};
} // namespace mode7

#endif /* CAMERA_HPP */
