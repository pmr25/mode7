#ifndef MODE7_ENTITY_HPP
#define MODE7_ENTITY_HPP

#include <glm/glm.hpp>

#include "UUID.hpp"

namespace mode7
{

template <int D>
class Entity
{
public:
    Entity(const glm::vec<D, float>& position = glm::vec<D, float>(0.0F),
           const glm::vec<D, float>& dimensions = glm::vec<D, float>(0.0F))
        : mPosition(position)
        , mDimensions(dimensions)
        , mUUID(uuid::generate_uuid_v4())
    {
    }
    ~Entity()
    {
    }
    auto getPosition() const -> const glm::vec<D, float>&
    {
        return mPosition;
    }
    auto getDimensions() const -> const glm::vec<D, float>&
    {
        return mDimensions;
    }
    auto getUUID() const -> const std::string&
    {
        return mUUID;
    }

private:
    glm::vec<D, float> mPosition;
    glm::vec<D, float> mDimensions;
    std::string mUUID;
};

} // namespace mode7

#endif // MODE7_ENTITY_HPP
