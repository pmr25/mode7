#ifndef CONTROLJOB_HPP
#define CONTROLJOB_HPP

#include "Job.hpp"

#include <cstdint>

#define CONTROLJOB_QUIT 0x1

namespace mode7
{
class ControlJob : public Job
{
public:
    ControlJob(uint8_t command)
        : Job()
        , mCommand(command)
    {
    }

    virtual ~ControlJob() = default;

    virtual void run()
    {
    }

    uint8_t getCommand()
    {
        return mCommand;
    }

private:
    uint8_t mCommand;
};
} // namespace mode7

#endif /* CONTROLJOB_HPP */
