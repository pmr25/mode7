#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "gl.hpp"

#include <glm/glm.hpp>
#include <string>

namespace mode7
{
enum class TexType
{
    DIFFUSE,
    SPECULAR,
    NORMAL,
    BUMP
};
enum class TexFilter
{
    NEAREST = GL_NEAREST,
    LINEAR = GL_LINEAR,
    LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR,
    NEAREST_MIPMAP_NEAREST = GL_NEAREST_MIPMAP_NEAREST
};

class Texture
{
public:
    Texture()
        : name("unnamed texture")
        , uvTransform(1.0f, 1.0f, 0.0f, 0.0f)
    {
    }
    virtual ~Texture() = default;

    bool isReady() const
    {
        return mIsReady;
    }

    void setReady(bool val)
    {
        mIsReady = val;
    }

    GLuint getId() const
    {
        return id;
    }

    TexType getType() const
    {
        return type;
    }

    void setUVTransform(float su, float sv, float ou, float ov)
    {
        uvTransform = glm::vec4(su, sv, ou, ov);
    }

    [[nodiscard]] auto getUVTransform() const -> const glm::vec4&
    {
        return uvTransform;
    }

    uint32_t getWidth() const
    {
        return width;
    }

    uint32_t getHeight() const
    {
        return height;
    }

    std::string getName() const
    {
        return name;
    }

    void setName(const std::string& n)
    {
        name = n;
    }

    virtual const uint8_t* getData() const
    {
        return nullptr;
    }

    void setType(TexType t)
    {
        type = t;
    }

    void setFilter(TexFilter nv, TexFilter fv)
    {
        mNearFilter = nv;
        mFarFilter = fv;
    }

    auto generateMipmap(bool) -> void;
    auto shouldGenerateMipmap() -> bool;

    auto genGPUTexture(const void* data, GLuint width, GLuint height,
                       GLuint mode) -> void;
    auto genGPUTexture() -> void;

protected:
    TexType type;
    GLuint id{0};
    TexFilter mNearFilter{TexFilter::NEAREST};
    TexFilter mFarFilter{TexFilter::LINEAR_MIPMAP_LINEAR};
    std::string name;
    uint32_t width{0};
    uint32_t height{0};
    bool mIsReady{false};
    bool mGenMipmap{true};
    glm::vec4 uvTransform;

    friend class TextureAtlas;
};
} // namespace mode7

#endif /* TEXTURE_HPP */
