/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef ORBITCONTROLS_HPP
#define ORBITCONTROLS_HPP

#include "Camera.hpp"
#include "Controls.hpp"

#include <cstdint>

#define ORBITCONTROLS_INPUT_KEYBOARD 0x1
#define ORBITCONTROLS_INPUT_MOUSE 0x2

namespace mode7
{
class OrbitControls : public Controls
{
public:
    OrbitControls()
        : Controls()
        , m_ranges{false, false, false}
        , m_sensitivity{0.5f, 0.5f, 0.5f}
        , xdir(-1)
        , ydir(-1)
    {
        Object::addChild(&m_arm);
        setDistance(300.0F);
        setInput(ORBITCONTROLS_INPUT_MOUSE, 1);
    }

    void setDistance(float);
    void setPitchRange(float, float);
    void setYawRange(float, float);
    void setRollRange(float, float);

    inline void attachCamera()
    {
        m_arm.addChild(&Camera::getCurrent().getObject());
    }

    inline void setInput(uint8_t bit, uint8_t stat)
    {
        m_inputMask = m_inputMask | (bit * stat);
    }

    virtual void updateControls();
    virtual glm::mat4 getControlMatrix();

private:
    Object m_arm;
    Object* mTarget{nullptr};
    bool m_ranges[3];
    float m_pitchRange[2];
    float m_yawRange[2];
    float m_rollRange[2];
    float m_sensitivity[3];
    int8_t xdir;
    int8_t ydir;
};
} // namespace mode7

#endif /* ORBITCONTROLS_HPP */
