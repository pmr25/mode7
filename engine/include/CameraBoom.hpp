#ifndef MODE7_CAMERABOOM_HPP
#define MODE7_CAMERABOOM_HPP

#include "Camera.hpp"
#include "DampedJoint.hpp"
#include "DampedScalar.hpp"
#include "Object.hpp"

#include <glm/glm.hpp>
#include <memory>

namespace mode7
{

class CameraBoom : public Object
{
public:
    using Object::update;

    CameraBoom()
        : Object("Camera Boom." + getIndex())
        , mSlide(0.0F, 0.99F)
        , mJoint(glm::vec3(2, 0.4, 0.5), glm::vec3(40, 4, 20))
    {
    }

    virtual ~CameraBoom() = default;

    auto setCoefficients(float, float, float, float, float) -> void;

    auto setCamera(std::shared_ptr<Camera>, glm::vec3, glm::vec3) -> void;
    auto setDive(float) -> void;
    auto setLag(float) -> void;
    auto setSlide(float) -> void;
    auto setFov(float) -> void;
    auto applyForce(float, float, float) -> void;

    auto getCamera() -> std::shared_ptr<Camera>
    {
        return mCamera;
    }

    auto update(const glm::mat4&, const glm::quat&, bool) -> void override;

private:
    static auto getIndex() -> std::string;
    // bounds values
    float mDiveFactor{1.0F};
    float mLagFactor{1.0F};
    float mSlideFactor{0.0F};
    float mDefaultFov{30.0F};
    float mFovFactor{0.0F};

    // control values
    float mFovMul{0.0F};
    float mDiveMul{0.0F};
    float mLagMul{0.0F};
    float mSlideMul{0.0F};

    DampedScalar mSlide;
    DampedJoint mJoint;

    glm::vec3 mCameraPosition = glm::vec3(0.0F);
    glm::vec3 mCameraLookAt = glm::vec3(0.0F);
    std::shared_ptr<Camera> mCamera;
};

} // namespace mode7

#endif // MODE7_CAMERABOOM_HPP
