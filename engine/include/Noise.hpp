#ifndef NOISE_HPP
#define NOISE_HPP

namespace mode7
{
class Noise
{
public:
    static float perlin2D(float, float, float, int);
};
} // namespace mode7

#endif /* NOISE_HPP */
