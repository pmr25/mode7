#ifndef MODE7_DAMPEDSCALAR_HPP
#define MODE7_DAMPEDSCALAR_HPP

#include "Clock.hpp"

#include <chrono>
#include <cstdint>

namespace mode7
{

class DampedScalar
{
public:
    DampedScalar(
        float current, float coeff,
        float step = std::chrono::duration<float>(
                         std::chrono::milliseconds(Clock::getInterval()))
                         .count())
        : mDampingCoefficient(coeff)
        , mStep(step)
    {
        reset(current);
    }

    ~DampedScalar() = default;

    auto setTarget(float) -> void;
    auto reset(float) -> void;
    auto update() -> void;

    [[nodiscard]] auto getPosition() const -> float;

private:
    float mVelocity{0.0F};
    float mCurrent{0.0F};
    float mTarget{0.0F};
    float mDampingCoefficient{0.0F};
    float mStep{0.0F};
};

} // namespace mode7

#endif // MODE7_DAMPEDSCALAR_HPP
