#ifndef TEXTUREATLAS_HPP
#define TEXTUREATLAS_HPP

#include "TextureImage.hpp"
#include "gl.hpp"
#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace mode7
{
class TextureAtlas
{
public:
    TextureAtlas();

    int32_t open(const std::string&, const std::string&);
    std::shared_ptr<Texture> getTexture(const std::string&);
    // glm::vec4 getUVTransform(const std::string&);

    inline std::vector<std::string> getKeys()
    {
        return m_keyIndirection;
    }

private:
    std::vector<glm::vec4> m_bbox;
    std::vector<std::string> m_keyIndirection;
    std::vector<std::shared_ptr<Texture>> m_subTextures;
    std::map<std::string, uint32_t> m_lookup;
    std::shared_ptr<TextureImage> m_atlas;
};
} // namespace mode7

#endif /* TEXTUREATLAS_HPP */
