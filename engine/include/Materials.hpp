#ifndef MODE7_MATERIALS_HPP
#define MODE7_MATERIALS_HPP

#include "Material.hpp"

#include <map>
#include <string>
#include <vector>

namespace mode7
{
class Materials
{
public:
    void addMaterial(Material);
    void addMaterial(const std::string&, Material);
    Material* getMaterial(const std::string&);

private:
    std::map<std::string, size_t> mLookup;
    std::vector<Material> mMaterials;
};
} // namespace mode7

#endif // MODE7_MATERIALS_HPP
