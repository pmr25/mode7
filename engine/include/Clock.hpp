/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef CLOCK_HPP
#define CLOCK_HPP

#include "flicks.h"
#include <cassert>
#include <cstdint>
#include <functional>

#define INTERVAL 20 // 20 ms/tick or 50 ticks/second
#define delta_t uint32_t
#define wall_t uint64_t

namespace mode7
{
class Clock
{
public:
    static void start();

    static void tick();

    static void lagTick();

    static auto delta() -> delta_t;

    static auto lagging() -> bool;

    static auto extrapolate() -> double;

    static auto fps() -> double;

    static auto millis() -> wall_t;

    static void enableMetrics(delta_t);

    constexpr static auto getInterval() -> delta_t
    {
        return INTERVAL;
    }

    constexpr static auto millisToTicks(delta_t millis) -> delta_t
    {
        assert(getInterval() > 0);
        return millis / getInterval();
    }

    static void addTrigger(delta_t, std::function<void()>);
};
} // namespace mode7

#endif /* CLOCK_HPP */
