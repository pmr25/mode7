/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MESHDATA_HPP
#define MESHDATA_HPP

#include "Vertex.hpp"

#include <cstdint>
#include <vector>

namespace mode7
{
class MeshData
{
public:
    MeshData() = default;

    void setVertices(const std::vector<Vertex>& vertices)
    {
        m_vertices = vertices;
        mNumVertices = vertices.size();
    }

    void setIndices(const std::vector<unsigned int>& indices)
    {
        mIndices = indices;
        mNumIndices = indices.size();
    }

    auto getVertices() -> std::vector<Vertex>&
    {
        return m_vertices;
    }

    auto getIndices() -> std::vector<unsigned int>&
    {
        return mIndices;
    }

    [[nodiscard]] auto hasElements() const -> bool
    {
        return mNumIndices > 0;
    }

    [[nodiscard]] auto getNumTriangles() const -> uint32_t
    {
        return mNumIndices;
    }

    auto discard() -> void;

private:
    std::size_t mNumVertices{0};
    std::size_t mNumIndices{0};
    std::vector<Vertex> m_vertices;
    std::vector<unsigned int> mIndices;
};
} // namespace mode7

#endif /* MESHDATA_HPP */
