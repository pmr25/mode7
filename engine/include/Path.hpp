#ifndef MODE7_PATH_HPP
#define MODE7_PATH_HPP

#include "Line.hpp"
#include "ParametricCurve.hpp"

#include <vector>

#include <glm/glm.hpp>

namespace mode7
{

class Path
{
public:
    //    Path() = default;
    //    Path(const Path& other)
    //    {
    //        std::cout << "here" << std::endl;
    //        mStepSize = other.mStepSize;
    //        mStartPoint = other.mStartPoint;
    //        mStartHeading = other.mStartHeading;
    //        mWaypoints = other.mWaypoints;
    //        mHeadings = other.mHeadings;
    //        mLengths = other.mLengths;
    //    }
    //    Path& operator=(const Path& other)
    //    {
    //        return *this = Path(other);
    //    }
    //    Path(Path&& other) noexcept
    //        : mStepSize(other.mStepSize)
    //        , mStartPoint(other.mStartPoint)
    //        , mStartHeading(other.mStartHeading)
    //        , mWaypoints(other.mWaypoints)
    //        , mHeadings(other.mHeadings)
    //        , mLengths(other.mLengths)
    //    {
    //    }
    //    ~Path() = default;
    void createFromCurves(std::vector<ParametricCurve<3>>&);
    void createFromCurves(std::vector<std::unique_ptr<ParametricCurve<3>>>&);
    std::pair<glm::mat4, glm::quat> interpolate(double) const;

private:
    void buildMatrices(glm::vec3, glm::vec3);

    float mStepSize{0.25F};
    glm::vec3 mStartPoint;
    glm::quat mStartHeading;
    std::vector<glm::vec3> mWaypoints;
    std::vector<glm::quat> mHeadings;
    std::vector<float> mLengths;
};

} // namespace mode7

#endif // MODE7_PATH_HPP
