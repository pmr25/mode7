#ifndef FONTS_HPP
#define FONTS_HPP

#ifdef __linux__

#include "Fonts.hpp"
#include "Logger.hpp"
#ifdef _WIN32
#include <gtk/cairo/cairo-ft.h>
#elif defined __linux__
#include <cairo/cairo-ft.h>
#endif
#include <cassert>
#include <cstdint>
#include <freetype/ftglyph.h>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace mode7
{
class FontMetrics
{
public:
    explicit FontMetrics(FT_Face face)
    {
        gatherMetrics(face);
    }

    auto gatherMetrics(FT_Face face) -> void
    {
        int err;
        int index = FT_Get_Char_Index(face, 65);

        err = FT_Load_Glyph(face, index, FT_LOAD_TARGET_NORMAL);
        if (err != 0)
        {
            SLOG(ERROR) << "cannot load glyph";
        }

        auto metrics{face->glyph->metrics};
        mOffsetX = -metrics.horiBearingX;
        mOffsetY = -metrics.horiBearingY;

        SLOG(INFO) << "offset = " << mOffsetX << "," << mOffsetY;
    }

    [[nodiscard]] auto getOffset() const -> std::pair<double, double>
    {
        const auto adj{0.05};
        return {double(mOffsetX) * adj, double(-mOffsetY) * adj};
    }

private:
    int mOffsetX{0};
    int mOffsetY{0};
};
class CairoFontFace
{
public:
    CairoFontFace(const std::string& name = "", FT_Face face = nullptr,
                  FontMetrics* metrics = nullptr)
        : mName(name)
        , mMetrics(metrics)
        , mFace(face)
    {
        if (face != nullptr)
        {
            cf = cairo_ft_font_face_create_for_ft_face(face, 0);
        }
    }
    CairoFontFace(CairoFontFace& old)
    {
        cf = old.cf;
        old.cf = nullptr;

        mMetrics = old.mMetrics;
        old.mMetrics = nullptr;
    }
    auto operator=(const CairoFontFace& other) -> CairoFontFace&
    {
        mName = other.mName;
        mFace = other.mFace;
        mMetrics = other.mMetrics;

        cf = cairo_ft_font_face_create_for_ft_face(mFace, 0);

        return *this;
    }
    ~CairoFontFace()
    {
        if (cf != nullptr)
        {
            cairo_font_face_destroy(cf);
            cf = nullptr;
        }

        mMetrics = nullptr;
    }

    auto operator*() -> cairo_font_face_t*
    {
        return cf;
    }

    operator bool()
    {
        return cf != nullptr && mMetrics != nullptr;
    }

    auto getOffset() -> std::pair<double, double>
    {
        assert(mMetrics != nullptr);
        return std::pair<double, double>{mMetrics->getOffset()};
    }

private:
    std::string mName;
    cairo_font_face_t* cf{nullptr};
    FontMetrics* mMetrics;
    FT_Face mFace;
};

class FontsBackendPango : public Fonts
{
public:
    FontsBackendPango(FontsBackendPango const&) = delete;
    void operator=(FontsBackendPango const&) = delete;

    static auto getFontsInstance() -> FontsBackendPango*
    {
        static FontsBackendPango instance;
        return &instance;
    }

    auto addFont(const std::string&, const std::string&) -> bool;
    auto addFontFileToSystem(const std::string& fn) -> bool override;
    auto getCairoFontFace(const std::string&) -> CairoFontFace;

    operator bool()
    {
        return mLibrary != nullptr;
    }

    auto drawText(cairo_t*, const std::string&, const std::string&, double,
                  double) -> void override;

private:
    FontsBackendPango()
    {
        auto status{FT_Init_FreeType(&mLibrary)};
        if (status != 0)
        {
            mLibrary = nullptr;
        }
    }

    ~FontsBackendPango()
    {
        for (auto& face : mFaces)
        {
            FT_Done_Face(face);
        }
        FT_Done_FreeType(mLibrary);
    }

    FT_Library mLibrary{nullptr};
    std::vector<FT_Face> mFaces;
    std::vector<FontMetrics> mMetrics;
    std::map<std::string, uint32_t> mFaceLookup;
};
} // namespace mode7

#endif /* __linux__ */

#endif /* FONTS_HPP */