#ifndef SCRIPTING_HPP
#define SCRIPTING_HPP

extern "C"
{
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
}

#include <cassert>
#include <string>

namespace mode7
{
class Scripting
{
public:
    Scripting()
    {
        mState = luaL_newstate();
        luaL_openlibs(mState);
    }

    ~Scripting()
    {
        assert(mState);
        lua_close(mState);
    }

    int loadScript(const std::string&);
    lua_State* getLuaState();

private:
    lua_State* mState;
};
} // namespace mode7

#endif /* SCRIPTING_HPP */
