/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef BBOX_HPP
#define BBOX_HPP

#include "Volume.hpp"

#include <glm/glm.hpp>

namespace mode7
{
class Plane;

class BBox : public Volume
{
public:
    BBox(glm::vec3 _pos = glm::vec3(0.0f), glm::vec3 _dim = glm::vec3(0.0f))
        : pos(_pos)
        , dim(_dim)
    {
    }

    virtual ~BBox() = default;

    virtual bool intersects(const BBox&) const;
    virtual bool intersects(const glm::vec3) const;
    virtual bool intersects(const geom::Plane*) const;

    virtual std::unique_ptr<Volume> clone() const
    {
        return std::make_unique<BBox>(*this);
    }

    glm::vec3 pos;
    glm::vec3 dim;
};
} // namespace mode7

#endif /* BBOX_HPP */
