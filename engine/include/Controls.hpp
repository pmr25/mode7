/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef CONTROLS_HPP
#define CONTROLS_HPP

#include "Object.hpp"

#include <cstdint>

namespace mode7
{
class Controls : public Object
{
public:
    Controls()
        : Object()
        , m_inputMask(0)
    {
    }
    virtual ~Controls() = default;

    virtual glm::mat4 getControlMatrix();
    virtual void updateControls();

protected:
    uint8_t m_inputMask;
};
} // namespace mode7

#endif /* CONTROLS_HPP */
