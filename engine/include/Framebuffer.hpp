/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP

#include "Shader.hpp"
#include "gl.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mode7
{
typedef enum _target_t
{
    TEXTURE,
    RENDERBUFFER
} target_t;

class Framebuffer
{
public:
    Framebuffer()
        : mHasDepthStencil(false)
        , m_fbo(0)
        , m_rbo(0)
        , m_fb_tex(0)
        , m_vao(0)
        , m_width(0)
        , m_height(0)
        , m_samples(0)
        , m_tp(0)
        , m_colortex_loc(0)
        , mClearColor{0.0f, 0.0f, 0.0f}
        , mName("Unnamed Framebuffer")
    {
    }

    ~Framebuffer()
    {
        if (m_target == TEXTURE)
        {
            glDeleteTextures(1, &m_fb_tex);
            glDeleteTextures(1, &m_fb_dep);
        }
        else if (m_target == RENDERBUFFER)
        {
            glDeleteFramebuffers(1, &m_fbo);
            glDeleteRenderbuffers(1, &m_rbo);
        }

        glDeleteVertexArrays(1, &m_vao);
    }

    int targetTexture(uint32_t, uint32_t, bool = true);
    int targetRenderbuffer(uint32_t, uint32_t, bool = true);
    int targetMultisampledRenderBuffer(uint32_t, uint32_t, uint32_t,
                                       bool = true);
    void setShader(std::shared_ptr<Shader>);
    void setForcedShader(std::shared_ptr<Shader>);

    void appendTextureLocation(const std::string&);

    inline Shader* getScreenShader()
    {
        return mShader.get();
    }

    inline Shader* getForcedShader()
    {
        return mForcedShader.get();
    }

    inline GLuint getTexGL()
    {
        return m_fb_tex;
    }

    inline const std::string& getName() const
    {
        return mName;
    }

    inline target_t getTarget() const
    {
        return m_target;
    }

    void begin();
    void resetTextureFeed();
    void feedTexture(GLint, GLuint);
    void feedTexture(GLint);
    void preDraw();
    void draw();
    void draw(Framebuffer*);
    void blitTo(GLint);
    void destroy();
    void setClearColor(float, float, float);

private:
    bool mHasDepthStencil;
    target_t m_target;
    GLuint m_fbo;
    GLuint m_rbo;
    GLuint m_fb_tex;
    GLuint m_fb_dep;
    GLuint m_vao;
    uint32_t m_width;
    uint32_t m_height;
    uint32_t m_samples;
    uint32_t m_tp;
    GLuint m_colortex_loc;
    GLuint m_dstex_loc;
    std::shared_ptr<Shader> mShader;
    std::shared_ptr<Shader> mForcedShader;
    std::vector<std::string> mTextureLocationNames;
    std::vector<GLint> m_textureLocations;
    float mClearColor[3];
    std::string mName;
};
} // namespace mode7

#endif /* FRAMEBUFFER_HPP */
