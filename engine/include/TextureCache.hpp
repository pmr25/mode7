#ifndef MODE7_TEXTURECACHE_HPP
#define MODE7_TEXTURECACHE_HPP

#include "TextureImage.hpp"

#include <future>
#include <memory>
#include <string>
#include <unordered_map>

namespace mode7
{

class TextureCache
{
public:
    auto open(const std::string& filename, TexType type) -> TextureImage*;
    auto finish() -> void;

private:
    auto convertFuture(const std::string& name) -> void;
    std::unordered_map<std::string, std::shared_ptr<TextureImage>> mCache;
    std::unordered_map<std::string, std::future<TextureImage>> mFutures;
};

} // namespace mode7

#endif // MODE7_TEXTURECACHE_HPP
