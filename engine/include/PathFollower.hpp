#ifndef MODE7_PATHFOLLOWER_HPP
#define MODE7_PATHFOLLOWER_HPP

#include "Object.hpp"
#include "Path.hpp"

namespace mode7
{

class PathFollower : public Object
{
public:
    void moveTo(float);
    void update() override;
    void setPath(const Path&);

private:
    Path mPath;
};

} // namespace mode7

#endif // MODE7_PATHFOLLOWER_HPP
