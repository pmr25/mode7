/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef UTIL_HPP
#define UTIL_HPP

#include "glmincludes.hpp"
#include "glmops.hpp"

#include <assimp/Importer.hpp>

#include <cmath>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#ifdef _BULLET_PHYSICS
#if __linux__
#include <bullet/btBulletDynamicsCommon.h>
#elif defined _WIN32
#include <bullet3/btBulletDynamicsCommon.h>
#endif
#endif /* _BULLET_PHYSICS */

#ifdef _DEBUG
#define ZERO_NAN(x) (std::isnan(x)) ? (0) : (x)
#else
#define ZERO_NAN(x) (x)
#endif

#define FORMAT_EULER(x)                                                        \
    (x - (360.0F * (fabsf(x) >= 360.0f) * int(floorf(x / 360.0F))) +           \
     (360.0F * (x < 0.0F) * int(ceilf(fabsf(x) / 360.0F))))

namespace mode7
{
class GLScreen;

class Util
{
public:
    static const std::string getInstallDir();

    static auto arbitraryQuadBilinear(glm::vec3 point, glm::vec2 qa,
                                      glm::vec2 qb, glm::vec2 qc, glm::vec2 qd)
        -> glm::vec3;
    static auto arbitraryQuadBilinear(glm::vec3 point, glm::vec3 qa,
                                      glm::vec3 qb, glm::vec3 qc, glm::vec3 qd)
        -> glm::vec3;
    static auto mapCoordsToUnitSquare(glm::vec2 point, glm::vec2 a,
                                      glm::vec2 qb, glm::vec2 qc, glm::vec2 qd)
        -> glm::vec2;
    static auto unitBilinear(float x, float y, float a, float b, float c,
                             float d) -> float;

    static auto computeIncline(glm::vec3 dir, glm::vec3 basisU,
                               glm::vec3 basisV) -> float;

    static float formatRad(float);

    static inline float deg(float rad)
    {
        return (float)(rad * 180.0 / M_PI);
    }

    static inline float rad(float deg)
    {
        return (float)(deg * M_PI / 180.0);
    }

    static inline glm::vec3 deg(glm::vec3 v)
    {
        return glm::vec3(deg(v.x), deg(v.y), deg(v.z));
    }

    static inline glm::vec3 rad(glm::vec3 v)
    {
        return glm::vec3(rad(v.x), rad(v.y), rad(v.z));
    }

    static inline glm::vec4 deg(glm::vec4 v)
    {
        return glm::vec4(deg(v.x), deg(v.y), deg(v.z), deg(v.w));
    }

    static inline glm::vec4 rad(glm::vec4 v)
    {
        return glm::vec4(rad(v.x), rad(v.y), rad(v.z), rad(v.w));
    }

    static inline float recoverAngleAxis(glm::quat q)
    {
        glm::vec3 xyz = glm::vec3(q.x, q.y, q.z);
        return 2 * atan2f(glm::length(xyz), q.w);
    }

    static inline glm::vec4 toPoint(glm::vec3 v)
    {
        return glm::vec4(v.x, v.y, v.z, 1.0f);
    }

    static inline glm::vec4 toVector(glm::vec3 v)
    {
        return glm::vec4(v.x, v.y, v.z, 0.0f);
    }

    static inline glm::vec3 trimW(glm::vec4 v)
    {
        return glm::vec3(v.x, v.y, v.z);
    }

    static std::string timestamp();

    static void seed();
    static int randInt(int min, int max)
    {
        return (rand() % (max - min + 1)) + min;
    }

    static constexpr inline glm::vec3 xAxis()
    {
        return glm::vec3(1, 0, 0);
    }

    static constexpr inline glm::vec3 yAxis()
    {
        return glm::vec3(0, 1, 0);
    }

    static constexpr inline glm::vec3 zAxis()
    {
        return glm::vec3(0, 0, 1);
    }

    static float angleBetween(glm::vec3 a, glm::vec3 b)
    {
        const float dot = glm::dot(a, b);
        const float cross = glm::length(glm::cross(a, b));
        const float mag = glm::length(a) * glm::length(b);
        return (acosf(dot / mag)) * ((cross <= 0) ? 1.0F : -1.0F);
    }

#ifdef _BULLET_PHYSICS
    static inline btVector3 toModelSpace(btRigidBody* rb, btVector3 v)
    {
        // grabs quaternion, creates transform with quaternion at origin,
        // transforms vector
        return btTransform(rb->getOrientation())(v);
    }

    static inline btVector3 toWorldSpace(btRigidBody* rb, btVector3 v)
    {
        return btTransform(rb->getOrientation().inverse())(v);
    }
#endif

#ifdef _BULLET_PHYSICS
    static btVector3 toBullet(glm::vec3);
    static btVector3 toBulletRaw(glm::vec3);
    static glm::vec3 fromBullet(btVector3);
    static glm::vec3 fromBulletRaw(btVector3);
    static std::string btVector3ToStr(btVector3);
#endif

    static float lerp(float, float, float);
    static float map(float, float, float, float, float);
    static glm::mat4 fromAi(const aiMatrix4x4&);
    static std::pair<int, int> getMonitorRes();
    static int getMonitorScale();
    static void split(std::vector<std::string>&, std::string, std::string);

    static glm::mat4 ai2glm(aiMatrix4x4 ai)
    {
        glm::mat4 out;

        for (uint32_t r = 0; r < 4; ++r)
        {
            for (uint32_t c = 0; c < 4; ++c)
            {
                out[r][c] = ai[r][c];
            }
        }

        return out;
    }

    static inline std::pair<glm::quat, glm::quat>
    swingTwistDecomp(glm::quat quat, glm::vec3 dir)
    {
        glm::quat swing;
        glm::quat twist;

        glm::vec3 ra(quat.x, quat.y, quat.z);
        glm::vec3 p = glm::proj(ra, dir);
        twist = glm::normalize(glm::quat(p.x, p.y, p.z, quat.w));

        swing = quat * glm::conjugate(twist);

        return std::pair<glm::quat, glm::quat>(swing, twist);
    }

    static inline float constrain(float val, float min, float max)
    {
        if (val <= min)
            val = min;
        if (val >= max)
            val = max;
        return val;
    }

    static bool strStartsWith(const std::string&, const std::string&);
    static bool strEndsWith(const std::string&, const std::string&);

#ifdef _DEBUG
    template <class T, typename F>
    static inline void vectorToCSV(const std::vector<T>& vec, F functor,
                                   const std::string& filename)
    {
        std::ofstream os(filename);
        for (auto& e : vec)
        {
            functor(os, e);
            os << std::endl;
        }
    }
#endif

    static std::vector<glm::vec3> computeMinBoundingRect(uint8_t const*,
                                                         uint32_t, uint32_t);
};
} // namespace mode7

#endif /* UTIL_HPP */
