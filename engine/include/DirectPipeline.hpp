#ifndef DIRECTPIPELINE_HPP
#define DIRECTPIPELINE_HPP

#include "GameState.hpp"
#include "Pipeline.hpp"

#include <memory>

namespace mode7
{
class Game;
class GameState;

class DirectPipeline : public Pipeline
{
public:
    DirectPipeline()
        : Pipeline()
    {
    }

    virtual ~DirectPipeline() = default;

    virtual bool run(Game*) const override;
    virtual bool stateRun(std::shared_ptr<GameState>) const override;
};
} // namespace mode7

#endif /* DIRECTPIPELINE_HPP */
