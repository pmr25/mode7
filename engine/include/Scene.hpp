/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef SCENE_HPP
#define SCENE_HPP

#include "Drawable.hpp"
#include "Mesh.hpp"
#include "Shader.hpp"
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace mode7
{
class Scene : public Object, Drawable
{
public:
    Scene();
    virtual ~Scene();

    void addMesh(const std::shared_ptr<Mesh>&);
    Mesh* getMesh(unsigned int);
    std::shared_ptr<Mesh> getMeshShared(unsigned int);
    std::vector<std::shared_ptr<Mesh>>& getMeshes();
    virtual void update();
    // void setScheduler(std::shared_ptr<pmr25::Scheduler<MeshUpdateWorker>>);

    void draw(Shader*);

    inline std::string query()
    {
        std::stringstream ss;
        for (unsigned int i = 0; i < meshes.size(); ++i)
        {
            ss << i << ": "
               << "..."
               << "\n";
        }

        return ss.str();
    }

private:
    std::vector<std::shared_ptr<Mesh>> meshes;
    // std::shared_ptr<pmr25::Scheduler<MeshUpdateWorker>> scheduler;

    friend class ModelLoader;
};
} // namespace mode7

#endif /* SCENE_HPP */
