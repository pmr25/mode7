#ifndef MODE7_CODEBENCHMARK_HPP
#define MODE7_CODEBENCHMARK_HPP

#include <chrono>
#include <cstdint>
#include <map>
#include <string>
#include <utility>

#define DEFAULT_FN "codebenchmark.csv"
#define CLOCK std::chrono::system_clock
#define TIME_POINT std::chrono::time_point<CLOCK>
#define NOW CLOCK::now

namespace mode7
{

class CodeBenchmark
{
public:
    auto tick(const std::string& name) -> std::string;
    auto tock(const std::string& name) -> uint64_t;
    void setFilename(const std::string& filename);

    static auto getInstance() -> CodeBenchmark&;

private:
    explicit CodeBenchmark(std::string filename = DEFAULT_FN)
        : mFilename(std::move(filename))
    {
    }

    ~CodeBenchmark()
    {
        save();
    }

    void save();

    std::string mFilename;
    std::map<std::string, uint32_t> mCounters;
    std::map<std::string, std::pair<TIME_POINT, TIME_POINT>> mIntervals;
};

} // namespace mode7

#endif // MODE7_CODEBENCHMARK_HPP
