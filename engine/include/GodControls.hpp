#ifndef GODCONTROLS_HPP
#define GODCONTROLS_HPP

#include "Camera.hpp"
#include "Controls.hpp"
#include "Object.hpp"
#include <glm/glm.hpp>
#include <memory>

namespace mode7
{
class GodControls : public Controls
{
public:
    GodControls()
        : Controls()
    {
        mCamera = std::make_shared<Camera>();
        addChild(&mCamera->getObject());
    }

    virtual ~GodControls() = default;

    inline void attachCamera()
    {
        mCamera->create(70.0f, 0.1f, 1000.0f);
        Camera::setCurrent(mCamera);
    }

    virtual void updateControls();
    virtual glm::mat4 getControlMatrix();

private:
    std::shared_ptr<Camera> mCamera;
};
} // namespace mode7

#endif /* GODCONTROLS_HPP */
