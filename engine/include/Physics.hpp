/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#if __linux__
#include <bullet/btBulletDynamicsCommon.h>
#elif defined _WIN32
#include <bullet/btBulletDynamicsCommon.h>
#endif
#include <cstdint>
#include <map>
#include <memory>
#include <vector>

#include "Object.hpp"
#include "Scene.hpp"

namespace mode7
{
class Physics
{
public:
    Physics();
    ~Physics();

    void addObject(Object*, const std::string&);
    btCollisionShape* getCollisionShape(const std::string&);
    void cacheCollisionShape(btCollisionShape*, const std::string&);
    void addScene(Scene*);
    void simulate();

private:
    btDefaultCollisionConfiguration* m_collisionConfig;
    btCollisionDispatcher* m_dispatcher;
    btBroadphaseInterface* m_broadPhase;
    btSequentialImpulseConstraintSolver* m_solver;
    btDiscreteDynamicsWorld* m_dynamicsWorld;

    std::vector<std::shared_ptr<btCollisionShape>> m_collisionShapes;
    std::map<std::string, uint32_t> m_shapeMap;

    std::vector<std::unique_ptr<btRigidBody>> m_rigidBodies;
};
} // namespace mode7

#endif /* PHYSICS_HPP */
