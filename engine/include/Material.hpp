/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <cstdint>
#include <glm/glm.hpp>
#include <vector>

namespace mode7
{
class Texture;

class Material
{
public:
    Material()
        : mAmbient(glm::vec3(0.5F), 1.0F)
        , mDiffuse(1.0F)
        , mEmission(0.0F)
        , mSpecular(0.0F)
    {
    }
    virtual ~Material() = default;

    void addMap(Texture*);

    [[nodiscard]] virtual auto getMap(uint32_t) const -> Texture*;
    [[nodiscard]] virtual auto numMaps() const -> uint32_t;

    void setAmbient(glm::vec4 clr)
    {
        mAmbient = clr;
    }

    void setDiffuse(glm::vec4 clr)
    {
        mDiffuse = clr;
    }

    void setEmission(glm::vec4 clr)
    {
        mEmission = clr;
    }

    void setSpecular(glm::vec4 clr)
    {
        mSpecular = clr;
    }

    void setShininess(float val)
    {
        mShininess = val;
    }

    void setFresnel(float val)
    {
        mFresnel = val;
    }

    [[nodiscard]] auto getAmbient() const -> const glm::vec4&
    {
        return mAmbient;
    }

    [[nodiscard]] auto getDiffuse() const -> const glm::vec4&
    {
        return mDiffuse;
    }

    [[nodiscard]] auto getEmission() const -> const glm::vec4&
    {
        return mEmission;
    }

    [[nodiscard]] auto getSpecular() const -> const glm::vec4&
    {
        return mSpecular;
    }

    [[nodiscard]] auto getShininess() const -> float
    {
        return mShininess;
    }

    [[nodiscard]] auto getFresnel() const -> float
    {
        return mFresnel;
    }

    void setFlat(bool val)
    {
        mDiffuseFloor = (float)val;
        mAmbient = glm::vec4(mDiffuseFloor);
        mShininess = 0;
        mFresnel = 0;
    }

    [[nodiscard]] auto getDiffuseFloor() const -> float
    {
        return mDiffuseFloor;
    }

    int tile{};

protected:
    glm::vec4 mAmbient;
    glm::vec4 mDiffuse;
    glm::vec4 mEmission;
    glm::vec4 mSpecular;
    float mShininess{0.0F};
    float mFresnel{0.0F};
    float mDiffuseFloor{0.0F};
    std::vector<Texture*> maps;
};
} // namespace mode7

#endif /* MATERIAL_HPP */
