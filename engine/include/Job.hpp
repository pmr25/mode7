#ifndef JOB_HPP
#define JOB_HPP

#include <cstdint>

namespace mode7
{
class Job
{
public:
    Job()
    {
        assignID();
    }
    virtual ~Job() = default;

    virtual void run() = 0;
    uint32_t getID() const
    {
        return mJobID;
    }

private:
    void assignID();
    uint32_t mJobID;
};
} // namespace mode7

#endif /* JOB_HPP */
