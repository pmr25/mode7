#ifndef CONSUMER_HPP
#define CONSUMER_HPP

#include <cstdint>
#include <mutex>

namespace mode7
{

class Producer;

class Consumer
{
public:
    Consumer(Producer* producer)
        : mProducer(producer)
        , mIsRunning(true)
    {
        assignID();
    }
    virtual ~Consumer() = default;

    void operator()();
    virtual void thread_main();
    uint32_t getID() const
    {
        return mID;
    }

private:
    void assignID();

    Producer* mProducer;
    bool mIsRunning;
    uint32_t mID;
};
} // namespace mode7

#endif /* CONSUMER_HPP */
