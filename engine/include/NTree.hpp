#ifndef MODE7_NTREE_HPP
#define MODE7_NTREE_HPP

#include "Bucket.hpp"
#include "Entity.hpp"
#include "SpacePartitioner.hpp"

#include <memory>
#include <vector>

#define SIZE 1024.0F
#define CAPACITY 10
#define DEPTHLIMIT 10

namespace mode7
{

template <int D>
class NTree : public SpacePartitioner
{
public:
    NTree()
        : mRoot(CAPACITY, DEPTHLIMIT, 0, glm::vec<D, float>(0.0F),
                glm::vec<D, float>(SIZE))
    {
    }
    ~NTree() = default;
    auto insert(std::shared_ptr<Entity<D>> entity) -> void
    {
        mRoot.insert(entity);
    }
    auto search(glm::vec<D, float> position, glm::vec<D, float> dimension)
        -> std::vector<std::weak_ptr<Entity<D>>>
    {
        std::vector<std::weak_ptr<Entity<D>>> results;

        mRoot.search(results, position, dimension);

        return results;
    }
    auto update() -> void
    {
        auto pruned{mRoot.prune()};
        for (auto& entity : pruned)
        {
            insert(entity);
        }
    }

private:
    Bucket<D> mRoot;
    std::vector<std::shared_ptr<Entity<D>>> mEntities;
};

} // namespace mode7

#endif // MODE7_NTREE_HPP
