/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef ASSETMANAGER_HPP
#define ASSETMANAGER_HPP

#include <cassert>
#include <map>
#include <memory>
#include <stack>
#include <string>

#include "filesystem.hpp"

namespace mode7
{
class AssetStructure
{
public:
    AssetStructure()
        : baseDir("")
        , dataDir("")
        , modelDir("")
        , shaderDir("")
        , textureDir("")
    {
    }

private:
    fs::path baseDir;
    fs::path dataDir;
    fs::path modelDir;
    fs::path shaderDir;
    fs::path textureDir;

    friend class AssetManager;
};

class AssetManager
{
public:
    AssetManager()
    {
        m_structures["default"] = AssetStructure();
        m_currentStructure = &m_structures["default"];
        m_structureStack.push("default");
    }

    int setBaseDirectory(const std::string&);
    int setDataDirectory(const std::string&);
    int setModelDirectory(const std::string&);
    int setShaderDirectory(const std::string&);
    int setTextureDirectory(const std::string&);

    int setStructure(const std::string&);
    int pushStructure(const std::string&);
    int popStructure();

    std::string assetPath(const std::string& fn = "") const;
    std::string dataPath(const std::string& fn = "") const;
    std::string modelPath(const std::string& fn = "") const;
    std::string shaderPath(const std::string& fn = "") const;
    std::string texturePath(const std::string& fn = "") const;

    static AssetManager& getCurrent()
    {
        assert(m_current.get() != nullptr);
        return *m_current;
    }

    static AssetManager* getCurrentPtr()
    {
        return m_current.get();
    }

    static std::shared_ptr<AssetManager> createAndMakeCurrent()
    {
        auto ret = std::make_shared<AssetManager>();
        m_current = ret;
        return ret;
    }

private:
    std::string constructPath(AssetStructure*, fs::path,
                              const std::string&) const;

    std::map<std::string, AssetStructure> m_structures;
    std::stack<std::string> m_structureStack;
    AssetStructure* m_currentStructure;

    static std::shared_ptr<AssetManager> m_current;
};
} // namespace mode7

#endif /* ASSETMANAGER_HPP */
