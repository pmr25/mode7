/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Animation.hpp"
#include "AssetManager.hpp"
#include "BBox.hpp"
#include "Billboard.hpp"
#include "Camera.hpp"
#include "Clock.hpp"
#include "Consumer.hpp"
#include "Controls.hpp"
#include "Core.hpp"
#include "Framebuffer.hpp"
#include "Game.hpp"
#include "Group.hpp"
#include "Job.hpp"
#include "Keyboard.hpp"
#include "Light.hpp"
#include "Logger.hpp"
#include "Material.hpp"
#include "Mesh.hpp"
#include "ModelLoader.hpp"
#include "Mouse.hpp"
#include "Object.hpp"
#include "OrbitControls.hpp"
#include "Physics.hpp"
#include "Primitives.hpp"
#include "Producer.hpp"
#include "Scene.hpp"
#include "Screen.hpp"
#include "Shader.hpp"
#include "Texture.hpp"
#include "TextureAtlas.hpp"
#include "TextureImage.hpp"
#include "Timer.hpp"
#include "Util.hpp"
#include "gl.hpp"

#endif /* ENGINE_HPP */
