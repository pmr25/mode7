/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <memory>
#include <string>
#include <vector>
#ifdef _BULLET_PHYSICS
#if __linux__
#include <bullet/btBulletDynamicsCommon.h>
#elif defined _WIN32
#include <bullet3/btBulletDynamicsCommon.h>
#endif
#endif /* _BULLET_PHYSICS */

#include "BBox.hpp"
#include "Updatable.hpp"
#include "glmincludes.hpp"

namespace mode7
{
class Object : public Updatable
{
public:
    Object(const std::string& name = "Unnamed Object")
        : preventUpdate(false)
        , m_position(0.0f)
        , m_euler(0.0f)
        , m_scale(1.0f)
        , m_velocity(0.0f)
        , m_front(0.0f, 0.0f, -1.0f)
        , m_right(1.0f, 0.0f, 0.0f)
        , m_up(0.0f, 1.0f, 0.0f)
        ,

        m_isDirty(true)
        , m_translationMatrix(1.0f)
        , m_rotationMatrix(1.0f)
        , m_scaleMatrix(1.0f)
        , m_matrix(1.0f)
        , m_quaternion()
        , m_worldQuat()
        , m_worldMatrix(1.f)
        , m_inherentMatrix(1.f)
        ,

        m_parent(nullptr)
        , m_children()
        , m_name(name)

#ifdef _BULLET_PHYSICS
        , m_mass(0.f)
        , m_physRequestActive(false)
        , m_rigidBody(nullptr)
#endif /* _BULLET_PHYSICS */
        , mCachedWorldPosition(0.0f)
    {
        setRotation(glm::vec3(0.f, 0.f, 0.f));
    }

    void addChild(Object*);
    void setParent(Object* parentObj)
    {
        m_parent = parentObj;
    }
    virtual void buildMatrices(const glm::mat4&);
#ifdef _BULLET_PHYSICS
    virtual void copyPhysics();
#endif /* _BULLET_PHYSICS */
    void decompose();
    void detachParent();
    const std::vector<Object*> getChildren() const;
    virtual const std::string& getName() const;
    Object* getParent();
    glm::vec3 getPosition() const;
    glm::vec3 getRotation() const;
    glm::vec3 getVelocity() const;
    glm::vec3 getScale() const;
    glm::mat4 getMatrix() const;
    glm::vec3 getFront() const;
    glm::vec3 getRight() const;
    glm::vec3 getUp() const;
    glm::vec3 getWorldPosition();
    glm::vec3 getWorldRotation() const;
    glm::vec3 getWorldScale() const;
    glm::vec3 getWorldVelocity() const;
    glm::mat4 getWorldMatrix() const;
    virtual inline glm::mat4& getShaderMatrix();
    inline void impulse(float, float, float);
    inline void impulse(glm::vec3);
    inline void localTranslate(float, float, float);
    inline void move();
    virtual void notifyCollision(const std::string&);
    virtual inline void preUpdate()
    {
    }
    virtual inline void postUpdate()
    {
    }
    void rotate(glm::vec3);
    void rotateAbout(float, glm::vec3);
    void rotateAboutLocal(float, glm::vec3);
    void scale(glm::vec3);
    inline void scale(float);
    virtual void setName(const std::string&);
    void setPosition(glm::vec3);
    inline void setPosition(float, float, float);
    inline void setPositionX(float);
    inline void setPositionY(float);
    inline void setPositionZ(float);
    void setRotation(glm::vec3);
    inline void setRotationX(float);
    inline void setRotationY(float);
    inline void setRotationZ(float);
    void setScale(glm::vec3);
    inline void setScale(float);
    inline void setScale(float, float, float);
    inline void setVelocity(glm::vec3);
    inline void setVelocity(float, float, float);
    inline void setVelocityX(float);
    inline void setVelocityY(float);
    inline void setVelocityZ(float);
    inline void rotate(float, float, float);
    inline void translate(float, float, float);
    void translate(glm::vec3);
    void translateLocal(glm::vec3);
    void translateLocalUp(glm::vec3, glm::vec3);
    virtual void update();
    virtual void update(const glm::mat4&, const glm::quat&);
    virtual void update(const glm::mat4&, const glm::quat&, bool);
    inline void worldImpulse(glm::vec3);
    void setMatrix(glm::mat4);

#ifdef _BULLET_PHYSICS
    btRigidBody* getRigidBody()
    {
        return m_rigidBody;
    }
    inline void notifyPhysicsCollision(btCollisionObject*);
    inline void physRequestActive(bool);
    inline void setMass(float);
    inline btRigidBody* getRigidBody() const;
#endif

    bool preventUpdate;

protected:
    inline void dirty();
    inline void clean();
    inline void formatEuler();
    inline glm::quat buildQuat(glm::vec3);

    // vector data
    glm::vec3 m_position;
    glm::vec3 m_euler;
    glm::vec3 m_scale;
    glm::vec3 m_velocity;
    glm::vec3 m_front;
    glm::vec3 m_right;
    glm::vec3 m_up;

    // matrix data
    bool m_isDirty;
    glm::mat4 m_translationMatrix;
    glm::mat4 m_rotationMatrix;
    glm::mat4 m_scaleMatrix;
    glm::mat4 m_matrix;
    glm::quat m_quaternion;
    glm::quat m_worldQuat;
    glm::mat4 m_worldMatrix;
    glm::mat4 m_inherentMatrix;

    // object tree data
    Object* m_parent;
    std::vector<Object*> m_children;
    std::string m_name;

    // physics data
#ifdef _BULLET_PHYSICS
    float m_mass;
    bool m_physRequestActive;
    btRigidBody* m_rigidBody;
#endif

    friend class Camera;
    friend class ModelLoader;
#ifdef _BULLET_PHYSICS
    friend class Physics;
#endif /* _BULLET_PHYSICS */

    friend class Scene;

private:
    glm::vec3 mCachedWorldPosition;
};
} // namespace mode7

#include "Object.inl"

#endif /* OBJECT_HPP */
