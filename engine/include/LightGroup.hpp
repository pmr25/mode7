#ifndef LIGHTGROUP_HPP
#define LIGHTGROUP_HPP

#include <gl.hpp>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "Light.hpp"
#include "Shader.hpp"

namespace mode7
{
class LightGroup
{
public:
    LightGroup()
        : mSSBO(0)
        , mNumLightsLoc(0)
    {
    }

    ~LightGroup()
    {
        if (mSSBO > 0) // is buffer allocated on GPU
        {
            destroy();
        }
    }

    void init();
    void destroy();
    void cacheUniformLocations(Shader*);
    void fillUniforms();

    inline void addLight(std::shared_ptr<Light> ptr)
    {
        mLights.push_back(ptr);
    }

private:
    GLuint mSSBO;
    GLuint mNumLightsLoc;
    std::vector<std::shared_ptr<Light>> mLights;
    std::map<std::string, GLuint> mLocations;
};
} // namespace mode7

#endif /* LIGHTGROUP_HPP */
