#ifndef HUDMANAGER_HPP
#define HUDMANAGER_HPP

#include "HUDElement.hpp"
#include "Shader.hpp"
#include "gl.hpp"

#include <memory>
#include <string>
#include <vector>

namespace mode7
{
class HUDManager
{
public:
    HUDManager(const std::string& vsfn, const std::string& fsfn)
        : mShader(vsfn, fsfn)
        , mPositionUniform(0)
        , mThetaUniform(0)
        , mTextureUniform(0)
    {
        setupShader();
    }

    ~HUDManager() = default;

    void addElement(std::shared_ptr<HUDElement>);
    void drawElements() const;
    void handleClick(uint32_t, uint32_t);
    void setupShader();
    void update();

    auto getElementByName(const std::string&) -> std::shared_ptr<HUDElement>;

private:
    Shader mShader;
    GLuint mPositionUniform;
    GLuint mThetaUniform;
    GLuint mTextureUniform;
    std::vector<std::shared_ptr<HUDElement>> mElements;
};
} // namespace mode7

#endif /* HUDMANAGER_HPP */
