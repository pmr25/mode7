#ifndef PRODUCER_HPP
#define PRODUCER_HPP

#include <cassert>
#include <condition_variable>
#include <cstdint>
#include <deque>
#include <memory>
#include <mutex>
#include <semaphore>
#include <thread>
#include <vector>

#include "Job.hpp"

namespace mode7
{

class Consumer;

class Producer
{
public:
    Producer(uint8_t numWorkers)
        : mNumRunningWorkers(0)
        , mLastRunningWorkers(0)
    {
        createConsumers(numWorkers);
    }

    virtual ~Producer()
    {
        destroyConsumers();
    }

    void createConsumers(uint8_t);
    void startConsumers();
    void destroyConsumers();
    void feedJob(Job*);
    std::mutex& getMutex()
    {
        return mMutex;
    }
    std::condition_variable& getCondVar()
    {
        return mNotifyWorkerCV;
    }
    std::binary_semaphore& getSemaphore()
    {
        return mSemaphore;
    }
    bool hasData()
    {
        return mReadyJobs.size() > 0;
    }
    void distribute();
    Job* getNextJob();

private:
    int32_t mNumRunningWorkers;
    int32_t mLastRunningWorkers;
    std::vector<Consumer*> mConsumers;
    std::vector<std::thread> mThreads;
    std::deque<Job*> mReadyJobs;
    std::vector<Job*> mDoneJobs;
    std::mutex mMutex;
    std::condition_variable mNotifyWorkerCV;
    std::binary_semaphore mSemaphore{0};
};
} // namespace mode7

#endif /* PRODUCER_HPP */
