/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MOUSE_HPP
#define MOUSE_HPP

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <cstdint>

#define NUM_BUTTONS 3

namespace mode7
{
typedef enum
{
    LEFT = 0,
    RIGHT = 1,
    MIDDLE = 2
} mousebutton_t;
class Mouse
{
public:
    static void attach();
    static void poll();
    static void clearEvent();
    static void sendEvent(SDL_Event*);
    static void grab();
    static void release();

    static int getX();
    static int getY();
    static int getRelX();
    static int getRelY();
    static int getDeltaX();
    static int getDeltaY();
    static int getClickDeltaX(mousebutton_t);
    static int getClickDeltaY(mousebutton_t);
    static bool getLeft();
    static bool getRight();
    static bool isGrabbed();

    static inline bool hasFocus()
    {
        return m_grabbed || m_button[LEFT] || m_button[RIGHT] ||
               m_button[MIDDLE];
    }

private:
    static int m_start[2];
    static int m_clickStartX[NUM_BUTTONS];
    static int m_clickStartY[NUM_BUTTONS];
    static int m_x;
    static int m_y;
    static int m_rx;
    static int m_ry;
    static uint32_t m_buttonMask;
    static bool m_button[NUM_BUTTONS];
    static bool m_grabbed;
};
} // namespace mode7

#endif /* MOUSE_HPP */
