/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MESH_HPP
#define MESH_HPP

#include "Drawable.hpp"
#include "Material.hpp"
#include "MeshData.hpp"
#include "Object.hpp"
#include "Shader.hpp"
#include "Vertex.hpp"
#include "Volume.hpp"
#include "gl.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mode7
{
class Mesh : public Object, Drawable
{
public:
    Mesh()
        : Object()
        , visible(true)
        , alt(nullptr)
        , m_name("unnamed mesh")
        , vao(0)
        , vbo(0)
        , ebo(0)
        , m_insideFrustum(true)
    {
    }

    virtual ~Mesh()
    {
        gpuFree();
    }

    void init();
    void setMaterial(std::shared_ptr<Material>);
    void createFromArrays(std::vector<Vertex>, std::vector<unsigned int>);
    void createFromShape(int);
    void drawTriangles();
    virtual void update();
    virtual void draw(Shader*);
    void setAltShader(Shader&);
    void setBoundingVolume(Volume*);
    Volume* getBoundingVolume();

    Material& getMaterial();

    void setName(const std::string&);
    const std::string& getName() const;

    auto getMeshData() -> MeshData*;

    auto discardMeshData() -> void;

    void gpuAlloc();
    void gpuFree();

    bool visible;

    static const int PLANE = 0;

protected:
    Shader* alt;
    std::string m_name;
    std::shared_ptr<Material> m_material;
    MeshData m_meshData;
    std::unique_ptr<Volume> m_boundingVolume;

private:
    GLuint vao;
    GLuint vbo;
    GLuint ebo;
    bool m_insideFrustum;

    friend class ModelLoader;
};
} // namespace mode7

#endif /* MESH_HPP */
