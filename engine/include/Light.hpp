/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef LIGHT_HPP
#define LIGHT_HPP

#include "Object.hpp"
#include "gl.hpp"
#include <cmath>
#include <sstream>
#include <string>

namespace mode7
{
typedef struct _LightParams
{
    glm::vec4 position;

    glm::vec4 color;
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;

    glm::vec3 spotDirection;
    float spotExponent;
    float spotCutoff;
    float spotCosCutoff;

    float attenConst;
    float attenLinear;
    float attenQuadratic;

    glm::vec3 attenuation;

    // TODO shadowmap
    // TODO shadow view matrix
} LightParams;

class Light : public Object
{
public:
    Light()
        : Object()
        , diffuse(1.0f)
        , specular(0.0f)
        , position(0.0f, 1.0f, 0.0f, 0.0f)
        , type(-1)
    {
    }

    virtual ~Light() = default;

    void create(int);
    inline LightParams getParams()
    {
        LightParams params;

        params.position = position;
        params.color = glm::vec4(1);
        params.diffuse = diffuse;
        params.specular = specular;
        params.spotDirection = params.position;
        params.spotCosCutoff = cosf(0.5f * 30.0f);
        params.spotCosCutoff = 0;
        params.spotExponent = 0.0f;

        params.attenConst = constant;
        params.attenLinear = linear;
        params.attenQuadratic = quadratic;

        return params;
    }

    // colors
    float intensity{1.0f};
    glm::vec4 diffuse;
    glm::vec4 specular;
    glm::vec4 position;

    // attenuation
    float constant{0.1f};
    float linear{0.05f};
    float quadratic{0.05f};

    const int AMBIENT = 0;
    const int DIRECTIONAL = 1;
    const int SPECULAR = 2;

private:
    int type;
};
} // namespace mode7

#endif /* LIGHT_HPP */
