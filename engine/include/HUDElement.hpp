#ifndef HUDELEMENT_HPP
#define HUDELEMENT_HPP

#include "Drawable.hpp"
#include "TextureImage.hpp"
#include "Updatable.hpp"
#include "gl.hpp"

#include <cstdint>

namespace mode7
{
class Shader;

class HUDElement : public Updatable
{
public:
    HUDElement(uint32_t w = 100, uint32_t h = 100, const std::string& name = "")
        : mIsVisible(true)
        , mTheta(0.0f)
        , mPosition(0.0f, 0.0f)
        , mWidth(0)
        , mHeight(0)
        , mDrawWidth(0)
        , mDrawHeight(0)
        , mTexSize(0)
        , mVAO(GL_INVALID_VALUE)
        , mVBO(GL_INVALID_VALUE)
        , mName("hud_element." + std::to_string(++sNumElements))
    {
        setSize(w, h);
        mTexture.generateMipmap(false);
        if (name.size() > 0)
        {
            mName = name;
        }
    }

    virtual ~HUDElement();

    virtual void init();

    void createMesh(float, float);
    void destroy();
    virtual void draw() const;
    inline glm::vec2 getPosition() const
    {
        return mPosition;
    }
    inline void setPosition(float x, float y)
    {
        mPosition = glm::vec2(x, y);
    }
    void setSize(uint32_t, uint32_t);
    inline float getTheta() const
    {
        return mTheta;
    }
    constexpr uint32_t getWidth() const
    {
        return mWidth;
    }
    constexpr uint32_t getHeight() const
    {
        return mHeight;
    }
    constexpr uint32_t getDrawWidth() const
    {
        return mDrawWidth;
    }
    constexpr uint32_t getDrawHeight() const
    {
        return mDrawHeight;
    }
    constexpr uint32_t getTexSize() const
    {
        return mTexSize;
    }
    inline glm::vec2 getDimensions() const
    {
        return {600, 400};
    }
    inline GLuint getTexture() const
    {
        return mTexture.getId();
    }
    inline void setVisible(bool val)
    {
        mIsVisible = val;
    }

    auto isVisible() const -> bool
    {
        return mIsVisible;
    }

    inline virtual void update()
    {
    }
    inline virtual void show()
    {
    }
    inline virtual void update(const glm::mat4&, const glm::quat&)
    {
        update();
    }
    inline virtual void update(const glm::mat4&, const glm::quat&, bool)
    {
        update();
    }

    bool contains(int32_t, int32_t);
    virtual void handleClick(int32_t, int32_t);
    inline virtual void clickCallback(int32_t, int32_t)
    {
    }

    auto getName() const -> std::string
    {
        return mName;
    }

    auto setName(const std::string& name) -> void
    {
        mName = name;
    }

protected:
    bool mIsVisible;
    float mTheta;
    glm::vec2 mPosition;
    TextureImage mTexture;

private:
    uint32_t mWidth;
    uint32_t mHeight;
    uint32_t mDrawWidth;
    uint32_t mDrawHeight;
    uint32_t mTexSize;
    GLuint mVAO;
    GLuint mVBO;
    std::string mName;

    static uint32_t sNumElements;
};
} // namespace mode7

#endif /* HUDELEMENT_HPP */
