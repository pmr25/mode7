#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "Volume.hpp"
#include <glm/glm.hpp>

namespace mode7
{
class Sphere : public Volume
{
public:
    Sphere(float r = 1.0f)
        : Volume()
        , m_radius(r)
    {
    }

    bool intersects(const glm::vec3) const override;
    bool intersects(const geom::Plane*) const override;

    virtual std::unique_ptr<Volume> clone() const override
    {
        return std::make_unique<Sphere>(*this);
    }

private:
    float m_radius;
};
} // namespace mode7

#endif /* SPHERE_HPP */
