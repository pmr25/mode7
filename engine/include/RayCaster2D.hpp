#ifndef MODE7_RAYCASTER2D_HPP
#define MODE7_RAYCASTER2D_HPP

#include "Line.hpp"
#include "Object2D.hpp"
#include "ParametricCurve.hpp"
#include "Polygon.hpp"

#include <glm/glm.hpp>
#include <vector>

namespace mode7
{

class RayCaster2D : public Object2D
{
public:
    RayCaster2D(glm::vec2 origin = {0, 0}, glm::vec2 dir = {0, 1})
        : Object2D(origin, dir)
    {
    }
    RayCaster2D(const RayCaster2D& other) = default;
    ~RayCaster2D() = default;

    [[nodiscard]] auto intersects(const std::vector<ParametricCurve<2>>&) const
        -> float;
    [[nodiscard]] auto intersects(const ParametricCurve<2>&) const -> float;
    [[nodiscard]] auto intersects(const Line<2>&) const -> float;
    [[nodiscard]] auto intersects(const std::vector<Line<2>>&) const -> float;
    [[nodiscard]] auto intersects(const geom::Polygon&) const -> float;

private:
    [[nodiscard]] auto
    intersectsRecursiveHelper(const std::vector<Line<2>>&) const -> float;
};

inline std::ostream& operator<<(std::ostream& os, const RayCaster2D& obj)
{
    os << "RayCaster{ "
       << "pos=" << glm::to_string(obj.getWorldPosition()) << "\t"
       << "dir=" << glm::to_string(obj.getWorldFront()) << " }";

    return os;
}

} // namespace mode7

#endif // MODE7_RAYCASTER2D_HPP
