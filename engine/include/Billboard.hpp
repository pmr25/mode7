/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef BILLBOARD_HPP
#define BILLBOARD_HPP

#include "Material.hpp"
#include "Mesh.hpp"
#include "Shader.hpp"

#include <cstdint>
#include <glm/glm.hpp>

namespace mode7
{
class Billboard : public Mesh
{
public:
    Billboard();
    ~Billboard();

    void fitToTexture(uint32_t);

    virtual inline glm::mat4& getShaderMatrix()
    {
        return m_internalMatrix;
    }

    virtual void postUpdate();
    virtual void create();
    // virtual void create(const float*, size_t);
    virtual void createFromQuadFile(const std::string&);
    //    virtual void createFromQuad(const std::vector<glm::vec3>&);
    virtual void draw(Shader*);

private:
    glm::mat4 m_internalMatrix;
};
} // namespace mode7

#endif /* BILLBOARD_HPP */
