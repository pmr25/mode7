#include "Texture.hpp"

namespace mode7
{

auto Texture::generateMipmap(bool value) -> void
{
    mGenMipmap = value;
}
auto Texture::shouldGenerateMipmap() -> bool
{
    return mGenMipmap;
}
auto Texture::genGPUTexture(const void* data, GLuint width, GLuint height,
                            GLuint mode) -> void
{
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, mode,
                 GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint)mNearFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint)mFarFilter);
    glGenerateMipmap(GL_TEXTURE_2D);
}

auto Texture::genGPUTexture() -> void
{
    assert(getData() != nullptr);
    genGPUTexture((void*)getData(), GLuint(width), GLuint(height), GL_RGBA);
}
} // namespace mode7
