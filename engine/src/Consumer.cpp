#include "Consumer.hpp"
#include "ControlJob.hpp"
#include "Producer.hpp"

#include <cassert>
#include <iostream>
#include <memory>

static uint32_t idTracker = 0;

namespace mode7
{
void Consumer::operator()()
{
    thread_main();
}

void Consumer::thread_main()
{
    assert(mProducer != nullptr);

    // std::cout << "thread " << getID() << " starting" << std::endl;

    while (mIsRunning)
    {
        // std::cout << "thread " << getID() << " waiting" << std::endl;
        // std::unique_lock lock(mProducer->getMutex());
        // mProducer->getCondVar().wait(lock,
        //                              [&] { return mProducer->hasData(); });

        mProducer->getSemaphore().acquire();

        // we own the lock
        // std::cout << "thread " << getID() << "in critical section" <<
        // std::endl;
        Job* job = mProducer->getNextJob();

        // std::cout << "thread " << getID() << "out critical section" <<
        // std::endl; lock.unlock(); mProducer->getCondVar().notify_one();

        mProducer->getSemaphore().release();

        // if (std::shared_ptr<ControlJob> ctrl =
        //         dynamic_pointer_cast<ControlJob>(job);
        //     ctrl != nullptr)
        if (ControlJob* ctrl = dynamic_cast<ControlJob*>(job); ctrl != nullptr)
        {
            // std::cout << "thread " << getID() << "received signal" <<
            // std::endl;
            switch (ctrl->getCommand())
            {
            case CONTROLJOB_QUIT:
                mIsRunning = false;
                break;

            default:
                mIsRunning = false;
                break;
            }
        }
        else if (job != nullptr)
        {
            // std::cout << "thread " << getID() << " working on job " <<
            // job->getID() << std::endl;
            job->run();
        }
    }

    // std::cout << "thread " << getID() << " shutting down" << std::endl;
}

void Consumer::assignID()
{
    mID = idTracker++;
}
} // namespace mode7
