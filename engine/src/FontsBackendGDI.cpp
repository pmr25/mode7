#ifdef _WIN32

#include "FontsBackendGDI.hpp"
#include "Bitmap.hpp"
#include "Logger.hpp"
#include "Screen.hpp"

namespace mode7
{
auto FontsBackendGDI::drawText(cairo_t* ctx, const std::string& text,
                               const std::string& font, double x, double y)
    -> void
{
    assert(mPrivateFontCollection->GetFamilyCount() > 0);

    const auto formatted{Fonts::formatString(font)};

    const int dimension{512 * int(GLScreen::getScale())};
    const std::wstring wText = std::wstring(text.begin(), text.end());
    const std::wstring wFont = std::wstring(formatted.begin(), formatted.end());
    const int fontPt = 28;

    Gdiplus::Font gdiFont(L"Racing Sans One", fontPt, Gdiplus::FontStyleRegular,
                          Gdiplus::UnitPixel, mPrivateFontCollection.get());

    Gdiplus::Bitmap bitmap(dimension, dimension);
    Gdiplus::RectF targetRect(0, 0, dimension, dimension);
    Gdiplus::CharacterRange range(0, wText.size());
    Gdiplus::StringFormat format(Gdiplus::StringFormat::GenericDefault());
    format.SetMeasurableCharacterRanges(wText.size(), &range);

    Gdiplus::SolidBrush brush(Gdiplus::Color::White);

    // graphics context scope
    {
        auto graphics{Gdiplus::Graphics::FromImage(&bitmap)};

        graphics->SetTextRenderingHint(
            Gdiplus::TextRenderingHintClearTypeGridFit);
        graphics->Clear(Gdiplus::Color::Transparent);
        graphics->DrawString(wText.c_str(), text.length(), &gdiFont, targetRect,
                             &format, &brush);

        delete graphics;
    }

    Gdiplus::BitmapData gdiData;
    auto rect{Gdiplus::Rect(0, 0, dimension, dimension)};
    bitmap.LockBits(&rect, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB,
                    &gdiData);

    Bitmap myBmp;
    myBmp.createWithData(gdiData.Scan0, gdiData.Width, gdiData.Height,
                         gdiData.Stride);

    bitmap.UnlockBits(&gdiData);

    myBmp.debugSavePNG("./test.png");

    cairo_save(ctx);

    cairo_set_operator(ctx, CAIRO_OPERATOR_SCREEN);
    cairo_set_source_surface(ctx, myBmp.createCairoSurface(), 0, 0);
    cairo_rectangle(ctx, 0, 0, dimension, dimension);
    cairo_fill(ctx);

    cairo_restore(ctx);
}
auto FontsBackendGDI::addFontFileToSystem(const std::string& name) -> bool
{
    if (mPrivateFontCollection == nullptr)
    {
        mPrivateFontCollection =
            std::make_unique<Gdiplus::PrivateFontCollection>();
    }

    SLOG(LG_DEBUG) << "add font " << name;
    const auto wName{std::wstring(name.begin(), name.end())};
    const auto status{mPrivateFontCollection->AddFontFile(wName.c_str())};
    if (status != Gdiplus::Status::Ok)
    {
        SLOG(LG_ERROR) << "GDI error: " << status;
    }
    return status == Gdiplus::Status::Ok;
    //    const int numAdded{AddFontResourceEx(name.c_str(), FR_PRIVATE, 0)};
    //    if (numAdded == 0)
    //    {
    //        SLOG(LG_WARN) << "font not added: " << name;
    //        return false;
    //    }
    //
    //    installedFonts.push_back(name);
    //    return true;
}

auto FontsBackendGDI::cleanup() -> void
{
    //    for (const auto& name : installedFonts)
    //    {
    //        const BOOL ok{RemoveFontResource(name.c_str())};
    //        if (!ok)
    //        {
    //            SLOG(LG_WARN) << "could not remove font: " << name;
    //        }
    //    }
}
} // namespace mode7

#endif /* _WIN32 */