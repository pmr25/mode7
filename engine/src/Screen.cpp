/** * This file is part of mode7. * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Screen.hpp"
#include "Camera.hpp"
#include "Logger.hpp"
#include "gsl/assert"
#include "sound.hpp"

#include <iostream>
#include <vector>

#ifdef _DEBUG
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>
#endif /* _DEBUG */

#ifdef __linux__
#include <X11/Xlib.h>
#include <SDL.h>
#include <SDL_image.h>
#endif

#ifdef __DEVIL__
#include <IL/il.h>
#endif /* __DEVIL__ */

#ifdef _WIN32
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#define WIN32_LEAN_AND_MEAN 1
#include <WinUser.h>
#include <Windows.h>
// clang-format off
#include <windows.h>
#include <gdiplus.h>
// clang-format on
static ULONG_PTR gdiplusToken;
#endif /* _WIN32 */

#define REFERENCE_PIXEL_HEIGHT 1080.0
#define GLSL_VERSION "#version 330 core"
#define _USE_OPENGL_CORE 1
#define _USE_OPENGL_ES 0

namespace mode7
{
static SDL_Window* window;
static SDL_GLContext ctx;
static int width;
static int height;
static int scale;

static int RESX;
static int RESY;

static int systemResolutionW;
static int systemResolutionH;
static double systemScale = 1.0;

#ifdef _DEBUG
static std::shared_ptr<ImGuiBuilder> guiBuilder =
    std::make_shared<ImGuiBuilder>();
#endif /* _DEBUG */
static std::shared_ptr<Pipeline> renderPipeline;

static bool ready = false;

#ifdef _DEBUG
void GLAPIENTRY MessageCallback(GLenum source, GLenum type, GLuint id,
                                GLenum severity, GLsizei length,
                                const GLchar* message, const void* userParam)
{
    // squash warnings
    (void)source;
    (void)id;
    (void)length;
    (void)userParam;

    SLOG(LG_DEBUG) << "GL CALLBACK: "
                   << (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "")
                   << " type=0x" << std::hex << type << " severity=0x"
                   << std::hex << severity << " message=" << message;
}
#endif /* _DEBUG */

void GLScreen::create(int desiredWidth, int desiredHeight, int resolutionWidth, int resolutionHeight, const std::string& title,
                      bool fullscreen, bool vsync, bool dpiAware)
{
    int numDisplays = SDL_GetNumVideoDisplays();
    if (numDisplays > 1)
    {
    }

    // let the OS know we're aware of the screen DPI
#ifdef _WIN32
    SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_SYSTEM_AWARE);
#elif defined __linux__
    // do nothing
#endif

#if defined _WIN32
    systemResolutionW = GetSystemMetrics(SM_CXSCREEN);
    systemResolutionH = GetSystemMetrics(SM_CYSCREEN);
#elif defined __linux__
    Display* disp = XOpenDisplay(nullptr);
    if (disp == nullptr)
    {
        exit(1);
    }
    Screen* scrn = DefaultScreenOfDisplay(disp);
    if (scrn == nullptr)
    {
        exit(1);
    }
    systemResolutionW = scrn->width;
    systemResolutionH = scrn->height;
#endif /* __linux__ */
    systemScale = (double)systemResolutionH / REFERENCE_PIXEL_HEIGHT;
    if (systemScale < 1.0)
    {
        systemScale = 1.0;
    }

    RESX = resolutionWidth;
    RESY = resolutionHeight;

    if (fullscreen)
    {
        width = systemResolutionW;
        height = systemResolutionH;
    }
    else
    {
        scale = int(systemScale);
        Ensures(scale > 0);
        width = desiredWidth * scale;
        height = desiredHeight * scale;
    }
    Ensures(width > 0);
    Ensures(height > 0);

    LOG(0, LG_INFO) << "going to create screen of size " << width << "x"
                    << height << " scale=" << scale;

    int err;

    err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    if (err < 0)
    {
        std::cout << "init video error: " << SDL_GetError() << std::endl;
        exit(1);
    }

    err = IMG_Init(IMG_INIT_PNG);
    if (err < 0)
    {
        std::cout << "init png: " << SDL_GetError() << std::endl;
        exit(1);
    }

#ifdef __DEVIL__
    ilInit();
#endif /* __DEVIL__ */

    // init sound
    sound::Init();

    if (_USE_OPENGL_CORE)
    {
        // SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        // SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
    }
    else if (_USE_OPENGL_ES)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_ES);
    }

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    unsigned int flags = 0;
    if (fullscreen)
    {
        flags |= SDL_WINDOW_BORDERLESS;
    }

    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, width, height,
                              SDL_WINDOW_OPENGL | flags);
    if (window == nullptr)
    {
        LOG(0, LG_ERROR) << "cannot create window: " << SDL_GetError();
        exit(1);
    }

    int windowId = SDL_GetWindowID(window);
    int monitorId = SDL_GetWindowDisplayIndex(window);
    (void)windowId;
    (void)monitorId;

    if (fullscreen)
    {
        SDL_SetWindowPosition(window, 0, 0);
    }

    ctx = SDL_GL_CreateContext(window);

    if (vsync)
    {
        err = SDL_GL_SetSwapInterval(1);
        if (err < 0)
        {
            std::cout << "vsync error: " << SDL_GetError() << std::endl;
            exit(1);
        }
    }
    else
    {
        err = SDL_GL_SetSwapInterval(0);
        if (err < 0)
        {
            std::cout << "vsync error 0: " << SDL_GetError() << std::endl;
            exit(1);
        }
    }

    glewExperimental = GL_TRUE;
    const GLenum glerr = glewInit();
    if (glerr != GLEW_OK)
    {
        printf("glew init error: %s\n", glewGetErrorString(glerr));
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

#ifdef _DEBUG
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);
#endif /* _DEBUG */

#ifdef _DEBUG
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();
    ImGui_ImplSDL2_InitForOpenGL(window, ctx);
    ImGui_ImplOpenGL3_Init(GLSL_VERSION);
#endif /* _DEBUG */

#ifdef _WIN32
    Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, nullptr);
#endif

    ready = true;
}

void GLScreen::runPipeline(Game* game)
{
    if (!renderPipeline)
    {
        LOG(LG_ERROR, LG_ERROR) << "no render pipeline set!";
        return;
    }

    clear();
    renderPipeline->run(game);
}

void GLScreen::runStatePipeline(std::shared_ptr<GameState> gs)
{
    if (!renderPipeline)
    {
        LOG(LG_ERROR, LG_ERROR) << "no render pipeline set!";
        return;
    }

    clear();

    renderPipeline->stateRun(gs);
}

void GLScreen::flip()
{
    SDL_GL_SwapWindow(window);
}

void GLScreen::postRender()
{
#ifdef _DEBUG
    if (guiBuilder)
    {
        guiBuilder->render();
    }
#endif /* _DEBUG */
}

#ifdef _DEBUG
void GLScreen::setImGuiBuilder(std::shared_ptr<ImGuiBuilder> builder)
{
    guiBuilder = builder;
}
#endif /* _DEBUG */

void GLScreen::setPipeline(std::shared_ptr<Pipeline> pl)
{
    renderPipeline = pl;
}

void GLScreen::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLScreen::beginRender()
{
}

void GLScreen::destroy()
{
    // destroy sound
    sound::Stop();

#ifdef _DEBUG
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
#endif /* _DEBUG */
    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();

#ifdef _WIN32
    Gdiplus::GdiplusShutdown(gdiplusToken);
#endif
}

auto GLScreen::getWindow() -> SDL_Window*
{
    return window;
}

auto GLScreen::getWidth() -> uint32_t
{
    return width;
}

auto GLScreen::getHeight() -> uint32_t
{
    return height;
}

auto GLScreen::getScale() -> uint32_t
{
    return scale;
}

auto GLScreen::getResolutionWidth() -> uint32_t
{
    return RESX;
}

auto GLScreen::getResolutionHeight() -> uint32_t
{
    return RESY;
}

auto GLScreen::isReady() -> bool
{
    return ready;
}
auto GLScreen::pixelToUnit(int x, int y) -> glm::vec2
{
    return {float(x) / float(getWidth() * getScale()) * 2.0f - 1.0f,
            float(y) / float(getHeight() * getScale()) * 2.0f - 1.0f};
}
void GLScreen::hideWindow()
{
    SDL_HideWindow(window);
}
} // namespace mode7
