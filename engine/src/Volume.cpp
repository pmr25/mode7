#include "Volume.hpp"
#include "AssetManager.hpp"
#include "Mesh.hpp"
#include "ModelLoader.hpp"
#include "Plane.hpp"

namespace mode7
{

static std::shared_ptr<Mesh> s_debugMesh;
static std::shared_ptr<Shader> s_debugShader;

void Volume::setDebugMesh(const std::string& filename)
{
    s_debugMesh = ModelLoader::openMesh(filename, AssetManager::getCurrent());
}

void Volume::setDebugShader(std::shared_ptr<Shader> s)
{
    s_debugShader = s;
}

void Volume::drawDebugMesh(Shader* shader)
{
    if (!s_debugMesh)
    {
        return;
    }

    s_debugMesh->setPosition(getPosition() + glm::vec3(0, 4, 0));
    s_debugMesh->update();
    s_debugMesh->draw(shader);
}

void Volume::setPosition(glm::vec3 pos)
{
    m_position = pos;
}

void Volume::setInternalPosition(glm::vec3 pos)
{
    m_internalPosition = pos;
}

glm::vec3 Volume::getPosition() const
{
    return m_internalPosition + m_position;
}

} // namespace mode7
