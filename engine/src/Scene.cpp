/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Scene.hpp"

#include <cassert>
#include <iostream>

namespace mode7
{
Scene::Scene()
    : Object()
{
}

Scene::~Scene()
{
    for (auto& e : m_children)
    {
        e->m_parent = nullptr;
    }
}

void Scene::addMesh(const std::shared_ptr<Mesh>& mesh)
{
    meshes.push_back(mesh);
    addChild((Object*)meshes.back().get());
}

Mesh* Scene::getMesh(unsigned int index)
{
    return meshes[index].get();
}

std::shared_ptr<Mesh> Scene::getMeshShared(unsigned int index)
{
    return meshes[index];
}

void Scene::draw(Shader* s)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    for (const std::shared_ptr<Mesh>& e : meshes)
    {
        e->draw(s);
    }
}

std::vector<std::shared_ptr<Mesh>>& Scene::getMeshes()
{
    return meshes;
}

void Scene::update()
{
    // if (scheduler)
    // {
    //     for (auto& e : meshes)
    //     {
    //         scheduler->addOneTimeJobData(e.get());
    //     }
    //     Object::update();
    // }
    // else
    {
        for (const std::shared_ptr<Mesh>& e : meshes)
        {
            e->update();
        }

        Object::update();
    }
}

// void Scene::setScheduler(std::shared_ptr<pmr25::Scheduler<MeshUpdateWorker>>
// sch)
// {
//     scheduler = sch;
// }
} // namespace mode7
