#include "HUDCairoElement.hpp"
#include "Logger.hpp"

#include <cstdlib>
#include <cstring>
#include <iostream>

namespace mode7
{
void HUDCairoElement::buildWidget(cairo_t*)
{
}

void HUDCairoElement::init()
{
    cairo_t* cr;

    mSurface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, getTexSize(),
                                          getTexSize());
    cr = cairo_create(mSurface);

    buildWidget(cr);

    uint8_t* data = cairo_image_surface_get_data(mSurface);
    size_t size = getTexSize() * getTexSize() * sizeof(uint32_t);
    uint8_t* conv = (uint8_t*)malloc(size);
    memcpy(conv, data, size);

    // process channels
    // for (uint32_t i = 0; i < getTexSize() * getTexSize(); ++i)
    // {
    //     std::cout << "pixel: " << (int)conv[i * 4 + 0] << "," << (int)conv[i
    //     * 4 + 1] << "," << (int)conv[i * 4 + 2] << "," << (int)conv[i * 4 +
    //     3] << std::endl; uint8_t swap = conv[i * 4 + 0]; conv[i * 4 + 0] =
    //     conv[i * 4 + 1]; conv[i * 4 + 1] = conv[i * 4 + 2]; conv[i * 4 + 2] =
    //     conv[i * 4 + 3]; conv[i * 4 + 3] = swap; std::cout << "conv: " <<
    //     (int)conv[i * 4 + 0] << "," << (int)conv[i * 4 + 1] << "," <<
    //     (int)conv[i * 4 + 2] << "," << (int)conv[i * 4 + 3] << std::endl;
    // }

    if (!mTexture.isReady())
    {
        mTexture.fill(conv, GL_BGRA, getTexSize(), getTexSize());
    }
    else
    {
        mTexture.update(conv, 0, 0, getTexSize(), getTexSize(), GL_BGRA);
    }
    free(conv);

    cairo_destroy(cr);

    createMesh(getDrawWidth(), getDrawHeight());
}

void HUDCairoElement::show()
{
    //    std::cout << "show: " << isDirty << ", " << getName() << std::endl;
    if (!isDirty)
    {
        return;
    }

    cairo_t* cr;

    cr = cairo_create(mSurface);
    if (cr == nullptr)
    {
        SLOG(LG_ERROR) << "cannot create cairo context";
        return;
    }

    //    std::cout << "pre-buildwidget" << std::endl;
    buildWidget(cr);

    uint8_t* data = cairo_image_surface_get_data(mSurface);
    if (data == nullptr)
    {
        SLOG(LG_ERROR) << "cannot get pixel data from surface";
        cairo_destroy(cr);
        return;
    }

    size_t size = getTexSize() * getTexSize() * sizeof(uint32_t);
    auto conv = (uint8_t*)malloc(size);
    memcpy(conv, data, size);
    if (!mTexture.isReady())
    {
        mTexture.fill(conv, GL_BGRA, getTexSize(), getTexSize());
    }
    else
    {
        mTexture.update(conv, 0, 0, getTexSize(), getTexSize(), GL_BGRA);
    }

    free(conv);
    cairo_destroy(cr);

    markClean();
}
void HUDCairoElement::clearSurface(cairo_t* ctx)
{
    cairo_set_source_rgba(ctx, 0, 0, 0, 0);
    cairo_set_operator(ctx, CAIRO_OPERATOR_CLEAR);
    cairo_rectangle(ctx, 0, 0, getWidth(), getHeight());
    cairo_paint_with_alpha(ctx, 1);

    cairo_set_source_rgba(ctx, 0, 0, 0, 1);
    cairo_set_operator(ctx, CAIRO_OPERATOR_SOURCE);
}
} // namespace mode7
