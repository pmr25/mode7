#include "Job.hpp"

static uint32_t idTracker = 0;

namespace mode7
{
void Job::assignID()
{
    mJobID = idTracker++;
}
} // namespace mode7
