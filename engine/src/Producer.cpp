#include "Producer.hpp"
#include "Consumer.hpp"
#include "ControlJob.hpp"

#include <iostream>

namespace mode7
{
void Producer::createConsumers(uint8_t numWorkers)
{
    // std::cout << "startup" << std::endl;
    for (auto i{0U}; i < numWorkers; ++i)
    {
        Consumer* worker = new Consumer(this);
        mConsumers.push_back(worker);

        // mThreads.push_back(std::thread(*worker));
    }
    mSemaphore.release();
}

void Producer::startConsumers()
{
    for (auto i{0U}; i < mConsumers.size(); ++i)
    {
        mThreads.push_back(std::thread(*mConsumers[i]));
    }
}

void Producer::destroyConsumers()
{
    // std::cout << "cleanup" << std::endl;
    for (auto i{0U}; i < mConsumers.size(); ++i)
    {
        delete mConsumers[i];
    }
}

void Producer::feedJob(Job* job)
{
    // std::lock_guard lock(mMutex);
    mSemaphore.acquire();
    mReadyJobs.push_back(job);
    mSemaphore.release();
}

void Producer::distribute()
{
    // signal stop
    for (auto i{0U}; i < mConsumers.size(); ++i)
    {
        feedJob(new ControlJob(CONTROLJOB_QUIT));
    }
    // mNotifyWorkerCV.notify_one();

    // wait for workers to stop
    // std::cout << "joining threads" << std::endl;
    for (auto& thread : mThreads)
    {
        thread.join();
    }
}

Job* Producer::getNextJob()
{
    // this runs in the critical section
    assert(mReadyJobs.size() > 0);
    auto next = mReadyJobs[0];
    mReadyJobs.pop_front();
    mDoneJobs.push_back(next);
    return next;
}
} // namespace mode7
