/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Clock.hpp"

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <chrono>
#include <iostream>
#include <vector>

#ifdef __linux__
#include <csignal>
#include <unistd.h>
#endif /* __linux__ */

#if defined _CLOCK_SOURCE_SDL
#define MILLIS SDL_GetTicks
#elif defined _CLOCK_SOURCE_CHRONO
#define MILLIS                                                                 \
    chrono::duration_cast<chrono::milliseconds>(                               \
        chrono::system_clock::now().time_since_epoch())                        \
        .count
namespace chrono = std::chrono;
#endif

static wall_t cur = 0;
static wall_t prev = 0;
static delta_t elapsed = 0;
static delta_t lag = 0;
static double m_fps;

#ifdef __linux__
static delta_t alarm_ival;
static volatile sig_atomic_t print_metrics_flag = false;
#endif

#ifdef __linux__
static void handle_alarm(int sig)
{
    (void)sig; // unused

    print_metrics_flag = true;
}
#endif

static void print_metrics_if_available()
{
#ifdef __linux__
    if (print_metrics_flag)
    {
        std::cout << "fps: " << m_fps << std::endl;
        print_metrics_flag = false;
        alarm(alarm_ival);
    }
#endif /* __linux__ */
}

static std::vector<std::pair<uint32_t, uint32_t>> countdowns;
static std::vector<std::function<void()>> functors;

namespace mode7
{
void Clock::start()
{
    prev = MILLIS();
    lag = 0;
}

wall_t Clock::millis()
{
    return MILLIS();
}

void Clock::tick()
{
    for (auto& trigger : countdowns)
    {
        --trigger.first;
        if (trigger.first == 0)
        {
            std::invoke(functors[trigger.second]);
        }
    }

    cur = MILLIS();
    elapsed = (delta_t)(cur - prev);
    m_fps = 1000.0 / (double)elapsed;
    prev = cur;
    lag += elapsed;
}

void Clock::lagTick()
{
    print_metrics_if_available();
    lag -= getInterval();
}

delta_t Clock::delta()
{
    return elapsed;
}

bool Clock::lagging()
{
    return lag >= getInterval();
}

double Clock::fps()
{
    return m_fps;
}

double Clock::extrapolate()
{
    double ex = (double)lag / (double)getInterval();
    if (ex < 0.0f || ex > 1.0f)
    {
        ex = 0.0f;
    }
    return ex;
}

void Clock::enableMetrics(delta_t ival)
{
#ifdef __linux__
    alarm_ival = ival;
    signal(SIGALRM, handle_alarm);
    alarm(alarm_ival);
#endif
}
void Clock::addTrigger(uint32_t ival, std::function<void()> func)
{
    countdowns.emplace_back(ival, functors.size());
    functors.push_back(func);
}
}; // namespace mode7
