#include "HUDManager.hpp"
#include "Mouse.hpp"
#include "Screen.hpp"

#include <iostream>

namespace mode7
{
void HUDManager::addElement(std::shared_ptr<HUDElement> elem)
{
    elem->init();
    mElements.push_back(elem);
}

void HUDManager::drawElements() const
{
    glViewport(0, 0, GLScreen::getWidth(), GLScreen::getHeight());

    uint32_t sw = GLScreen::getWidth() / GLScreen::getScale();
    uint32_t sh = GLScreen::getHeight() / GLScreen::getScale();

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    mShader.onlyUse();

    for (const auto& element : mElements)
    {
        if (!element->isVisible())
        {
            continue;
        }

        // set pos, theta
        const glm::vec2 elemOrigin =
            element->getPosition() +
            0.5F * glm::vec2(element->getWidth(), element->getHeight());
        const glm::vec2 realPos =
            (elemOrigin * glm::vec2(2.0F / (float)sw, 2.0F / (float)sh)) -
            glm::vec2(1.0F);

        glUniform2fv(mPositionUniform, 1, VEC(realPos));
        glUniform1f(mThetaUniform, element->getTheta());

        // set, enable texture
        const GLuint tid = element->getTexture();
        glUniform1i(mTextureUniform, 0);
        glActiveTexture(GL_TEXTURE0 + 0);
        glBindTexture(GL_TEXTURE_2D, tid);

        // perform draw
        element->draw();
    }
    glEnable(GL_DEPTH_TEST);
}

void HUDManager::handleClick(uint32_t clickX, uint32_t clickY)
{
    // TODO improve search
    for (std::shared_ptr<HUDElement> e : mElements)
    {
        e->handleClick(clickX, clickY);
    }
}

void HUDManager::setupShader()
{
    mPositionUniform = mShader.getUniformLocation("widgetPosition");
    mThetaUniform = mShader.getUniformLocation("widgetTheta");
    mTextureUniform = mShader.getUniformLocation("tex");
}

void HUDManager::update()
{
    if (true)
    {
        handleClick(Mouse::getX(), Mouse::getY());
    }
    for (std::shared_ptr<HUDElement> e : mElements)
    {
        e->update();
        e->show();
    }
}
auto HUDManager::getElementByName(const std::string& name)
    -> std::shared_ptr<HUDElement>
{
    for (auto& element : mElements)
    {
        if (element->getName() == name)
        {
            return element;
        }
    }

    return {}; // nullptr
}
} // namespace mode7
