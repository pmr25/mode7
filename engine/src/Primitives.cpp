/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Primitives.hpp"

#include "ModelLoader.hpp"
#include "Vertex.hpp"

#include "gl.hpp"
#include <vector>

namespace mode7
{
const std::string cube_obj = R""""(o Cube
v 1.000000 1.000000 -1.000000
v 1.000000 -1.000000 -1.000000
v 1.000000 1.000000 1.000000
v 1.000000 -1.000000 1.000000
v -1.000000 1.000000 -1.000000
v -1.000000 -1.000000 -1.000000
v -1.000000 1.000000 1.000000
v -1.000000 -1.000000 1.000000
vt 0.333333 0.000000
vt 0.000000 0.333333
vt 0.000000 0.000000
vt 0.333333 0.666667
vt 0.000000 0.333333
vt 0.333333 0.333333
vt 0.666667 0.333333
vt 0.333333 0.000000
vt 0.666667 0.000000
vt 0.666667 0.333333
vt 0.333333 0.666667
vt 0.333333 0.333333
vt 0.333333 1.000000
vt 0.000000 0.666667
vt 0.333333 0.666667
vt 1.000000 0.333333
vt 0.666667 0.000000
vt 1.000000 0.000000
vt 0.333333 0.333333
vt 0.000000 0.666667
vt 0.333333 0.333333
vt 0.666667 0.666667
vt 0.000000 1.000000
vt 0.666667 0.333333
vn 0.0000 1.0000 0.0000
vn 0.0000 0.0000 1.0000
vn -1.0000 0.0000 0.0000
vn 0.0000 -1.0000 0.0000
vn 1.0000 0.0000 0.0000
vn 0.0000 0.0000 -1.0000
usemtl Material
s off
f 5/1/1 3/2/1 1/3/1
f 3/4/2 8/5/2 4/6/2
f 7/7/3 6/8/3 8/9/3
f 2/10/4 8/11/4 6/12/4
f 1/13/5 4/14/5 2/15/5
f 5/16/6 2/17/6 6/18/6
f 5/1/1 7/19/1 3/2/1
f 3/4/2 7/20/2 8/5/2
f 7/7/3 5/21/3 6/8/3
f 2/10/4 4/22/4 8/11/4
f 1/13/5 3/23/5 4/14/5
f 5/16/6 1/24/6 2/17/6
)"""";

std::shared_ptr<Mesh> Primitives::cube(float length)
{
    return rectangle(length, length, length);
}

std::shared_ptr<Mesh> Primitives::rectangle(float length, float width,
                                            float height)
{
    (void)length;
    (void)width;
    (void)height;
    return ModelLoader::openSharedFromMem(cube_obj, AssetManager::getCurrent())
        ->getMeshShared(0);
}
} // namespace mode7
