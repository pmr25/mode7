#include "RollingAverage.hpp"

namespace mode7
{

auto RollingAverage::ingest(float val) -> void
{
    mBuffer.push_front(val);
    mBuffer.pop_back();
}
auto RollingAverage::compute() const -> float
{
    float sum{0.0F};

    for (std::size_t i{0}; i < mWeights.size(); ++i)
    {
        sum += mBuffer[i] * mWeights[i];
    }

    return sum;
}

} // namespace mode7