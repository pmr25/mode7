#include "Object2D.hpp"
#include "Util.hpp"

#include <glm/gtx/string_cast.hpp>
#include <iostream>

namespace mode7
{
void Object2D::setPosition(glm::vec2 vec)
{
    mPosition = vec;
    dirty();
}
void Object2D::setRotation(float theta)
{
    mTheta = theta;
    dirty();
}
void Object2D::setScale(glm::vec2 vec)
{
    mScale = vec;
    dirty();
}
void Object2D::setFront(glm::vec2 vec)
{
    vec = glm::normalize(vec);
    const double newTheta = Util::deg(Util::angleBetween(
        glm::vec3(vec.x, vec.y, 0), glm::vec3(mFront.x, mFront.y, 0)));

    rotate(float(newTheta));
    mFront = vec;

    dirty();
}
void Object2D::setVelocity(glm::vec2 vec)
{
    mVelocity = vec;
    dirty();
}
void Object2D::translate(glm::vec2 vec)
{
    setPosition(mPosition + vec);
}
void Object2D::rotate(float theta)
{
    setRotation(mTheta + theta);
}
void Object2D::scale(glm::vec2 vec)
{
    setScale(mScale * vec);
}
void Object2D::impulse(glm::vec2 vec)
{
    setVelocity(mVelocity + vec);
}
void Object2D::update()
{
    update(glm::mat2(1.0F), glm::vec2(0.0F));
}
void Object2D::update(const glm::mat2& accumulate, glm::vec2 accumulatePosition)
{
    if (mShouldUpdate)
    {
        const double radians = double(mTheta) * M_PI / 180.0;
        const glm::mat2 rotation{cos(radians), sin(radians), -sin(radians),
                                 cos(radians)};
        const glm::mat2 scale{mScale.x, 0, 0, mScale.y};

        mRotationMatrix = rotation;
        mScaleMatrix = scale;
        mMatrix = rotation * scale;
        mWorldMatrix = accumulate * mMatrix;
        mWorldPosition = accumulatePosition + mPosition;

        mFront = glm::normalize(mRotationMatrix * glm::vec2(1, 0));
        glm::vec3 right3 =
            glm::cross(glm::vec3(mFront.x, 0, mFront.y), glm::vec3(0, 1, 0));
        mRight = {right3.x, right3.z};

        clean();
    }

    for (auto& child : mChildren)
    {
        child->dirty();
        child->update(mWorldMatrix, mWorldPosition);
    }
}
auto Object2D::transform(glm::vec2 point) const -> glm::vec2
{
    return mWorldMatrix * point + mWorldPosition;
}
void Object2D::dirty()
{
    mShouldUpdate = true;
}
void Object2D::clean()
{
    mShouldUpdate = false;
}
auto Object2D::getWorldPosition() const -> glm::vec2
{
    return mWorldPosition;
}
auto Object2D::getWorldFront() const -> glm::vec2
{
    return glm::normalize(mWorldMatrix * glm::vec2(1, 0));
}
void Object2D::addChild(Object2D* child)
{
    update();
    child->update();

    child->setParent(this);
    mChildren.push_back(child);
}
void Object2D::setParent(Object2D* parent)
{
    mParent = parent;
    mPosition -= mParent->mPosition;
    mTheta -= mParent->mTheta;
}
auto Object2D::getPosition() const -> glm::vec2
{
    return mPosition;
}
auto Object2D::getFront() const -> glm::vec2
{
    return mFront;
}
void Object2D::setName(const std::string& name)
{
    mName = name;
}
auto Object2D::getName() const -> std::string
{
    return mName;
}
auto Object2D::getTheta() const -> float
{
    return mTheta;
}
} // namespace mode7