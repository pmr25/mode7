#include "LightGroup.hpp"
#include "glec.hpp"
#include <cstdint>
#include <iostream>
#include <sstream>

#define LIGHTS_BIND_INDEX 2
#define BUFFER_TYPE GL_UNIFORM_BUFFER
#define NUM_LIGHTS 16

namespace mode7
{
void LightGroup::init()
{
    glGenBuffers(1, &mSSBO);
    glBindBuffer(BUFFER_TYPE, mSSBO);
    // glBufferData()
    glBindBufferBase(BUFFER_TYPE, LIGHTS_BIND_INDEX, mSSBO); // assign to index
    glBindBuffer(BUFFER_TYPE, 0);                            // unbind
}

void LightGroup::destroy()
{
    glDeleteBuffers(1, &mSSBO);
    mSSBO = 0;
}

void LightGroup::cacheUniformLocations(Shader* shader)
{
    const GLuint pid = shader->pid();
    shader->onlyUse();

    mNumLightsLoc = glGetUniformLocation(pid, "numLights");

    std::string name;
    const std::string& prefix = "lights";
    std::stringstream ss;
    for (uint32_t i = 0; i < NUM_LIGHTS; ++i)
    {
        // TODO fix this is horrible
        ss.str("");
        ss.clear();
        ss << prefix << "[" << i << "]"
           << ".";

        name = ss.str() + "position";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "color";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "ambient";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "diffuse";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "specular";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "spotDirection";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "spotExponent";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "spotCutoff";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "spotCosCutoff";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "attenConst";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "attenLinear";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "attenQuadratic";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());

        name = ss.str() + "attenuation";
        mLocations[name] = glGetUniformLocation(pid, name.c_str());
    }
}

void LightGroup::fillUniforms()
{
    glUniform1ui(mNumLightsLoc, mLights.size());

    GLuint loc;
    std::stringstream ss;
    const std::string& prefix = "lights";

    for (uint32_t i = 0; i < mLights.size(); ++i)
    {
        ss.str("");
        ss.clear();
        ss << prefix << "[" << i << "]"
           << ".";

        const LightParams params = mLights[i]->getParams();

        loc = mLocations[ss.str() + "position"];
        glUniform4fv(loc, 1, VEC(params.position));

        loc = mLocations[ss.str() + "color"];
        glUniform4fv(loc, 1, VEC(params.color));

        loc = mLocations[ss.str() + "ambient"];
        glUniform4fv(loc, 1, VEC(params.ambient));

        loc = mLocations[ss.str() + "diffuse"];
        glUniform4fv(loc, 1, VEC(params.diffuse));

        loc = mLocations[ss.str() + "specular"];
        glUniform4fv(loc, 1, VEC(params.specular));

        loc = mLocations[ss.str() + "spotDirection"];
        glUniform3fv(loc, 1, VEC(params.spotDirection));

        loc = mLocations[ss.str() + "spotExponent"];
        glUniform1f(loc, (params.spotExponent));

        loc = mLocations[ss.str() + "spotCutoff"];
        glUniform1f(loc, (params.spotCutoff));

        loc = mLocations[ss.str() + "spotCosCutoff"];
        glUniform1f(loc, (params.spotCosCutoff));

        loc = mLocations[ss.str() + "attenConst"];
        glUniform1f(loc, (params.attenConst));

        loc = mLocations[ss.str() + "attenLinear"];
        glUniform1f(loc, (params.attenLinear));

        loc = mLocations[ss.str() + "attenQuadratic"];
        glUniform1f(loc, (params.attenQuadratic));

        loc = mLocations[ss.str() + "attenuation"];
        glUniform3fv(loc, 1, VEC(params.attenuation));
    }
}
} // namespace mode7
