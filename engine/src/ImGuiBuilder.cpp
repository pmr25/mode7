#ifdef _DEBUG

#include "ImGuiBuilder.hpp"
#include "gl.hpp"
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>
#include <iostream>

namespace mode7
{
void ImGuiBuilder::show()
{
    ImGui::ShowDemoWindow();
}

void ImGuiBuilder::render()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    show();

    ImGui::Render();
    const ImGuiIO& io = ImGui::GetIO();
    glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
} // namespace mode7

#endif /* _DEBUG */
