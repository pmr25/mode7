/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Timer.hpp"
#include "Clock.hpp"

#include <iostream>

namespace mode7
{
Timer::Timer()
    : position(0)
    , interval(0)
    , loop(false)
    , running(false)
{
}

Timer::~Timer()
{
}

void Timer::create(int inter, bool l)
{
    interval = inter;
    loop = l;
}

void Timer::start()
{
    running = true;
    reset();
}

void Timer::reset()
{
    position = interval;
}

int Timer::hookMillis()
{
    if (running && !hooks.empty() && hooks.contains(position))
    {
        return position * Clock::getInterval();
    }

    return -1;
}

bool Timer::tick()
{
    if (!running)
    {
        return false;
    }

    if (position > 0)
    {
        --position;
    }
    else
    {
        if (loop)
        {
            reset();
        }
        else
        {
            running = false;
        }

        return true;
    }

    return false;
}
void Timer::setIntervalTicks(int val)
{
    interval = val;
}
void Timer::setIntervalMillis(int ms)
{
    interval = (ms / Clock::getInterval());
}
void Timer::addHookMillis(int remaining)
{
    hooks.insert(remaining / Clock::getInterval());
}
auto Timer::getMillisRemaining() const -> int
{
    std::cout << position * Clock::getInterval() << std::endl;
    return position * Clock::getInterval();
}
} // namespace mode7