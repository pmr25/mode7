#include "GodControls.hpp"
#include "Keyboard.hpp"
#include "Mouse.hpp"

#define SENSITIVITY 0.5f

namespace mode7
{
void GodControls::updateControls()
{
    if (Mouse::isGrabbed())
    {
        const float dx = -(float)Mouse::getRelX() * SENSITIVITY;
        const float dy = -(float)Mouse::getRelY() * SENSITIVITY;
        const glm::vec3 uv = Util::yAxis();
        if (Mouse::getRelY() != 0)
        {
            rotateAboutLocal(dy, glm::vec3(1, 0, 0));
        }
        if (Mouse::getRelX() != 0)
        {
            rotateAbout(dx, uv);
        }
        const glm::vec3 rotation = getRotation();
        const float TOP = 90 - 10;
        const float BOT = 270 + 10;

        if (rotation.x < 180 && rotation.x > TOP)
        {
            rotateAboutLocal(-dy, Util::xAxis());
        }
        else if (rotation.x < BOT && rotation.x > 180)
        {
            rotateAboutLocal(-dy, Util::xAxis());
        }
    }

    float front = 0.0F;
    float right = 0.0F;
    float xAxis = 0.0F;
    float yAxis = 0.0F;
    float zAxis = 0.0F;
    if (Keyboard::isDown(SDL_SCANCODE_W))
    {
        front -= 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_S))
    {
        front += 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_A))
    {
        right -= 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_D))
    {
        right += 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_UP))
    {
        zAxis -= 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_DOWN))
    {
        zAxis += 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_LEFT))
    {
        xAxis -= 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_RIGHT))
    {
        xAxis += 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_PAGEDOWN))
    {
        yAxis -= 1.0F;
    }
    if (Keyboard::isDown(SDL_SCANCODE_PAGEUP))
    {
        yAxis += 1.0F;
    }

    if (Keyboard::isPressed(SDL_SCANCODE_G))
    {
        if (Mouse::isGrabbed())
        {
            Mouse::release();
        }
        else
        {
            Mouse::grab();
        }
    }

    translateLocal(glm::vec3(right, 0, front));

    const glm::vec3 globalDV = glm::vec3(xAxis, yAxis, zAxis);
    translateLocalUp(globalDV, Util::yAxis());

    update();
    mCamera->updateView();
}

auto GodControls::getControlMatrix() -> glm::mat4
{
    return glm::mat4(1.0f);
}
} // namespace mode7
