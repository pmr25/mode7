#include "StateBasedGame.hpp"
#include "Clock.hpp"
#include "Core.hpp"
#include "Keyboard.hpp"
#include "Logger.hpp"
#include "Mouse.hpp"
#include "Screen.hpp"

#include <cassert>
#include <imgui.h>
#include <imgui_impl_sdl.h>

namespace mode7
{
void StateBasedGame::addState(const std::string& key,
                              std::shared_ptr<GameState> gs)
{
    gs->mManager = this;

    mStates.push_back(gs);
    mStateLookup[key] = mStates.size();
}

void StateBasedGame::setCurrentState(const std::string& key)
{
    auto loc = mStateLookup.find(key);

    if (loc == mStateLookup.end())
    {
        SLOG(LG_ERROR) << "game state " << key << " does not exist";
        return;
    }

    uint32_t index = (*loc).second;

    assert(index > 0);

    if (mCurrentState != nullptr)
    {
        mCurrentState->onLeave();
    }

    mCurrentState = mStates[index - 1];
    mCurrentState->onEnter();
}

void StateBasedGame::quit()
{
    mIsRunning = false;
}

void StateBasedGame::pollInput()
{
    Mouse::clearEvent();
    while (SDL_PollEvent(&mSdlEvent))
    {
#ifdef _DEBUG
        ImGui_ImplSDL2_ProcessEvent(&mSdlEvent);
#endif /* _DEBUG */

        if (mSdlEvent.type == SDL_QUIT)
        {
            mIsRunning = false;
        }

        Mouse::sendEvent(&mSdlEvent);
    }

    // SDL_PumpEvents();
    Keyboard::poll();
    Mouse::poll();
}

void StateBasedGame::mainLoop(int argc, char** argv)
{
    (void)argc;
    (void)argv;

    Keyboard::attach();
    Mouse::attach();

    for (auto& e : mStates)
    {
        e->init();
    }

    Clock::start();
    while (mIsRunning)
    {

        GLScreen::runStatePipeline(mCurrentState);
        mCurrentState->postRender();
        GLScreen::postRender();
        GLScreen::flip();

        Clock::tick();
        while (Clock::lagging())
        {
            pollInput();
            mCurrentState->update();
            Clock::lagTick();
        }
    }

    for (auto& e : mStates)
    {
        e->destroy();
    }

    mStates.clear();

    GLScreen::destroy();
}
std::shared_ptr<GameState> StateBasedGame::getState(const std::string& key)
{
    const auto& it{mStateLookup.find(key)};
    if (it == mStateLookup.end())
    {
        return std::shared_ptr<GameState>(nullptr);
    }

    const uint32_t idx{(*it).second};

    assert(idx <= mStates.size());

    return mStates[idx - 1];
}
} // namespace mode7
