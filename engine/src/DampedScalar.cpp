#include "DampedScalar.hpp"
#include "gsl/assert"

namespace mode7
{

auto DampedScalar::setTarget(float target) -> void
{
    mTarget = target;

    const float diff{mTarget - mCurrent};
    Expects(mStep > 0.0F);
    mVelocity = diff / mStep;
}

auto DampedScalar::reset(float target) -> void
{
    mTarget = target;
    mCurrent = target;
}

auto DampedScalar::update() -> void
{
    Expects(mStep > 0.0F);

    const float accel{mDampingCoefficient * -mVelocity};
    mCurrent += (mVelocity + accel) * mStep;
    if (mCurrent > 1.0F)
    {
        mCurrent = 1.0F;
        mVelocity = 0.0F;
    }
    else if (mCurrent < 0.0F)
    {
        mCurrent = 0.0F;
        mVelocity = 0.0F;
    }

    const float diff{mTarget - mCurrent};
    mVelocity = diff / mStep;
}
auto DampedScalar::getPosition() const -> float
{
    return mCurrent;
}

} // namespace mode7