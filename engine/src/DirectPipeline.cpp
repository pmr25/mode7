#include "DirectPipeline.hpp"
#include "Game.hpp"
#include "Screen.hpp"

#include <cassert>
#include <iostream>

namespace mode7
{
bool DirectPipeline::run(Game* game) const
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glViewport(0, 0, GLScreen::getWidth(), GLScreen::getHeight());
    // glClearColor(0.6, 0.6, 0.6, 1.0);
    // glClear(GL_COLOR_BUFFER_BIT);

    game->drawShader(nullptr);

    return true;
}

bool DirectPipeline::stateRun(std::shared_ptr<GameState> gs) const
{
    assert(gs.get() != nullptr);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glViewport(0, 0, GLScreen::getWidth(), GLScreen::getHeight());
    // glClearColor(0.6, 0.6, 0.6, 1.0);
    // glClear(GL_COLOR_BUFFER_BIT);

    gs->drawShader(nullptr);

    return true;
}
} // namespace mode7
