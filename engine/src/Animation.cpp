/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Animation.hpp"
#include "Core.hpp"
#include "Logger.hpp"
#include "TextureCache.hpp"
#include "TextureImage.hpp"

#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace mode7
{
Animation::Animation()
    : mCurrentFrame(0)
{
}

Animation::~Animation()
{
}

void Animation::open(const std::string& fn, const mode7::AssetManager& assets)
{
    std::string ap = mode7::AssetManager::getCurrent().assetPath(fn);
    LOG(0, LG_INFO) << "[Animation] open " << ap;
    std::ifstream in(ap);
    if (!in)
    {
        LOG(0, LG_WARN) << "[Animation] cannot open " << ap;
        return;
    }

    json o;
    in >> o;
    in.close();

    // parse
    for (auto& e : o["frames"])
    {
        LOG(0, LG_INFO) << "[Animation] add "
                        << e["diffuse"].get<std::string>();
        //        frames.push_back(
        //            TexCache::open(assets.texturePath(e["diffuse"].get<std::string>()),
        //                           TexType::DIFFUSE));

        TextureCache* texCache = Core::getTextureCacheInstance();
        frames.push_back(
            texCache->open(assets.texturePath(e["diffuse"].get<std::string>()),
                           TexType::DIFFUSE));
    }

    assert(frames.size() > 0);
}

void Animation::setCurrentFrame(uint32_t index)
{

    if (index >= frames.size())
    {
        LOG(0, LG_WARN) << "[Animation] cannot set current frame to " << index;
        return;
    }

    mCurrentFrame = index;
}

Texture* Animation::getMap() const
{
    assert(mCurrentFrame < frames.size());
    return frames[mCurrentFrame];
}

Texture* Animation::getMap(uint32_t index) const
{
    (void)index;

    return frames[mCurrentFrame];
}

uint32_t Animation::numMaps() const
{
    return 1;
}
} // namespace mode7