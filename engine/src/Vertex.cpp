/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Vertex.hpp"

namespace mode7
{

auto Vertex::interleave(std::vector<float>& buffer, uint8_t flags) -> void
{
    if (SHOULD_INCLUDE(flags, VERTEX_POSITION))
    {
        buffer.push_back(position[0]);
        buffer.push_back(position[1]);
        buffer.push_back(position[2]);
    }

    if (SHOULD_INCLUDE(flags, VERTEX_NORMAL))
    {
        buffer.push_back(normal[0]);
        buffer.push_back(normal[1]);
        buffer.push_back(normal[2]);
    }

    if (SHOULD_INCLUDE(flags, VERTEX_UV))
    {
        buffer.push_back(uv[0]);
        buffer.push_back(uv[1]);
    }

    if (SHOULD_INCLUDE(flags, VERTEX_TANGENT))
    {
        buffer.push_back(tangent[0]);
        buffer.push_back(tangent[1]);
        buffer.push_back(tangent[2]);
    }
}
auto Vertex::getStride(uint8_t flags) -> size_t
{
    return 3 * size_t(SHOULD_INCLUDE(flags, VERTEX_POSITION)) +
           3 * size_t(SHOULD_INCLUDE(flags, VERTEX_NORMAL)) +
           2 * size_t(SHOULD_INCLUDE(flags, VERTEX_UV)) +
           3 * size_t(SHOULD_INCLUDE(flags, VERTEX_TANGENT));
}

} // namespace mode7
