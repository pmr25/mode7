#include "Bitmap.hpp"
#include "Logger.hpp"

#ifdef __linux__
#include <SDL_image.h>
#elif defined _WIN32
#include <SDL2/SDL_image.h>
#endif
#include <cassert>
#include <cmath>

namespace mode7
{

auto Bitmap::createCairoSurface() -> cairo_surface_t*
{
    [[likely]] if (mCairoSurface != nullptr)
    {
        return mCairoSurface;
    }

    const auto cairoStride{
        cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, int(mWidth))};
    formattedData.resize(cairoStride * mHeight);
    for (auto row{0U}; row < mHeight; ++row)
    {
        for (auto col{0U}; col < mStride; ++col)
        {
            formattedData[row * cairoStride + col] = mData[row * mStride + col];
        }
    }

    mCairoSurface = cairo_image_surface_create_for_data(
        formattedData.data(), CAIRO_FORMAT_ARGB32, int(mWidth), int(mHeight),
        cairoStride);

    if (mCairoSurface == nullptr)
    {
        SLOG(LG_ERROR) << "cannot create surface";
        return nullptr;
    }

    const auto status{cairo_surface_status(mCairoSurface)};
    if (status != CAIRO_STATUS_SUCCESS)
    {
        SLOG(LG_ERROR) << cairo_status_to_string(status);
    }

    return mCairoSurface;
}

auto Bitmap::halveImage() -> void
{
    auto halfWidth{mWidth / 2};
    auto halfHeight{mHeight / 2};
    auto halfStride = halfWidth * getBytesPerPixel();
    std::vector<uint8_t> halfData;
    halfData.resize(halfStride * halfHeight);

    for (auto row{0U}; row < halfHeight; ++row)
    {
        for (auto col{0U}; col < halfWidth; ++col)
        {
            const auto pixel{row * halfStride + col * getBytesPerPixel()};
            const auto sample{row * 2 * mStride + col * 2 * getBytesPerPixel()};
            const auto sampleA{sample};
            const auto sampleB{sample + getBytesPerPixel()};
            const auto sampleC{sample + mStride};
            const auto sampleD{sample + mStride + getBytesPerPixel()};

            for (auto i{0U}; i < getBytesPerPixel(); ++i)
            {
                const double numPixels{4.0};
                halfData[pixel + i] =
                    uint8_t(round((uint32_t(mData[sampleA + i]) +
                                   uint32_t(mData[sampleB + i]) +
                                   uint32_t(mData[sampleC + i]) +
                                   uint32_t(mData[sampleD + i])) /
                                  numPixels));
            }
        }
    }

    mData = std::move(halfData);
    mWidth = halfWidth;
    mHeight = halfHeight;
    mStride = halfStride;
}
auto Bitmap::create(uint32_t width, uint32_t height, uint32_t bitsPerPixel)
    -> void
{
    const auto bytesPerPixel{bitsPerPixel / 8};
    const auto size{width * height * bytesPerPixel};
    mWidth = width;
    mHeight = height;
    mStride = width * (bytesPerPixel);
    mData.resize(size);
}
auto Bitmap::getBitsPerPixel() -> uint32_t
{
    return getBytesPerPixel() * 8;
}
auto Bitmap::getBytesPerPixel() const -> uint32_t
{
    return mStride / mWidth;
}
auto Bitmap::open(const std::string& fn) -> bool
{
    auto* sdlSurface{IMG_Load(fn.c_str())};
    if (sdlSurface == nullptr)
    {
        return false;
    }
    assert(sdlSurface->pixels);

    mWidth = sdlSurface->w;
    mHeight = sdlSurface->h;
    mStride = sdlSurface->pitch;

    mData =
        std::vector<uint8_t>((uint8_t*)sdlSurface->pixels,
                             (uint8_t*)sdlSurface->pixels + mStride * mHeight);
    SDL_FreeSurface(sdlSurface);

    if (getBytesPerPixel() == 4)
    {
        for (auto i{0U}; i < mData.size() / getBytesPerPixel(); ++i)
        {
            reformat(mData.begin() + (i * getBytesPerPixel()), {3, 0, 1, 2});
        }
    }
    else if (getBytesPerPixel() == 3)
    {
        const auto alphaStride{mWidth * 4};
        std::vector<uint8_t> alphaData(alphaStride * mHeight, 0);
        for (auto i{0U}; i < alphaData.size() / 4; ++i)
        {
            alphaData[i * 4 + 0] = mData[i * 3 + 0];
            alphaData[i * 4 + 1] = mData[i * 3 + 1];
            alphaData[i * 4 + 2] = mData[i * 3 + 2];
            alphaData[i * 4 + 3] = 255;
        }

        mData = std::move(alphaData);
        mStride = alphaStride;
    }
    return true;
}
auto Bitmap::debugSavePNG(const std::string& fn) -> cairo_status_t
{
    cairo_surface_t* surf = createCairoSurface();
    const auto status{cairo_surface_write_to_png(surf, fn.c_str())};
    return status;
}
auto Bitmap::reformat(std::vector<uint8_t>::iterator pixel,
                      std::vector<int> order) -> void
{
    std::vector<uint8_t> scratch(order.size(), 0);
    for (auto i{0U}; i < order.size(); ++i)
    {
        //        const auto chan{order[i]};
        const auto idx{pixel + order[i]};
        scratch[i] = *idx;
    }

    for (auto i{0U}; i < order.size(); ++i)
    {
        *(pixel + i) = scratch[i];
    }
}
auto Bitmap::getHeight() -> uint32_t
{
    return mHeight;
}
auto Bitmap::getWidth() -> uint32_t
{
    return mWidth;
}
auto Bitmap::bilinearResize(uint32_t width, uint32_t height) -> void
{
    const auto stride{width * getBytesPerPixel()};
    std::vector<uint8_t> resizedData(stride * height, 0);

    const double widthRatio{double(mWidth - 1) / (width - 1)};
    const double heightRatio{double(mHeight - 1) / (height - 1)};

    for (auto row{0U}; row < height; ++row)
    {
        for (auto col{0U}; col < width; ++col)
        {
            const double xFloor{floor(widthRatio * col)};
            const double yFloor{floor(heightRatio * row)};
            const double xCeil{ceil(widthRatio * col)};
            const double yCeil{ceil(heightRatio * row)};

            const double xWeight = (widthRatio * col) - xFloor;
            const double yWeight = (heightRatio * row) - yFloor;

            auto a{mData.begin() + yFloor * mStride +
                   xFloor * getBytesPerPixel()};
            auto b{mData.begin() + yFloor * mStride +
                   xCeil * getBytesPerPixel()};
            auto c{mData.begin() + yCeil * mStride +
                   xFloor * getBytesPerPixel()};
            auto d{mData.begin() + yCeil * mStride +
                   xCeil * getBytesPerPixel()};

            for (auto channel{0U}; channel < getBytesPerPixel(); ++channel)
            {
                const double pixel{
                    double(*(a + channel)) * (1 - xWeight) * (1 - yWeight) +
                    double(*(b + channel)) * xWeight * (1 - yWeight) +
                    double(*(c + channel)) * yWeight * (1 - xWeight) +
                    double(*(d + channel)) * xWeight * yWeight};
                resizedData[row * stride + col * getBytesPerPixel() + channel] =
                    pixel;
            }
        }
    }

    mData = std::move(resizedData);
    mWidth = width;
    mHeight = height;
    mStride = stride;
}
auto Bitmap::shrink(uint32_t width, uint32_t height) -> void
{
    if (width > mWidth || height > mHeight)
    {
        return;
    }

    while (mWidth / 2 >= width && mHeight / 2 >= height)
    {
        halveImage();
    }

    if (mWidth > width && mHeight > height)
    {
        bilinearResize(width, height);
    }
}
auto Bitmap::createWithData(void* buffer, uint32_t width, uint32_t height,
                            uint32_t stride) -> void
{
    create(width, height, (stride / width) * 8);
    for (uint32_t i = 0; i < mData.size(); ++i)
    {
        mData[i] = ((uint8_t*)buffer)[i];
    }
}

} // namespace mode7