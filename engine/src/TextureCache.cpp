#include "TextureCache.hpp"
#include "filesystem.hpp"

namespace mode7
{
auto TextureCache::open(const std::string& filename, TexType type)
    -> TextureImage*
{
    const std::string name = fs::path(filename).filename().string();

    const auto& futureNameTexturePair = mFutures.find(name);
    if (futureNameTexturePair != mFutures.end()) // future hit
    {
        convertFuture(name);
        return mCache[name].get();
    }

    const auto& cacheNameTexturePair = mCache.find(name);
    if (cacheNameTexturePair != mCache.end()) // cache hit
    {
        return cacheNameTexturePair->second.get();
    }

    // cache miss

    auto filenamePtr = std::make_shared<std::string>(filename);

    auto future = std::async(
        std::launch::async,
        [](std::string fn)
        {
            TextureImage texture;
            texture.loadIntoMemory(fn);
            return texture;
        },
        filename);
    mFutures[name] = std::move(future);

    mCache[name] = std::make_shared<TextureImage>();
    mCache[name]->setType(type);
    return mCache[name].get();
}

auto TextureCache::finish() -> void
{
    while (mFutures.size() > 0)
    {
        const auto& nameFuturePair = *mFutures.begin();
        const auto& name = nameFuturePair.first;
        convertFuture(name);
    }
}

auto TextureCache::convertFuture(const std::string& name) -> void
{
    auto nameFuturePair = mFutures.find(name);
    assert(nameFuturePair != mFutures.end());
    assert(mCache.find(name) != mCache.end());
    TexType transfer = mCache[name]->getType();
    *(mCache[name]) = nameFuturePair->second.get();
    mCache[name]->genGPUTexture();
    mCache[name]->setType(transfer);
    mFutures.erase(nameFuturePair);
}

} // namespace mode7