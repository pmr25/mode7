#include "HUDButtonWidget.hpp"
#include "Core.hpp"
#include "Logger.hpp"
#include "Mouse.hpp"
#include "Screen.hpp"

#include <iostream>

#define FONT_PT 30.0
#define FONT_NM "klein_headline"
#define OFFSET 30.0

namespace mode7
{
void HUDButtonWidget::buildWidget(cairo_t* ctx)
{
    if (!mIsActive)
    {
        buildInactiveState(ctx);
    }
    else
    {
        if (mIsPressed)
        {
            buildPressedState(ctx);
        }
        else
        {
            buildDefaultState(ctx);
        }
    }
}

void HUDButtonWidget::buildDefaultState(cairo_t* ctx)
{
    const auto scale{GLScreen::getScale()};

    cairo_set_source_rgba(ctx, 0.4, 0.4, 0.4, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_fill(ctx);

    cairo_set_line_width(ctx, 10.0 * scale);
    cairo_set_source_rgba(ctx, 1.0, 0.0, 1.0, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_stroke(ctx);

    if (mText.length() > 0)
    {
        const double padding{3.0};
        Core::getFontsInstance()->drawText(ctx, mText, "racing Regular 30",
                                           padding + 10, padding);
    }
}

void HUDButtonWidget::buildPressedState(cairo_t* ctx)
{
    const auto scale{GLScreen::getScale()};

    cairo_set_source_rgba(ctx, 0.3, 0.3, 0.3, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_fill(ctx);

    cairo_set_line_width(ctx, 10.0 * scale);
    cairo_set_source_rgba(ctx, 0.0, 1.0, 0.0, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_stroke(ctx);

    if (mText.length() > 0)
    {
        const double padding{3.0};
        Core::getFontsInstance()->drawText(ctx, mText, "racing Regular 30",
                                           padding + 10, padding);
    }
}

void HUDButtonWidget::buildInactiveState(cairo_t* ctx)
{
    const auto scale{GLScreen::getScale()};

    cairo_set_line_width(ctx, 10.0 * scale);
    cairo_set_source_rgba(ctx, 0.6, 0.6, 0.6, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_fill(ctx);

    cairo_set_source_rgba(ctx, 0.4, 0.4, 0.4, 1.0);
    cairo_rectangle(ctx, 0, 0, getDrawWidth(), getDrawHeight());
    cairo_stroke(ctx);

    if (mText.length() > 0)
    {
        const double padding{6.0};
        Core::getFontsInstance()->drawText(ctx, mText, "racing Regular 30",
                                           padding + 10, padding);
    }
}

void HUDButtonWidget::press()
{
    if (mIsPressed)
    {
        return;
    }

    mIsPressed = true;
    markDirty();
}

void HUDButtonWidget::cancel()
{
    if (!mIsPressed)
    {
        return;
    }

    mIsPressed = false;
    markDirty();
}

void HUDButtonWidget::release()
{
    if (!mIsPressed)
    {
        return;
    }

    if (mCallback != nullptr)
    {
        mCallback(mCallbackData);
    }

    mIsPressed = false;
    markDirty();
}

bool HUDButtonWidget::listen()
{
    return false;
}

void HUDButtonWidget::handleClick(int32_t clickX, int32_t clickY)
{
    if (!mIsVisible)
    {
        return;
    }

    if (contains(clickX, GLScreen::getHeight() / GLScreen::getScale() - clickY))
    {
        if (Mouse::getLeft())
        {
            press();
        }
        else
        {
            release();
        }
    }
    else
    {
        cancel();
    }
}

void HUDButtonWidget::setCallback(void (*cb)(void*), void* data)
{
    mCallback = cb;
    mCallbackData = data;
}

void HUDButtonWidget::setText(const std::string& str)
{
    mText = str;
}
} // namespace mode7
