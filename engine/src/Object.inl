/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Object.hpp"
#include "Util.hpp"

namespace mode7
{
inline void Object::move()
{
    translate(m_velocity.x * m_right + m_velocity.y * m_up +
              m_velocity.z * -m_front);
}

inline void Object::localTranslate(float x, float y, float z)
{
    translate(x * m_right);
    translate(y * m_up);
    translate(z * m_front);
}

inline void Object::rotate(float x, float y, float z)
{
    rotate(glm::vec3(x, y, z));
}

inline void Object::setPosition(float x, float y, float z)
{
    setPosition(glm::vec3(x, y, z));
}

inline void Object::setPositionX(float val)
{
    setPosition(glm::vec3(val, m_position.y, m_position.z));
}

inline void Object::setPositionY(float val)
{
    setPosition(glm::vec3(m_position.x, val, m_position.z));
}

inline void Object::setPositionZ(float val)
{
    setPosition(glm::vec3(m_position.x, m_position.y, val));
}

inline void Object::setRotationX(float val)
{
    setRotation(glm::vec3(val, m_euler.y, m_euler.z));
}

inline void Object::setRotationY(float val)
{
    setRotation(glm::vec3(m_euler.x, val, m_euler.z));
}

inline void Object::setRotationZ(float val)
{
    setRotation(glm::vec3(m_euler.x, m_euler.y, val));
}

inline void Object::translate(float x, float y, float z)
{
    translate(glm::vec3(x, y, z));
}

inline void Object::scale(float val)
{
    scale(glm::vec3(val));
}

inline void Object::setScale(float val)
{
    setScale(val, val, val);
}

inline void Object::setScale(float x, float y, float z)
{
    setScale(glm::vec3(x, y, z));
}

inline void Object::impulse(float x, float y, float z)
{
    impulse(glm::vec3(x, y, z));
}

inline void Object::impulse(glm::vec3 vec)
{
    m_velocity += vec;
    dirty();
}

inline void Object::worldImpulse(glm::vec3 vec)
{
    glm::mat4 rot = glm::inverse(glm::toMat4(m_worldQuat));
    glm::vec4 v = rot * glm::vec4(vec.x, vec.y, vec.z, 0.0f);
    impulse(glm::vec3(v.x, v.y, v.z));
}

inline void Object::setVelocity(glm::vec3 vec)
{
    m_velocity = vec;
    dirty();
}

inline void Object::setVelocity(float x, float y, float z)
{
    setVelocity(glm::vec3(x, y, z));
}

inline void Object::setVelocityX(float val)
{
    setVelocity(glm::vec3(val, m_velocity.y, m_velocity.z));
}

inline void Object::setVelocityY(float val)
{
    setVelocity(glm::vec3(m_velocity.x, val, m_velocity.z));
}

inline void Object::setVelocityZ(float val)
{
    setVelocity(glm::vec3(m_velocity.x, m_velocity.y, val));
}

inline Object* Object::getParent()
{
    return m_parent;
}

inline glm::mat4& Object::getShaderMatrix()
{
    return m_worldMatrix;
}

inline const std::vector<Object*> Object::getChildren() const
{
    return m_children;
}

#ifdef _BULLET_PHYSICS
inline void Object::notifyPhysicsCollision(btCollisionObject* co)
{
    (void)co; // unused

    // do nothing
}
#endif

inline void Object::dirty()
{
    m_isDirty = true;
}

inline void Object::clean()
{
    m_isDirty = false;
}

inline void Object::formatEuler()
{
    m_euler.x = FORMAT_EULER(m_euler.x);
    m_euler.y = FORMAT_EULER(m_euler.y);
    m_euler.z = FORMAT_EULER(m_euler.z);
}

inline glm::quat Object::buildQuat(glm::vec3 v)
{
    return glm::angleAxis(Util::rad(v.x), Util::xAxis()) *
           glm::angleAxis(Util::rad(v.y), Util::yAxis()) *
           glm::angleAxis(Util::rad(v.z), Util::zAxis());
}

#ifdef _BULLET_PHYSICS
inline void Object::physRequestActive(bool val)
{
    m_physRequestActive = val;
}

inline void Object::setMass(float m)
{
    m_mass = m;
}

inline btRigidBody* Object::getRigidBody() const
{
    return m_rigidBody;
}
#endif

inline glm::vec3 Object::getWorldPosition()
{
    // if (m_isDirty)
    {
        glm::vec4 p = m_worldMatrix * glm::vec4(0.f, 0.f, 0.f, 1.f);
        mCachedWorldPosition = glm::vec3(p.x, p.y, p.z);
        // return glm::vec3(p.x, p.y, p.z);
    }

    return mCachedWorldPosition;
}

// TODO this sucks temp
/*inline glm::vec3 Object::getWorldVelocity() const
{
    glm::vec4 v = glm::toMat4(m_quaternion) * glm::vec4(m_velocity.x,
m_velocity.y, m_velocity.z, 0.0f); if (m_parent)
    {
        glm::mat4 pr = glm::toMat4(m_parent->m_quaternion);
        glm::vec3 pv = m_parent->getWorldVelocity();
        v = (pr * v) + glm::vec4(pv.x, pv.y, pv.z, 0.0f);
    }

    return v;
}*/

inline glm::vec3 Object::getWorldVelocity() const
{
    glm::vec4 v = glm::toMat4(m_worldQuat) *
                  glm::vec4(m_velocity.x, m_velocity.y, m_velocity.z, 0.0f);
    return v;
}

// TODO this sucks, temporary
inline glm::vec3 Object::getWorldScale() const
{
    if (m_parent)
    {
        return m_parent->getWorldScale() * m_scale;
    }

    return m_scale;
}

} // namespace mode7
