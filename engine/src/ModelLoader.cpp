/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "ModelLoader.hpp"
#include "Core.hpp"
#include "TextureCache.hpp"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <memory>

namespace mode7
{
std::shared_ptr<Scene> ModelLoader::openShared(const std::string& fn,
                                               const AssetManager& assets)
{
    std::shared_ptr<Scene> scene = std::make_shared<Scene>();
    int err = openFile(scene.get(), fn, assets);
    if (err != 0)
    {
        SLOG(LG_ERROR) << "loader error " << err;
        return nullptr;
    }

    return scene;
}

std::unique_ptr<Scene> ModelLoader::openUnique(const std::string& fn,
                                               const AssetManager& assets)
{
    std::unique_ptr<Scene> scene = std::make_unique<Scene>();
    int err = openFile(scene.get(), fn, assets);
    if (err != 0)
    {
        SLOG(LG_ERROR) << "loader error " << err;
        return nullptr;
    }

    return scene;
}

std::shared_ptr<Scene>
ModelLoader::openSharedFromMem(const std::string& src,
                               const AssetManager& assets)
{
    LOG(0, LG_DEBUG) << "[ModelLoader] open from mem";

    std::vector<std::shared_ptr<Mesh>> meshes;
    std::shared_ptr<Scene> s = std::make_shared<Scene>();
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFileFromMemory(
        src.c_str(), src.length(),
        aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE ||
        !scene->mRootNode)
    {
        LOG(0, 2) << "[ModelLoader] assimp error: "
                  << importer.GetErrorString();
        return s;
    }

    processNode(meshes, scene->mRootNode, scene, aiMatrix4x4(), assets);

    for (auto& e : meshes)
    {
        s->addMesh(e);
    }

    return s;
}

auto ModelLoader::openFile(Scene* scenePtr, const std::string& filename,
                           const AssetManager& assetManager) -> int
{
    std::vector<std::shared_ptr<Mesh>> meshes;
    Assimp::Importer importer;
    const aiScene* assimpScene =
        importer.ReadFile(filename, aiProcess_Triangulate | aiProcess_FlipUVs |
                                        aiProcess_CalcTangentSpace);
    if ((assimpScene == nullptr) ||
        ((assimpScene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) != 0U) ||
        (assimpScene->mRootNode == nullptr))
    {
        SLOG(LG_ERROR) << "[ModelLoader] assimp error: "
                       << importer.GetErrorString();
        return -1;
    }

    processNode(meshes, assimpScene->mRootNode, assimpScene, aiMatrix4x4(),
                assetManager);

    for (auto& mesh : meshes)
    {
        scenePtr->addMesh(mesh);
    }
    Core::getTextureCacheInstance()->finish();

    return 0;
}

std::shared_ptr<Mesh> ModelLoader::openMesh(const std::string& fn,
                                            const AssetManager& assets)
{
    auto scene = openUnique(fn, assets);
    if (scene->meshes.size() > 0)
    {
        std::shared_ptr<Mesh> out = scene->meshes[0];
        out->m_parent = nullptr;
        return out;
    }

    return std::shared_ptr<Mesh>(nullptr);
}

void ModelLoader::processNode(std::vector<std::shared_ptr<Mesh>>& vec,
                              aiNode* node, const aiScene* scene,
                              aiMatrix4x4 transform, const AssetManager& assets)
{
    const std::string name(node->mName.C_Str());

    for (unsigned int i = 0; i < node->mNumMeshes; ++i)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        vec.push_back(processMesh(name, mesh, scene,
                                  transform * node->mTransformation, assets));
    }

    for (unsigned int i = 0; i < node->mNumChildren; ++i)
    {
        processNode(vec, node->mChildren[i], scene,
                    transform * node->mTransformation, assets);
    }
}

std::shared_ptr<Mesh> ModelLoader::processMesh(const std::string& nodeName,
                                               aiMesh* mesh,
                                               const aiScene* scene,
                                               aiMatrix4x4 transform,
                                               const AssetManager& assets)
{
    const std::string name(mesh->mName.C_Str());

    std::shared_ptr<Mesh> out = std::make_shared<Mesh>();
    std::shared_ptr<Material> mat = std::make_shared<Material>();

    out->setName(nodeName);

    assert(mesh->HasNormals());
    assert(mesh->HasTangentsAndBitangents());

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    Vertex v;
    for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
    {
        v.position = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y,
                               mesh->mVertices[i].z);

        v.normal = glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y,
                             mesh->mNormals[i].z);

        if (mesh->mTextureCoords[0])
        {
            v.uv = glm::vec2(mesh->mTextureCoords[0][i].x,
                             mesh->mTextureCoords[0][i].y);
        }
        else
        {
            v.uv = glm::vec2(0.0F);
        }

        v.tangent = glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y,
                              mesh->mTangents[i].z);

        vertices.push_back(v);
    }

    for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
    {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; ++j)
        {
            indices.push_back(face.mIndices[j]);
        }
    }

    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    std::vector<Texture*> d = loadTextures(material, aiTextureType_DIFFUSE,
                                           "texture_diffuse", assets);
    for (auto& e : d)
    {
        mat->addMap(e);
    }

    out->m_inherentMatrix = Util::fromAi(transform);
    out->setMaterial(mat);
    out->createFromArrays(vertices, indices);

    return out;
}

std::vector<Texture*> ModelLoader::loadTextures(aiMaterial* mat,
                                                aiTextureType type,
                                                const std::string& name,
                                                const AssetManager& assets)
{
    TexType tt = TexType::DIFFUSE; // default

    if (name == "texture_diffuse")
    {
        tt = TexType::DIFFUSE;
    }
    else if (name == "texture_specular")
    {
        tt = TexType::SPECULAR;
    }
    std::vector<Texture*> out;

    std::string fn;
    for (unsigned int i = 0; i < mat->GetTextureCount(type); ++i)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        fn = assets.texturePath(std::string(str.C_Str()));
        out.push_back(Core::getTextureCacheInstance()->open(fn, tt));
    }

    return out;
}

} // namespace mode7
