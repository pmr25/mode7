#include "TextureAtlas.hpp"

#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace mode7
{
TextureAtlas::TextureAtlas()
{
}

int32_t TextureAtlas::open(const std::string& img_fn, const std::string& dat_fn)
{
    m_atlas = std::make_shared<TextureImage>();
    m_atlas->open(img_fn);

    std::ifstream in(dat_fn);
    if (!in)
    {
        std::cout << "cannot open " << dat_fn << std::endl;
        return 1;
    }
    json o;
    in >> o;

    if (!o.contains("num_images"))
    {
        return 1;
    }

    int num_images = o["num_images"].get<int>();

    if (!o.contains("metadata"))
    {
        return 2;
    }

    int index = 0;
    int x;
    int y;
    int w;
    int h;
    float su;
    float sv;
    float ou;
    float ov;
    std::string name;
    for (auto& e : o["metadata"])
    {
        name = e["name"];
        m_keyIndirection.push_back(name);
        m_lookup[name] = index;
        x = e["x"].get<int>();
        y = e["y"].get<int>();
        w = e["w"].get<int>();
        h = e["h"].get<int>();
        su = (float)w / (float)m_atlas->getWidth();
        sv = (float)h / (float)m_atlas->getHeight();
        ou = (float)x / (float)m_atlas->getWidth();
        ov = (float)y / (float)m_atlas->getHeight();

        std::shared_ptr<Texture> subtex = std::make_shared<Texture>();
        subtex->name = name;
        subtex->id = m_atlas->id;
        subtex->width = w;
        subtex->height = h;
        subtex->setUVTransform(su, sv, ou, ov);

        m_subTextures.push_back(subtex);

        ++index;
    }

    if (index != num_images)
    {
        return 3;
    }

    return 0;
}

std::shared_ptr<Texture> TextureAtlas::getTexture(const std::string& name)
{
    int index = m_lookup[name];
    return m_subTextures[index];
}

// glm::vec4 TextureAtlas::getUVTransform(const std::string& name)
// {
//     glm::vec4 bbox = m_bbox[m_lookup[name]];
//     float su = m_atlas->getWidth() / bbox[2];
//     float sv = m_atlas->getHeight() / bbox[3];
//     float ou = bbox[0] / m_atlas->getWidth();
//     float ov = bbox[1] / m_atlas->getHeight();

//     return glm::vec4(ou, ov, su, sv);
// }
} // namespace mode7
