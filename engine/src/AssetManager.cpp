/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "AssetManager.hpp"

namespace mode7
{
std::shared_ptr<AssetManager> AssetManager::m_current;

int AssetManager::setBaseDirectory(const std::string& dn)
{
    m_currentStructure->baseDir = fs::path(dn).lexically_normal();
    if (!fs::exists(m_currentStructure->baseDir) ||
        !fs::is_directory(m_currentStructure->baseDir))
    {
        return 1;
    }

    return 0;
}

int AssetManager::setModelDirectory(const std::string& dn)
{
    m_currentStructure->modelDir = fs::path(dn).lexically_normal();
    if (!fs::exists(m_currentStructure->modelDir) ||
        !fs::is_directory(m_currentStructure->modelDir))
    {
        return 1;
    }

    return 0;
}

int AssetManager::setShaderDirectory(const std::string& dn)
{
    m_currentStructure->shaderDir = fs::path(dn).lexically_normal();
    if (!fs::exists(m_currentStructure->shaderDir) ||
        !fs::is_directory(m_currentStructure->shaderDir))
    {
        return 1;
    }

    return 0;
}

int AssetManager::setTextureDirectory(const std::string& dn)
{
    m_currentStructure->textureDir = fs::path(dn).lexically_normal();
    if (!fs::exists(m_currentStructure->textureDir) ||
        !fs::is_directory(m_currentStructure->textureDir))
    {
        return 1;
    }

    return 0;
}

int AssetManager::setStructure(const std::string& key)
{
    auto it = m_structures.find(key);

    if (it == m_structures.end())
    {
        return 1;
    }

    m_currentStructure = &m_structures[key];

    return 0;
}

int AssetManager::pushStructure(const std::string& key)
{
    if (m_structures.find(key) != m_structures.end())
    {
        return 1;
    }

    std::string oldKey = (*m_structures.end()).first;
    m_structureStack.push(oldKey);

    m_structures[key] = AssetStructure();
    m_currentStructure = &m_structures[key];

    return 0;
}

int AssetManager::popStructure()
{
    if (m_structureStack.size() <= 1)
    {
        return 1;
    }

    std::string key = m_structureStack.top();
    m_structureStack.pop();
    m_currentStructure = &m_structures[key];

    return 0;
}

std::string AssetManager::constructPath(AssetStructure* st, fs::path mid,
                                        const std::string& filename) const
{
    assert(st != nullptr);
    fs::path tmp =
        (st->baseDir / fs::path(mid) / fs::path(filename)).lexically_normal();
    return tmp.string();
}

std::string AssetManager::assetPath(const std::string& filename) const
{
    return constructPath(m_currentStructure, fs::path(), filename);
}

std::string AssetManager::dataPath(const std::string& filename) const
{
    return constructPath(m_currentStructure, m_currentStructure->dataDir,
                         filename);
}

std::string AssetManager::modelPath(const std::string& filename) const
{
    return constructPath(m_currentStructure, m_currentStructure->modelDir,
                         filename);
}

std::string AssetManager::shaderPath(const std::string& filename) const
{
    fs::path out = m_currentStructure->baseDir / m_currentStructure->shaderDir /
                   fs::path(filename);
    return out.string();
}

std::string AssetManager::texturePath(const std::string& filename) const
{
    fs::path out = m_currentStructure->baseDir /
                   m_currentStructure->textureDir / fs::path(filename);
    return out.string();
}
} // namespace mode7
