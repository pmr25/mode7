#include "CameraBoom.hpp"

static size_t counter = 0;

namespace mode7
{

auto CameraBoom::setDive(float t) -> void
{
    mDiveMul = Util::lerp(0.0F, mDiveFactor, t);
}

auto CameraBoom::setLag(float t) -> void
{
    mLagMul = Util::lerp(0.0F, mLagFactor, t);
}

auto CameraBoom::setSlide(float t) -> void
{
    mSlideMul = Util::lerp(0.0F, mSlideFactor, t);
    mSlide.setTarget(t);
}

auto CameraBoom::setCamera(std::shared_ptr<Camera> camera, glm::vec3 offset,
                           glm::vec3 lookAt) -> void
{
    mCameraPosition = offset;
    mCameraLookAt = lookAt;
    mCamera = camera;
    addChild(mCamera->getObjectPtr());
}

auto CameraBoom::update(const glm::mat4& mat, const glm::quat& quat,
                        bool forceUpdate) -> void
{
    if (!mCamera)
    {
        return;
    }
    if (Object::getParent() == nullptr)
    {
        return;
    }

    const auto deltaPos{Object::getParent()->getWorldPosition() -
                        getWorldPosition()};
    mJoint.applyForce({0, -deltaPos.y, 0});

    mSlide.update();
    mJoint.update();

    glm::vec3 positionAdjust(0, 0, 0);
    glm::vec3 lookAdjust(0, 0, 0);

    positionAdjust += -mDiveMul * glm::vec3(0, 1, 0);

    positionAdjust += mJoint.getPosition();
    lookAdjust += mJoint.getPosition();

    assert(mCamera);
    mCamera->lookAt(mCameraPosition + positionAdjust,
                    mCameraLookAt + lookAdjust);
    mCamera->setFOV(mDefaultFov + mFovMul);

    Object::update(mat, quat, forceUpdate);
}

auto CameraBoom::setFov(float t) -> void
{
    mFovMul = Util::lerp(0.0F, mFovFactor, t);
}

auto CameraBoom::setCoefficients(float diveFactor, float lagFactor,
                                 float slideFactor, float fovFactor,
                                 float fovConst) -> void
{
    mDiveFactor = diveFactor;
    mLagFactor = lagFactor;
    mSlideFactor = slideFactor;
    mFovFactor = fovFactor;
    mDefaultFov = fovConst;
}

auto CameraBoom::getIndex() -> std::string
{
    return std::to_string(counter++);
}
auto CameraBoom::applyForce(float x, float y, float z) -> void
{
    mJoint.applyForce(glm::vec3(x, y, z));
}

} // namespace mode7
