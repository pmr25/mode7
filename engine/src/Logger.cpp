#include "Logger.hpp"
#include "filesystem.hpp"

namespace mode7
{
static fs::path logDir;

void Logger::init(int argc, char** argv)
{
    loguru::init(argc, argv);
    loguru::g_stderr_verbosity = 1;
}

void Logger::setLogDir(const std::string& dn)
{
    logDir = fs::path(dn);
    fs::create_directory(logDir);
}

void Logger::setOutputFile(const std::string& filename)
{
    const fs::path path = logDir / fs::path(filename);
    loguru::add_file(path.string().c_str(), loguru::Append,
                     loguru::Verbosity_MAX);
}
} // namespace mode7
