/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Shader.hpp"
#include "Camera.hpp"
#include "Logger.hpp"
#include "Screen.hpp"
#include "Texture.hpp"

#include "filesystem.hpp"
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

namespace mode7
{
static std::string openFile(const std::string& filename)
{
    std::ifstream fp(filename);
    if (!fp)
    {
        return "";
    }

    std::stringstream code;
    code << fp.rdbuf();
    fp.close();

    return code.str();
}

static int compile(GLuint id, const std::string& code)
{
    GLint res = 0;
    int loglen;

    const char* ptr = code.c_str();
    glShaderSource(id, 1, &ptr, nullptr);
    glCompileShader(id);

    glGetShaderiv(id, GL_COMPILE_STATUS, &res);
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 0)
    {
        std::vector<char> errMsg;
        errMsg.resize(loglen + 1);
        glGetShaderInfoLog(id, loglen, NULL, &errMsg[0]);
        printf("%s\n", &errMsg[0]);
    }

    return res;
}

int Shader::open(const std::string& vfilename, const std::string& ffilename)
{
    assert(GLScreen::isReady());

    GLuint vsid = glCreateShader(GL_VERTEX_SHADER);
    GLuint fsid = glCreateShader(GL_FRAGMENT_SHADER);

    setName(fs::path(vfilename).filename().string() + " - " +
            fs::path(ffilename).filename().string());

    std::string vs_code = openFile(vfilename);
    std::string fs_code = openFile(ffilename);
    if (!vs_code.size() || !fs_code.size())
    {
        std::cout << "ERROR! cannot find " << vfilename << "," << ffilename
                  << std::endl;
        return 1;
    }

    LOG(0, 3) << "[" << m_name << "] compile " << vfilename;
    compile(vsid, vs_code);

    LOG(0, 3) << "[" << m_name << "] compile " << vfilename;
    compile(fsid, fs_code);

    GLuint pid = glCreateProgram();
    glAttachShader(pid, vsid);
    glAttachShader(pid, fsid);
    glLinkProgram(pid);

    GLint res;
    int loglen;

    glGetProgramiv(pid, GL_LINK_STATUS, &res);
    glGetProgramiv(pid, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 0)
    {
        std::vector<char> errMsg;
        errMsg.resize(loglen + 1);
        glGetShaderInfoLog(id, loglen, NULL, &errMsg[0]);
        printf("%s\n", &errMsg[0]);
    }

    glDetachShader(pid, vsid);
    glDetachShader(pid, fsid);
    glDeleteShader(vsid);
    glDeleteShader(fsid);

    id = pid;
    glUseProgram(id);

    cacheLocations();

    LOG(lg, 3) << "[" << m_name << "] id=" << id << std::endl;

    return 0;
}

auto Shader::open(const std::string& vfilename, const std::string& gfilename, const std::string& ffilename)
-> int
{
    assert(GLScreen::isReady());

    GLuint vsid = glCreateShader(GL_VERTEX_SHADER);
    GLuint gsid = glCreateShader(GL_GEOMETRY_SHADER);
    GLuint fsid = glCreateShader(GL_FRAGMENT_SHADER);

    setName(fs::path(vfilename).filename().string() + " - " +
            fs::path(ffilename).filename().string());

    std::string vs_code = openFile(vfilename);
    std::string gs_code = openFile(gfilename);
    std::string fs_code = openFile(ffilename);
    if (!vs_code.size() || !fs_code.size())
    {
        std::cout << "ERROR! cannot find " << vfilename << "," << ffilename
                  << std::endl;
        return 1;
    }

    SLOG(LG_DEBUG) << "[" << m_name << "] compile " << vfilename;
    compile(vsid, vs_code);

    SLOG(LG_DEBUG) << "[" << m_name << "] compile " << gfilename;
    compile(gsid, gs_code);

    SLOG(LG_DEBUG) << "[" << m_name << "] compile " << vfilename;
    compile(fsid, fs_code);

    GLuint pid = glCreateProgram();
    glAttachShader(pid, vsid);
    glAttachShader(pid, gsid);
    glAttachShader(pid, fsid);
    glLinkProgram(pid);

    GLint res;
    int loglen;

    glGetProgramiv(pid, GL_LINK_STATUS, &res);
    glGetProgramiv(pid, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 0)
    {
        std::vector<char> errMsg;
        errMsg.resize(loglen + 1);
        glGetShaderInfoLog(id, loglen, nullptr, &errMsg[0]);
        SLOG(LG_ERROR) << std::string(errMsg.data());
    }

    glDetachShader(pid, vsid);
    glDetachShader(pid, gsid);
    glDetachShader(pid, fsid);
    glDeleteShader(vsid);
    glDeleteShader(gsid);
    glDeleteShader(fsid);

    id = pid;
    glUseProgram(id);

    cacheLocations();

    return 0;
}

void Shader::cacheCameraMatrices()
{
    projection = glGetUniformLocation(id, "p");
    view = glGetUniformLocation(id, "v");

    if (projection < 0)
    {
        LOG(lg, LG_INFO) << "[" << m_name << "] cannot find projection uniform";
    }

    if (view < 0)
    {
        LOG(lg, LG_INFO) << "[" << m_name << "] cannot find view uniform";
    }
}

void Shader::cacheLocations()
{
    const uint32_t n_diffuseMaps = 16;
    const uint32_t n_specularMaps = 16;

    cacheCameraMatrices();

    uvTransform = glGetUniformLocation(id, "uv_transform");

    model = glGetUniformLocation(id, "m");
    if (model < 0)
    {
        LOG(lg, LG_INFO) << "[" << m_name << "] cannot find model uniform";
    }

    diffuseTexture = glGetUniformLocation(id, "tex");
    if (diffuseTexture < 0)
    {
        LOG(lg, LG_INFO) << "[" << m_name << "] cannot find tex uniform";
    }

    uv_tile = glGetUniformLocation(id, "uv_tile");
    if (uv_tile < 0)
    {
        LOG(lg, LG_INFO) << "[" << m_name << "] cannot find uv_tile uniform";
    }

    std::stringstream ss;
    m_diffuseMaps.reserve(n_diffuseMaps);
    ss.str("");
    ss.clear();
    for (uint32_t i = 0; i < n_diffuseMaps; ++i)
    {
        ss << "diffuseMap[" << i << "]";
        m_diffuseMaps.push_back(glGetUniformLocation(id, ss.str().c_str()));
        ss.clear();
    }

    m_specularMaps.reserve(n_specularMaps);
    ss.str("");
    ss.clear();
    for (uint32_t i = 0; i < n_specularMaps; ++i)
    {
        ss << "specularMap[" << i << "]";
        m_specularMaps.push_back(glGetUniformLocation(id, ss.str().c_str()));
        ss.clear();
    }

    m_normalMaps.reserve(n_specularMaps);
    ss.str("");
    ss.clear();
    for (uint32_t i = 0; i < n_specularMaps; ++i)
    {
        ss << "normalMap[" << i << "]";
        m_normalMaps.push_back(glGetUniformLocation(id, ss.str().c_str()));
        ss.clear();
    }

    camera_params_loc = glGetUniformLocation(id, "camera_params");

    always_up_loc = glGetUniformLocation(id, "always_up");
    particle_type_loc = glGetUniformLocation(id, "particle_type");
    blur_amt_loc = glGetUniformLocation(id, "blur_amt");
    alpha_threshold_loc = glGetUniformLocation(id, "alpha_threshold");
    particle_color_loc = glGetUniformLocation(id, "particle_color");

    mAmbientLoc = glGetUniformLocation(id, "material.ambient");
    mDiffuseLoc = glGetUniformLocation(id, "material.diffuse");
    mEmissionLoc = glGetUniformLocation(id, "material.emission");
    mSpecularLoc = glGetUniformLocation(id, "material.specular");
    mShininessLoc = glGetUniformLocation(id, "material.shininess");
    mFresnelLoc = glGetUniformLocation(id, "material.fresnel");
    mDiffuseFloorLoc = glGetUniformLocation(id, "material.diffuseFloor");
}

void Shader::cacheLocations(uint32_t flags)
{
    if (flags & MATRICES)
    {
    }

    if (flags & TEXTUREMAPS)
    {
    }

    if (flags & TEXTURECOORDS)
    {
    }
}

void Shader::onlyUse() const
{
    if (!isReady())
    {
        return;
    }

    glUseProgram(id);
}

void Shader::use() const
{
    onlyUse();

    glUniformMatrix4fv(projection, 1, GL_FALSE,
                       MAT(Camera::getCurrent().getProjection()));
    glUniformMatrix4fv(view, 1, GL_FALSE, MAT(Camera::getCurrent().getView()));
}

void Shader::setMaterial(const Material& m)
{
    [[unlikely]] if (!isReady())
    {
        return;
    }

    glUniform1i(uv_tile, m.tile);

    GLuint loc = 0;
    uint32_t diffuseTracker = 0;
    uint32_t normalTracker = 0;
    uint32_t specularTracker = 0;
    for (uint32_t i = 0; i < m.numMaps(); ++i)
    {
        Texture* t = m.getMap(i);
        assert(t != nullptr);
        if (t->getType() == TexType::DIFFUSE)
        {
            assert(i < m_diffuseMaps.size());
            loc = m_diffuseMaps[diffuseTracker++];
        }
        else if (t->getType() == TexType::SPECULAR)
        {
            assert(i < m_specularMaps.size());
            loc = m_specularMaps[specularTracker++];
        }
        else if (t->getType() == TexType::NORMAL)
        {
            assert(i < m_normalMaps.size());
            loc = m_normalMaps[normalTracker++];
        }
        else
        {
            loc = m_diffuseMaps[diffuseTracker++]; // default
            SLOG(LG_ERROR) << "texture has no type";
        }

        glUniform4fv(uvTransform, 1, VEC(t->getUVTransform()));
        glUniform1i(loc, i);
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, t->getId());
    }

    glUniform4fv(mAmbientLoc, 1, VEC(m.getAmbient()));
    glUniform4fv(mDiffuseLoc, 1, VEC(m.getDiffuse()));
    glUniform4fv(mEmissionLoc, 1, VEC(m.getEmission()));
    glUniform4fv(mSpecularLoc, 1, VEC(m.getSpecular()));
    glUniform1f(mShininessLoc, m.getShininess());
    glUniform1f(mFresnelLoc, m.getFresnel());
    glUniform1f(mDiffuseFloorLoc, m.getDiffuseFloor());
}

void Shader::setModel(Object* o)
{
    [[unlikely]] if (!isReady())
    {
        SLOG(LG_WARN) << " [" << m_name << "]  Shader not ready!";
        return;
    }

    glUniformMatrix4fv(model, 1, GL_FALSE, MAT(o->getShaderMatrix()));
}

void Shader::setVec3(const std::string& key, float* vec)
{
    (void)key;
    (void)vec;
}

void Shader::setCameraParams(Camera* cam)
{
    glm::vec4 params(cam->getFov(), cam->getNear(), cam->getFar(), 0.0);

    glUniform4fv(camera_params_loc, 1, (float*)(&params[0]));
}

void Shader::setAlwaysUp(GLint val)
{
    glUniform1i(always_up_loc, val);
}

void Shader::setParticleType(particletype_t type)
{
    glUniform1i(particle_type_loc, (GLint)type);
}

void Shader::setBlurAmount(float amt)
{
    glUniform1f(blur_amt_loc, amt);
}

void Shader::setAlphaThreshold(float thr)
{
    glUniform1f(alpha_threshold_loc, thr);
}
} // namespace mode7
