/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "ParticleEmitter.hpp"
#include "Util.hpp"

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

namespace mode7
{
ParticleEmitter::ParticleEmitter()
    : position(0.0)
    , direction(0.0, 1.0, 3.0)
    , n_particles(0)
    , m_enabled(false)
    , m_speed(2.0)
    , m_defaultLife(1.0)
    , m_dirSpread(0.0)
    , m_velSpread(0.0)
    , m_lifeSpread(0.0)
{
}

ParticleEmitter::~ParticleEmitter()
{
}

void ParticleEmitter::init(uint32_t np, uint32_t tid)
{
    m_shader.open("assets/shaders/particle_v.glsl",
                  "assets/shaders/particle_f.glsl");

    n_particles = np;
    m_particles.resize(n_particles);
    for (uint32_t i = 0; i < np; ++i)
    {
        m_ready.push(&m_particles[i]);

        m_particles[i].init(&m_shader, tid, glm::vec4(1.0, 1.0, 1.0, 1.0));
    }
}

Particle* ParticleEmitter::firstAvailable()
{
    Particle* out;
    out = m_ready.front();
    m_ready.pop();
    return out;
}

glm::vec3 ParticleEmitter::randomRadius(float mag)
{
    Util::seed();

    // compute basis vectors
    glm::vec3 nprime(-direction.x, direction.x, direction.y);
    glm::vec3 p = position + nprime;
    float dist = glm::dot(p - position, direction);
    glm::vec3 delta = glm::normalize(direction) * dist;
    glm::vec3 b1 = glm::normalize(p - delta);
    glm::vec3 b2 = glm::cross(glm::normalize(direction), b1);

    const float sq2 = sqrtf(2.0);
    const float rx =
        sq2 * mag * static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    const float ry =
        sq2 * mag * static_cast<float>(rand()) / static_cast<float>(RAND_MAX);

    return rx * b1 + ry * b2;
}

float ParticleEmitter::randomScalar(float mag)
{
    return mag * static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

void ParticleEmitter::update()
{
    if (!m_enabled)
    {
        return;
    }

    uint32_t n_active = (uint32_t)(m_particles.size() - m_ready.size());
    if (n_active < n_particles)
    {
        Particle* next = m_ready.front();
        m_ready.pop();
        next->spawn(position,
                    (m_speed + randomScalar(m_velSpread)) *
                        glm::normalize(direction + randomRadius(m_dirSpread)),
                    m_defaultLife + randomScalar(m_lifeSpread));
        n_active = (uint32_t)(m_particles.size() - m_ready.size());
    }

    bool done;
    for (auto& e : m_particles)
    {
        if (!e.isVisible())
        {
            continue;
        }

        // e.m_pos = position;
        done = e.update();
        if (done)
        {
            m_ready.push(&e);
        }
    }
}

void ParticleEmitter::draw()
{
    if (!m_enabled)
    {
        return;
    }

    glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    for (auto& e : m_particles)
    {
        if (!e.isVisible())
        {
            continue;
        }
        e.draw();
    }
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
}
} // namespace mode7
