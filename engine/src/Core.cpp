/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Core.hpp"
#include "Clock.hpp"
#include "FontsBackendGDI.hpp"
#include "FontsBackendPango.hpp"
#include "Logger.hpp"
#include "Scripting.hpp"
#include "TextureCache.hpp"
#include "Util.hpp"

#include <map>
#include <string>
#include <vector>

#ifdef _DEBUG
#include <cfenv>
#endif /* _DEBUG */

static bool ready = false;

namespace mode7
{

static TextureCache sTextureCache;

bool Core::isReady()
{
    return ready;
}

static std::map<std::string, std::string>
parseFlags(std::vector<std::string> args)
{
    std::map<std::string, std::string> flagValueMap;
    bool parsingFlag = false;
    std::string flagName;
    std::string flagValue;

    for (auto& e : args)
    {
        if (parsingFlag)
        {
            if (e[0] == '-') // flag has no value field, set to true and look
                             // for next flag
            {
                flagValueMap[flagName] = "1";
            }
            else
            {
                flagValue = e;
                flagValueMap[flagName] = flagValue;
                parsingFlag = false;
            }
        }
        if (e[0] == '-')
        {
            parsingFlag = true;
            flagName = e.substr(1);
        }
    }
    if (parsingFlag)
    {
        flagValueMap[flagName] = "1";
    }

    return flagValueMap;
}

static std::vector<std::string> parseArgs(std::vector<std::string> rawArgs)
{
    std::vector<std::string> args;

    for (auto& e : rawArgs)
    {
        if (e[0] != '-')
        {
            args.push_back(e);
        }
    }

    return args;
}

int Core::init(int argc, char** argv)
{
    std::vector<std::string> rawArgs;
    for (int i = 1; i < argc; ++i)
    {
        rawArgs.push_back(argv[i]);
    }

    const auto flags = parseFlags(rawArgs);
    const auto args = parseArgs(rawArgs);

    Logger::init(argc, argv);
    initLogging();

    LOG(0, 0) << "[Core] logging system ready";

#ifdef _DEBUG
    if (flags.find("nofpe") == flags.end())
    {
#ifdef __linux__
        feenableexcept(FE_INVALID | FE_OVERFLOW);
#endif /* __linux__ */
#ifdef _WIN32
        _controlfp(_MCW_EM, _MCW_EM);
#endif /* _WIN32 */
    }
#endif /* _DEBUG */

    if (flags.find("clockMetrics") != flags.end())
    {
        Clock::enableMetrics(1);
    }

    if (Core::getFontsInstance() == nullptr)
    {
        SLOG(ERROR) << "cannot get fonts manager instance";
        return 1;
    }

    SLOG(INFO) << "[Core] core ready";

    ready = true;

    return 0;
}

int Core::initLogging()
{
    Logger::setLogDir("./logs");
    Logger::setOutputFile(Util::timestamp() + ".log");

    return 0;
}
Fonts* Core::getFontsInstance()
{
#ifdef __linux__
    auto* fonts{mode7::FontsBackendPango::getFontsInstance()};
#elif defined _WIN32
    auto* fonts{mode7::FontsBackendGDI::getInstance()};
#endif

    return fonts;
}

TextureCache* Core::getTextureCacheInstance()
{
    return &sTextureCache;
}
} // namespace mode7
