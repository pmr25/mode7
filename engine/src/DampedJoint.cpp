#include "DampedJoint.hpp"

namespace mode7
{

auto DampedJoint::applyForce(glm::vec3 forceVector) -> void
{
    mXAxis.applyForce(forceVector.x);
    mYAxis.applyForce(forceVector.y);
    mZAxis.applyForce(forceVector.z);
}

auto DampedJoint::update() -> void
{
    mXAxis.update();
    mYAxis.update();
    mZAxis.update();
}

auto DampedJoint::getPosition() -> glm::vec3
{
    return {mXAxis.getPosition(), mYAxis.getPosition(), mZAxis.getPosition()};
}

} // namespace mode7