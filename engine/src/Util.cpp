/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifdef _WIN32
#include <Windows.h>
#include <tchar.h>
#endif

#include "Line.hpp"
#include "Util.hpp"
#include "filesystem.hpp"
#include "gsl/assert"

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>

#ifdef __linux__
#include <limits.h>
#include <unistd.h>
#endif

namespace mode7
{
const std::string Util::getInstallDir()
{
#ifdef __linux__
    ssize_t r;

    char buf[PATH_MAX];
    r = readlink("/proc/self/exe", buf, PATH_MAX - 1);
    if (r != -1)
    {
        buf[r] = '\0';
        return fs::path(std::string(buf)).parent_path().string();
    }

    return "";
#endif
#ifdef _WIN32
    TCHAR buf[MAX_PATH];
    if (!GetModuleFileName(NULL, buf, MAX_PATH))
    {
        return "";
    }

    const std::wstring tmp(buf);
    return fs::path(std::string(tmp.begin(), tmp.end())).parent_path().string();
#endif
}

void Util::split(std::vector<std::string>& dst, std::string src,
                 std::string delim)
{
    size_t pos = 0;
    std::string token;

    dst.clear();

    while ((pos = src.find(delim)) != std::string::npos)
    {
        token = src.substr(0, pos);
        dst.push_back(token);
        src.erase(0, pos + delim.length());
    }

    dst.push_back(src);
}

float Util::formatRad(float rad)
{
    if (rad > M_2_PI)
    {
        return rad - floorf((rad / M_2_PI)) * M_2_PI;
    }
    else if (rad < -M_2_PI)
    {
        return rad + floorf((rad / M_2_PI)) * M_2_PI;
    }

    return rad;
}

void Util::seed()
{
    srand((size_t)time(NULL));
}

float Util::lerp(float v0, float v1, float t)
{
    return (1 - t) * v0 + t * v1;
}

float Util::map(float x, float a0, float a1, float b0, float b1)
{
    return ((x - a0) / (a1 - a0)) * (b1 - b0) + b0;
}

#ifdef _BULLET_PHYSICS

btVector3 Util::toBullet(glm::vec3 gv)
{
    return btVector3((btScalar)gv[0], (btScalar)gv[2], (btScalar)gv[1]);
}

btVector3 Util::toBulletRaw(glm::vec3 gv)
{
    return btVector3((btScalar)gv[0], (btScalar)gv[1], (btScalar)gv[2]);
}

std::string Util::btVector3ToStr(btVector3 v)
{
    std::stringstream ss;
    ss << "btVector3(" << v.x() << ", " << v.y() << ", " << v.z() << ")";
    return ss.str();
}

glm::vec3 Util::fromBullet(btVector3 bv)
{
    return glm::vec3((float)bv.x(), (float)bv.z(), (float)bv.y());
}

glm::vec3 Util::fromBulletRaw(btVector3 bv)
{
    return glm::vec3((float)bv.x(), (float)bv.y(), (float)bv.z());
}

#endif

glm::mat4 Util::fromAi(const aiMatrix4x4& from)
{
    return {from.a1, from.b1, from.c1, from.d1, from.a2, from.b2,
            from.c2, from.d2, from.a3, from.b3, from.c3, from.d3,
            from.a4, from.b4, from.c4, from.d4};
}

std::string Util::timestamp()
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y_%H-%M-%S");
    return oss.str();
}

bool Util::strStartsWith(const std::string& str, const std::string& prefix)
{
    if (prefix.length() > str.length())
    {
        return false;
    }

    return prefix == str.substr(0, prefix.length());
}

bool Util::strEndsWith(const std::string& str, const std::string& postfix)
{
    if (postfix.length() > str.length())
    {
        return false;
    }

    return postfix == str.substr(str.length() - postfix.length());
}

std::vector<glm::vec3> Util::computeMinBoundingRect(uint8_t const* pixels,
                                                    uint32_t width,
                                                    uint32_t height)
{
    Expects(width != 0);
    Expects(height != 0);

    // assuming 32 bpp
    // assuming alpha channel is first byte in pixel
    // bounding to first pixel with alpha > 0

    const uint32_t bytes_per_pixel = 4;
    std::vector<glm::vec3> quad;
    quad.resize(8);

    int32_t minW = width;
    int32_t maxW = 0;
    int32_t minH = height;
    int32_t maxH = 0;

    for (uint32_t y = 0; y < height; ++y)
    {
        for (uint32_t x = 0; x < width; ++x)
        {
            const uint32_t idx = (y * width + x) * bytes_per_pixel;
            const uint8_t alpha = pixels[idx + 0];
            if (alpha > 0)
            {
                minW = (uint32_t)fmin(minW, x);
                maxW = (uint32_t)fmax(maxW, x);
                minH = (uint32_t)fmin(minH, height - y);
                maxH = (uint32_t)fmax(maxH, height - y);
            }
        }
    }

    const uint32_t pad = 1;
    minW -= pad - 1;
    maxW += pad;
    minH -= pad - 1;
    maxH += pad;
    const float invW = 1.0f / (float)width;
    const float invH = 1.0f / (float)height;
    const float shift = 0;
    const glm::vec3 scale(invW, invH, 0);
    quad[0] = scale * glm::vec3(minW, minH - shift, 0);
    quad[1] = scale * glm::vec3(maxW, minH - shift, 0);
    quad[2] = scale * glm::vec3(maxW, maxH - shift, 0);
    quad[3] = scale * glm::vec3(minW, maxH - shift, 0);
    quad[4] = scale * glm::vec3(maxW, -minH + height, 0);
    quad[5] = scale * glm::vec3(minW, -minH + height, 0);
    quad[6] = scale * glm::vec3(minW, -maxH + height, 0);
    quad[7] = scale * glm::vec3(maxW, -maxH + height, 0);

    return quad;
}

/********** https://www.particleincell.com/2012/quad-interpolation/ **********/

auto Util::unitBilinear(float x, float y, float a, float b, float c, float d)
    -> float
{
    return a * (1 - x) * (1 - y) + b * x * (1 - y) + d * (1 - x) * y +
           c * x * y;
}

static constexpr auto constantsMatrix() -> glm::mat4
{
    return {glm::vec4(1, 1, 1, 1), glm::vec4(0, 1, 1, 0), glm::vec4(0, 0, 1, 1),
            glm::vec4(0, 0, 1, 0)};
}

static auto inverseConstantsMatrix() -> glm::mat4
{
    return glm::inverse(constantsMatrix());
}

auto Util::arbitraryQuadBilinear(glm::vec3 point, glm::vec3 qa, glm::vec3 qb,
                                 glm::vec3 qc, glm::vec3 qd) -> glm::vec3
{
    auto unitSpace = mapCoordsToUnitSquare(
        glm::vec2(point.x, point.z), glm::vec2(qa.x, qa.z),
        glm::vec2(qb.x, qb.z), glm::vec2(qc.x, qc.z), glm::vec2(qd.x, qd.z));
    const float interp{
        unitBilinear(unitSpace.x, unitSpace.y, qa.y, qd.y, qc.y, qb.y)};
    return {point.x, interp, point.y};
}
auto Util::mapCoordsToUnitSquare(glm::vec2 point, glm::vec2 qa, glm::vec2 qb,
                                 glm::vec2 qc, glm::vec2 qd) -> glm::vec2
{
    const glm::vec4 xVector(qa.x, qb.x, qc.x, qd.x);
    const glm::vec4 yVector(qa.y, qb.y, qc.y, qd.y);
    const auto mat = inverseConstantsMatrix();
    const glm::vec4 alpha{mat * xVector};
    const glm::vec4 beta{mat * yVector};

    const float x{point.x};
    const float y{point.y};
    const float a{alpha[3] * beta[2] - alpha[2] * beta[3]};
    const float b{alpha[3] * beta[0] - alpha[0] * beta[3] + alpha[1] * beta[2] -
                  alpha[2] * beta[1] + x * beta[3] - y * alpha[3]};
    const float c{alpha[1] * beta[0] - alpha[0] * beta[1] + x * beta[1] -
                  y * alpha[1]};

    if (a == 0.0F) // try linear transformation
    {
        const auto Bu = qb - qa;
        const auto Bv = qd - qa;
        const auto P = glm::mat2(Bu, Bv);
        const auto Pi = glm::inverse(P);
        const auto dx = (Pi * glm::vec2(1, 0)).x;
        const auto dy = (Pi * glm::vec2(0, 1)).y;
        const auto preTranslate = Pi * point;
        return preTranslate + glm::vec2(dx, dy);
    }

    const float numerator{-b + sqrtf(b * b - 4.0F * a * c)};
    Ensures(!std::isnan(numerator));
    const float denominator{2.0F * a};
    const float m{denominator == 0.0F ? 0.0F : numerator / denominator};
    const float l{(x - alpha[0] - alpha[2] * m) / (alpha[1] + alpha[3] * m)};
    return {m, l};
}
auto Util::arbitraryQuadBilinear(glm::vec3 point, glm::vec2 qa, glm::vec2 qb,
                                 glm::vec2 qc, glm::vec2 qd) -> glm::vec3
{
    return arbitraryQuadBilinear(
        point, glm::vec3(qa.x, 0, qa.y), glm::vec3(qb.x, 0, qb.y),
        glm::vec3(qc.x, 0, qc.y), glm::vec3(qd.x, 0, qd.y));
}
auto Util::computeIncline(glm::vec3 dir, glm::vec3 basisU, glm::vec3 basisV)
    -> float
{
    const glm::vec3 normal{glm::normalize(
        glm::cross(glm::normalize(basisU), glm::normalize(basisV)))};
    const glm::vec3 projN{glm::dot(glm::normalize(dir), normal) * normal};
    const glm::vec3 projPlane{glm::normalize(dir - projN)};
    const float dy{projPlane.y};
    const float incline{float(atan(dy) * 180.0 / M_PI)};

    return incline;
}

} // namespace mode7
