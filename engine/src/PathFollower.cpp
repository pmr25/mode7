#include "PathFollower.hpp"

namespace mode7
{
void PathFollower::moveTo(float t)
{
    auto pair = mPath.interpolate(t);
    m_translationMatrix = pair.first;
    m_quaternion = pair.second;
    buildMatrices(glm::mat4(1.0F));
    decompose();
}
void PathFollower::update()
{
    auto pair = mPath.interpolate(0.0F);
    m_translationMatrix = pair.first;
    m_quaternion = pair.second;
    buildMatrices(glm::mat4(1.0F));
    decompose();
}
void PathFollower::setPath(const Path& path)
{
    mPath = path;
}
} // namespace mode7