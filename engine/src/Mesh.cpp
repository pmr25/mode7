/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Mesh.hpp"
#include "Camera.hpp"
#include "Logger.hpp"
#include "Screen.hpp"

#include <iostream>

namespace mode7
{

static const std::vector<Vertex> plane_vertex_data{
    {{-1.f, -1.f, 0.f}, {0.f, 0.f, -1.f}, {1.f, 1.f}, {0.f, 0.f, 0.f}},
    {{1.f, -1.f, 0.f}, {0.f, 0.f, -1.f}, {0.f, 1.f}, {0.f, 0.f, 0.f}},
    {{1.f, 1.f, 0.f}, {0.f, 0.f, -1.f}, {0.f, 0.f}, {0.f, 0.f, 0.f}},
    {{-1.f, 1.f, 0.f}, {0.f, 0.f, -1.f}, {1.f, 0.f}, {0.f, 0.f, 0.f}}};

static const std::vector<unsigned int> plane_index_data{
    0, 1, 2, 2, 3, 0,
};

Material& Mesh::getMaterial()
{
    return *m_material;
}

void Mesh::setMaterial(std::shared_ptr<Material> mat)
{
    m_material = mat;
}

void Mesh::createFromShape(int shape)
{
    if (shape == PLANE)
    {
        createFromArrays(plane_vertex_data, plane_index_data);
    }
    else
    {
        SLOG(LG_ERROR) << "unknown shape";
        exit(1);
    }

    //    glGenVertexArrays(1, &vao);
    //    glGenBuffers(1, &vbo);
    //    glGenBuffers(1, &ebo);
    //
    //    glBindVertexArray(vao);
    //
    //    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //    glBufferData(GL_ARRAY_BUFFER, num_vertices, vertices, GL_STATIC_DRAW);
    //
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    //    glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangles, indices,
    //    GL_STATIC_DRAW);
    //
    //    const size_t stride = 11;
    //
    //    glEnableVertexAttribArray(0);
    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride *
    //    sizeof(float),
    //                          (void*)0);
    //
    //    glEnableVertexAttribArray(1);
    //    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride *
    //    sizeof(float),
    //                          (void*)(3 * sizeof(float)));
    //
    //    glEnableVertexAttribArray(2);
    //    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride *
    //    sizeof(float),
    //                          (void*)(6 * sizeof(float)));
    //
    //    glEnableVertexAttribArray(3);
    //    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride *
    //    sizeof(float),
    //                          (void*)(8 * sizeof(float)));
}

void Mesh::createFromArrays(std::vector<Vertex> vertices,
                            std::vector<unsigned int> indices)
{
    //    m_meshData = std::make_shared<MeshData>();
    m_meshData.setVertices(vertices);
    m_meshData.setIndices(indices);

    gpuFree();
    gpuAlloc();
}

void Mesh::setAltShader(Shader& s)
{
    alt = &s;
}

void Mesh::drawTriangles()
{
    glBindVertexArray(vao);

    //    assert(m_meshData != nullptr);
    if (m_meshData.hasElements())
    {
        glDrawElements(GL_TRIANGLES, m_meshData.getNumTriangles(),
                       GL_UNSIGNED_INT, 0);
    }
    else
    {
        glDrawArrays(GL_TRIANGLES, 0, m_meshData.getNumTriangles());
    }
}

void Mesh::update()
{
    if (!visible)
    {
        return;
    }

    Object::update();

    if (m_boundingVolume)
    {
        m_boundingVolume->setPosition(Object::getWorldPosition());
    }

    const Volume* vol = getBoundingVolume();
    if (vol != nullptr)
    {
        auto& cur{Camera::getCurrent()};
        m_insideFrustum = cur.isInsideFrustum(vol);
    }
}

void Mesh::draw(Shader* s)
{
    //    if (m_meshData == nullptr)
    //    {
    //        return;
    //    }

    if (!(visible && m_insideFrustum))
    {
        return;
    }

    s->setMaterial(*m_material);
    s->setModel(this);

    drawTriangles();
}

void Mesh::setBoundingVolume(Volume* vol)
{
    m_boundingVolume = vol->clone();
    m_boundingVolume->setPosition(getWorldPosition());
    assert(m_boundingVolume);
}

auto Mesh::getBoundingVolume() -> Volume*
{
    return m_boundingVolume.get();
}

void Mesh::setName(const std::string& name)
{
    m_name = name;
}

auto Mesh::getName() const -> const std::string&
{
    return m_name;
}

void Mesh::gpuFree()
{
    if (vao > 0)
    {
        glDeleteBuffers(1, &vao);
        vao = 0;
    }

    if (vbo > 0)
    {
        glDeleteBuffers(1, &vbo);
        vbo = 0;
    }

    if (ebo > 0)
    {
        glDeleteBuffers(1, &ebo);
        ebo = 0;
    }
}

auto Mesh::getMeshData() -> MeshData*
{
    return &m_meshData;
}

auto Mesh::discardMeshData() -> void
{
    m_meshData.discard();
}

void Mesh::gpuAlloc()
{
    auto& vertices{m_meshData.getVertices()};
    auto& indices{m_meshData.getIndices()};

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
                 &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
                 &indices[0], GL_STATIC_DRAW);

    const size_t stride = vertices[0].getStride(
        VERTEX_POSITION | VERTEX_NORMAL | VERTEX_UV | VERTEX_TANGENT);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float),
                          (void*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float),
                          (void*)(3 * sizeof(float)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride * sizeof(float),
                          (void*)(6 * sizeof(float)));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride * sizeof(float),
                          (void*)(8 * sizeof(float)));
}

} // namespace mode7
