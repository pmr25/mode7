#include "Sphere.hpp"
#include "Plane.hpp"

#include <iostream>

namespace mode7
{
bool Sphere::intersects(const glm::vec3 point) const
{
    float distance = glm::length(point - getPosition());
    return distance <= m_radius;
}

bool Sphere::intersects(const geom::Plane* plane) const
{
    float distance = plane->distanceTo(getPosition());
    return distance >= -m_radius;
}
} // namespace mode7
