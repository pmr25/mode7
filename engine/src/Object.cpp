/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Object.hpp"
#include "Logger.hpp"

#include <cassert>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

namespace mode7
{
void Object::notifyCollision(const std::string& source)
{
    (void)source; // unused

    // do nothing
}

void Object::translate(glm::vec3 v)
{
    m_position += v;
    m_translationMatrix = glm::translate(m_translationMatrix, v);
    dirty();
}

void Object::translateLocal(glm::vec3 v)
{
    const glm::vec3 localV{v.x * getRight() + v.y * getUp() + v.z * getFront()};
    translate(localV);
}

void Object::translateLocalUp(glm::vec3 v, glm::vec3 up)
{
    const glm::vec3 flatFront{getFront().x, 0, getFront().z};
    const glm::vec3 flatRight{getRight().x, 0, getRight().z};
    const glm::vec3 localV{v.x * flatRight + v.y * up + v.z * flatFront};
    translate(localV);
}

void Object::setPosition(glm::vec3 v)
{
    m_position = v;
    m_translationMatrix = glm::translate(glm::mat4(1.0f), v);
    dirty();
}

void Object::rotate(glm::vec3 v)
{
    m_euler += v;
    formatEuler();

    m_quaternion = m_quaternion * buildQuat(v);
    // m_quaternion = buildQuat(v) * m_quaternion;

    dirty();
}

void Object::rotateAbout(float degrees, glm::vec3 axis)
{
    const glm::quat q =
        glm::angleAxis(Util::rad(degrees), glm::normalize(axis));

    const glm::vec3 rv = glm::eulerAngles(q);
    const glm::vec3 dv(Util::deg(rv.x), Util::deg(rv.y), Util::deg(rv.z));
    m_euler += dv;
    formatEuler();

    m_quaternion = q * m_quaternion;

    dirty();
}

void Object::rotateAboutLocal(float degrees, glm::vec3 axis)
{
    const glm::quat q =
        glm::angleAxis(Util::rad(degrees), glm::normalize(axis));

    const glm::vec3 rv = glm::eulerAngles(q);
    const glm::vec3 dv(Util::deg(rv.x), Util::deg(rv.y), Util::deg(rv.z));
    m_euler += dv;
    formatEuler();

    m_quaternion = m_quaternion * q;

    dirty();
}

void Object::setRotation(glm::vec3 v)
{
    m_euler = v;
    formatEuler();

    m_quaternion = buildQuat(v);

    dirty();
}

void Object::scale(glm::vec3 v)
{
    m_scaleMatrix = glm::scale(m_scaleMatrix, v);
    m_scale = glm::vec3(m_scaleMatrix[0][0], m_scaleMatrix[1][1],
                        m_scaleMatrix[2][2]);
    dirty();
}

void Object::setScale(glm::vec3 v)
{
    m_scaleMatrix = glm::scale(glm::mat4(1.0f), v);
    m_scale = glm::vec3(m_scaleMatrix[0][0], m_scaleMatrix[1][1],
                        m_scaleMatrix[2][2]);
    dirty();
}

void Object::buildMatrices(const glm::mat4& acc)
{
    m_matrix = m_translationMatrix * glm::toMat4(m_quaternion) * m_scaleMatrix;

    m_worldMatrix = acc * m_matrix * m_inherentMatrix;

    m_up = glm::vec3(0, 1, 0);
    m_front = m_worldMatrix * glm::vec4(0, 0, 1, 0);
    assert(glm::length(m_front) > 1e-4);
    assert(glm::length(m_up) > 1e-4);
    m_right = -glm::cross(m_front, m_up);
    m_up = glm::cross(m_front, m_right);
}

void Object::update(const glm::mat4& acc, const glm::quat& qacc,
                    bool forceUpdate)
{
    if (preventUpdate)
    {
        return;
    }

    preUpdate();

    m_isDirty |= forceUpdate;

    if (m_isDirty)
    {
        buildMatrices(acc);
        m_worldQuat = qacc * m_quaternion;
    }

    for (auto& e : m_children)
    {
        e->update(m_worldMatrix, m_worldQuat, m_isDirty);
    }

    clean();

    postUpdate();
}

void Object::update(const glm::mat4& mat, const glm::quat& quat)
{
#ifdef _BULLET_PHYSICS
    if (m_rigidBody != nullptr)
    {
        copyPhysics();
    }
    else
    {
        update(mat, quat, false);
    }
#endif
    update(mat, quat, false);
}

void Object::update()
{
    const glm::mat4 identityMat(1.0F);
    const glm::quat noRotation(1, 0, 0, 0);
    update(identityMat, noRotation);
}

#ifdef _BULLET_PHYSICS

void Object::copyPhysics()
{
    preUpdate();

    btTransform trans;
    if (m_rigidBody->getMotionState())
    {
        m_rigidBody->getMotionState()->getWorldTransform(trans);
        // LOG(0, LG_DEBUG) << name << " " << trans.getOrigin().getX() << "," <<
        // trans.getOrigin().getY() << "," << trans.getOrigin().getZ() <<
        // std::endl;

        m_position = Util::fromBulletRaw(trans.getOrigin());
        // m_euler = Util::fromBulletRaw(trans.getRotation());

        m_translationMatrix = glm::translate(
            glm::mat4(1.0f), Util::fromBulletRaw(trans.getOrigin()));
        // m_rotationMatrix = glm::toMat4(glm::quat(trans.getRotation().getW(),
        // trans.getRotation().getX(), trans.getRotation().getY(),
        // trans.getRotation().getZ()));
        m_quaternion =
            glm::quat(trans.getRotation().getX(), trans.getRotation().getY(),
                      trans.getRotation().getZ(), trans.getRotation().getW());
        m_scaleMatrix = glm::scale(glm::mat4(1.0f), m_scale);

        glm::mat4 pmat(1.f);

        buildMatrices(pmat);

        // if (shouldDecompose)
        {
            decompose();

            // etc
            m_front = m_worldQuat * Util::zAxis();
            m_right = -glm::cross(m_front, m_up);
            m_up = glm::cross(m_front, m_right);
        }

        for (auto& e : m_children)
        {
            e->update(m_worldMatrix, m_worldQuat, m_isDirty);
        }

        postUpdate();

        clean();
    }
    else
    {
        LOG(0, LG_WARN) << m_name << " did not copy physics";
    }
}

#endif

void Object::decompose()
{
    glm::vec3 skew;
    glm::vec4 persp;

    glm::decompose(m_matrix, m_scale, m_quaternion, m_position, skew, persp);
}

void Object::addChild(Object* child)
{
    child->update();
    child->decompose();
    child->setParent(this);
    m_children.push_back(child);

    m_front = m_worldQuat * Util::zAxis();
    m_right = -glm::cross(m_front, m_up);
    m_up = glm::cross(m_front, m_right);

    child->setPosition(child->getPosition() - m_position);
}

void Object::detachParent()
{
    if (m_parent == nullptr)
    {
        return;
    }

    m_matrix = m_parent->getWorldMatrix() * m_matrix * m_inherentMatrix;
    decompose();

    m_parent = nullptr;
}

void Object::setMatrix(glm::mat4 mat)
{
    m_matrix = mat;
    decompose();
}

glm::vec3 Object::getPosition() const
{
    return m_position;
}

glm::vec3 Object::getRotation() const
{
    return m_euler;
}

glm::vec3 Object::getVelocity() const
{
    return m_velocity;
}

glm::vec3 Object::getScale() const
{
    return m_scale;
}

glm::mat4 Object::getMatrix() const
{
    return m_matrix;
}

glm::vec3 Object::getFront() const
{
    return m_front;
}

glm::vec3 Object::getRight() const
{
    return m_right;
}

glm::vec3 Object::getUp() const
{
    return m_up;
}

void Object::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Object::getName() const
{
    return m_name;
}

glm::mat4 Object::getWorldMatrix() const
{
    return m_worldMatrix;
}
} // namespace mode7
