#include "Materials.hpp"

namespace mode7
{

void Materials::addMaterial(Material mat)
{
    (void)mat;
}

void Materials::addMaterial(const std::string& name, Material mat)
{
    auto it = mLookup.find(name);
    if (it != mLookup.end())
    {
        return;
    }

    mLookup[name] = mMaterials.size();
    mMaterials.push_back(mat);
}
Material* Materials::getMaterial(const std::string& name)
{
    auto it = mLookup.find(name);
    if (it == mLookup.end())
    {
        return nullptr;
    }

    return &mMaterials[it->second];
}

} // namespace mode7
