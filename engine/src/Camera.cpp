/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Camera.hpp"
#include "Screen.hpp"
#include "Volume.hpp"

#include <iostream>

namespace mode7
{
std::shared_ptr<Camera> Camera::m_current;

void Camera::create(float fov, float n, float f)
{
    create(fov, n, f, (float)GLScreen::getResolutionWidth(),
           (float)GLScreen::getResolutionHeight());
}

void Camera::create(float fov, float nv, float fv, float width, float height)
{
    m_object.setName("Camera");
    m_width = width;
    m_height = height;
    m_near = nv;
    m_far = fv;
    m_fov = fov;
    m_projMatrix =
        glm::perspective(glm::radians(fov), m_width / m_height, m_near, m_far);

    m_frustum.resize(6);

    lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1));
}

void Camera::lookAt(glm::vec3 eye, glm::vec3 target)
{
    // m_object.m_matrix = glm::inverse(
    //     glm::lookAt(
    //         eye,
    //         target,
    //         glm::vec3(0, 1, 0)
    //     )
    // );
    // m_object.decompose();
    // m_object.dirty();

    m_viewMatrix = glm::lookAt(eye, target, mode7::Util::yAxis());
}

void Camera::defineFrustum(glm::vec3 pos, glm::vec3 front, glm::vec3 up)
{
    const glm::vec3 right = glm::cross(up, front);
    const float halfV = m_far * tanf(Util::rad(m_fov) * 0.5f);
    const float halfH = halfV * (m_width / m_height);
    assert(halfV > 0.0f);
    assert(halfH > 0.0f);

    m_frustum[PLANE_NEAR].definePointNormal(pos + m_near * front, front);
    m_frustum[PLANE_FAR].definePointNormal(pos + m_far * front, -front);

    // const glm::vec3 a = m_far * front;
    // const glm::vec3 b = halfH * right;
    // const glm::vec3 c = a - b;

    m_frustum[PLANE_LEFT].definePointNormal(
        pos, glm::cross((m_far * front) + (halfH * right), up));
    m_frustum[PLANE_RIGHT].definePointNormal(
        pos, glm::cross(up, (m_far * front) - (halfH * right)));
    m_frustum[PLANE_TOP].definePointNormal(
        pos, glm::cross(right, (m_far * front) + (halfV * up)));
    m_frustum[PLANE_BOTTOM].definePointNormal(
        pos, glm::cross((m_far * front) - (halfV * up), right));
}

void Camera::updateFrustum()
{
    const glm::vec3 front(0, 0, -1);
    const glm::vec4 tmp =
        m_object.getWorldMatrix() * glm::vec4(front.x, front.y, front.z, 0);

    defineFrustum(m_object.getWorldPosition(), glm::vec3(tmp.x, tmp.y, tmp.z),
                  glm::vec3(0, 1, 0));
}

auto Camera::isInsideFrustum(const Volume* vol) const -> bool
{
    for (auto& e : m_frustum)
    {
        if (!vol->intersects((const geom::Plane*)&e))
        {
            return false;
        }
    }

    return true;
}

auto Camera::getNear() const -> float
{
    return m_near;
}

auto Camera::getFar() const -> float
{
    return m_far;
}

auto Camera::getFov() const -> float
{
    return m_fov;
}

} // namespace mode7
