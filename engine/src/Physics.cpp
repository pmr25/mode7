/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifdef _BULLET_PHYSICS

#include "Physics.hpp"
#include "Clock.hpp"
#include "Util.hpp"

#define SUBSTEPS (10)
#define GRAVITY (9.81)

namespace mode7
{
Physics::Physics()
{
    m_broadPhase = new btDbvtBroadphase();
    m_collisionConfig = new btDefaultCollisionConfiguration();
    m_dispatcher = new btCollisionDispatcher(m_collisionConfig);
    m_solver = new btSequentialImpulseConstraintSolver;
    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_broadPhase,
                                                  m_solver, m_collisionConfig);

    m_dynamicsWorld->setGravity(btVector3(0.0f, -(float)GRAVITY, 0.0f));

    m_collisionShapes.push_back(std::shared_ptr<btCollisionShape>(
        new btBoxShape(btVector3(1.0f, 1.0f, 1.0f))));
    m_shapeMap["cube"] = (uint32_t)(m_collisionShapes.size() - 1);

    m_collisionShapes.push_back(std::shared_ptr<btCollisionShape>(
        new btBoxShape(btVector3(10.0, 0.0, 10.0))));
    m_shapeMap["plane"] = (uint32_t)(m_collisionShapes.size() - 1);
}

Physics::~Physics()
{
    // must be deleted in this order
    delete m_dynamicsWorld;
    delete m_solver;
    delete m_dispatcher;
    delete m_collisionConfig;
}

void Physics::cacheCollisionShape(btCollisionShape* shape,
                                  const std::string& key)
{
    if (m_shapeMap.find(key) != m_shapeMap.end())
    {
        return;
    }

    m_collisionShapes.push_back(std::shared_ptr<btCollisionShape>(shape));
    m_shapeMap[key] = (uint32_t)(m_collisionShapes.size() - 1);
}

btCollisionShape* Physics::getCollisionShape(const std::string& key)
{
    btCollisionShape* out = nullptr;
    auto loc = m_shapeMap.find(key);
    if (loc == m_shapeMap.end())
    {
        return out;
    }

    out = m_collisionShapes[(*loc).second].get();

    return out;
}

void Physics::addObject(Object* o, const std::string& shape)
{
    if (m_shapeMap.find(shape) == m_shapeMap.end())
    {
        return;
    }

    // ensure obj up to date
    o->update();

    // std::cout << "add shape " << shape << " at position " <<
    // glm::to_string(o->position) << ", " <<
    // Util::btVector3ToStr(Util::toBulletRaw(o->position)) << std::endl;

    btCollisionShape* collisionShape =
        m_collisionShapes[m_shapeMap[shape]].get();
    btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(
        // btQuaternion(o->m_quaternion.w, o->m_quaternion.x, o->m_quaternion.y,
        // o->m_quaternion.z),
        btQuaternion(o->m_quaternion.x, o->m_quaternion.y, o->m_quaternion.z,
                     o->m_quaternion.w),
        Util::toBulletRaw(o->getPosition())));

    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(
        o->m_mass, motionState, collisionShape, btVector3(0.1f, 0.1f, 0.1f));

    m_rigidBodies.push_back(std::make_unique<btRigidBody>(rigidBodyCI));
    m_dynamicsWorld->addRigidBody(m_rigidBodies.back().get());
    m_rigidBodies.back()->setUserPointer((void*)o);
    o->m_rigidBody = m_rigidBodies.back().get();
}

void Physics::addScene(Scene* s)
{
    for (auto& e : s->getChildren())
    {
        addObject(e, "cube");
    }
}

void Physics::simulate()
{
    m_dynamicsWorld->stepSimulation(
        (float)((double)Clock::getInterval() / 1000.0), 10);
    btTransform trans;
    for (auto& e : m_rigidBodies)
    {
        ((Object*)e->getUserPointer())->update();
    }

    btCollisionObject* obj;
    // btRigidBody* body;
    Object* gameObject;
    for (int i = 0; i < m_dynamicsWorld->getNumCollisionObjects(); ++i)
    {
        obj = m_dynamicsWorld->getCollisionObjectArray()[i];
        // body = btRigidBody::upcast(obj);
        gameObject = (Object*)obj->getUserPointer();
        gameObject->notifyPhysicsCollision(obj);
    }
}
} // namespace mode7

#endif
