#include "AABB.hpp"
#include "Plane.hpp"

#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <limits>

namespace mode7
{
void AABB::fitVertices(const std::vector<Vertex>& vertices)
{
    float maxLength = std::numeric_limits<float>::min();
    float maxWidth = std::numeric_limits<float>::min();
    float maxHeight = std::numeric_limits<float>::min();

    float minLength = std::numeric_limits<float>::max();
    float minWidth = std::numeric_limits<float>::max();
    float minHeight = std::numeric_limits<float>::max();

    glm::vec3 vpos;
    for (auto& v : vertices)
    {
        vpos = v.position * 3.0f;
        maxLength = fmaxf(maxLength, vpos.x);
        maxWidth = fmaxf(maxWidth, vpos.y);
        maxHeight = fmaxf(maxHeight, vpos.z);
        minLength = fminf(minLength, vpos.x);
        minWidth = fminf(minWidth, vpos.y);
        minHeight = fminf(minHeight, vpos.z);
    }

    setDimensions(maxLength - minLength, maxWidth - minWidth,
                  maxHeight - minHeight);
    vpos.x = minLength + m_length / 2.0f;
    vpos.y = minWidth + m_width / 2.0f;
    vpos.z = minHeight + m_height / 2.0f;
    setInternalPosition(vpos);
}

void AABB::setDimensions(float l, float w, float h)
{
    m_length = l;
    m_width = w;
    m_height = h;

    const float hl = l / 2.0f;
    const float hw = w / 2.0f;
    const float hh = h / 2.0f;

    m_faces = new geom::Plane[6];

    glm::vec3 a;
    glm::vec3 b;
    glm::vec3 c;

    // bottom
    a = glm::vec3(hl, hw, -hh);
    b = glm::vec3(-hl, hw, -hh);
    c = glm::vec3(hl, -hw, -hh);
    m_faces[0].definePoints(m_position + a, m_position + b, m_position + c);

    // top
    a = glm::vec3(hl, hw, hh);
    b = glm::vec3(-hl, hw, hh);
    c = glm::vec3(hl, -hw, hh);
    m_faces[1].definePoints(m_position + a, m_position + b, m_position + c);

    // left
    a = glm::vec3(-hl, hw, hh);
    b = glm::vec3(-hl, -hw, hh);
    c = glm::vec3(-hl, hw, -hh);
    m_faces[2].definePoints(m_position + a, m_position + b, m_position + c);

    // right
    a = glm::vec3(hl, hw, hh);
    b = glm::vec3(hl, -hw, hh);
    c = glm::vec3(hl, hw, -hh);
    m_faces[3].definePoints(m_position + a, m_position + b, m_position + c);

    // front
    a = glm::vec3(hl, hw, hh);
    b = glm::vec3(-hl, hw, hh);
    c = glm::vec3(hl, -hw, hh);
    m_faces[4].definePoints(m_position + a, m_position + b, m_position + c);

    // back
    a = glm::vec3(hl, hw, -hh);
    b = glm::vec3(-hl, hw, -hh);
    c = glm::vec3(hl, -hw, -hh);
    m_faces[5].definePoints(m_position + a, m_position + b, m_position + c);
}

// TODO fix this is terrible
bool AABB::intersects(const geom::Plane* plane) const
{
    const float distance = plane->distanceTo(getPosition());
    const float maxDimension = fmaxf(fmaxf(m_length, m_width), m_height);
    return distance >= -maxDimension;
}

bool AABB::intersects(const glm::vec3 point) const
{
    (void)point;
    return false;
}
} // namespace mode7
