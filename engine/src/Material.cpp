/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Material.hpp"
#include "TextureImage.hpp"

namespace mode7
{
void Material::addMap(Texture* map)
{
    maps.push_back(map);
}

auto Material::getMap(uint32_t index) const -> Texture*
{
    return maps[index];
}

auto Material::numMaps() const -> uint32_t
{
    return (uint32_t)maps.size();
}
} // namespace mode7
