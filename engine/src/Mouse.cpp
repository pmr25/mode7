/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Mouse.hpp"
#include "Screen.hpp"

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <iostream>

namespace mode7
{
int Mouse::m_x = 0;
int Mouse::m_y = 0;
int Mouse::m_rx = 0;
int Mouse::m_ry = 0;
uint32_t Mouse::m_buttonMask = 0;
bool Mouse::m_button[NUM_BUTTONS] = {0};
int Mouse::m_start[2] = {0};
int Mouse::m_clickStartX[NUM_BUTTONS] = {0};
int Mouse::m_clickStartY[NUM_BUTTONS] = {0};
bool Mouse::m_grabbed = false;

void Mouse::attach()
{
}

void Mouse::poll()
{
    // memset(m_button, false, 3 * sizeof(bool));
    m_start[0] = m_x;
    m_start[1] = m_y;

    // if (m_grabbed)
    // {
    //     m_buttonMask = SDL_GetRelativeMouseState(&m_x, &m_y);
    // }
    // else
    // {
    //     m_buttonMask = SDL_GetMouseState(&m_x, &m_y);
    // }

    if (m_buttonMask & SDL_BUTTON_LMASK)
    {
        m_clickStartX[0] = m_x;
        m_clickStartY[0] = m_y;
        m_button[0] = true;
    }

    if (m_buttonMask & SDL_BUTTON_RMASK)
    {
        m_clickStartX[1] = m_x;
        m_clickStartY[1] = m_y;
        m_button[1] = true;
    }
}

void Mouse::clearEvent()
{
    m_rx = 0;
    m_ry = 0;
}

void Mouse::sendEvent(SDL_Event* e)
{
    if (e->type == SDL_MOUSEMOTION)
    {
        clearEvent();
        m_rx = e->motion.xrel;
        m_ry = e->motion.yrel;
        m_x = e->motion.x;
        m_y = e->motion.y;
    }
    else if (e->type == SDL_MOUSEBUTTONDOWN)
    {
        m_button[e->button.button - 1] = true;
    }
    else if (e->type == SDL_MOUSEBUTTONUP)
    {
        m_button[e->button.button - 1] = false;
    }
}

void Mouse::grab()
{
    SDL_SetWindowGrab(GLScreen::getWindow(), SDL_TRUE);
    m_grabbed = true;
    SDL_SetRelativeMouseMode(SDL_TRUE);
}

void Mouse::release()
{
    SDL_SetWindowGrab(GLScreen::getWindow(), SDL_FALSE);
    m_grabbed = false;
    SDL_SetRelativeMouseMode(SDL_FALSE);
}

int Mouse::getX()
{
    return m_x / GLScreen::getScale();
}

int Mouse::getY()
{
    return m_y / GLScreen::getScale();
}

int Mouse::getRelX()
{
    return m_rx;
}

int Mouse::getRelY()
{
    return m_ry;
}

int Mouse::getDeltaX()
{
    return m_x - m_start[0];
}

int Mouse::getDeltaY()
{
    return m_y - m_start[1];
}

int Mouse::getClickDeltaX(mousebutton_t btn)
{
    return m_x - m_clickStartX[btn];
}

int Mouse::getClickDeltaY(mousebutton_t btn)
{
    return m_y - m_clickStartY[btn];
}

bool Mouse::getLeft()
{
    return m_button[LEFT];
}

bool Mouse::getRight()
{
    return m_button[RIGHT];
}

bool Mouse::isGrabbed()
{
    return m_grabbed;
}
} // namespace mode7
