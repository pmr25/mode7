/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Billboard.hpp"
#include "Screen.hpp"
#include "Texture.hpp"
#include "Util.hpp"
#include "Vertex.hpp"
#include "gl.hpp"

#include <fstream>
#include <glm/gtx/string_cast.hpp>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

// constexpr float DEFAULT_RECT[] = {-1.f, -1.f, 0.f, 0.f, 0.f, 1.f, 1.f, 1.f,
//
//                                   1.f,  -1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 1.f,
//
//                                   1.f,  1.f,  0.f, 0.f, 0.f, 1.f, 0.f, 0.f,
//
//                                   1.f,  1.f,  0.f, 0.f, 0.f, 1.f, 0.f, 0.f,
//
//                                   -1.f, 1.f,  0.f, 0.f, 0.f, 1.f, 1.f, 0.f,
//
//                                   -1.f, -1.f, 0.f, 0.f, 0.f, 1.f, 1.f, 1.f};

namespace mode7
{

static auto genIncreasingIndices(const size_t lim) -> std::vector<uint32_t>
{
    std::vector<uint32_t> out;

    for (auto i = 0U; i < lim; ++i)
    {
        out.push_back(i);
    }

    return out;
}

static auto getDefaultRect() -> std::vector<Vertex>
{
    std::vector<Vertex> out;

    const glm::vec3 n(0, 0, 1);

    const Vertex a(glm::vec3(-1, -1, 0), n, glm::vec2(1, 1));
    const Vertex b(glm::vec3(1, -1, 0), n, glm::vec2(0, 1));
    const Vertex c(glm::vec3(1, 1, 0), n, glm::vec2(0, 0));
    const Vertex d(glm::vec3(-1, 1, 0), n, glm::vec2(1, 0));

    out.push_back(a);
    out.push_back(b);
    out.push_back(c);

    out.push_back(c);
    out.push_back(d);
    out.push_back(a);

    return out;
}

Billboard::Billboard()
    : m_internalMatrix(1.0F)
{
}

Billboard::~Billboard() = default;

void Billboard::fitToTexture(uint32_t tid)
{
    const Texture* tex = m_material->getMap(tid);
    std::vector<glm::vec3> quad = Util::computeMinBoundingRect(
        tex->getData(), tex->getWidth(), tex->getHeight());

    // TODO
    // createFromQuad(quad);
}

void Billboard::create()
{
    Mesh::createFromArrays(getDefaultRect(), genIncreasingIndices(6));
}

// void Billboard::create(const float* buffer, size_t size)
//{
//     assert(size >= 6);
//     const uint32_t stride = 8 * sizeof(float);
//     Mesh::createFromBuffer(buffer, size);
// }

void Billboard::createFromQuadFile(const std::string& filename)
{
    std::ifstream in(filename);
    json obj;

    if (!in)
    {
        return;
    }

    in >> obj;

    size_t size = 0;
    size = obj["size"];
    const auto& jdata = obj["data"];
    if (jdata.size() != size)
    {
        return;
    }

    std::vector<Vertex> data;
    std::vector<uint32_t> indices;
    const int stride = 8;
    for (uint32_t i = 0; i < size / stride; ++i)
    {
        indices.push_back(i);
        const size_t start = i * 8;
        const glm::vec3 position(jdata[start + 0], jdata[start + 1],
                                 jdata[start + 2]);
        const glm::vec3 normal(jdata[start + 3], jdata[start + 4],
                               jdata[start + 5]);
        const glm::vec2 uv(jdata[start + 6], jdata[start + 7]);
        data.emplace_back(position, normal, uv);
    }

    //    SDL_HideWindow(GLScreen::getWindow());

    createFromArrays(data, indices);
    discardMeshData();
}

// void Billboard::createFromQuad(const std::vector<glm::vec3>& pts)
//{
//     std::vector<glm::vec3> triangles;
//     triangles.reserve(6);
//     triangles.push_back(pts[0]);
//     triangles.push_back(pts[1]);
//     triangles.push_back(pts[2]);
//     triangles.push_back(pts[2]);
//     triangles.push_back(pts[3]);
//     triangles.push_back(pts[0]);
//
//     std::vector<glm::vec3> uvs;
//     uvs.push_back(pts[4]);
//     uvs.push_back(pts[5]);
//     uvs.push_back(pts[6]);
//     uvs.push_back(pts[6]);
//     uvs.push_back(pts[7]);
//     uvs.push_back(pts[4]);
//
//     std::vector<float> buffer;
//     buffer.reserve(sizeof(float) * 8 * 6);
//
//     for (uint32_t i = 0; i < triangles.size(); ++i)
//     {
//         const glm::vec3 vtx = triangles[i];
//         const glm::vec3 uv = uvs[i];
//
//         // add vertex
//         buffer.push_back(vtx.x * 2.0f - 1.0f);
//         buffer.push_back(vtx.y * 2.0f - 1.0f);
//         buffer.push_back(vtx.z);
//
//         // add normal
//         buffer.push_back(0.0f);
//         buffer.push_back(0.0f);
//         buffer.push_back(1.0f);
//
//         // add uv
//         // const float u = DEFAULT_RECT[(i * 8) + 6];
//         // const float v = DEFAULT_RECT[(i * 8) + 7];
//         // const float u = vtx.x * DEFAULT_RECT[(i * 8) + 6];
//         // const float v = DEFAULT_RECT[(i * 8) + 7] / 2.5 - 0.075;
//         const float u = uv.x;
//         const float v = uv.y;
//         buffer.push_back(u);
//         buffer.push_back(v);
//
//         std::cout << "uv: (" << u << "," << v << ")" << std::endl;
//     }
//
//     create(&buffer[0], buffer.size());
// }

void Billboard::postUpdate()
{
    m_internalMatrix =
        glm::translate(glm::mat4(1.0), Object::getWorldPosition());
}

void Billboard::draw(Shader* s)
{
    if (!visible)
    {
        return;
    }

    s->use();

    const glm::vec2 scale2d = glm::vec2(getScale().x, getScale().y);
    glUniform2fv(glGetUniformLocation(s->pid(), "scale"), 1,
                 VEC(scale2d)); // todo fix

    s->setMaterial(*m_material);
    s->setModel(this);

    drawTriangles();
}
} // namespace mode7
