/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "TextureImage.hpp"

#include "Logger.hpp"
#include "gl.hpp"

#include "filesystem.hpp"

#include <cassert>
#include <iostream>
#if __linux__
#include <SDL.h>
#include <SDL_image.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#endif

#ifdef __DEVIL__
#include <IL/il.h>
#endif /* __DEVIL__ */

namespace mode7
{
TextureImage::TextureImage()
    : Texture()
    , m_pixels(nullptr)
{
}

void TextureImage::open(const std::string& filename, TexType type,
                        bool preserveData)
{
    this->type = type;
    open(filename);
    name = filename;
    addSrcRect(0, 0, m_pixels->w, m_pixels->h);

    if (!preserveData)
    {
        SDL_FreeSurface(m_pixels);
        m_pixels = nullptr;
    }
}

uint32_t TextureImage::getId() const
{
    return id;
}

TexType TextureImage::getType() const
{
    return type;
}

void TextureImage::fill(void* data, int w, int h)
{
    fill(data, GL_RGBA, w, h);
}

void TextureImage::fill(void* data, GLenum internal, int w, int h)
{
    (void)internal;

    // wrapper data
    width = w;
    height = h;
    this->type = TexType::DIFFUSE;

    // bind texture
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    // store data
    const auto mips{int(log(double(height)) / log(2.0))};
    assert(mips > 0);
    if (shouldGenerateMipmap())
    {
        glTexStorage2D(GL_TEXTURE_2D, mips + 1, GL_RGBA8, w, h);
    }
    else
    {
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, w, h);
    }
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE,
                    data);
    // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, internal,
    // GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // set parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    if (shouldGenerateMipmap())
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                        (GLint)mNearFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        (GLint)mFarFilter);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    // unbind
    glBindTexture(GL_TEXTURE_2D, 0);
    setReady(true);
}

void TextureImage::update(void* data, GLint xoff, GLint yoff, GLsizei w,
                          GLsizei h, GLenum format)
{
    glBindTexture(GL_TEXTURE_2D, id);
    glTexSubImage2D(GL_TEXTURE_2D, 0, xoff, yoff, w, h, format,
                    GL_UNSIGNED_BYTE, data);
    glBindTexture(GL_TEXTURE_2D, 0);
}

int TextureImage::open(const std::string& filename)
{
    loadIntoMemory(filename);
    assert(m_pixels != nullptr);

    int mode = GL_RGB;
    if (m_pixels->format->BytesPerPixel == 4)
    {
        mode = GL_RGBA;
    }

    genGPUTexture(m_pixels->pixels, width, height, mode);
    setReady(true);

    return 0;
}

void TextureImage::addSrcRect(int32_t x, int32_t y, int32_t w, int32_t h)
{
    SDL_Rect rect = {x, y, w, h};
    m_srcRects.push_back(rect);
}

void TextureImage::useSrcRect(uint32_t indx)
{
    (void)indx;

    if (!id)
    {
        return;
    }

    SDL_Rect* r = &m_srcRects.back();

    glBindTexture(GL_TEXTURE_2D, id);
    int mode = GL_RGB;
    if (m_pixels->format->BytesPerPixel == 4)
    {
        mode = GL_RGBA;
    }
    glTexSubImage2D(GL_TEXTURE_2D, 0, r->x, r->y, r->w, r->h, mode,
                    GL_UNSIGNED_BYTE, m_pixels->pixels);
}

void TextureImage::destroy()
{
    if (m_pixels)
    {
        std::cout << "[Texture] FreeSurface of " << name << std::endl;
        SDL_FreeSurface(m_pixels);
        m_pixels = nullptr;
    }
    if ((GLint)id != 0)
    {
        std::cout << "[Texture] glDeleteTextures of " << name << std::endl;
        glDeleteTextures(1, &id);
        id = 0;
    }
}
void TextureImage::loadIntoMemory(const std::string& filename)
{
    std::string name = fs::path(filename).filename().string();
    setName(name);

#ifdef __DEVIL__

    ILubyte* lump;
    ILuint size;
    FILE* fp;
    ILuint img;

    ilGenImages(1, &img);
    assert(img >= 1);
    ilBindImage(img);

    // fp = fopen(filename.c_str(), "rb");
    // fseek(fp, 0, SEEK_END);
    // size = ftell(fp);
    // std::cout << "size=" << size << std::endl;
    // lump = (ILubyte*)malloc(size);
    // fseek(fp, 0, SEEK_SET);
    // fread(lump, 1, size, fp);
    // fclose(fp);
    // ilLoadL(IL_PNG, lump, size);
    // free(lump);

    ILboolean success = ilLoadImage(filename.c_str());
    if (!success)
    {
        ILenum err = ilGetError();
        std::cout << "error: " << err << std::endl;
        if (err == IL_COULD_NOT_OPEN_FILE)
        {
            std::cout << "could not open " << filename << std::endl;
        }
        else if (err == IL_ILLEGAL_OPERATION)
        {
            std::cout << "illegal operation" << std::endl;
        }
        else if (err == IL_INVALID_EXTENSION)
        {
            std::cout << "invalid extension" << std::endl;
        }
        else if (err == IL_INVALID_PARAM)
        {
            std::cout << "invalid param" << std::endl;
        }
        else if (err == IL_OUT_OF_MEMORY)
        {
            std::cout << "out of mem" << std::endl;
        }
        assert(false);
    }

    ILuint active = ilGetInteger(IL_CUR_IMAGE);
    assert(img == active);

    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);

    std::cout << "w,h = " << width << "," << height << std::endl;

    ILubyte* pixels;
    pixels = (ILubyte*)malloc(width * height * 4 * sizeof(ILubyte));
    ilCopyPixels(0, 0, 0, width, height, 1, IL_RGBA, IL_UNSIGNED_BYTE, pixels);

    ilDeleteImages(1, &img);

    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint)mNearFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint)mFarFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    free(pixels);

#else

    LOG(0, LG_INFO) << "[TextureImage] load texture " << filename;

    m_pixels = IMG_Load(filename.c_str());
    if (m_pixels == nullptr)
    {
        std::cout << SDL_GetError() << std::endl;
        LOG(0, 2) << "[TextureImage] failed to load " << filename;
        return;
    }

    width = m_pixels->w;
    height = m_pixels->h;

#endif /* __DEVIL__ */
}
} // namespace mode7
