/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Framebuffer.hpp"
#include "Screen.hpp"

#include <cassert>
#include <cstdlib>
#include <iostream>

namespace mode7
{
static float WMAX = 1.0;
static float screen_quad_data[] = {
    -1.0f, -1.0f, 0.f, 0.f, WMAX,  -1.0f, 1.f, 0.f, WMAX,  WMAX,  1.f, 1.f,

    WMAX,  WMAX,  1.f, 1.f, -1.0f, WMAX,  0.f, 1.f, -1.0f, -1.0f, 0.f, 0.f};

int Framebuffer::targetTexture(uint32_t w, uint32_t h, bool depthStencil)
{
    mHasDepthStencil = depthStencil;

    assert(w > 0);
    assert(h > 0);

    m_target = TEXTURE;
    m_width = w;
    m_height = h;

    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    glGenTextures(1, &m_fb_tex);
    glBindTexture(GL_TEXTURE_2D, m_fb_tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    // glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, NULL);

    assert(m_fb_tex > 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           m_fb_tex, 0);

    if (depthStencil)
    {
        glGenTextures(1, &m_fb_dep);
        glBindTexture(GL_TEXTURE_2D, m_fb_dep);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_STENCIL, w, h, 0,
                     GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

        assert(m_fb_dep > 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                               GL_TEXTURE_2D, m_fb_dep, 0);

        // glGenRenderbuffers(1, &m_rbo);
        // glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
        // glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h);
        // glBindRenderbuffer(GL_RENDERBUFFER, 0);
        // glFramebufferRenderbuffer(GL_FRAMEBUFFER,
        // GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_rbo);
    }

    const auto fbStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (fbStatus != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "error: fb not complete! " << fbStatus << std::endl;
        exit(1);
        return 1;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    GLuint vbo;
    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &vbo);

    glBindVertexArray(m_vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 2 * 3 * 4 * sizeof(float), screen_quad_data,
                 GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float),
                          (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float),
                          (void*)(2 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return 0;
}

int Framebuffer::targetRenderbuffer(uint32_t w, uint32_t h, bool depthSencil)
{
    mHasDepthStencil = depthSencil;

    assert(w > 0);
    assert(h > 0);

    m_target = RENDERBUFFER;
    m_width = w;
    m_height = h;

    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    GLuint rbo;

    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB8, w, h);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, rbo);

    if (depthSencil)
    {
        glGenRenderbuffers(1, &m_rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                                  GL_RENDERBUFFER, m_rbo);
    }

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "error: fb not complete!" << std::endl;
        return 1;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return 0;
}

int Framebuffer::targetMultisampledRenderBuffer(uint32_t w, uint32_t h,
                                                uint32_t samples,
                                                bool depthStencil)
{
    mHasDepthStencil = depthStencil;

    assert(w > 0);
    assert(h > 0);

    m_target = RENDERBUFFER;
    m_width = w;
    m_height = h;
    m_samples = samples;

    GLint maxSamples;
    glGetIntegerv(GL_MAX_SAMPLES, &maxSamples);
    if (m_samples > (uint32_t)maxSamples)
    {
        std::cout << "note: max_samples=" << maxSamples << std::endl;
        m_samples = 0;
    }

    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, m_samples, GL_RGB8, w, h);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, rbo);

    if (depthStencil)
    {
        glGenRenderbuffers(1, &m_rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, m_rbo);
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, m_samples,
                                         GL_DEPTH24_STENCIL8, w, h);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
                                  GL_RENDERBUFFER, m_rbo);
    }

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "error: fb not complete!" << std::endl;
        return 1;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return 0;
}

void Framebuffer::setShader(std::shared_ptr<Shader> sh)
{
    mShader = sh;
    mName = sh->getName();

    m_colortex_loc = glGetUniformLocation(mShader->pid(), "colorTexture");
    appendTextureLocation("colorTexture");

    if (mHasDepthStencil)
    {
        m_dstex_loc =
            glGetUniformLocation(mShader->pid(), "depthStencilTexture");
        appendTextureLocation("depthStencilTexture");
    }
}

void Framebuffer::setForcedShader(std::shared_ptr<Shader> sh)
{
    mForcedShader = sh;
}

void Framebuffer::appendTextureLocation(const std::string& name)
{
    GLint loc = glGetUniformLocation(mShader->pid(), name.c_str());
    m_textureLocations.push_back(loc);
    mTextureLocationNames.push_back(name);
}

void Framebuffer::preDraw()
{
    if (m_target == TEXTURE)
    {
        mShader->onlyUse();
        resetTextureFeed();
        feedTexture(m_fb_tex);
    }
}

void Framebuffer::draw()
{
    glDisable(GL_DEPTH_TEST);

    if (m_target == TEXTURE)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glViewport(0, 0, GLScreen::getWidth(), GLScreen::getHeight());

        glBindVertexArray(m_vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
    else if (m_target == RENDERBUFFER)
    {
        assert(m_fbo != 0);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        assert(GLScreen::getWidth() > 0);
        assert(GLScreen::getHeight() > 0);
        glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, GLScreen::getWidth(),
                          GLScreen::getHeight(), GL_COLOR_BUFFER_BIT,
                          GL_LINEAR);
    }
}

void Framebuffer::draw(Framebuffer* target)
{
    glDisable(GL_DEPTH_TEST);

    if (target == nullptr)
    {
        if (m_target == TEXTURE)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            glViewport(0, 0, GLScreen::getWidth(), GLScreen::getHeight());
            // glViewport(0, 0, GLScreen::getResolutionWidth(),
            // GLScreen::getResolutionHeight());

            glBindVertexArray(m_vao);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindVertexArray(0);
        }
        else if (m_target == RENDERBUFFER)
        {
            assert(m_fbo != 0);
            glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

            assert(GLScreen::getWidth() > 0);
            assert(GLScreen::getHeight() > 0);
            glBlitFramebuffer(0, 0, m_width, m_height, 0, 0,
                              GLScreen::getWidth(), GLScreen::getHeight(),
                              GL_COLOR_BUFFER_BIT, GL_LINEAR);
            // glBlitFramebuffer(0, 0, m_width, m_height, 0, 0,
            // GLScreen::getResolutionWidth(), GLScreen::getResolutionHeight(),
            // GL_COLOR_BUFFER_BIT, GL_LINEAR);
        }
    }
    else
    {
        if (m_target == TEXTURE)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, target->m_fbo);
            glViewport(0, 0, target->m_width, target->m_height);

            m_tp = 0;
            mShader->onlyUse();
            feedTexture(m_colortex_loc, m_fb_tex);
            feedTexture(m_dstex_loc, m_fb_dep);

            glBindVertexArray(m_vao);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindVertexArray(0);
        }
        else if (m_target == RENDERBUFFER)
        {
            assert(m_fbo != 0);
            glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target->m_fbo);

            assert(GLScreen::getWidth() > 0);
            assert(GLScreen::getHeight() > 0);
            glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, target->m_width,
                              target->m_height, GL_COLOR_BUFFER_BIT, GL_LINEAR);
        }
    }
}

void Framebuffer::blitTo(GLint fb)
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);

    glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, GLScreen::getWidth(),
                      GLScreen::getHeight(), GL_COLOR_BUFFER_BIT, GL_LINEAR);
}

void Framebuffer::begin()
{
    glClearColor(mClearColor[0], mClearColor[1], mClearColor[2], 1.0f);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, m_width, m_height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Framebuffer::resetTextureFeed()
{
    m_tp = 0;
}

void Framebuffer::feedTexture(GLint loc, GLuint id)
{
    glUniform1i(loc, m_tp);
    glActiveTexture(GL_TEXTURE0 + m_tp);
    glBindTexture(GL_TEXTURE_2D, id);
    ++m_tp;
}

void Framebuffer::feedTexture(GLint textureID)
{
    feedTexture(m_textureLocations[m_tp], textureID);
}

void Framebuffer::destroy()
{
    glDeleteFramebuffers(1, &m_fbo);
}

void Framebuffer::setClearColor(float r, float g, float b)
{
    mClearColor[0] = r;
    mClearColor[1] = g;
    mClearColor[2] = b;
}
} // namespace mode7
