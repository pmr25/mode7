#include "HUDElement.hpp"
#include "Screen.hpp"
#include "Shader.hpp"
#include <cassert>
#include <cmath>
#include <cstdint>
#include <glm/glm.hpp>
#include <vector>

namespace mode7
{
uint32_t HUDElement::sNumElements = 0;

HUDElement::~HUDElement()
{
    destroy();
}

void HUDElement::init()
{
}

static std::vector<glm::vec2> genVertices(float width, float height,
                                          float texSize)
{
    std::vector<glm::vec2> verts;
    // const float sw = sc * 0.5f * width / (float)(GLScreen::getWidth() /
    // GLScreen::getScale()); const float sh = sc * 0.5f * height /
    // (float)(GLScreen::getHeight() / GLScreen::getScale());
    const float sw = width / (float)(GLScreen::getWidth());
    const float sh = height / (float)(GLScreen::getHeight());
    // const float sw = width / (float)GLScreen::getResolutionWidth();
    // const float sh = height / (float)GLScreen::getResolutionHeight();
    const float maxU = width / texSize;
    const float maxV = (height / texSize);

    // triangle 1
    verts.emplace_back(-sw, -sh);
    verts.emplace_back(0, maxV);
    verts.emplace_back(sw, -sh);
    verts.emplace_back(maxU, maxV);
    verts.emplace_back(sw, sh);
    verts.emplace_back(maxU, 0);

    // triangle 2
    verts.emplace_back(sw, sh);
    verts.emplace_back(maxU, 0);
    verts.emplace_back(-sw, sh);
    verts.emplace_back(0, 0);
    verts.emplace_back(-sw, -sh);
    verts.emplace_back(0, maxV);

    return verts;
}

void HUDElement::destroy()
{
    if (mVBO != GL_INVALID_VALUE)
    {
        glDeleteBuffers(1, &mVBO);
    }

    if (mVAO != GL_INVALID_VALUE)
    {
        glDeleteVertexArrays(1, &mVAO);
    }
}

void HUDElement::createMesh(float width, float height)
{
    std::vector<glm::vec2> verts =
        genVertices(width, height, mTexture.getHeight());

    glGenVertexArrays(1, &mVAO);
    if (mVAO == GL_INVALID_VALUE)
    {
        return;
    }

    glGenBuffers(1, &mVBO);
    if (mVBO == GL_INVALID_VALUE)
    {
        glDeleteVertexArrays(1, &mVAO);
    }

    glBindVertexArray(mVAO);

    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(GL_ARRAY_BUFFER, verts.size() * 2 * sizeof(float), &verts[0],
                 GL_STATIC_DRAW);

    constexpr GLsizei stride = 4 * sizeof(float);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride, (void*)(0));

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride,
                          (void*)(2 * sizeof(float)));
}

void HUDElement::draw() const
{
    if (!mIsVisible)
    {
        return;
    }

    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glBindVertexArray(mVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void HUDElement::setSize(uint32_t w, uint32_t h)
{
    const auto scale{float(GLScreen::getScale())};
    assert(scale != 0.0f);

    mWidth = w;
    mHeight = h;
    mDrawWidth = w * scale;
    mDrawHeight = h * scale;

    // compute texture size
    double max = fmax(mDrawWidth, mDrawHeight);
    double power = ceil(log(max) / log(2.0));
    mTexSize = (uint32_t)(pow(2, power));

    assert(mTexSize >= mDrawWidth);
    assert(mTexSize >= mDrawHeight);
}

bool HUDElement::contains(int32_t px, int32_t py)
{
    const int32_t localX{int32_t(px - mPosition.x)};
    const int32_t localY{int32_t(py - mPosition.y)};

    return localX >= 0 && localX <= (int32_t)mWidth && localY >= 0 &&
           localY <= (int32_t)mHeight;
}

void HUDElement::handleClick(int32_t clickX, int32_t clickY)
{
    if (clickX < 0 || clickY < 0)
    {
        return;
    }

    if (!contains(clickX, clickY))
    {
        return;
    }

    clickCallback(clickX - mPosition.x, clickY - mPosition.y);
}
} // namespace mode7
