/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "OrbitControls.hpp"
#include "Mouse.hpp"
#include "Util.hpp"

#define PITCH 0
#define YAW 1
#define ROLL 2

#define EPSILON (1e-1)

namespace mode7
{
void OrbitControls::setDistance(float dist)
{
    // m_arm.position.z = dist;
    m_arm.setPosition(0, 0, dist);
}

void OrbitControls::setPitchRange(float min, float max)
{
    m_pitchRange[0] = Util::rad(min);
    m_pitchRange[1] = Util::rad(max);
    m_ranges[PITCH] = true;
}

void OrbitControls::setYawRange(float min, float max)
{
    m_yawRange[0] = min;
    m_yawRange[1] = max;
}

void OrbitControls::setRollRange(float min, float max)
{
    m_rollRange[0] = min;
    m_rollRange[1] = max;
}

void OrbitControls::updateControls()
{
    if ((m_inputMask & ORBITCONTROLS_INPUT_MOUSE) && Mouse::hasFocus())
    {
        float dx = (float)xdir * (float)Mouse::getRelX() * m_sensitivity[PITCH];
        float dy = (float)ydir * (float)Mouse::getRelY() * m_sensitivity[YAW];

        Object::rotate(dy, dx, 0.f);

        glm::vec3 rotation = getRotation();
        float d0 = rotation.x - m_pitchRange[0];
        float d1 = rotation.x - m_pitchRange[1];
        if (m_ranges[PITCH] && rotation.x < m_pitchRange[0])
        {
            Object::rotate(d0, 0.f, 0.f);
            // Object::setRotationX(m_pitchRange[0]);
        }
        if (m_ranges[PITCH] && rotation.x > m_pitchRange[1])
        {
            Object::rotate(d1, 0.f, 0.f);
            // Object::setRotationX(m_pitchRange[1]);
        }
    }

    Object::update();
}

glm::mat4 OrbitControls::getControlMatrix()
{
    return m_arm.getWorldMatrix();
}
} // namespace mode7