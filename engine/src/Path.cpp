#include "Path.hpp"
#include "Line.hpp"
#include "Util.hpp"

#include <cassert>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

namespace mode7
{

void Path::createFromCurves(std::vector<ParametricCurve<3>>& curves)
{
    for (const auto& curve : curves)
    {
        const auto points = curve.interpolate(mStepSize);

        for (auto i{1U}; i < points.size(); ++i)
        {
            buildMatrices(points[i - 1], points[i]);
        }
    }

    mWaypoints.insert(mWaypoints.begin(), curves[0].interpolate(mStepSize)[0]);
    mHeadings.insert(mHeadings.begin(), mHeadings[0]);
}

void Path::buildMatrices(glm::vec3 a, glm::vec3 b)
{
    const glm::vec3 up(0, 1, 0);
    const glm::vec3 dir = b - a;
    const glm::vec3 normal = glm::cross(dir, up);
    const glm::vec3 yaw = dir * glm::vec3(1, 0, 1);
    const glm::vec3 pitch = dir * glm::vec3(0, 1, 1);
    const float angleUp = Util::angleBetween(yaw, glm::vec3(0, 0, -1));
    const float angleNorm = Util::angleBetween(pitch, up);
    glm::vec3 position = dir;
    glm::quat rotation = glm::angleAxis(glm::degrees(angleUp), up) *
                         glm::angleAxis(glm::degrees(angleNorm), normal);

    mWaypoints.push_back(position);
    mHeadings.push_back(rotation);
    mLengths.push_back(glm::length(dir));
}

auto Path::interpolate(double t) const -> std::pair<glm::mat4, glm::quat>
{
    const double inv{1.0 / float(mWaypoints.size() - 1)};
    size_t startIdx{size_t(floor(t / inv))};
    size_t endIdx{size_t(floor(t / inv))};
    const double internalT{(t / inv) - float(startIdx)};

    assert(startIdx < mWaypoints.size());
    assert(endIdx < mWaypoints.size());

    glm::vec3 startPoint{mWaypoints[startIdx]};
    glm::vec3 endPoint{mWaypoints[endIdx]};
    glm::quat startRot{mHeadings[startIdx]};
    glm::quat endRot{mHeadings[endIdx]};

    glm::vec3 mixMat = glm::mix(startPoint, endPoint, 1.0F);
    glm::quat mixQuat = glm::slerp(startRot, endRot, float(round(internalT)));

    return std::pair<glm::mat4, glm::quat>{
        glm::translate(glm::mat4(1.0F), mixMat), mixQuat};
}

void Path::createFromCurves(
    std::vector<std::unique_ptr<ParametricCurve<3>>>& curves)
{
    for (const auto& curve : curves)
    {
        const auto points = curve->interpolate(mStepSize);

        for (auto i{1U}; i < points.size(); ++i)
        {
            buildMatrices(points[i - 1], points[i]);
        }
    }

    mWaypoints.insert(mWaypoints.begin(), curves[0]->interpolate(mStepSize)[0]);
    mHeadings.insert(mHeadings.begin(), mHeadings[0]);
}

} // namespace mode7