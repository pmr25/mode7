#include "RayCaster2D.hpp"
#include "GeometryUtils.hpp"

namespace mode7
{
auto RayCaster2D::intersects(
    const std::vector<ParametricCurve<2>>& curves) const -> float
{
    float minDist = std::numeric_limits<float>::infinity();
    for (const auto& curve : curves)
    {
        minDist = fminf(minDist, intersects(curve));
    }
    return minDist;
}

auto RayCaster2D::intersects(const ParametricCurve<2>& curve) const -> float
{
    return intersectsRecursiveHelper(
        GeometryUtils::pointsToSegments(curve.interpolate(1.0F)));
}

[[nodiscard]] auto RayCaster2D::intersects(const Line<2>& segment) const
    -> float
{
    Line<2> rayPath;
    rayPath.definePointVector(getWorldPosition(), getWorldFront());
    const auto dist{rayPath.computeIntersect(segment)};
    return dist < 0.0F ? std::numeric_limits<float>::infinity() : dist;
}

auto RayCaster2D::intersectsRecursiveHelper(
    const std::vector<Line<2>>& segments) const -> float
{
    Line<2> minFoundSegment;
    float minDist = std::numeric_limits<float>::infinity();
    for (const auto& segment : segments)
    {
        const auto dist{intersects(segment)};
        if (dist < minDist)
        {
            minFoundSegment = segment;
            minDist = dist;
        }
    }

    const Line<2> nextParent{minFoundSegment};
    if (nextParent.getLength() <= 0.1F)
    {
        return minDist;
    }

    return intersectsRecursiveHelper(
        GeometryUtils::pointsToSegments(nextParent.interpolate(1.0F)));
}
auto RayCaster2D::intersects(const std::vector<Line<2>>& lines) const -> float
{
    float minDist{std::numeric_limits<float>::infinity()};
    for (const auto& line : lines)
    {
        minDist = fminf(minDist, intersects(line));
    }
    return minDist;
}
auto RayCaster2D::intersects(const geom::Polygon& polygon) const -> float
{
    return intersects(polygon.getSides());
}
} // namespace mode7