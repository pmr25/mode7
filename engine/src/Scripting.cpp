#include "Scripting.hpp"

namespace mode7
{
static void fatal(const char* message)
{
    fprintf(stderr, "%s\n", message);
    assert(0);
}

lua_State* Scripting::getLuaState()
{
    return mState;
}

int Scripting::loadScript(const std::string& filename)
{
    int err;
    assert(mState);
    err = luaL_dofile(mState, filename.c_str());
    if (err != LUA_OK)
    {
        fatal(lua_tostring(mState, -1));
    }
    return err;
}
} // namespace mode7
