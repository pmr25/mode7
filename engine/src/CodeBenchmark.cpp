#include "CodeBenchmark.hpp"
#include <fstream>

namespace mode7
{

auto CodeBenchmark::tick(const std::string& name) -> std::string
{
    if (mCounters.find(name) == mCounters.end())
    {
        mCounters[name] = 0;
    }

    std::string entryName = name + '.' + std::to_string(mCounters[name]++);

    mIntervals[entryName] =
        std::make_pair<TIME_POINT, TIME_POINT>(NOW(), NOW());

    return entryName;
}

auto CodeBenchmark::tock(const std::string& name) -> uint64_t
{
    auto iterator{mIntervals.find(name)};
    if (iterator == mIntervals.end())
    {
        return 0;
    }

    iterator->second.second = NOW();

    return std::chrono::duration_cast<std::chrono::nanoseconds>(
               iterator->second.second - iterator->second.first)
        .count();
}

auto CodeBenchmark::getInstance() -> CodeBenchmark&
{
    static CodeBenchmark instance;
    return instance;
}

void CodeBenchmark::save()
{
    std::ofstream out(mFilename);
    if (!out)
    {
        return;
    }

    out << "event,duration," << std::endl;

    for (const auto& nameIntervalPair : mIntervals)
    {
        const std::string& name = nameIntervalPair.first;
        const auto& interval = nameIntervalPair.second;
        const auto duration =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                interval.second - interval.first);

        out << name << "," << duration << "," << std::endl;
    }
}

void CodeBenchmark::setFilename(const std::string& filename)
{
    mFilename = filename;
}

} // namespace mode7