#include "RestoringScalar.hpp"

namespace mode7
{

auto RestoringScalar::applyForce(float force) -> void
{
    mForce += force;
}

auto RestoringScalar::update() -> void
{
    const float springForce = -mSpringCoefficient * mPosition;
    const float dampForce = -mDampingCoefficient * mVelocity;
    const float appliedForce = mForce;
    const float sumForces{appliedForce + dampForce + springForce};

    const float deltaV = sumForces * mStep;
    mVelocity += deltaV;
    mPosition += mVelocity;

    mForce = 0;
}

auto RestoringScalar::getPosition() const -> float
{
    return mPosition;
}

auto RestoringScalar::moveTo(float pos) -> void
{
    mPosition = pos;
}

} // namespace mode7
