#ifdef __linux__

#include "FontsBackendPango.hpp"
#include "Screen.hpp"

#include <ft2build.h>
#include FT_SFNT_NAMES_H
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include FT_BBOX_H
#include FT_TYPE1_TABLES_H
#include <pango/pangocairo.h>

#include <cassert>

namespace mode7
{

auto FontsBackendPango::addFont(const std::string& key, const std::string& fn)
    -> bool
{
    FT_Face face;

    auto status{FT_New_Face(mLibrary, fn.c_str(), 0, &face)};
    if (status != 0)
    {
        return false;
    }

    mFaceLookup[key] = mFaces.size();
    mFaces.push_back(face);

    mMetrics.emplace_back(face);

    return addFontFileToSystem(fn);
}

auto FontsBackendPango::getCairoFontFace(const std::string& key)
    -> CairoFontFace
{
    if (mFaceLookup.find(key) == mFaceLookup.end())
    {
        return {};
    }
    const auto loc{mFaceLookup[key]};
    assert(loc < mFaces.size());
    assert(loc < mMetrics.size());

    return {key, mFaces[loc], &mMetrics[loc]};
}

auto FontsBackendPango::drawText(cairo_t* ctx, const std::string& text,
                                 const std::string& style, double x, double y)
    -> void
{
    PangoLayout* layout;
    PangoFontDescription* desc;

    const auto formatted{Fonts::formatString(style)};

    // setup
    layout = pango_cairo_create_layout(ctx);
    pango_layout_set_text(layout, text.c_str(), -1);
    desc = pango_font_description_from_string(formatted.c_str());
    const auto size{pango_font_description_get_size(desc) *
                    GLScreen::getScale()};
    pango_font_description_set_size(desc, size);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    int width;
    int height;
    pango_layout_get_size(layout, &width, &height);

    // draw
    cairo_save(ctx);

    cairo_set_source_rgb(ctx, 1, 1, 1);
    cairo_move_to(ctx, x, y);
    pango_cairo_show_layout(ctx, layout);

    cairo_restore(ctx);

    // cleanup
    g_object_unref(layout);
}

auto FontsBackendPango::addFontFileToSystem(const std::string& fn) -> bool
{
    const auto* fcFilename = (const FcChar8*)fn.c_str();
    FcBool fcAddFontStatus =
        FcConfigAppFontAddFile(FcConfigGetCurrent(), fcFilename);

    return bool(fcAddFontStatus);
}

} // namespace mode7

#endif /* __linux__ */