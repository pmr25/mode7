#include "Pipeline.hpp"
#include "AssetManager.hpp"
#include "Game.hpp"
#include "Logger.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <queue>

namespace mode7
{
static inline bool startswith(const std::string& str, const std::string& prefix)
{
    return str.compare(0, prefix.length(), prefix) == 0;
}

static inline std::string extract(const std::string& str,
                                  const uint32_t start = 0,
                                  const char delim = ' ')
{
    std::string found = "";

    if (start >= str.length())
    {
        return "";
    }

    auto it = str.begin() + start;
    while (*it != delim && it != str.end())
    {
        found += *it;
        ++it;
    }

    return found;
}

static inline std::string skip(const std::string& str, const uint32_t start = 0,
                               const char delim = ' ')
{
    if (start >= str.length())
    {
        return "";
    }

    auto it = str.begin() + start;

    while (*it == delim)
    {
        ++it;
    }

    uint32_t off = it - str.begin();
    if (off >= str.length())
    {
        return "";
    }

    return str.substr(off);
}

static inline std::vector<std::string> extract_skip(const std::string& str,
                                                    const char delim = ' ')
{
    std::vector<std::string> extracted;
    std::string tmp;
    std::string cpy = str;
    size_t prevLen = cpy.length();

    while (prevLen > 0)
    {
        tmp = extract(cpy, 0, delim);
        cpy = skip(cpy, tmp.length(), delim);

        if (tmp[0] == ';')
        {
            break;
        }

        extracted.push_back(tmp);

        assert(cpy.length() < prevLen);
        prevLen = cpy.length();
    }

    return extracted;
}

int32_t Pipeline::loadPipeline(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in)
    {
        std::cout << "error couldn't open " << filename << std::endl;
    }

    constexpr char delim = '\n';

    // config settings
    std::string dotext = ".glsl"; // default

    for (std::string line; std::getline(in, line, delim);)
    {
        if (line[0] == '#')
        {
            continue;
        }

        if (line[0] == ';')
        {
            continue;
        }

        std::vector<std::string> parts = extract_skip(line);

        if (parts.size() < 1)
        {
            continue;
        }

        if (parts[0][0] == '.')
        {
            if (parts.size() < 2)
            {
                SLOG(LG_ERROR) << "malformed line";
                exit(1);
            }

            if (parts[0] == ".ext")
            {
                dotext = parts[1];
            }
            else if (parts[0] == ".flow")
            {
                for (auto it = parts.begin() + 1; it != parts.end(); ++it)
                {
                    const char* cstr = (*it).c_str();
                    char dir = cstr[0];
                    uint32_t id = atoi(cstr + 1);

                    mFlow.push_back(std::pair<uint32_t, char>(id, dir));
                }
            }
            else if (parts[0] == ".alloc")
            {
                uint32_t amt = atoi(parts[1].c_str());
                mFrameBuffers.reserve(amt);
                mInputs.resize(amt);
                mTargets.resize(amt);
            }
            else if (parts[0] == ".bind")
            {
                if (parts.size() != 4)
                {
                    SLOG(LG_ERROR) << "malformed line .bind";
                    exit(1);
                }

                uint32_t fbID = atoi(parts[1].c_str()) - 1;
                uint32_t tgID = atoi(parts[2].c_str()) - 1;
                std::string& texName = parts[3];
                (void)tgID;

                if (mFrameBuffers.size() <= fbID)
                {
                    SLOG(LG_ERROR) << "invalid framebuffer id";
                    exit(1);
                }

                mFrameBuffers[fbID]->appendTextureLocation(texName);
            }
            else
            {
                SLOG(LG_ERROR) << "unknown framebuffer config param";
                exit(1);
            }
        }
        else
        {
            if (parts.size() < 3)
            {
                SLOG(LG_ERROR) << "malformed line";
                exit(1);
            }

            uint32_t id = atoi(parts[0].c_str());
            std::string vsfilename = parts[1];
            std::string fsfilename = parts[2];
            std::string mode = parts[3];
            bool targetScreen = false;
            bool depthBuffer = true;
            int32_t err;

            for (uint32_t i = 4; i < parts.size(); ++i)
            {
                std::string& cur = parts[i];
                if (cur[0] == 't')
                {
                    uint32_t tid = atoi(cur.substr(1).c_str());
                    err = addTarget(id, tid);
                    if (err > 0)
                    {
                        std::cout << "addTarget error: " << err << std::endl;
                    }
                }
                else if (cur[0] == 'i')
                {
                    uint32_t iid = atoi(cur.substr(1).c_str());
                    err = addInput(id, iid);
                    if (err > 0)
                    {
                        std::cout << "addInput error: " << err << std::endl;
                    }
                }
                else if (cur[0] == 'd')
                {
                    uint32_t val = atoi(cur.substr(1).c_str());
                    depthBuffer = val == 1;
                }
            }

            std::shared_ptr<Framebuffer> fb = std::make_shared<Framebuffer>();
            uint32_t w = 1920;
            uint32_t h = 1080;
            uint32_t samples = 4;

            AssetManager& assets = AssetManager::getCurrent();

            if (startswith(mode, "rbo"))
            {
                fb->targetRenderbuffer(w, h, depthBuffer);
            }
            else if (startswith(mode, "tex"))
            {
                std::string forcedVS = extract(mode, 4, ',');
                std::string forcedFS =
                    extract(mode, 4 + forcedVS.length() + 1, ',');
                fb->targetTexture(w, h, depthBuffer);
                if (forcedVS != "x" && forcedFS != "x")
                {
                    // TODO set forced shader
                    fb->setForcedShader(std::make_shared<Shader>(
                        assets.shaderPath(forcedVS + ".glsl"),
                        assets.shaderPath(forcedFS + ".glsl")));
                }

                fb->setShader(std::make_shared<Shader>(
                    assets.shaderPath(vsfilename + ".glsl"),
                    assets.shaderPath(fsfilename + ".glsl")));
            }
            else if (startswith(mode, "msrbo"))
            {
                fb->targetMultisampledRenderBuffer(w, h, samples, depthBuffer);
            }
            else
            {
                SLOG(LG_ERROR) << "unknown framebuffer type";
                exit(1);
            }

            uint32_t internalId = addFramebuffer(fb, targetScreen);
            (void)internalId;
            assert(id == internalId);
        }
    }

    return 0;
}

uint32_t Pipeline::addFramebuffer(std::shared_ptr<Framebuffer> fb,
                                  bool isScreenOutput)
{
    mFrameBuffers.push_back(fb);
    mInputs.push_back(std::make_unique<std::vector<uint32_t>>());
    mTargets.push_back(std::make_unique<std::vector<uint32_t>>());
    uint32_t id = mFrameBuffers.size();

    if (isScreenOutput)
    {
        mScreenOutput = (int32_t)id;
    }

    return id;
}

int32_t Pipeline::addInput(uint32_t dest, uint32_t src)
{
    dest -= 1;

    // assert(mInputs.size() == mFrameBuffers.size());

    /*if (dest >= mFrameBuffers.size())
    {
        return 1;
    }

    if (src >= mFrameBuffers.size())
    {
        return 2;
    }*/

    assert(dest < mInputs.size());
    assert(mInputs[dest].get() != nullptr);
    mInputs[dest]->push_back(src);

    return 0;
}

int32_t Pipeline::addTarget(uint32_t from, uint32_t to)
{
    from -= 1;

    // assert(mTargets.size() == mFrameBuffers.size());

    /*if (from >= mFrameBuffers.size())
    {
        return 1;
    }

    if (to < UINT32_MAX && to >= mFrameBuffers.size())
    {
        return 2;
    }*/

    assert(from < mTargets.size());
    assert(mTargets[from].get() != nullptr);
    mTargets[from]->push_back(to);

    return 0;
}

int32_t Pipeline::addFlow(char dir, uint32_t id)
{
    mFlow.emplace_back(id, dir);
    return 0;
}

int32_t Pipeline::computeFlow()
{
    return 0;
}

bool Pipeline::run(Game* game) const
{
    for (std::pair<uint32_t, char> e : mFlow)
    {
        uint32_t flowID = e.first - 1;
        char dir = e.second;

        assert(flowID < mFrameBuffers.size());

        std::shared_ptr<Framebuffer> curFB = mFrameBuffers[flowID];

        if (dir == 'i')
        {
            curFB->begin();
            game->drawShader(curFB->getForcedShader());
        }
        else if (dir == 'o')
        {
            curFB->preDraw();
            assert(flowID < mInputs.size());
            assert(mInputs[flowID].get() != nullptr);
            for (auto& f : *(mInputs[flowID]))
            {
                uint32_t inputID = f - 1;
                assert(inputID < mFrameBuffers.size());

                curFB->feedTexture(mFrameBuffers[inputID]->getTexGL());
            }

            assert(flowID < mTargets.size());
            assert(mTargets[flowID].get() != nullptr);
            for (auto& f : *(mTargets[flowID]))
            {
                // drawing to other fb in pipeline
                if (f > 0)
                {
                    uint32_t targetID = f - 1;
                    assert(targetID < mFrameBuffers.size());

                    std::shared_ptr<Framebuffer> tgt = mFrameBuffers[targetID];

                    curFB->draw(tgt.get());
                }
                // drawing to screen
                else
                {
                    curFB->draw(nullptr);
                }
            }
        }
    }

    return true;
}

bool Pipeline::stateRun(std::shared_ptr<GameState> gs) const
{
    for (std::pair<uint32_t, char> e : mFlow)
    {
        uint32_t flowID = e.first - 1;
        char dir = e.second;

        assert(flowID < mFrameBuffers.size());

        std::shared_ptr<Framebuffer> curFB = mFrameBuffers[flowID];

        if (dir == 'i')
        {
            curFB->begin();
            gs->drawShader(curFB->getForcedShader());
        }
        else if (dir == 'o')
        {
            curFB->preDraw();
            assert(flowID < mInputs.size());
            assert(mInputs[flowID].get() != nullptr);
            for (auto& f : *(mInputs[flowID]))
            {
                uint32_t inputID = f - 1;
                assert(inputID < mFrameBuffers.size());

                curFB->feedTexture(mFrameBuffers[inputID]->getTexGL());
            }

            assert(flowID < mTargets.size());
            assert(mTargets[flowID].get() != nullptr);
            for (auto& f : *(mTargets[flowID]))
            {
                // drawing to other fb in pipeline
                if (f > 0)
                {
                    uint32_t targetID = f - 1;
                    assert(targetID < mFrameBuffers.size());

                    std::shared_ptr<Framebuffer> tgt = mFrameBuffers[targetID];

                    curFB->draw(tgt.get());
                }
                // drawing to screen
                else
                {
                    curFB->draw(nullptr);
                }
            }
        }
    }

    return true;
}

void Pipeline::summarize() const
{
#ifdef _DEBUG
    std::cout << "========== Pipeline Summary ==========" << std::endl;

    std::cout << "framebuffers:" << std::endl;
    for (uint32_t i = 0; i < mFrameBuffers.size(); ++i)
    {
        std::cout << "\t" << i + 1 << ": " << mFrameBuffers[i]->getName()
                  << std::endl;
    }

    std::cout << "inputs:" << std::endl;
    for (uint32_t i = 0; i < mInputs.size(); ++i)
    {
        if ((*(mInputs[i])).size() < 1)
        {
            continue;
        }

        std::cout << "\t" << i + 1 << " inputs:" << std::endl;
        for (auto& e : *(mInputs[i]))
        {
            std::cout << "\t\t" << (int32_t)e << std::endl;
        }
    }

    std::cout << "targets:" << std::endl;
    for (uint32_t i = 0; i < mTargets.size(); ++i)
    {
        if ((*(mTargets[i])).size() < 1)
        {
            continue;
        }

        std::cout << "\t" << i + 1 << " targets:" << std::endl;
        for (auto& e : *(mTargets[i]))
        {
            std::cout << "\t\t" << (int32_t)e << std::endl;
        }
    }

    std::cout << "flow: ";
    for (auto& e : mFlow)
    {
        std::cout << e.second << (int32_t)e.first << " ";
    }
    std::cout << std::endl;

    std::cout << "======================================" << std::endl;
#endif /* _DEBUG */
}
} // namespace mode7
