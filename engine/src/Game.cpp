/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Game.hpp"
#include "Camera.hpp"
#include "Clock.hpp"
#include "Core.hpp"
#include "Keyboard.hpp"
#include "Logger.hpp"
#include "Mouse.hpp"
#include "Screen.hpp"
#include "Shader.hpp"

#if __linux__
#include <SDL.h>
#elif defined _WIN32
#include <SDL2/SDL.h>
#endif
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <iostream>

namespace mode7
{
void Game::destroy()
{
    m_running = false;
}

void Game::setup()
{
    Keyboard::attach();
    Mouse::attach();
}

void Game::pollInput()
{
    Mouse::clearEvent();
    while (SDL_PollEvent(&m_sdlEvent))
    {
#ifdef _DEBUG
        ImGui_ImplSDL2_ProcessEvent(&m_sdlEvent);
#endif /* _DEBUG */
        if (m_sdlEvent.type == SDL_QUIT)
        {
            m_running = false;
        }

        Mouse::sendEvent(&m_sdlEvent);
    }

    SDL_PumpEvents();
    Keyboard::poll();
    Mouse::poll();
}

void Game::mainLoop(int argc, char** argv)
{
    Core::init(argc, argv);

    Keyboard::attach();
    Mouse::attach();

    init();

    LOG(0, LG_DEBUG) << "[Game] Enter mainLoop";

    m_running = true;
    Clock::start();
    while (m_running)
    {
        GLScreen::runPipeline(this);
        GLScreen::postRender();
        GLScreen::flip();
        Clock::tick();
        while (Clock::lagging())
        {
            pollInput();
            update();
            Camera::getCurrent().updateView();
            Clock::lagTick();
        }
    }

    LOG(0, LG_DEBUG) << "[Game] Leaving mainLoop";

    destroy();

    LOG(0, LG_DEBUG) << "[Game] Destroy GLScreen";
    GLScreen::destroy();
    LOG(0, LG_DEBUG) << "[Game] Quit SDL";
}
} // namespace mode7
