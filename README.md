# mode7

## System Requirements

- OpenGL 3.3
- 1G RAM
- 2G Storage

## Game Dependencies

### vcpkg
- assimp
- benchmark
- bullet
- boost
- cairo
- catch2
- cereal
- lua
- sdl2
- sdl2-image
- sdl2-ttf
- sdl2-gfx

### Minimum Requirements
- assimp
- bullet
- boost
- cairo
- catch2
- cereal
- imgui
- imguicolortextedit
- json
- loguru
- lua
- sdl2
- sdl2-image
- sdl2-ttf
- sdl2-gfx

### Submodules
- assimp
- benchmark
- catch2
- imgui
- imguicolortextedit
- json
- libdpiaware
- libmsvg
- loguru
- lua
- luaaa
- protobuf
- sdl2
- tinyfiledialogs
- worker

### Game Dependencies
- bullet
- boost
- cairo
- cereal
- glm
- glew
- sdl2_image
- sdl2_ttf
- sdl2_gfx
- openssl
- vorbis
- x11
- libzip

### Development Dependencies
- cmake
- git
- python3
- rsync
- shotwell
- unzip
- wget
- xxd

Built on Ubuntu 20.04 or later
Packages can be found under tools/packages.txt

## Tool Dependencies

Some tools require Go to build. To use tools anywhere, run `source tools/toolpath` from the root directory.

## Build Dependencies

- C++ compiler
- CMake + preferred buildsystem
- Python3
- rsync (linux)
- robocopy (windows)
- xargs (for installing dependencies)

## Installing Dependencies

### Libraries

- Vcpkg (windows or linux)
- `xargs sudo apt install <tools/packages.txt`

### Toolchains

- Any way

## Building

### Linux

#### Using vcpkg

- `mkdir build`
- `cd build`
- `cmake .. -DCMAKE_TOOLCHAIN_FILE=<path to vcpkg.cmake> -DCMAKE_BUILD_TYPE=<preferred build type>`
- `make`

#### Finding packages with pkg-config

- `mkdir build`
- `cd build`
- `cmake .. -DCMAKE_BUILD_TYPE=<preferred build type>`
- `make`

### Windows

- Using Visual Studio
- Go to File -> Open -> CMake...
- Select CMakeLists.txt in root directory
- Right click CMakeLists.txt, select CMake Settings for ...
- Set CMAKE_TOOLCHAIN_FILE to path to vcpkg.cmake

Devtools won\'t be built on windows, but can be built with MINGW, WSL, etc.
