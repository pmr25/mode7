#include "ShaderEditor.hpp"
#include <iostream>

void ShaderEditor::init()
{
    mShader = std::make_shared<mode7::Shader>(mVertFn, mFragFn);
}

void ShaderEditor::show()
{
    TextEditor::Coordinates cpos = mVertEditor.GetCursorPosition();
    ImGui::Begin("Vertex Shader", nullptr,
                 ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_MenuBar); // | ImGuiWindowFlags_NoBackground);
    ImGui::SetWindowSize(ImVec2(800, 600), ImGuiCond_FirstUseEver);
    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("Save", "Ctrl-S"))
            {
                auto textToSave = mVertEditor.GetText();
                {
                    std::ofstream out(mVertFn);
                    if (out)
                    {
                        out << textToSave;
                    }
                    else
                    {
                        std::cout << "error! cannot save " << mFragFn << std::endl;
                    }
                }
                init();
            }
            if (ImGui::MenuItem("Quit", "Alt-F4"))
            {
                // TODO close
            }
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Edit"))
        {
            bool ro = mVertEditor.IsReadOnly();
            if (ImGui::MenuItem("Read-only mode", nullptr, &ro))
                mVertEditor.SetReadOnly(ro);
            ImGui::Separator();

            if (ImGui::MenuItem("Undo", "ALT-Backspace", nullptr, !ro && mVertEditor.CanUndo()))
                mVertEditor.Undo();
            if (ImGui::MenuItem("Redo", "Ctrl-Y", nullptr, !ro && mVertEditor.CanRedo()))
                mVertEditor.Redo();

            ImGui::Separator();

            if (ImGui::MenuItem("Copy", "Ctrl-C", nullptr, mVertEditor.HasSelection()))
                mVertEditor.Copy();
            if (ImGui::MenuItem("Cut", "Ctrl-X", nullptr, !ro && mVertEditor.HasSelection()))
                mVertEditor.Cut();
            if (ImGui::MenuItem("Delete", "Del", nullptr, !ro && mVertEditor.HasSelection()))
                mVertEditor.Delete();
            if (ImGui::MenuItem("Paste", "Ctrl-V", nullptr, !ro && ImGui::GetClipboardText() != nullptr))
                mVertEditor.Paste();

            ImGui::Separator();

            if (ImGui::MenuItem("Select all", nullptr, nullptr))
                mVertEditor.SetSelection(TextEditor::Coordinates(), TextEditor::Coordinates(mVertEditor.GetTotalLines(), 0));

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("View"))
        {
            if (ImGui::MenuItem("Dark palette"))
                mVertEditor.SetPalette(TextEditor::GetDarkPalette());
            if (ImGui::MenuItem("Light palette"))
                mVertEditor.SetPalette(TextEditor::GetLightPalette());
            if (ImGui::MenuItem("Retro blue palette"))
                mVertEditor.SetPalette(TextEditor::GetRetroBluePalette());
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }

    ImGui::Text("%6d/%-6d %6d lines  | %s | %s | %s | %s", cpos.mLine + 1, cpos.mColumn + 1, mVertEditor.GetTotalLines(),
                mVertEditor.IsOverwrite() ? "Ovr" : "Ins", mVertEditor.CanUndo() ? "*" : " ",
                mVertEditor.GetLanguageDefinition().mName.c_str(), mVertFn.c_str());

    mVertEditor.Render("TextEditor");
    ImGui::End();

    ImGui::Begin("Fragment Shader", nullptr,
                 ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_MenuBar); // | ImGuiWindowFlags_NoBackground);
    ImGui::SetWindowSize(ImVec2(800, 600), ImGuiCond_FirstUseEver);
    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("Save", "Ctrl-S"))
            {
                auto textToSave = mFragEditor.GetText();
                {
                    std::ofstream out(mFragFn);
                    if (out)
                    {
                        out << textToSave;
                    }
                    else
                    {
                        std::cout << "error! cannot save " << mFragFn << std::endl;
                    }
                }
                init();
            }
            if (ImGui::MenuItem("Quit", "Alt-F4"))
            {
                // TODO close
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit"))
        {
            bool ro = mFragEditor.IsReadOnly();
            if (ImGui::MenuItem("Read-only mode", nullptr, &ro))
                mFragEditor.SetReadOnly(ro);
            ImGui::Separator();

            if (ImGui::MenuItem("Undo", "ALT-Backspace", nullptr, !ro && mFragEditor.CanUndo()))
                mFragEditor.Undo();
            if (ImGui::MenuItem("Redo", "Ctrl-Y", nullptr, !ro && mFragEditor.CanRedo()))
                mFragEditor.Redo();

            ImGui::Separator();

            if (ImGui::MenuItem("Copy", "Ctrl-C", nullptr, mFragEditor.HasSelection()))
                mFragEditor.Copy();
            if (ImGui::MenuItem("Cut", "Ctrl-X", nullptr, !ro && mFragEditor.HasSelection()))
                mFragEditor.Cut();
            if (ImGui::MenuItem("Delete", "Del", nullptr, !ro && mFragEditor.HasSelection()))
                mFragEditor.Delete();
            if (ImGui::MenuItem("Paste", "Ctrl-V", nullptr, !ro && ImGui::GetClipboardText() != nullptr))
                mFragEditor.Paste();

            ImGui::Separator();

            if (ImGui::MenuItem("Select all", nullptr, nullptr))
                mFragEditor.SetSelection(TextEditor::Coordinates(), TextEditor::Coordinates(mFragEditor.GetTotalLines(), 0));

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("View"))
        {
            if (ImGui::MenuItem("Dark palette"))
                mFragEditor.SetPalette(TextEditor::GetDarkPalette());
            if (ImGui::MenuItem("Light palette"))
                mFragEditor.SetPalette(TextEditor::GetLightPalette());
            if (ImGui::MenuItem("Retro blue palette"))
                mFragEditor.SetPalette(TextEditor::GetRetroBluePalette());
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }

    ImGui::Text("%6d/%-6d %6d lines  | %s | %s | %s | %s", cpos.mLine + 1, cpos.mColumn + 1, mFragEditor.GetTotalLines(),
                mFragEditor.IsOverwrite() ? "Ovr" : "Ins", mFragEditor.CanUndo() ? "*" : " ",
                mFragEditor.GetLanguageDefinition().mName.c_str(), mFragFn.c_str());

    mFragEditor.Render("TextEditor");
    ImGui::End();
}
