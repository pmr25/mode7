#include <iostream>
#include <memory>

#include "MeshViewer.hpp"
#include <iostream>

#define WIDTH 1280
#define HEIGHT 960

static void print_help()
{
    std::cout
        << "usage: shaderlab <vertex shader> <fragment shader> <model> [flags]"
        << std::endl;
}

int main(int argc, char** argv)
{
    std::string model;
    std::string vsfn;
    std::string fsfn;

    if (argc < 4)
    {
        print_help();

        model = "sphere.dae";
        vsfn = "default_vert.glsl";
        fsfn = "default_frag.glsl";
    }
    else
    {
        model = argv[1];
        vsfn = argv[2];
        fsfn = argv[3];
    }

    mode7::Core::init(argc, argv);

    mode7::GLScreen::create(WIDTH, HEIGHT, WIDTH, HEIGHT, "Shader Lab", false,
                            true, false);

    std::shared_ptr<mode7::AssetManager> assets =
        mode7::AssetManager::createAndMakeCurrent();
    assets->setBaseDirectory(mode7::Util::getInstallDir() + "/assets");
    assets->setModelDirectory("./");
    assets->setShaderDirectory("./");
    assets->setTextureDirectory("./");

    std::shared_ptr<MeshViewer> meshViewer =
        std::make_shared<MeshViewer>(model, vsfn, fsfn);

    meshViewer->mainLoop(argc, argv);

    return 0;
}
