#ifndef SHADEREDITOR_HPP
#define SHADEREDITOR_HPP

#include "TextEditor.h"
#include <ImGuiBuilder.hpp>
#include <common.hpp>
#include <fstream>
#include <memory>

class ShaderEditor : public mode7::ImGuiBuilder
{
public:
    ShaderEditor(const std::string& vsfn, const std::string& fsfn)
        : ImGuiBuilder()
        , mVertFn(vsfn)
        , mFragFn(fsfn)
    {
        std::ifstream vsin(mVertFn);
        if (vsin)
        {
            std::string str((std::istreambuf_iterator<char>(vsin)), std::istreambuf_iterator<char>());
            mVertEditor.SetText(str);
        }

        std::ifstream fsin(mFragFn);
        if (fsin)
        {
            std::string str((std::istreambuf_iterator<char>(fsin)), std::istreambuf_iterator<char>());
            mFragEditor.SetText(str);
        }
    }

    virtual ~ShaderEditor() = default;

    void init();

    virtual void show();

    inline mode7::Shader* getShader() const
    {
        return mShader.get();
    }

private:
    TextEditor mVertEditor;
    TextEditor mFragEditor;
    std::string mVertFn;
    std::string mFragFn;
    std::shared_ptr<mode7::Shader> mShader;
};

#endif /* SHADEREDITOR_HPP */
