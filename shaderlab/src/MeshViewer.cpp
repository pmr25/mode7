#include "MeshViewer.hpp"

static float inc = 0.0f;

void MeshViewer::init()
{
    mPipeline = std::make_shared<mode7::DirectPipeline>();
    mode7::GLScreen::setPipeline(mPipeline);

    std::shared_ptr<mode7::Camera> camera = mode7::Camera::createAndMakeCurrent(70.0f, 0.1f, 1000.0f);
    camera->lookAt(glm::vec3(2.0f), glm::vec3(0, -1.5, 0));

    const mode7::AssetManager& assets = mode7::AssetManager::getCurrent();
    mSphere = mode7::ModelLoader::openUnique(assets.modelPath(mModelPath), assets);

    mEditor->init();
    mode7::GLScreen::setImGuiBuilder(mEditor);

    mDirLight = std::make_shared<mode7::Light>();
    mDirLight->diffuse = glm::vec4(1, 1, 1, 1);
    mDirLight->specular = glm::vec4(1, 1, 1, 1);
    mDirLight->position = glm::vec4(-3, 3, 3, 0);
    mLights.addLight(mDirLight);

    mPointLight = std::make_shared<mode7::Light>();
    mPointLight->diffuse = glm::vec4(1.0f);
    mPointLight->specular = glm::vec4(1.0f);
    mPointLight->position = glm::vec4(2, 0, 0, 1);
    mLights.addLight(mPointLight);

    mLights.cacheUniformLocations(mEditor->getShader());

    mUVTex.open(assets.texturePath("rose.png"));
    mUVTex.setType(mode7::TexType::DIFFUSE);

    mNormTex.open(assets.texturePath("normal.png"));
    mNormTex.setType(mode7::TexType::NORMAL);

    mMaterial = std::make_shared<mode7::Material>();
    mMaterial->setAmbient(glm::vec4(glm::vec3(0.2f), 1.0f));
    mMaterial->setDiffuse(glm::vec4(1.0f));
    mMaterial->setShininess(500.0f);
    mMaterial->setSpecular(glm::vec4(1.0f));

    mMaterial->addMap(&mUVTex);
    mMaterial->addMap(&mNormTex);

    mSphere->getMesh(0)->setMaterial(mMaterial);
}

void MeshViewer::update()
{
    mSphere->rotate(glm::vec3(0.25f, 0.0f, 0.25f));
    mSphere->update();
    mPointLight->position = glm::vec4(0, 0, 0, 1) + sinf(inc) * glm::vec4(5, 0, 0, 0) + cosf(inc) * glm::vec4(0, 0, 5, 0);
    inc += 0.05f;
}

void MeshViewer::drawShader(mode7::Shader* shader)
{
    if (shader == nullptr)
    {
        mEditor->getShader()->use();
        mLights.fillUniforms();
        mSphere->draw(mEditor->getShader());
    }
    else
    {
        assert(false);
    }
}

void MeshViewer::destroy()
{
}
