#ifndef MESHVIEWER_HPP
#define MESHVIEWER_HPP

#include "ShaderEditor.hpp"
#include <common.hpp>
#include <memory>
#include <iostream>

class MeshViewer : public mode7::Game
{
public:
    MeshViewer(const std::string& model, const std::string& vsfn, const std::string& fsfn)
        : Game()
        , mModelPath(model)
    {
        const mode7::AssetManager& assets = mode7::AssetManager::getCurrent();
        mEditor = std::make_shared<ShaderEditor>(assets.shaderPath(vsfn), assets.shaderPath(fsfn));
        std::cout << "[MeshViewer] " << model << ", " << vsfn << ", " << fsfn << std::endl;
    }

    virtual ~MeshViewer() = default;

    virtual void init() override;
    virtual void update() override;
    virtual void drawShader(mode7::Shader*) override;
    virtual void destroy() override;

private:
    std::shared_ptr<mode7::DirectPipeline> mPipeline;
    std::unique_ptr<mode7::Scene> mSphere;
    std::shared_ptr<ShaderEditor> mEditor;
    mode7::LightGroup mLights;
    std::shared_ptr<mode7::Light> mDirLight;
    std::shared_ptr<mode7::Light> mPointLight;
    std::shared_ptr<mode7::Material> mMaterial;
    mode7::TextureImage mUVTex;
    mode7::TextureImage mNormTex;
    std::string mModelPath;
};

#endif /* MESHVIEWER_HPP */
