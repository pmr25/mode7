#version 330 core
precision highp float;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 vert_uv;
layout(location = 3) in vec3 tangent;

// uniform int numLights;
// layout(std140) uniform Lights
// {
//     vec3 position[];

//     vec4 color[];
//     vec4 ambient[];
//     vec4 diffuse[];
//     vec4 specular[];

//     vec3 spotDirection[];
//     float spotExponent[];
//     float spotCutoff[];
//     float spotCosCutoff[];

//     float attenConst[];
//     float attenLinear[];
//     float attenQuadratic[];
//     vec3 attenuation[];
// };

uniform uint numLights;

out vec2 uv;
out vec3 norm;
out vec3 binorm;
out vec3 tang;
out vec3 vert;

uniform mat4 p;
uniform mat4 v;
uniform mat4 m;

void main()
{
    vec3 bn = normalize(cross(tangent, normal));
    binorm = normalize((v * m * vec4(bn, 0)).xyz);
    norm = normalize((v * m * vec4(normal, 0)).xyz);
    tang = normalize((v * m * vec4(tangent, 0)).xyz);
    uv = vert_uv;
    vert = (v * m * vec4(position,1)).xyz;
    gl_Position = p * v * m * vec4(position, 1);
}

