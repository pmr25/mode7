#ifdef _WIN32
#define BENCHMARK_STATIC_DEFINE
#endif /* _WIN32 */
#include <benchmark/benchmark.h>

#include "Object.hpp"

static void BM_ObjectCreate(benchmark::State& state)
{
    for (auto _ : state)
    {
        mode7::Object o;
    }
}
BENCHMARK(BM_ObjectCreate);

static void BM_ObjectUpdate(benchmark::State& state)
{
    mode7::Object o;

    for (auto _ : state)
    {
        o.setPosition(glm::vec3(0.0f));
        o.update();
    }
}
BENCHMARK(BM_ObjectUpdate);

static void BM_ObjectDecompose(benchmark::State& state)
{
    mode7::Object o;

    for (auto _ : state)
    {
        o.decompose();
    }
}
BENCHMARK(BM_ObjectDecompose);
