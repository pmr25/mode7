#ifdef _WIN32
#define BENCHMARK_STATIC_DEFINE
#endif /* _WIN32 */
#include <benchmark/benchmark.h>

#include "Camera.hpp"
#include "Sphere.hpp"

void BM_CameraUpdateFrustum(benchmark::State& state)
{
    mode7::Camera c;
    c.create(70.0f, 1.0f, 100.0f, 1920.0f, 1080.0f);
    for (auto _ : state)
    {
        c.updateFrustum();
    }
}
BENCHMARK(BM_CameraUpdateFrustum);

void BM_CameraGetView(benchmark::State& state)
{
    mode7::Camera c;
    c.create(70.0f, 1.0f, 100.0f, 1920.0f, 1080.0f);
    for (auto _ : state)
    {
        c.getView();
    }
}
BENCHMARK(BM_CameraGetView);

void BM_CameraIsInsideFrustum(benchmark::State& state)
{
    mode7::Camera c;
    mode7::Sphere v;

    c.create(70.0f, 1.0f, 100.0f, 1920.0f, 1080.0f);

    for (auto _ : state)
    {
        c.isInsideFrustum(&v);
    }
}
BENCHMARK(BM_CameraIsInsideFrustum);
