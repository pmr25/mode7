#ifdef _WIN32
#define BENCHMARK_STATIC_DEFINE
#endif /* _WIN32 */
#include <benchmark/benchmark.h>
#include <cmath>
#include <cstdint>
#include <iostream>

#include "Job.hpp"
#include "Producer.hpp"

class SqrtJob : public mode7::Job
{
public:
    SqrtJob()
        : mode7::Job()
    {
    }

    virtual ~SqrtJob() = default;

    virtual void run()
    {
        for (auto i{0U}; i < 100'000'000; ++i)
        {
            sqrtf(rand());
        }
    }
};

static void BM_SingleThreaded(benchmark::State& state)
{
    for (auto _ : state)
    {
        for (auto i{0}; i < 100'000'000; ++i)
        {
            sqrtf(rand());
        }
    }
}
BENCHMARK(BM_SingleThreaded);

static void BM_TwoThreads(benchmark::State& state)
{
    std::vector<std::unique_ptr<SqrtJob>> jobs;
    for (auto i{0U}; i < 100U; ++i)
    {
        jobs.push_back(std::make_unique<SqrtJob>());
    }
    for (auto _ : state)
    {
        mode7::Producer producer(2);
        for (auto& job : jobs)
        {
            producer.feedJob(job.get());
        }
        producer.distribute();
    }
}
BENCHMARK(BM_TwoThreads);

static void BM_FourThreads(benchmark::State& state)
{
    std::vector<std::unique_ptr<SqrtJob>> jobs;
    for (auto i{0U}; i < 1000U; ++i)
    {
        jobs.push_back(std::make_unique<SqrtJob>());
    }
    for (auto _ : state)
    {
        mode7::Producer producer(4);
        for (auto& job : jobs)
        {
            producer.feedJob(job.get());
        }
        producer.distribute();
    }
}
BENCHMARK(BM_FourThreads);

static void BM_EightThreads(benchmark::State& state)
{
    std::vector<std::unique_ptr<SqrtJob>> jobs;
    for (auto i{0U}; i < 1000U; ++i)
    {
        jobs.push_back(std::make_unique<SqrtJob>());
    }
    for (auto _ : state)
    {
        mode7::Producer producer(8);
        for (auto& job : jobs)
        {
            producer.feedJob(job.get());
        }
        producer.distribute();
    }
}
BENCHMARK(BM_EightThreads);