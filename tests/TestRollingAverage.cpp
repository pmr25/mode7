#include "RollingAverage.hpp"
#include "gtest/gtest.h"

#include <cmath>

TEST(RollingAverageTest, testUniform)
{
    mode7::RollingAverage avg(4);
    avg.ingest(1.0F);
    avg.ingest(2.0F);
    avg.ingest(3.0F);
    avg.ingest(4.0F);
    const float val{avg.compute()};
    const float ans{2.5};
    EXPECT_TRUE(fabsf(val - ans) <= FLT_EPSILON);
}

TEST(RollingAverageTest, testUniform_rollover)
{
    mode7::RollingAverage avg(4);
    avg.ingest(1.0F);
    avg.ingest(2.0F);
    avg.ingest(3.0F);
    avg.ingest(4.0F);
    avg.ingest(5.0F);
    const float val{avg.compute()};
    const float ans{3.5F};
    EXPECT_TRUE(fabsf(val - ans) <= FLT_EPSILON);
}

TEST(RollingAverageTest, testWeighted)
{
    mode7::RollingAverage avg({0.5F, 0.25F, 0.125F, 0.125F});
    avg.ingest(1.0F);
    avg.ingest(2.0F);
    avg.ingest(3.0F);
    avg.ingest(4.0F);
    const float val{avg.compute()};
    const float ans{3.125F};
    EXPECT_TRUE(fabsf(val - ans) <= FLT_EPSILON);
}

TEST(RollingAverageTest, testWeighted_rollover)
{
    mode7::RollingAverage avg({0.5F, 0.25F, 0.125F, 0.125F});
    avg.ingest(1.0F);
    avg.ingest(2.0F);
    avg.ingest(3.0F);
    avg.ingest(4.0F);
    avg.ingest(5.0F);
    const float val{avg.compute()};
    const float ans{4.125F};
    EXPECT_TRUE(fabsf(val - ans) <= FLT_EPSILON);
}
