#include "RayCaster2D.hpp"

#include "Quad.hpp"
#include "gtest/gtest.h"

TEST(TestRayCaster2D, intersects_line)
{
    mode7::RayCaster2D ray({0, 0}, {1, 0});
    Line<2> segment({2, 2}, {2, -2});
    EXPECT_NE(ray.intersects(segment), std::numeric_limits<float>::infinity());
    EXPECT_GT(ray.intersects(segment), 0.0F);
    EXPECT_EQ(ray.intersects(segment), 2.0F);
}

TEST(TestRayCaster2D, intersects_line_behind)
{
    mode7::RayCaster2D ray({0, 0}, {-1, 0});
    Line<2> segment({2, 2}, {2, -2});
    EXPECT_EQ(ray.intersects(segment), std::numeric_limits<float>::infinity());
}

TEST(TestRayCaster2D, intersects_quad)
{
    mode7::RayCaster2D ray({0, 0}, {0, -1});
    Quad quad{
        {-0.5, -2.0},
        {-0.5, -1.0},
        {0.5, -1.0},
        {0.5, -2.0}
    };

    const float dist1 = ray.intersects(quad);
    EXPECT_FLOAT_EQ(dist1, 1.0F);
}

TEST(TestRayCaster2D, correct_distance)
{
    // RayCaster{ pos=vec2(0.000000, 0.000000)	dir=vec2(-0.000000, -1.000000) }
    // Polygon(4)
    //	vec2(-0.500000, -2.000000) -> vec2(-0.500000, -1.000000)
    //	vec2(-0.500000, -1.000000) -> vec2(0.500000, -1.000000)
    //	vec2(0.500000, -1.000000) -> vec2(0.500000, -2.000000)
    //	vec2(0.500000, -2.000000) -> vec2(-0.500000, -2.000000)
    // 0.333333
    mode7::RayCaster2D ray({0, 0}, {0, -1});
    Line<2> segment1({-0.5, -2}, {0.5, -2});
    Line<2> segment2({-0.5, -1}, {0.5, -1});
    Line<2> miss1({-0.5, -2},{-0.5, -1});
    Line<2> miss2({0.5, -1},{0.5, -2});

    const float dist1 = ray.intersects(segment1);
    EXPECT_FLOAT_EQ(dist1, 2.0F);

    const float dist2 = ray.intersects(segment2);
    EXPECT_FLOAT_EQ(dist2, 1.0F);

    EXPECT_EQ(ray.intersects(miss1), std::numeric_limits<float>::infinity());
    EXPECT_EQ(ray.intersects(miss2), std::numeric_limits<float>::infinity());
}
