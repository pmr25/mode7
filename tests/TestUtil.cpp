#include "Util.hpp"
#include "gtest/gtest.h"

TEST(UtilTest, unitBilinear_corners)
{
    EXPECT_EQ(mode7::Util::unitBilinear(0, 0, 1, 2, 3, 4), 1);
    EXPECT_EQ(mode7::Util::unitBilinear(1, 0, 1, 2, 3, 4), 2);
    EXPECT_EQ(mode7::Util::unitBilinear(1, 1, 1, 2, 3, 4), 3);
    EXPECT_EQ(mode7::Util::unitBilinear(0, 1, 1, 2, 3, 4), 4);
}

TEST(UtilTest, unitBilinear_center)
{
    EXPECT_EQ(mode7::Util::unitBilinear(0.5, 0.5, 0, 0, 1, 1), 0.5);
}

TEST(UtilTest, mapCoordsToUnitSquare_corners_linear)
{
    const glm::vec2 a{-1, -1};
    const glm::vec2 b{1, -1};
    const glm::vec2 c{1, 1};
    const glm::vec2 d{-1, 1};

    const auto mapA = mode7::Util::mapCoordsToUnitSquare(a, a, b, c, d);
    const auto mapB = mode7::Util::mapCoordsToUnitSquare(b, a, b, c, d);
    const auto mapC = mode7::Util::mapCoordsToUnitSquare(c, a, b, c, d);
    const auto mapD = mode7::Util::mapCoordsToUnitSquare(d, a, b, c, d);

    EXPECT_EQ(mapA, glm::vec2(0, 0));
    EXPECT_EQ(mapB, glm::vec2(1, 0));
    EXPECT_EQ(mapC, glm::vec2(1, 1));
    EXPECT_EQ(mapD, glm::vec2(0, 1));
}


TEST(UtilTest, mapCoordsToUnitSquare_corners_linear_rotate)
{
    const glm::vec2 a{-1, 0};
    const glm::vec2 b{0, -1};
    const glm::vec2 c{1, 0};
    const glm::vec2 d{0, 1};

    const auto mapA = mode7::Util::mapCoordsToUnitSquare(a, a, b, c, d);
    const auto mapB = mode7::Util::mapCoordsToUnitSquare(b, a, b, c, d);
    const auto mapC = mode7::Util::mapCoordsToUnitSquare(c, a, b, c, d);
    const auto mapD = mode7::Util::mapCoordsToUnitSquare(d, a, b, c, d);

    EXPECT_EQ(mapA, glm::vec2(0, 0));
    EXPECT_EQ(mapB, glm::vec2(1, 0));
    EXPECT_EQ(mapC, glm::vec2(1, 1));
    EXPECT_EQ(mapD, glm::vec2(0, 1));
}

TEST(UtilTest, mapCoordsToUnitSquare_corners_nonlinear)
{
    const glm::vec2 a{-1, -1};
    const glm::vec2 b{8, 3};
    const glm::vec2 c{13, 11};
    const glm::vec2 d{-4, 8};

    const auto mapA = mode7::Util::mapCoordsToUnitSquare(a, a, b, c, d);
    const auto mapB = mode7::Util::mapCoordsToUnitSquare(b, a, b, c, d);
    const auto mapC = mode7::Util::mapCoordsToUnitSquare(c, a, b, c, d);
    const auto mapD = mode7::Util::mapCoordsToUnitSquare(d, a, b, c, d);

    EXPECT_EQ(mapA, glm::vec2(0, 0));
    EXPECT_EQ(mapB, glm::vec2(0, 1));
    EXPECT_EQ(mapC, glm::vec2(1, 1));
    EXPECT_EQ(mapD, glm::vec2(1, 0));
}

TEST(UtilTest, arbitraryQuadBilinear_avg)
{
//    //vec3(86.825844, 0.000000, 124.316055)	vec3(58.504360, 1.000000, 134.210190)	vec3(57.282715, 2.000000, 130.591354)	vec3(86.252266, 3.000000, 122.796143)
//    const glm::vec3 a{86.825844, 0.000000, 124.316055};
//    const glm::vec3 b{58.504360, 1.000000, 134.210190};
//    const glm::vec3 c{57.282715, 2.000000, 130.591354};
//    const glm::vec3 d{86.252266, 3.000000, 122.796143};
//
//    glm::vec3 avg{a.x + b.x + c.x + d.x, 0, a.y + b.y + c.y + d.y};
//    avg *= 0.25F;
//
//    const float v = mode7::Util::arbitraryQuadBilinear(avg, c, d, a, b).y;
//
//    EXPECT_FALSE(isnan(v));
}

TEST(UtilTest, arbitraryQuadBilinear_sharp)
{
    const glm::vec3 a{-1, 0,-1};
    const glm::vec3 b{6, 0,1};
    const glm::vec3 c{13, 0, 1};
    const glm::vec3 d{-4, 0,0};

    const auto mapA = mode7::Util::arbitraryQuadBilinear(a, a, b, c, d);
    const auto mapB = mode7::Util::arbitraryQuadBilinear(a, d, c, b, a);
    const auto mapC = mode7::Util::arbitraryQuadBilinear(a, b, c, d, a);
    const auto mapD = mode7::Util::arbitraryQuadBilinear(a, c, d, a, b);
}

TEST(UtilTest, arbitraryQuadBilinear_hugeoffset)
{
    const auto ox{1000};
    const auto oy{1000};
    const glm::vec2 a{-1 + ox, -1 + oy};
    const glm::vec2 b{8 + ox, 3 + oy};
    const glm::vec2 c{13 + ox, 11 + oy};
    const glm::vec2 d{-4 + ox, 8 + oy};

    const auto mapA = mode7::Util::mapCoordsToUnitSquare(a, a, b, c, d);
    const auto mapB = mode7::Util::mapCoordsToUnitSquare(a, d, c, b, a);
    const auto mapC = mode7::Util::mapCoordsToUnitSquare(a, b, c, d, a);
    const auto mapD = mode7::Util::mapCoordsToUnitSquare(a, c, d, a, b);
}

TEST(UtilTest, arbitraryQuadBilinear_nofail)
{
    const glm::vec3 a{-1, 0, -1};
    const glm::vec3 b{8, 1, 3 - 0};
    const glm::vec3 c{13, 2, 11 - 00};
    const glm::vec3 d{-4, 3, 8};
    const auto mapA = mode7::Util::arbitraryQuadBilinear(a, a, b, c, d);
    const auto mapB = mode7::Util::arbitraryQuadBilinear(b, d, c, b, a);
    const auto mapC = mode7::Util::arbitraryQuadBilinear(c, b, c, d, a);
    const auto mapD = mode7::Util::arbitraryQuadBilinear(d, c, d, a, b);
}


TEST(UtilTest, arbitraryQuadBilinear_fpe)
{
    //vec3(86.825844, 0.000000, 124.316055)	vec3(58.504360, 1.000000, 134.210190)	vec3(57.282715, 2.000000, 130.591354)	vec3(86.252266, 3.000000, 122.796143)
    const glm::vec3 a{86.825844, 0.000000, 124.316055};
    const glm::vec3 b{58.504360, 1.000000, 134.210190};
    const glm::vec3 c{57.282715, 2.000000, 130.591354};
    const glm::vec3 d{86.252266, 3.000000, 122.796143};

    const float va = mode7::Util::arbitraryQuadBilinear(a, a, b, c, d).y;
    const float vb = mode7::Util::arbitraryQuadBilinear(b, a, b, c, d).y;
    const float vc = mode7::Util::arbitraryQuadBilinear(c, a, b, c, d).y;
    const float vd = mode7::Util::arbitraryQuadBilinear(d, a, b, c, d).y;

    EXPECT_TRUE(fabsf(va - 0.0F) < 1e-4);
    EXPECT_TRUE(fabsf(vb - 1.0F) < 1e-4);
    EXPECT_TRUE(fabsf(vc - 2.0F) < 1e-4);
    EXPECT_TRUE(fabsf(vd - 3.0F) < 1e-4);
}
