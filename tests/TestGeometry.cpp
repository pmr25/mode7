/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#define CATCH_CONFIG_RUNNER
#include <catch2/catch_all.hpp>

#include "Bezier.hpp"
#include "Line.hpp"
#include "Quad.hpp"
#include "Util.hpp"
#include "gl.hpp"

#include <iostream>

#define EPSILON 0.001

template <int D>
static bool compareVec(glm::vec<D, float> a, glm::vec<D, float> b)
{
    bool out = true;

    for (int i{0}; i < D; ++i)
    {
        out = out & (a[i] == b[i]);
    }

    return out;
}

TEST_CASE("Test libgeometry lines", "[libgeometry]")
{
    Line<2> line;

    line.connectPoints(glm::vec2(0, 0), glm::vec2(2, 2));

    REQUIRE(
        compareVec<2>(line.computeNormal(), glm::normalize(glm::vec2(-1, 1))));
    REQUIRE(line.isSegment() == true);
    REQUIRE(compareVec<2>(line.derivative(0.5f), glm::vec2(2, 2)));
    REQUIRE(compareVec<2>(line.derivative2(0.5f), glm::vec2(0, 0)));
}

TEST_CASE("Test libgeometry bezier curves with one control point",
          "[libgeometry]")
{
    Bezier<2> curve;

    curve.createOneControl(glm::vec2(2, 0), glm::vec2(0, 0), glm::vec2(1, 1));

    std::cout << curve.curvature(0.1) << std::endl;
}

TEST_CASE("Test libgeometry bezier curves with two control points",
          "[libgeometry]")
{
    Bezier<2> curve;

    curve.createTwoControl(glm::vec2(0, 0), glm::vec2(4, 0), glm::vec2(1, 1),
                           glm::vec2(2, -1));
}

TEST_CASE("Test libgeometry quads", "[libgeometry]")
{
    Quad quad;
    bool inside;
    bool outside;

    quad.connectPoints(glm::vec2(1, 1), glm::vec2(2, 1), glm::vec2(2, 2),
                       glm::vec2(1, 2));
    inside = quad.checkIntersect(glm::vec2(1.5, 1.5));
    outside = quad.checkIntersect(glm::vec2(0.5, 2.5));
    REQUIRE(inside);
    REQUIRE_FALSE(outside);

    quad.connectPoints(glm::vec2(0, 0), glm::vec2(2, 0), glm::vec2(1, 1),
                       glm::vec2(-1, 1));
    REQUIRE(quad.getNumSides() == 4);
    inside = quad.checkIntersect(glm::vec2(0.5, 0.5));
    outside = quad.checkIntersect(glm::vec2(-1, -3));
    REQUIRE(inside);
    REQUIRE_FALSE(outside);
}

TEST_CASE("Test Util Class", "[Util]")
{
    float vis = 90;
    glm::quat rotation =
        glm::angleAxis(mode7::Util::rad(vis), mode7::Util::xAxis()) *
        glm::angleAxis(mode7::Util::rad(vis), mode7::Util::yAxis()) *
        glm::angleAxis(mode7::Util::rad(vis), mode7::Util::zAxis());
    glm::quat swingX;
    glm::quat twistX;
    glm::quat swingY;
    glm::quat twistY;
    glm::quat swingZ;
    glm::quat twistZ;
    std::pair<glm::quat, glm::quat> pair;

    pair = mode7::Util::swingTwistDecomp(rotation, mode7::Util::xAxis());
    swingX = pair.first;
    twistX = pair.second;

    pair = mode7::Util::swingTwistDecomp(rotation, mode7::Util::yAxis());
    swingY = pair.first;
    twistY = pair.second;

    pair = mode7::Util::swingTwistDecomp(rotation, mode7::Util::zAxis());
    swingZ = pair.first;
    twistZ = pair.second;

    float ax = mode7::Util::recoverAngleAxis(swingX);
    float ay = mode7::Util::recoverAngleAxis(swingY);
    float az = mode7::Util::recoverAngleAxis(swingZ);

    REQUIRE(ax == (float)M_PI);
    REQUIRE(ay == (float)M_PI);
    REQUIRE(az == (float)M_PI);
}

int main(int argc, char* argv[])
{
    // global setup...
    int result = Catch::Session().run(argc, argv);
    // global clean-up...
    return result;
}