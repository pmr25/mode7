#include "Bucket.hpp"
#include <catch2/catch_all.hpp>
#include <memory>
#include <vector>

TEST_CASE("Bucket", "[NTree]")
{
    mode7::Bucket<2> bucket(10, 10, 0, glm::vec2(0.0F), glm::vec2(1000.0F));
    std::vector<std::shared_ptr<mode7::Entity<2>>> entities;

    // populate entities vector
    const auto numEntities{100};
    const auto dimensions{glm::vec2(1.0F)};
    for (auto i{0}; i < numEntities; ++i)
    {
        const auto position{glm::vec2(float(i))};
        auto entity{std::make_shared<mode7::Entity<2>>(position, dimensions)};
        entities.push_back(entity);
    }

    SECTION("bucket contains all entities")
    {
        for (auto entity : entities)
        {
            REQUIRE(bucket.contains(entity));
        }
    }

    SECTION("bucket does not contain outside entity")
    {
        REQUIRE_FALSE(bucket.contains(std::make_shared<mode7::Entity<2>>(
            glm::vec2(-100.0F), glm::vec2(1.0F))));
    }

    SECTION("insert succeeds")
    {
        for (auto entity : entities)
        {
            auto inserted{bucket.insert(entity)};
            REQUIRE(inserted >= 0);
        }
    }

    SECTION("subdivides correctly")
    {
        for (auto entity : entities)
        {
            auto inserted{bucket.insert(entity)};
            REQUIRE(inserted >= 0);
        }

        const auto& branches{bucket.getBranches()};

        REQUIRE(branches[0].getOrigin() == glm::vec2(0, 0));
        REQUIRE(branches[1].getOrigin() == glm::vec2(500, 0));
        REQUIRE(branches[2].getOrigin() == glm::vec2(0, 500));
        REQUIRE(branches[3].getOrigin() == glm::vec2(500, 500));

        REQUIRE(branches[0].getDimensions() == glm::vec2(500));
        REQUIRE(branches[1].getDimensions() == glm::vec2(500));
        REQUIRE(branches[2].getDimensions() == glm::vec2(500));
        REQUIRE(branches[3].getDimensions() == glm::vec2(500));
    }

    SECTION("will not recurse infinitely")
    {
        std::vector<std::shared_ptr<mode7::Entity<2>>> stacked;
        for (auto i{0}; i < 100; ++i)
        {
            stacked.push_back(std::make_shared<mode7::Entity<2>>());
        }

        for (auto& entity : stacked)
        {
            bucket.insert(entity);
        }
    }

    SECTION("search returns expected results")
    {
        for (auto entity : entities)
        {
            auto inserted{bucket.insert(entity)};
            REQUIRE(inserted >= 0);
        }
        REQUIRE(bucket.getNumInserts() == 100);

        auto err{0};
        std::vector<std::weak_ptr<mode7::Entity<2>>> results;
        err = bucket.search(results, glm::vec2(0, 0), glm::vec2(10, 10));
        REQUIRE(err == 0);
        REQUIRE(results.size() == 19);
    }

    SECTION("search unique returns expected results")
    {
        for (const auto& entity : entities)
        {
            auto inserted{bucket.insert(entity)};
            REQUIRE(inserted >= 0);
        }
        REQUIRE(bucket.getNumInserts() == 100);

        auto err{0};
        std::map<boost::uuids::uuid, std::weak_ptr<mode7::Entity<2>>> results;
        err = bucket.searchUnique(results, glm::vec2(0, 0), glm::vec2(10, 10));
        REQUIRE(err == 0);
        REQUIRE(results.size() == 16);
    }
}

TEST_CASE("NTree", "[NTree]")
{

}
