#include "Loader.hpp"
#include <catch2/catch_all.hpp>
#include "testfiles.hpp"

#include <fstream>
#include <iostream>

TEST_CASE("test loader startswith", "[libmeshes]")
{
    REQUIRE(startswith("test string", "test") == true);
    REQUIRE(startswith("should fail", "fail") == false);
    REQUIRE(startswith("diff len", "different lengths") == false);
}

TEST_CASE("test loader extractVector", "[libmeshes]")
{
    REQUIRE(extractVector<3>("1.000 1.000 1.000", " ") == glm::vec3(1.0));
    REQUIRE(extractVector<4>("1.0,1.00,1.000,1.0000", ",") == glm::vec4(1.0));
    REQUIRE(extractVector<4>("1.0,1.00,1.000,1.0000", ",") == glm::vec4(1.0));
}

TEST_CASE("test loader load .ply", "[libmeshes]")
{
}

TEST_CASE("test loader load .obj", "[libmeshes]")
{
    {
        std::ofstream out(".temporary.obj");
        out << TEST_OBJ_FILE << std::endl;
    }

    auto model{Loader(".temporary.obj").getModel()};

    REQUIRE(model.get() != nullptr);
    REQUIRE(model->getNumVertices() == 4);

    REQUIRE(model->getVertices()[0].position == glm::vec3(-1, 0, 1));
    REQUIRE(model->getVertices()[0].uv == glm::vec2(0, 0));

    for (auto& e : model->getVertices())
    {
        REQUIRE(e.normal == glm::vec3(0, 1, 0));
    }

    remove(".temporary.obj");
}
