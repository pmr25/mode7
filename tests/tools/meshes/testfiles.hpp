#ifndef TESTFILES_HPP
#define TESTFILES_HPP

#include <string>

extern std::string TEST_OBJ_FILE;
extern std::string TEST_PLY_FILE;

#endif /* TESTFILES_HPP */
