#include "Object2D.hpp"
#include "gtest/gtest.h"
#include <iostream>

TEST(TestObject2D, defaults)
{
    mode7::Object2D object;
    EXPECT_EQ(object.getFront().x, 1.0F);
    EXPECT_EQ(object.getFront().y, 0.0F);
}

TEST(TestObject2D, translate)
{
    mode7::Object2D object;
    object.translate({1, 1});
    EXPECT_EQ(object.getPosition().x, 1.0F);
    EXPECT_EQ(object.getPosition().y, 1.0F);
}

TEST(TestObject2D, rotate)
{
    mode7::Object2D object;
    object.rotate(90);
    EXPECT_EQ(object.getFront().x, 1.0F);
    EXPECT_EQ(object.getFront().y, 0.0F);
    object.update();
    EXPECT_LT(object.getFront().x, 1e-4);
    EXPECT_EQ(object.getFront().y, 1.0F);
}

TEST(TestObject2D, addChild)
{
    mode7::Object2D parent;
    mode7::Object2D child;

    parent.translate({2, 0});
    parent.rotate(45);

    child.translate({1, 0});
    child.rotate(90);

    parent.addChild(&child);

    EXPECT_FLOAT_EQ(child.getPosition().x, -1.0F);
    EXPECT_FLOAT_EQ(child.getTheta(), 45.0F);
}