#include "gtest/gtest.h"
#include "AIMetrics.hpp"
#include <iostream>

TEST(TestAIMetrics, dummy)
{
    AIMetrics metrics;
}

TEST(TestAIMetrics, processQuads)
{
    AIMetrics metrics;
    std::vector<Quad> quads;
    Quad top{
        {0, 0},
        {0, 1},
        {1, 1},
        {1, 0}
    };
    top.translate({-0.5, 1.0});
    Quad bottom{
        {0, 0},
        {0, -1},
        {1, -1},
        {1, 0}
    };
    bottom.translate({-0.5, -1.0});
    quads.push_back(top);
    quads.push_back(bottom);

    const auto distances = metrics.processQuads(quads);

    EXPECT_FLOAT_EQ(distances[0], 1.0F);
    EXPECT_FLOAT_EQ(distances[1], sqrtf(2));
    EXPECT_FLOAT_EQ(distances[2], std::numeric_limits<float>::infinity());
    EXPECT_FLOAT_EQ(distances[3], sqrtf(2));
    EXPECT_FLOAT_EQ(distances[4], 1.0F);
}