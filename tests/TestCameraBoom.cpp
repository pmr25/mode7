//#include <catch2/catch_all.hpp>
#include "gtest/gtest.h"
#include "Camera.hpp"
#include "CameraBoom.hpp"
#include "Object.hpp"

#include <memory>

//TEST_CASE("no issues with setup", "[CameraBoomTest]")
TEST(CameraBoomTest, init)
{
    mode7::Object target;
    std::cout << "\t" << sizeof(mode7::CameraBoom) << std::endl;
    mode7::CameraBoom boom;
    std::cout << "\t" << sizeof(boom) << std::endl;
    std::shared_ptr<mode7::Camera> camera{std::make_shared<mode7::Camera>()};

    std::cout << "before set\t" << sizeof(boom) << std::endl;
    boom.setCamera(camera, glm::vec3(0.0F), glm::vec3(0.0F));
    std::cout << "after set\t" << sizeof(boom) << std::endl;

    boom.update();

    //boom.setCamera(camera, glm::vec3(0, 1, 1), glm::vec3(0, 0, 1));

    target.setPosition(glm::vec3(0));
    //target.addChild(&boom);

    EXPECT_EQ(target.getPosition(), glm::vec3(0));
}
