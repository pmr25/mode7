#!/usr/bin/env bash

manjaro_id='ManjaroLinux'
ubuntu_id='Ubuntu'
current_id=$(lsb_release -i)

if [[ $current_id == *$ubuntu_id* ]]; then
  xargs sudo apt install -y <tools/packages-dev.deb.txt
  xargs sudo apt install -y <tools/packages.deb.txt
fi

if [[ $current_id == *$manjaro_id* ]]; then
  xargs sudo pacman --noconfirm -Sy <tools/packages.arch.txt
fi

if [[ "$SKIP_DL" != 1 ]]; then
  tools/scripts/download_assets.sh
fi

#python3 -m pip install -r tools/requirements.txt
