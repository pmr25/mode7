#version 330 core
precision highp float;

// in
in vec2 uv;
in float camParamZ;

// out
out vec4 color;

// uniforms
uniform mat4 p;
uniform sampler2D diffuseMap[16];
uniform sampler2D normalMap[16];
uniform sampler2D bumpMap[16];
uniform float alpha_threshold;

// <fov, near, far, unused>
uniform vec4 camera_params;

// billboard and particle uniforms
uniform vec4 particle_color;

void main()
{
    vec4 texel = texture(diffuseMap[0], uv);
    if (texel.a < alpha_threshold)
    {
        discard;
    }

    float near = camera_params[1];
    float far  = camera_params[2];
    float depth = (-camParamZ - near) / (far - near);

    color = vec4(vec3(depth), 1);
}
