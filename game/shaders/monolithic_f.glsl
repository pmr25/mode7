#version 330 core
precision highp float;

// in
in vec2 uv;
in vec3 norm;
in vec3 binorm;
in vec3 tang;
in vec3 vert;

// out
out vec4 color;

// uniforms
uniform sampler2D diffuseMap[16];
uniform sampler2D normalMap[16];
uniform sampler2D bumpMap[16];
uniform sampler2D specularMap[16];
uniform float alpha_threshold;

// billboard and particle uniforms
uniform vec4 particle_color;

uniform struct Light
{
    vec4 position;

    vec4 color;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;

    vec3 spotDirection;
    float spotExponent;
    float spotCutoff;
    float spotCosCutoff;

    float attenConst;
    float attenLinear;
    float attenQuadratic;
    vec3 attenuation;
} lights[16];

uniform struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 emission;
    vec4 specular;
    float shininess;
    float fresnel;
    float diffuseFloor;
} material;

uniform mat4 v;

uniform uint numLights;

float gamma = 1.0 / 2.2;

vec3 extract_normal()
{
    vec4 texel = texture(normalMap[0], uv);
    vec3 normal;
    
    normal = normalize(texel.rgb * 2.0 - 1.0);
    normal = normalize(mat3(tang, binorm, norm) * normal);
    
    // TODO
    // return normal
    return norm;
}

vec4 sRGB(vec4 rgba)
{
    rgba.rgb = pow(rgba.rgb, vec3(gamma));
    return rgba;
}

vec4 diffuse_color()
{
    vec4 clr = texture(diffuseMap[0], uv);
    // TODO
    // return clr
    return sRGB(clr);
}

void main()
{
    vec4 diffuse = vec4(vec3(0), 1);
    vec4 specular = vec4(vec3(0), 1);
    vec4 ambient = vec4(vec3(0), 1);

    vec3 my_norm = extract_normal();

    for (uint i = 0U; i < numLights; ++i)
    {
        vec3 dir = (v * lights[i].position).xyz - vert * lights[i].position.w;
        vec3 udir = normalize(dir);
        vec3 eye = normalize(-vert);
        vec3 ref = normalize(-reflect(udir, my_norm));
        vec3 hlf = normalize(udir + eye);

        float dist = length(dir);
        float attenuation = 1 / 
            (lights[i].attenConst + lights[i].attenLinear * dist + lights[i].attenQuadratic * dist * dist);

        float diffuseI = dot(my_norm, udir);
        if (diffuseI < material.diffuseFloor)
        {
            continue;
        }
        diffuseI = max(material.diffuseFloor, diffuseI);
        
        vec4 diffuseTex = diffuse_color();
        vec4 diffuseTemp = 
            vec4(
                clamp(diffuseTex.rgb * lights[i].diffuse.rgb * diffuseI, 0, 1), 1);
        
        float fresnelFac = dot(my_norm, eye);
        fresnelFac = max(fresnelFac, 0);
        fresnelFac = 1.0 - fresnelFac;
        fresnelFac = pow(fresnelFac, material.fresnel);
        vec4 matSpec = material.specular;
        matSpec.rgb = 
            mix(matSpec.rgb, vec3(1.0), fresnelFac);
        
        float specularI = max(dot(ref, eye), 0);
        //specularI = dot(norm, hlf); // weird issues
        vec4 specularTemp = 
            clamp(
                  vec4(matSpec.rgb, 1)
                * lights[i].specular * pow(specularI, material.shininess)
            , 0, 1);

        // float unitLightDirDelta = 
        //     dot(normalize(lights[i].spotDirection), udir);
            
        // if (unitLightDirDelta < lights[i].spotCosCutoff)
        // {
        //     continue;
        // }
        
        // diffuseTemp *= pow(unitLightDirDelta, lights[i].spotExponent);
        
        if (lights[i].position.w == 1)
        {
            diffuseTemp.rgb *= attenuation;
            specularTemp.rgb *= attenuation;
        }
        
        diffuse += diffuseTemp;
        if (material.shininess > 0)
        {
            specular += specularTemp;
        }
    }
    
    ambient += material.ambient * diffuse_color();
    // color = ambient + diffuse + specular;
    color = clamp(diffuse, ambient, diffuse_color()) + specular;
    color.a = diffuse_color().a;
    if (diffuse_color().a < alpha_threshold)
    {
        discard;
    }
}

// void main()
// {
//     vec4 texel = texture(diffuseMap[0], uv);

//     color = texel;
    
//     if (color.a < alpha_threshold)
//     {
//         discard;
//     }
// }
