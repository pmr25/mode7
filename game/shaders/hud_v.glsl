#version 330 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 vertex_uv;

uniform vec2 widgetPosition;
uniform float widgetTheta;

out vec2 uv;

void main()
{
    // transfer stuff to fragment shader
    uv = vertex_uv;

    // create rotation matrix
    mat2 rot; // column, row
    rot[0][0] =  cos(widgetTheta);
    rot[1][0] = -sin(widgetTheta);
    rot[0][1] =  sin(widgetTheta);
    rot[1][1] =  cos(widgetTheta);
    vec2 rpos = rot * position + widgetPosition;
    vec4 pos4 = vec4(rpos, 0, 1);

    gl_Position = pos4;
}
