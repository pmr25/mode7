#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

void main()
{
    vec3 s = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 t = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 norm = normalize(cross(s, t));
    vec3 center = (gl_in[0].gl_Position + gl_in[1].gl_Position + gl_in[2].gl_Position).xyz * (1.0 / 3.0);

    gl_Position = center;
    EmitVertex();

    gl_Position = center + norm;
    EmitVertex();

    EndPrimitive();
}