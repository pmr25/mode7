#version 330 core
precision highp float;

// in
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 vertex_uv;
layout(location = 3) in vec3 tangent;

// out
out vec2 uv;
out vec3 norm;
out vec3 binorm;
out vec3 tang;
out vec3 vert;

// uniforms
uniform mat4 p;
uniform mat4 v;
uniform mat4 m;
uniform vec4 uv_transform;
uniform int uv_tile;
uniform vec4 camera_params;

// billboard and particle uniforms
uniform int always_up;
uniform int particle_type;

// post effects
uniform float blur_amt;
uniform vec4 bloom_color;

vec2 calculate_uv()
{
    // uv_transform format: <scale_x, offset_x, scale_y, offset_y>
    return vec2(vertex_uv.x * uv_transform.x + uv_transform.z, vertex_uv.y * uv_transform.y + uv_transform.w);
}

void main()
{
    uv = calculate_uv();
    norm = normal;
    
    if (particle_type > 0)
    {
        vec3 camera_right_worldsp;
        vec3 camera_up_worldsp;
        vec3 vtx;

        camera_right_worldsp = vec3(v[0][0], v[1][0], v[2][0]);
        camera_up_worldsp = vec3(v[0][1], v[1][1], v[2][1]);
        vtx = (camera_right_worldsp * position.x + camera_up_worldsp * position.y);

        gl_Position = p * v * m * vec4(vtx, 1);

        norm = camera_up_worldsp;
    }
    else
    {
        gl_Position = p * v * m * vec4(position, 1);
    }

    vec3 bn = normalize(cross(tangent, normal));
    mat4 modelview = v * m;
    binorm = normalize((modelview * vec4(bn, 0)).xyz);
    norm = normalize((modelview * vec4(norm, 0)).xyz);
    tang = normalize((modelview * vec4(tangent, 0)).xyz);
    vert = (modelview * vec4(position, 1)).xyz;
}

