#version 330 core

centroid in vec2 tc;

out vec4 color;

uniform vec4 fogColor;

uniform sampler2D colorTexture;
uniform sampler2D depthBloomBlurTexture;
uniform sampler2D noAntiAliasTexture;

const float pi_2 = 3.14159264338327 * 2.0;
const float blur_n_dir = 8.0;
const float blur_quality = 2.0;
const float blur_size = 3.0;
const float inv = 1.0 / (blur_quality * blur_n_dir);
const float stp = 1.0 / blur_quality;
const vec2 rad = vec2(blur_size / 1600, blur_size / 900);

vec4 blur(sampler2D tex, vec2 tc, float amt)
{
    float d;
    float i;
    vec4 outc = vec4(0, 0, 0, 1);
    vec2 step1;
    
    for (d = 0.0; d < pi_2; d += pi_2 / blur_n_dir)
    {
        step1 = amt * vec2(cos(d), sin(d)) * rad;
        for (i = stp; i <= 1.0; i += stp)
        {
            outc += texture(tex, step1 * i + tc);
        }
    }
    outc *= inv;

    return outc;
}

void main()
{
    // vec4 fragColor = texture(noAntiAliasTexture, tc);
    vec4 fragColor = texture(colorTexture, tc);
    vec4 dbbMetrics = texture(depthBloomBlurTexture, tc);
    float depth = min(1.0, dbbMetrics.x);
    float bloomAmt = dbbMetrics.z;
    float blurAmt = dbbMetrics.y;
    float unusedAmt = dbbMetrics.w;

    // vec4 blurMix = blur(noAntiAliasTexture, tc, 2.0 * blurAmt);
    vec4 blurMix = blur(colorTexture, tc, 2.0 * blurAmt);
    //vec4 fogMix = mix(fragColor, 0.3 * fragColor + 0.7 * fogColor, sqrt(depth));
    vec4 fogMix = mix(fragColor, 0.1 * fragColor + 0.9 * vec4(0.8, 0.9, 1.0, 1.0), sqrt(depth));

    color = 0.5 * blurMix + 0.5 * fogMix;
}
