/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Mode7Game.hpp"
#include "DirectPipeline.hpp"
#include "GamePipeline.hpp"
#include "HUDManager.hpp"
#include "Logger.hpp"
#include "Screen.hpp"
#include "Settings.hpp"
#include "StateBasedGame.hpp"
#include "Volume.hpp"
#include "sound.hpp"

#include <fstream>

void Mode7Game::createHUD()
{
    mode7::AssetManager& assets = mode7::AssetManager::getCurrent();

    hudManager = std::make_shared<mode7::HUDManager>(
        assets.shaderPath("v2/hud_v.glsl"), assets.shaderPath("v2/hud_f.glsl"));

    perfWidget = std::make_shared<PerformanceWidget>();
    perfWidget->setPosition(0, 0);
    hudManager->addElement(perfWidget);
}

void Mode7Game::loadShaders()
{
    screenShader = std::make_shared<mode7::Shader>();
    screenShader->open(assets->shaderPath("pass_v.glsl"),
                       assets->shaderPath("pass_f.glsl"));
    depthBlurShader = std::make_shared<mode7::Shader>();
    depthBlurShader->open(assets->shaderPath("v2/depthBlur_v.glsl"),
                          assets->shaderPath("v2/depthBlur_f.glsl"));
    monolithicShader = std::make_shared<mode7::Shader>();
    monolithicShader->open(assets->shaderPath("v2/monolithic_v.glsl"),
                           assets->shaderPath("v2/monolithic_f.glsl"));
    postShader = std::make_shared<mode7::Shader>();
    postShader->open(assets->shaderPath("v2/post_v.glsl"),
                     assets->shaderPath("v2/post_f.glsl"));
}

void Mode7Game::loadModels()
{
    LOG(0, LG_DEBUG) << "create and load track";
    track = std::make_shared<Track>();
    track->open(assets->assetPath("models/complex3.dae"), *assets);
    track->getScene()->scale(3.0f);
    track->getScene()->setPositionY(-track->getScene()->getScale().y);
    track->getScene()->update();
    track->setReversed(true);
    track->attachData(assets->assetPath("track_data/complex/complex.obj.td2"));

    Car::loadGlobalProperties(
        assets->assetPath("cars/newcar/global_properties.json"));
    if (Car::loadScript(assets->assetPath("scripts/ai.lua")))
    {
        assert(0);
    }
    if (Car::loadScript(assets->assetPath("scripts/car.lua")))
    {
        assert(0);
    }

    car = std::make_shared<Car>("car");
    car->create(assets->assetPath("cars/newcar/car_properties.json"));
    car->setName("player");

    aicar = std::make_shared<AICar>("aicar");
    aicar->create(assets->assetPath("cars/newcar/car_properties.json"));
    aicar->setName("ai");

    LOG(0, LG_DEBUG) << "[Mode7Game] Setup object relations";

    car->setFollowed(mode7::Camera::getCurrentShPtr());

    LOG(0, LG_DEBUG) << "update models";
    // only need to update once
    track->getScene()->update();
    track->update();

    skybox = mode7::ModelLoader::openShared(assets->modelPath("skybox2.dae"),
                                            mode7::AssetManager::getCurrent());
    skybox->getMesh(0)->scale(1000.0f);
    std::shared_ptr<mode7::Material> flat = std::make_shared<mode7::Material>();
    skybox->getMesh(0)->rotate(glm::vec3(90, 0, 0));
    skybox->getMesh(0)->getMaterial().setFlat(true);

    createHUD();
}

void Mode7Game::init()
{
}

void Mode7Game::onEnter()
{
    LOG(0, LG_DEBUG) << "enter game state";

    Settings& settings = Settings::getCurrent();
    assets = mode7::AssetManager::getCurrentPtr();

    sound::SetMasterVolumeDouble(1.0);
    sound::SetSampleVolumeDouble((double)settings.getGameSfxVolume());
    sound::SetMusicVolumeDouble((double)settings.getMusicVolume());

    camera = mode7::Camera::createAndMakeCurrent((float)settings.getFov(),
                                                 (float)settings.getNear(),
                                                 (float)settings.getFar());

    LOG(0, LG_DEBUG) << "[Mode7Game] Load Shaders";
    loadShaders();

    std::shared_ptr<GamePipeline> pipeline =
        std::make_shared<GamePipeline>(settings);
    mode7::GLScreen::setPipeline(pipeline);

    LOG(0, LG_DEBUG) << "[Mode7Game] Load Models";
    loadModels();

    track->getTrackData()->autoGenerateSectors(10);
    logic.setTrack(track);
    logic.addCar(car);
    logic.addCar(aicar);

    mDirLight = std::make_shared<mode7::Light>();
    mDirLight->position = glm::vec4(1.0f, 1.0f, 1.0f, 0);

    mDirLight2 = std::make_shared<mode7::Light>();
    mDirLight2->position = glm::vec4(0, 1, 0, 0);

    mLights.init();
//    mLights.addLight(mDirLight);
//    mLights.addLight(mDirLight2);
    mLights.cacheUniformLocations(monolithicShader.get());
}

void Mode7Game::onLeave()
{
    LOG(0, LG_DEBUG) << "leave game state";
    logic = GameLogic(); // reset game logic
}

void Mode7Game::update()
{
    sound::Update();

    bool esc = mode7::Keyboard::isDown("escape");
    if (esc || mode7::Keyboard::isDown("q"))
    {
        getManager()->quit();
    }

    if (mode7::Keyboard::isPressed("h"))
    {
        // hot reload
        Car::loadGlobalProperties(
            assets->assetPath("cars/newcar/global_properties.json"));

        car->loadProperties(
            assets->assetPath("cars/newcar/car_properties.json"));
    }

    if (mode7::Keyboard::isPressed(SDL_SCANCODE_G))
    {
        mode7::Mouse::grab();
        godControls.attachCamera();
    }
    else if (mode7::Keyboard::isPressed(SDL_SCANCODE_P))
    {
        mode7::Mouse::release();
        mode7::Camera::setCurrent(camera);
    }

    car->input();

    car->update();
    //    aicar->update();
    track->getScene()->update();

    track->handleWallCollisions(car.get());
    //    track->handleWallCollisions(aicar.get());

    skybox->setPosition(car->getWorldPosition());
    skybox->update();

    car->updateSound();
    //    aicar->updateSound();

    mode7::Camera::getCurrent().updateView();

    godControls.updateControls();

    logic.update();

    hudManager->update();
}

void Mode7Game::drawShader(mode7::Shader* shader)
{
    if (shader != nullptr)
    {
        shader->onlyUse();
        shader->setCameraParams(&mode7::Camera::getCurrent());
        shader->setParticleType(mode7::NONE);
        skybox->draw(shader);
        track->draw(shader);
        car->draw(shader);
        aicar->draw(shader);
    }
    else
    {
        monolithicShader->use();
        mLights.fillUniforms();
        monolithicShader->setCameraParams(&mode7::Camera::getCurrent());
        monolithicShader->setParticleType(mode7::NONE);
        skybox->draw(monolithicShader.get());
        track->draw(monolithicShader.get());
        car->draw(monolithicShader.get());
        aicar->draw(monolithicShader.get());
        hudManager->drawElements();
    }
}

void Mode7Game::destroy()
{
}
