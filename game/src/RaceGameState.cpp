#include "RaceGameState.hpp"
#include "AICar.hpp"
#include "CountdownWidget.hpp"
#include "GamePipeline.hpp"
#include "LapCounterWidget.hpp"
#include "ModelLoader.hpp"
#include "Mouse.hpp"
#include "PerformanceWidget.hpp"
#include "Screen.hpp"
#include "Settings.hpp"
#include "StateBasedGame.hpp"

void RaceGameState::init()
{
}

void RaceGameState::onEnter()
{
    SLOG(LG_DEBUG) << "entering state";

    auto& settings{Settings::getCurrent()};
    auto& assets{mode7::AssetManager::getCurrent()};

    mHudManager = std::make_shared<mode7::HUDManager>(
        assets.shaderPath("v2/hud_v.glsl"), assets.shaderPath("v2/hud_f.glsl"));
    auto perfWidget{std::make_shared<PerformanceWidget>()};
    perfWidget->setPosition(0, 0);
    mHudManager->addElement(perfWidget);

    auto countdownWidget{std::make_shared<CountdownWidget>(256, 256)};
    countdownWidget->setPosition(100, 100);
    mHudManager->addElement(countdownWidget);

    auto lapCountWidget{std::make_shared<LapCounterWidget>(512, 512)};
    lapCountWidget->setPosition(200, 200);
    mHudManager->addElement(lapCountWidget);

    auto pipeline{std::make_shared<GamePipeline>(settings)};
    mode7::GLScreen::setPipeline(pipeline);

    mGameLogic.setHudManager(mHudManager);

    // setup sound
    sound::SetMasterVolumeDouble(1.0);
    sound::SetSampleVolumeDouble((double)settings.getGameSfxVolume());
    sound::SetMusicVolumeDouble((double)settings.getMusicVolume());

    // setup camera
    mCamera = mode7::Camera::createAndMakeCurrent((float)settings.getFov(),
                                                  (float)settings.getNear(),
                                                  (float)settings.getFar());

    Car::loadGlobalProperties(
        assets.assetPath("cars/newcar/global_properties.json"));
    loadShaders();
    loadModels();

    SLOG(LG_DEBUG) << "setup lights";
    // setup lights
    mDirLight = std::make_shared<mode7::Light>();
    mDirLight->position = glm::vec4(1.0F, 1.0F, 1.0F, 0);
    mDirLight2 = std::make_shared<mode7::Light>();
    mDirLight2->position = glm::vec4(0, 1, 0, 0);
    mLights.init();
    mLights.addLight(mDirLight);
    mLights.addLight(mDirLight2);
    mLights.cacheUniformLocations(monolithicShader.get());

    mGameLogic.setAllControls(false);
}

void RaceGameState::onLeave()
{
    SLOG(LG_DEBUG) << "leaving state";
    mGameLogic = GameLogic(); // reset
}

void RaceGameState::update()
{
    int esc = mode7::Keyboard::isDown("escape");
    if (esc || mode7::Keyboard::isDown("q"))
    {
        assert(getManager() != nullptr);
        getManager()->quit();
    }

    sound::Update();

    if (mode7::Keyboard::isPressed(SDL_SCANCODE_G))
    {
        mode7::Mouse::grab();
        mGodControls.attachCamera();
    }
    else if (mode7::Keyboard::isPressed(SDL_SCANCODE_P))
    {
        mode7::Mouse::release();
        mode7::Camera::setCurrent(mCamera);
    }

    for (auto& car : mCars)
    {
        assert(car != nullptr);
        car->input();
        car->update();
        assert(mTrack != nullptr);
        mTrack->handleWallCollisions(car.get());
    }

    for (auto& car : mCars)
    {
        assert(car != nullptr);
        car->updateSound();
    }

    assert(mTrack != nullptr);
    assert(mTrack->getScene() != nullptr);
    mTrack->getScene()->update();

    mGameLogic.update();

    mode7::Camera::getCurrent().updateView();
    mGodControls.updateControls();

    mHudManager->update();
}

void RaceGameState::drawShader(mode7::Shader* shader)
{
    if (shader != nullptr)
    {
        shader->onlyUse();
        shader->setCameraParams(&mode7::Camera::getCurrent());
        shader->setParticleType(mode7::NONE);
        mSkybox->draw(shader);
        mTrack->draw(shader);
        for (auto& car : mCars)
        {
            car->draw(shader);
        }
    }
    else
    {
        monolithicShader->use();
        mLights.fillUniforms();
        monolithicShader->setCameraParams(&mode7::Camera::getCurrent());
        monolithicShader->setParticleType(mode7::NONE);
        mSkybox->draw(monolithicShader.get());
        mTrack->draw(monolithicShader.get());
        for (auto& car : mCars)
        {
            car->draw(monolithicShader.get());
        }
    }
}

void RaceGameState::postRender()
{
    mHudManager->drawElements();
}

void RaceGameState::destroy()
{
}

void RaceGameState::loadShaders()
{
    SLOG(LG_DEBUG) << "loadShaders";

    const auto& assets{mode7::AssetManager::getCurrent()};

    screenShader = std::make_shared<mode7::Shader>();
    screenShader->open(assets.shaderPath("pass_v.glsl"),
                       assets.shaderPath("pass_f.glsl"));
    depthBlurShader = std::make_shared<mode7::Shader>();
    depthBlurShader->open(assets.shaderPath("v2/depthBlur_v.glsl"),
                          assets.shaderPath("v2/depthBlur_f.glsl"));
    monolithicShader = std::make_shared<mode7::Shader>();
    monolithicShader->open(assets.shaderPath("v2/monolithic_v.glsl"),
                           assets.shaderPath("v2/monolithic_f.glsl"));
    postShader = std::make_shared<mode7::Shader>();
    postShader->open(assets.shaderPath("v2/post_v.glsl"),
                     assets.shaderPath("v2/post_f.glsl"));
}

void RaceGameState::loadModels()
{
    SLOG(LG_DEBUG) << "loadModels";

    const auto& assets{mode7::AssetManager::getCurrent()};

    SLOG(LG_DEBUG) << "load track";
    mTrack = std::make_shared<Track>();
    mTrack->open(assets.modelPath(mConfig.trackModelFn), assets);
    mTrack->getScene()->scale(3.0f);
    mTrack->getScene()->setPositionY(-mTrack->getScene()->getScale().y);
    mTrack->getScene()->update();
    mTrack->update();

    SLOG(LG_DEBUG) << "load track data";
    mTrack->setReversed(true);
    mTrack->attachData(
        mode7::AssetManager::getCurrent().assetPath(mConfig.trackDataFn));
    mTrack->getTrackData()->autoGenerateSectors(10);
    for (auto& mesh : mTrack->getScene()->getMeshes())
    {
        mesh->discardMeshData();
    }

    SLOG(LG_DEBUG) << "load skybox";
    mSkybox =
        mode7::ModelLoader::openShared(assets.modelPath("skybox2.dae"), assets);
    mSkybox->getMesh(0)->rotate(90, 0, 0);
    mSkybox->getMesh(0)->getMaterial().setFlat(true);
    mSkybox->scale(900.0f);
    mSkybox->update();

    SLOG(LG_DEBUG) << "load car properties, scripts";
    Car::loadGlobalProperties(
        assets.assetPath("cars/newcar/global_properties.json"));
    if (Car::loadScript(assets.assetPath("scripts/ai.lua")))
    {
        assert(0);
    }
    if (Car::loadScript(assets.assetPath("scripts/car.lua")))
    {
        assert(0);
    }

    SLOG(LG_DEBUG) << "make player cars";
    // temporarily make some cars for testing
    mCars.push_back(makeCar<Car>(
        "player", assets.assetPath("cars/newcar/car_properties.json")));
    SLOG(LG_DEBUG) << "follow player car with camera";
    mCars.back()->setFollowed(
        mode7::Camera::getCurrentShPtr()); // the player car gets the camera

    SLOG(LG_DEBUG) << "make ai cars";
    mCars.push_back(makeCar<AICar>(
        "aicar", assets.assetPath("cars/newcar/car_properties.json")));

    SLOG(LG_DEBUG) << "add track to gamelogic";
    mGameLogic.setTrack(mTrack);

    mGameLogic.addCar(mCars[0], true);
    mGameLogic.addCar(mCars[1]);
    //    for (auto& car : mCars)
    //    {
    //        SLOG(LG_DEBUG) << "add car " << car->getName();
    //        mGameLogic.addCar(car);
    //    }
}
