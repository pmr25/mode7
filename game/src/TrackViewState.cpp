#include "AssetManager.hpp"
#include "TrackViewState.hpp"
#include "DirectPipeline.hpp"
#include "Screen.hpp"
#include "Settings.hpp"
#include "Logger.hpp"
#include "ModelLoader.hpp"
#include "StateBasedGame.hpp"

void TrackViewState::init()
{
}
void TrackViewState::onEnter()
{
    SLOG(LG_DEBUG) << "enter state";

    auto& settings{Settings::getCurrent()};

    mode7::GLScreen::setPipeline(std::make_shared<mode7::DirectPipeline>());
    mode7::Camera::createAndMakeCurrent((float)settings.getFov(),
                                        (float)settings.getNear(),
                                        (float)settings.getFar());
    mControls.attachCamera();

    const auto& assets{mode7::AssetManager::getCurrent()};

    mShader = std::make_shared<mode7::Shader>(
        assets.shaderPath("v2/monolithic_v.glsl"),
        assets.shaderPath("v2/monolithic_f.glsl"));

    const auto err = mTrack.open(assets.modelPath(mConfig.trackModelFn), assets);
    if (err)
    {
        getManager()->setCurrentState("title");
        return;
    }

    mTrack.getScene()->scale(1.0F);
    mTrack.getScene()->update();
    mTrack.update();

    mSkybox =
        mode7::ModelLoader::openShared(assets.modelPath("skybox2.dae"), assets);
    mSkybox->getMesh(0)->rotate(90, 0, 0);
    mSkybox->getMesh(0)->getMaterial().setFlat(true);
    mSkybox->scale(900.0f);
    mSkybox->update();
}
void TrackViewState::onLeave()
{
    SLOG(LG_DEBUG) << "leave state";
}
void TrackViewState::update()
{
    mControls.updateControls();
}
void TrackViewState::drawShader(mode7::Shader* forcedShader)
{
    if (forcedShader != nullptr)
    {
        forcedShader->onlyUse();
        mSkybox->draw(forcedShader);
        mTrack.draw(forcedShader);
    }
    else
    {
        mShader->use();
        mSkybox->draw(mShader.get());
        mTrack.draw(mShader.get());
    }
}
void TrackViewState::destroy()
{
}
void TrackViewState::postRender()
{
    GameState::postRender();
}
