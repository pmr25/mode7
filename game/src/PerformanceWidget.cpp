#include "PerformanceWidget.hpp"
#include "Clock.hpp"
#include "Core.hpp"
#include "FontsBackendPango.hpp"

#ifdef __linux__
#include <sstream>
#endif
#include <string>

void PerformanceWidget::buildWidget(cairo_t* cr)
{
    // draw background
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, 0, 0, getWidth(), getHeight());
    cairo_fill(cr);

    // draw graph bg
    double margin = 4.0;
    cairo_set_source_rgb(cr, 0.1, 0.1, 0.1);
    cairo_rectangle(cr, margin, margin, getWidth() - 2.0 * margin,
                    getHeight() - 2.0 * margin);
    cairo_fill(cr);

    if (mFpsBuffer.size() < 1)
    {
        return;
    }

    // draw graphs
    cairo_set_source_rgb(cr, 0.2, 0.6, 1.0);
    const double end = getWidth() - 1.0 * margin;
    const double dx = (getWidth() - 2.0 * margin) / (double)(mNumSamples - 1);
    const double orY = getHeight() - 2.0 * margin;
    double curX = end;
    double curY = orY;
    cairo_move_to(cr, end, orY); // move to bottom corner
    for (uint32_t e : mFpsBuffer)
    {
        curY = orY - (double)e * 0.5;
        cairo_line_to(cr, curX, curY);
        curX -= dx;
    }
    cairo_line_to(cr, curX + dx, orY); // +dx to correct for last line of loop
    cairo_close_path(cr);
    cairo_fill(cr);

    const std::string prefix = "FPS: ";
    std::stringstream strm;
    strm << prefix << *mFpsBuffer.begin();
    std::string txt = strm.str();

    mode7::Core::getFontsInstance()->drawText(cr, txt, "Sans Regular 18",
                                              margin, margin);
}

void PerformanceWidget::update()
{
    if (mUpdateCtr > 0)
    {
        --mUpdateCtr;
        return;
    }
    mUpdateCtr = mUpdateIval; // reset ctr

    double sample;

    sample = mode7::Clock::fps();
    mFpsBuffer.push_front(sample);
    while (mFpsBuffer.size() > mNumSamples)
    {
        mFpsBuffer.pop_back();
    }

    markDirty();
}
