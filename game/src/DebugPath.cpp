/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifdef _BUILD_DEBUG_TOOLS

#include "DebugPath.hpp"
#include "Util.hpp"

#include <iostream>

void DebugPath::init()
{
    shader.open("assets/shaders/debugpath_v.glsl",
                "assets/shaders/debugpath_f.glsl");
    m_colorUniform = glGetUniformLocation(shader.pid(), "color");
}

void DebugPath::createFromPoints(std::vector<glm::vec2>& points, float y)
{
    m_isReady = true;
    std::vector<glm::vec3> converted;
    // converted.reserve(points.size() * 2);
    for (unsigned long i = 0; i <= points.size(); ++i)
    {
        int ia = (i + 0) % points.size();
        int ib = (i + 1) % points.size();
        glm::vec3 a = glm::vec3(points[ia].x, y, points[ia].y);
        glm::vec3 b = glm::vec3(points[ib].x, y, points[ib].y);
        converted.push_back(a);
        converted.push_back(b);
    }

    auto lambda = [](std::ofstream& os, const glm::vec3& v)
    { os << v[0] << "," << v[2] << ","; };
    mode7::Util::vectorToCSV<glm::vec3>(converted, lambda, "debugpath.csv");

    num_lines = converted.size();

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, converted.size() * 3 * sizeof(float),
                 &converted[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                          (void*)0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void DebugPath::draw()
{
    if (m_isReady)
    {
        shader.use();
        glUniform3fv(m_colorUniform, 1, &m_color[0]);
        shader.setModel(this);

        glBindVertexArray(vao);
        glDrawArrays(GL_LINES, 0, num_lines);
    }
}

#endif /* _BUILD_DEBUG_TOOLS */
