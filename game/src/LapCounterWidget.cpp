#include "LapCounterWidget.hpp"
#include "Clock.hpp"
#include "Core.hpp"
#include "FontsBackendPango.hpp"
#include "Mouse.hpp"
#include "Screen.hpp"

auto LapCounterWidget::buildWidget(cairo_t* ctx) -> void
{
    clearSurface(ctx);

    cairo_save(ctx);

    const auto scale{mode7::GLScreen::getScale()};
    const std::string text{formatLapCount()};

    const double padding{10.0 * scale};
    mode7::Core::getFontsInstance()->drawText(ctx, text, "racing Regular 48",
                                              padding, padding);

    cairo_restore(ctx);
}

auto LapCounterWidget::incrementLap() -> void
{
    ++mCurrentLap;
    markDirty();
}

auto LapCounterWidget::setLap(uint32_t lap) -> void
{
    mCurrentLap = lap;
    markDirty();
}

auto LapCounterWidget::formatLapCount() const -> std::string
{
    return std::to_string(int(fmin(mNumLaps, mCurrentLap))) + "/" +
           std::to_string(mNumLaps);
}

auto LapCounterWidget::setRaceLaps(uint32_t laps) -> void
{
    mNumLaps = laps;
}
