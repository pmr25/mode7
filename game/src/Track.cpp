/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Track.hpp"
#include "AABB.hpp"
#include "Car.hpp"
#include "CodeBenchmark.hpp"
#include "Logger.hpp"
#include "ModelLoader.hpp"
#include "Object.hpp"
#include "Sphere.hpp"
#include "TextureImage.hpp"

#include <cassert>
#include <iostream>
#include <nlohmann/json.hpp>

#define EXTEND_BACK 50
#define EXTEND_FRONT 50

using json = nlohmann::json;

int32_t Track::getStartFinishZone()
{
    return m_data.getStartFinishZone();
}

int Track::open(const std::string& filename, const mode7::AssetManager& assets)
{
    std::string id =
        mode7::CodeBenchmark::getInstance().tick("track.openUnique");
    m_scene = mode7::ModelLoader::openUnique(filename, assets);
    if (m_scene == nullptr)
    {
        SLOG(LG_ERROR) << "cannot open " << filename;
        return 1;
    }
    mode7::CodeBenchmark::getInstance().tock(id);

    id = mode7::CodeBenchmark::getInstance().tick("track.computeBbox");
    for (auto& e : m_scene->getMeshes())
    {
        e->update();

        if (e->getName().rfind("Tree", 0) == 0 ||
            e->getName().rfind("Rock", 0) == 0)
        {
            e->setBoundingVolume(new mode7::Sphere(20.0f));
        }
        if (e->getName().rfind("Track") == 0 || e->getName().rfind("Wall") == 0)
        {
            mode7::AABB* tmp = new mode7::AABB();
            tmp->fitVertices(e->getMeshData()->getVertices());
            e->discardMeshData();
            e->setBoundingVolume(tmp);
            e->getBoundingVolume()->setPosition(tmp->getPosition());
            delete tmp;
            assert(e->getBoundingVolume() != nullptr);
        }
    }
    mode7::CodeBenchmark::getInstance().tock(id);

    return 0;
}

void Track::attachData(const std::string& filename)
{
    LOG(0, LG_INFO) << "loading track data " << filename;

    int err;

    if (mode7::Util::strEndsWith(filename, ".tdat"))
    {
        err = m_data.openTDAT(filename);
        return;
    }
    err = m_data.open(filename);
    //    else
    //    {
    //        LOG(balls, LG_ERROR) << "Unknown file format: " << filename;
    //        return;
    //    }

    if (err)
    {
        std::cout << "[Track] error " << err << ": failed to open " << filename
                  << std::endl;
        return;
    }
    m_data.transform(m_scene->getWorldMatrix());
    m_data.genTrackZones();

    for (auto& e : m_data.getDiscreteWalls())
    {
        assert(e.getSupports().size() >= 3);
        std::shared_ptr<Wall> wall = std::make_shared<Wall>();
        wall->defineThreePoints(e.getSupports()[0], e.getSupports()[1],
                                e.getSupports()[2]);
        wall->setMargin(2.0f);

        m_walls.push_back(wall);
    }

    LOG(0, LG_INFO) << "trackdata summary: " << m_data;

#ifdef _BUILD_DEBUG_TOOLS

    std::cout << "build debug" << std::endl;
    m_centerLineDbg.init();
    m_centerLineDbg.setColor(1.0, 1.0, 1.0);
    m_trackBoundDbg.init();
    m_trackBoundDbg.setColor(0.0, 1.0, 0.0);
    m_runoffBoundDbg.init();
    m_runoffBoundDbg.setColor(1.0, 1.0, 0.0);
    m_wallBoundDbg.init();
    m_wallBoundDbg.setColor(1.0, 0.0, 0.0);
    m_lapBoundDbg.init();
    m_lapBoundDbg.setColor(1.0, 0.0, 1.0);

    std::vector<glm::vec2> pts;

    pts = m_data.getCenterLinePts();
    assert(pts.size() > 0);
    m_centerLineDbg.createFromPoints(pts, 0.1);
    assert(m_centerLineDbg.isReady());

    pts = m_data.getTrackBoundPts();
    assert(pts.size() > 0);
    m_trackBoundDbg.createFromPoints(pts, 0.4);
    assert(m_trackBoundDbg.isReady());

    pts = m_data.getRunoffBoundPts();
    assert(pts.size() > 0);
    m_runoffBoundDbg.createFromPoints(pts, 0.3);
    assert(m_runoffBoundDbg.isReady());

    pts = m_data.getWallBoundPts();
    assert(pts.size() > 0);
    m_wallBoundDbg.createFromPoints(pts, 0.2);
    assert(m_wallBoundDbg.isReady());

    // pts.clear();
    // auto zones = m_data.getLapZones();
    // for (auto& e : zones)
    // {
    //     pts.push_back(e.getWallRect().getPoint(0));
    //     pts.push_back(e.getWallRect().getPoint(1));

    //     pts.push_back(e.getWallRect().getPoint(1));
    //     pts.push_back(e.getWallRect().getPoint(3));

    //     pts.push_back(e.getWallRect().getPoint(2));
    //     pts.push_back(e.getWallRect().getPoint(3));

    //     pts.push_back(e.getWallRect().getPoint(2));
    //     pts.push_back(e.getWallRect().getPoint(0));
    // }
    // m_lapBoundDbg.createFromPoints(pts, 0.05f);
    // assert(m_lapBoundDbg.isReady());

#endif /* _BUILD_DEBUG_TOOLS */
}

void Track::placeCarOnGrid(Car* car, uint32_t gridPosition)
{
    assert(car != nullptr);

    car->setTrack(this);

    const auto centerLines = m_data.getCenterLines();
    assert(!centerLines.empty());
    const float originX = centerLines[0].getPoint().x;
    const float originY = centerLines[0].getPoint().y;
    const float marginX = 6;
    const float marginY = 6;

    const glm::vec3 segmentDir(
        centerLines[0].getEndpoint().x - centerLines[0].getPoint().x, 0.0F,
        centerLines[0].getEndpoint().y - centerLines[0].getPoint().y);
    const glm::vec3 carFront = car->getFront();
    const glm::vec3 segmentNorm(glm::cross(segmentDir, glm::vec3(0, 1, 0)));

    float angle = -acosf(glm::dot(segmentDir, carFront) /
                         (glm::length(segmentDir) * glm::length(carFront)));
    if (m_data.isReversed())
    {
        angle *= -1.0F;
    }
    car->rotate(0.0F, mode7::Util::deg(angle), 0.0F);
    car->update();

    const int side = (gridPosition + 1) % 2;

    car->setPosition(originX, 1.0F, originY);

    const glm::vec3 carOffset =
        marginY * (gridPosition / 2) * glm::normalize(segmentDir) +
        (marginX * float(side == 1) - marginX * float(side == 0)) *
            glm::normalize(segmentNorm);
    car->translate(carOffset);
    car->update();
}

void Track::handleWallCollisions(Car* car)
{
    for (auto& e : m_walls)
    {
        e->handleCollision(car);
    }
}

auto Track::getScene() -> mode7::Scene*
{
    return m_scene.get();
}

void Track::update()
{
#ifdef _BUILD_DEBUG_TOOLS

    m_centerLineDbg.update();
    m_trackBoundDbg.update();
    m_runoffBoundDbg.update();
    m_wallBoundDbg.update();
    m_lapBoundDbg.update();

#endif /* _BUILD_DEBUG_TOOLS */
}

void Track::draw(mode7::Shader* s)
{
    assert(s != nullptr);
    s->setParticleType(mode7::NONE);
    m_scene->draw(s);
}

void Track::drawDebugInfo()
{
#ifdef _BUILD_DEBUG_TOOLS
    glDisable(GL_DEPTH_TEST);
    // m_wallBoundDbg.draw();
    // m_runoffBoundDbg.draw();
    m_trackBoundDbg.draw();
    // m_centerLineDbg.draw();
    // m_lapBoundDbg.draw();
    glEnable(GL_DEPTH_TEST);
#endif /* _BUILD_DEBUG_TOOLS */
}

void Track::destroy()
{
}

int32_t pymod(int32_t n, int32_t m)
{
    return ((n % m) + m) % m;
}

std::vector<std::pair<uint32_t, mode7::TrackZone*>>
Track::getNearbyZones(uint32_t curZone)
{
    std::vector<std::pair<uint32_t, mode7::TrackZone*>> out;

    out.reserve(EXTEND_BACK + EXTEND_FRONT);

    // (b + (a%b)) % b
    // ((-1 % 10) + 10) % 10
    // back = (m_data.getNumZones() + ((int32_t)curZone - d) %
    // m_data.getNumZones()) % m_data.getNumZones();
    const uint32_t back =
        pymod((int32_t)curZone - EXTEND_BACK, m_data.getNumZones());
    const uint32_t front =
        ((int32_t)curZone + EXTEND_FRONT) % m_data.getNumZones();

    //    std::cout << "cur=" << curZone << "\tback=" << back << "\tfront=" <<
    //    front  << "\tnzones=" << m_data.getNumZones() << std::endl; std::cout
    //    << curZone << std::endl;

    uint32_t i = front;
    uint32_t index;
    while (i != back)
    {
        index = i;
        out.push_back(std::pair<uint32_t, mode7::TrackZone*>(
            index, &m_data.getTrackZones()[index]));
        // i = (m_data.getNumZones() + ((int32_t)i - 1) % m_data.getNumZones())
        // % m_data.getNumZones();
        i = pymod((int32_t)i - 1, m_data.getNumZones());
    }

    // this doesn't work on windows for some reason
    // assert(out.size() == d * 2);

    return out;
}

uint32_t Track::getNumZones()
{
    return m_centerline.size();
}

void Track::setReversed(bool rev)
{
    m_data.setReversed(rev);
}

mode7::TrackData* Track::getTrackData()
{
    return &m_data;
}
