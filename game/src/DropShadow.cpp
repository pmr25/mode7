/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "DropShadow.hpp"
#include "Camera.hpp"
#include "Core.hpp"
#include "Logger.hpp"
#include "TextureCache.hpp"
#include "TextureImage.hpp"
#include "gl.hpp"

void DropShadow::create(const std::string& filename)
{
    SLOG(LG_DEBUG) << "load texture " << filename;
    //    mode7::Texture* t =
    //        mode7::TexCache::open(filename, mode7::TexType::DIFFUSE);
    //    assert(t != nullptr);
    mode7::Texture* texture = mode7::Core::getTextureCacheInstance()->open(
        filename, mode7::TexType::DIFFUSE);

    SLOG(LG_DEBUG) << "create material";
    m_material = std::make_shared<mode7::Material>();
    m_material->addMap(texture);

    SLOG(LG_DEBUG) << "generate mesh";
    Mesh::createFromShape(mode7::Mesh::PLANE);

    SLOG(LG_DEBUG) << "transform mesh";
    setScale(1.2f, 0.35f, 1.0f);
    rotate((float)(M_PI / 2.0), 0.0f, 0.0f);
}

void DropShadow::apply(Mesh& parent)
{
    parent.addChild(this);
    setPositionY(-1.0f);
}

void DropShadow::setTracked(bool val)
{
    tracked = val;
}

void DropShadow::update()
{
    Mesh::update();
}

void DropShadow::draw(mode7::Shader* s)
{
    glDisable(GL_DEPTH_TEST);
    // glEnable(GL_BLEND);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE,
    // GL_ONE);

    Mesh::draw(s);

    // glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
}
