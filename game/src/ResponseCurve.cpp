/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "ResponseCurve.hpp"
#include "Util.hpp"
#include <fstream>
#include <iostream>
#include <utility>

void ResponseCurve::open(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in)
    {
        std::cout << "cannot open " << filename << std::endl;
        return;
    }
    std::string line;
    std::vector<std::string> parts;
    int32_t px = 0;
    int32_t py = 0;
    bool first = true;
    Line<2> l;

    while (std::getline(in, line))
    {
        mode7::Util::split(parts, line, ",");

        if (parts.size() < 2)
        {
            std::cout << "bad line" << std::endl;
        }
        else
        {
            int32_t x, y;
            x = atoi(parts[0].c_str());
            y = atoi(parts[1].c_str());
            if (x == px)
            {
                px = x;
                py = y;
                if (first)
                    continue;
            }
            // l.create(px, py, x, y);
            l.connectPoints(glm::vec2(px, py), glm::vec2(x, y));
            m_segments.push_back(l);
            first = false;
            px = x;
            py = y;
        }
    }
}

float ResponseCurve::getY(float x)
{
    Line<2>* l;

    for (int i = m_segments.size() - 1; i >= 0; --i)
    {
        l = &m_segments[i];
        assert(l->isSegment());
        auto p0 = l->getPoint();
        auto p1 = l->getEndpoint();

        // if (x >= (float)p->x)
        if (x >= p0.x && x <= p1.x)
        {
            return l->solveForY(x);
        }
    }

    return 0.0f;
}

float ResponseCurve::getX(float y)
{
    Line<2>* l;

    for (int i = m_segments.size() - 1; i >= 0; --i)
    {
        l = &m_segments[i];
        assert(l->isSegment());
        auto p0 = l->getPoint();
        auto p1 = l->getEndpoint();

        if (y >= p0.y && y <= p1.y)
        {
            return l->solveForX(y);
        }
    }

    return 0.0f;
}
