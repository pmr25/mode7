#include "AICar.hpp"

#include "Line.hpp"
#include <cstdint>
#include <iostream>

void AICar::update()
{
    Line<2> line =
        m_track->getTrackData()->getTrackZones()[m_currentZone].getCenterLine();

    float dir = 1.0f;
    glm::vec2 front2d(Object::getFront().x, Object::getFront().z);
    glm::vec2 right2d(Object::getRight().x, Object::getRight().z);
    glm::vec3 pos = Object::getPosition();
    glm::vec2 pos2d(pos.x, pos.z);
    glm::vec2 diff = dir * line.getDir();
    float dot = glm::dot(front2d, diff);
    float dotRight = glm::dot(right2d, diff);
    // float cross = glm::cross(glm::vec3(front2d.x, 0, front2d.y),
    // glm::vec3(diff.x, 0, diff.y)).y;
    float distToCenterline = line.distTo(pos2d);
    float distToWall = 5.f - fabs(distToCenterline);

    lua_State* L = lua.getLuaState();
    lua_getglobal(L, "Command");
    lua_pushnumber(L, getPosition().x);
    lua_pushnumber(L, getPosition().z);
    lua_pushnumber(L, getVelocity().x);
    lua_pushnumber(L, getVelocity().z);
    lua_pushnumber(L, line.computeNormal().x);
    lua_pushnumber(L, line.computeNormal().y);
    lua_pushnumber(L, fabs(distToCenterline));
    lua_pushnumber(L, copysign(1.0, glm::dot(front2d, line.computeNormal())));
    lua_pushnumber(L, distToWall);
    lua_pushnumber(L, dot);
    lua_pushnumber(L, dotRight);

    lua_call(L, 11, 3);

    int should_accel = (int)lua_tointeger(L, -3);
    int should_brake = (int)lua_tointeger(L, -2);
    int turn_dir = (int)lua_tointeger(L, -1);

    lua_pop(L, 3);

    if (should_accel)
    {
        Car::accelerate();
    }

    if (should_brake)
    {
        Car::brake();
    }

    Car::turn(turn_dir);

    Car::update();
}
