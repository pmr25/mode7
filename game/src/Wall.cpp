#include "Wall.hpp"
#include "Clock.hpp"
#include "Object.hpp"
#include "Util.hpp"

#include <iostream>

void Wall::defineVectorPoint(glm::vec3 n, glm::vec3 p, glm::vec3 up)
{
    mLocalPlane.definePointNormal(p, n, up);
}

void Wall::defineThreePoints(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
    mLocalPlane.definePoints(a, b, c, true);
    m_width = glm::length(b - a);
    m_height = glm::length(c - b);
}

void Wall::setDimensions(float w, float h)
{
    m_width = w;
    m_height = h;
    mLocalPlane.setDimensions(w, h);
}

void Wall::setMargin(float m)
{
    m_margin = m;
}

float Wall::distanceTo(glm::vec3 p)
{
    return mLocalPlane.distanceTo(p);
}

float Wall::distanceFromOrigin(glm::vec3 p)
{
    return mLocalPlane.distanceFromOrigin(p);
}

bool Wall::checkCollision(glm::vec3 p)
{
    return ((fabs(distanceTo(p)) - m_margin) < 0.0f) &&
           (fabs(distanceFromOrigin(p)) < (m_width / 2.0f));
}

bool Wall::checkCollision(glm::vec3 p, glm::vec3 v, float timestep)
{
    return checkCollision(p + (v * timestep));
}

bool Wall::checkIfApproaching(glm::vec3 p, glm::vec3 v)
{
    float dist = distanceTo(p);
    float dot = glm::dot(v, mLocalPlane.getNormal());

    return (dist > 0.0f && dot > 0.0f) || (dist < 0.0f && dot < 0.0f);
}

bool Wall::handleCollision(mode7::Object* obj)
{
    if (glm::length(obj->getVelocity()) < 1e-4)
    {
        return false;
    }

    const float dist = distanceTo(obj->getWorldPosition());
    const float dir = copysignf(1.0f, dist);
    const float distToMargin = dir * (m_margin - fabs(dist));
    glm::vec3 vhat = glm::normalize(obj->getWorldVelocity());
    if (!(glm::length2(vhat) > 0.0f))
    {
        vhat = glm::vec3(0.f);
    }
    const bool collisionDetected =
        checkCollision(obj->getWorldPosition(), obj->getWorldVelocity(),
                       mode7::Clock::getInterval() / 1000.0f);

    if (collisionDetected)
    {
        obj->impulse(-0.01f * obj->getVelocity()); // simulate friction
        obj->translate(distToMargin *
                       mLocalPlane.getNormal()); // move outide of wall
        obj->notifyCollision("wall");
    }

    return collisionDetected;
}

void Wall::update(glm::mat4 m)
{
    mWorldPlane = mLocalPlane;
    mWorldPlane.transform(m);
}
