#include "HelloWorldWidget.hpp"
#include <iostream>

void HelloWorldWidget::buildWidget(cairo_t* cr)
{
    // draw surface
    cairo_set_source_rgba(cr, sin(mTime + M_PI_4), sin(mTime + M_PI_2),
                          sin(mTime + 3 * M_PI_4), 1.0);
    cairo_rectangle(cr, 0, 0, getWidth(), getHeight());
    cairo_fill(cr);
}

void HelloWorldWidget::update()
{
    mTime += 0.01;
    markDirty();
}
