#include "TrackSelectionScreen.hpp"
#include "AssetManager.hpp"
#include "DirectPipeline.hpp"
#include "Logger.hpp"
#include "RaceGameState.hpp"
#include "Screen.hpp"

#include <nlohmann/json.hpp>

#include <fstream>

#include "TrackViewState.hpp"
#include "gsl/assert"

using json = nlohmann::json;

static void pageLeftCallback(void* data)
{
    Expects(data != nullptr);

    TrackSelectionScreen* widget = static_cast<TrackSelectionScreen*>(data);
    Expects(widget != nullptr);

    widget->pageLeft();
}

static void pageRightCallback(void* data)
{
    Expects(data != nullptr);

    TrackSelectionScreen* widget = static_cast<TrackSelectionScreen*>(data);
    Expects(widget != nullptr);

    widget->pageRight();
}

static void trackButtonClickCallback(void* data)
{
    Expects(data != nullptr);
    TrackEntry* entry = static_cast<TrackEntry*>(data);
    Expects(entry != nullptr);

    Expects(entry->data != nullptr);
    TrackSelectionScreen* widget =
        static_cast<TrackSelectionScreen*>(entry->data);
    Expects(widget != nullptr);

    Expects(widget->getManager() != nullptr);
    const auto& raceStateBase{widget->getManager()->getState("race")};
    Expects(raceStateBase != nullptr);
    const auto& raceState{std::static_pointer_cast<RaceGameState>(raceStateBase)};
    Expects(raceState != nullptr);
    raceState->setConfig(RaceConfig(entry->modelFn, entry->dataFn));

    const auto& viewStateBase{widget->getManager()->getState("track_viewer")};
    Expects(viewStateBase != nullptr);
    const auto& viewState{std::static_pointer_cast<TrackViewState>(viewStateBase)};
    viewState->setConfig(RaceConfig(entry->modelFn, entry->dataFn));
}

void cancelCallback(void* data)
{
    Expects(data != nullptr);
    TrackSelectionScreen* widget = static_cast<TrackSelectionScreen*>(data);
    Expects(widget != nullptr);

    widget->getManager()->setCurrentState("title");
}

void startRaceCallback(void* data)
{
    Expects(data != nullptr);
    TrackSelectionScreen* widget = static_cast<TrackSelectionScreen*>(data);
    Expects(widget != nullptr);

    widget->getManager()->setCurrentState("race");
}

void viewTrackCallback(void* data)
{
    Expects(data != nullptr);
    TrackSelectionScreen* widget = static_cast<TrackSelectionScreen*>(data);
    Expects(widget != nullptr);

    widget->getManager()->setCurrentState("track_viewer");
}

void TrackSelectionScreen::loadTrackMetadata(const std::string& fn)
{
    json metadata;

    // input stream scope
    {
        std::ifstream in(fn);
        if (!in)
        {
            LOG(0, LG_ERROR) << "cannot open " << fn;
            //            Expects(getManager());
            //            getManager()->quit();
            return;
        }

        in >> metadata;
    }

    uint32_t counter = 0;
    for (const auto& obj : metadata["tracks"])
    {
        std::shared_ptr<mode7::HUDButtonWidget> trackButton =
            std::make_shared<mode7::HUDButtonWidget>();

        const uint8_t row = counter / numGridCols;
        const uint8_t col = counter % numGridCols;

        const float x =
            mGridOffsetX +
            (float)(col) * (float)(trackButton->getWidth() + gridPadding);
        const float y =
            mGridOffsetY + (float)(row % numGridRows) *
                               -(float)(trackButton->getHeight() + gridPadding);
        trackButton->setPosition(x, y);

        trackButton->setVisible(row < 2);

        const std::string& name = obj["name"];
        trackButton->setText(name);
        mTrackFiles[name] = TrackEntry{counter,
                                       row,
                                       col,
                                       obj["model_file"],
                                       obj["data_file"],
                                       obj["thumbnail_file"],
                                       this};

        trackButton->setCallback(trackButtonClickCallback, &mTrackFiles[name]);

        mTrackButtons.push_back(trackButton);

        ++counter;
    }

    mNumPages = counter / (numGridRows * numGridCols);
}

void TrackSelectionScreen::init()
{
    if (mTrackButtons.empty())
    {
        getManager()->quit();
    }
}

void TrackSelectionScreen::onEnter()
{
    mode7::GLScreen::setPipeline(std::make_shared<mode7::DirectPipeline>());
    mode7::AssetManager& assets = mode7::AssetManager::getCurrent();

    mHudManager = std::make_shared<mode7::HUDManager>(
        assets.shaderPath("v2/hud_v.glsl"), assets.shaderPath("v2/hud_f.glsl"));

    mRaceButton = std::make_shared<mode7::HUDButtonWidget>();
    mRaceButton->setPosition(600, 100);
    mRaceButton->setText("Race!");
    mRaceButton->setCallback(startRaceCallback, this);
    mHudManager->addElement(mRaceButton);

    mBackButton = std::make_shared<mode7::HUDButtonWidget>();
    mBackButton->setPosition(400, 100);
    mBackButton->setText("Back");
    mBackButton->setCallback(cancelCallback, this);
    mHudManager->addElement(mBackButton);

    mPageLeftButton = std::make_shared<mode7::HUDButtonWidget>();
    mPageLeftButton->setText("<");
    mPageLeftButton->setPosition(10, 100);
    mPageLeftButton->setCallback(pageLeftCallback, this);
    mHudManager->addElement(mPageLeftButton);

    mPageRightButton = std::make_shared<mode7::HUDButtonWidget>();
    mPageRightButton->setText(">");
    mPageRightButton->setPosition(800, 100);
    mPageRightButton->setCallback(pageRightCallback, this);
    mHudManager->addElement(mPageRightButton);

    for (auto& button : mTrackButtons)
    {
        mHudManager->addElement(button);
    }

    auto viewButton{std::make_shared<mode7::HUDButtonWidget>()};
    viewButton->setText("View Track");
    viewButton->setPosition(1000, 100);
    viewButton->setCallback(viewTrackCallback, this);
    mHudManager->addElement(viewButton);
}

void TrackSelectionScreen::onLeave()
{
}

void TrackSelectionScreen::update()
{
    Expects(mHudManager != nullptr);
    mHudManager->update();
}

void TrackSelectionScreen::drawShader(mode7::Shader* shader)
{
    if (shader == nullptr) // drawing without forced shader
    {
        mHudManager->drawElements();
    }
}

void TrackSelectionScreen::destroy()
{
}

void TrackSelectionScreen::pageLeft()
{
    const uint8_t nextPage = mCurrentPage - 1U;
    if (nextPage >= mNumPages)
    {
        return;
    }

    Expects(mCurrentPage != 0);

    --mCurrentPage;

    updateVisibility();
}

void TrackSelectionScreen::pageRight()
{
    const uint8_t nextPage = mCurrentPage + 1U;
    if (nextPage >= mNumPages)
    {
        return;
    }

    ++mCurrentPage;

    updateVisibility();
}

void TrackSelectionScreen::updateVisibility()
{
    for (auto& button : mTrackButtons)
    {
        const auto& entry{mTrackFiles[button->getText()]};
        const uint8_t desiredPage = entry.row / 2U;
        button->setVisible(desiredPage == mCurrentPage);
    }
}

std::ostream& operator<<(std::ostream& os, const TrackEntry& entry)
{
    os << "index: " << entry.index << " modelFn: " << entry.modelFn
       << " dataFn: " << entry.dataFn << " thumbnailFn: " << entry.thumbnailFn;
    return os;
}
