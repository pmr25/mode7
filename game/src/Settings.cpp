#include "Settings.hpp"

static Settings current;

Settings& Settings::getCurrent()
{
    return current;
}

bool Settings::getFullscreen() const
{
    return fullscreen;
}

bool Settings::getDpiAware() const
{
    return dpiAware;
}

uint32_t Settings::getWindowWidth() const
{
    return windowWidth;
}

uint32_t Settings::getWindowHeight() const
{
    return windowHeight;
}

uint32_t Settings::getResolutionWidth() const
{
    return resolutionWidth;
}

uint32_t Settings::getResolutionHeight() const
{
    return resolutionHeight;
}

double Settings::getFov() const
{
    return cameraFov;
}

double Settings::getNear() const
{
    return cameraNear;
}

double Settings::getFar() const
{
    return cameraFar;
}

double Settings::getCameraDistance() const
{
    return cameraDistance;
}

double Settings::getCameraHeight() const
{
    return cameraHeight;
}

uint32_t Settings::getMSAASamples() const
{
    return msaaSamples;
}

uint32_t Settings::getNumUpdateWorkers() const
{
    return numUpdateWorkers;
}

bool Settings::getVsyncEnabled() const
{
    return vsyncEnabled;
}

double Settings::getPostResMultiplier() const
{
    return postResMultiplier;
}

float Settings::getMusicVolume() const
{
    return (float)musicVolume / (float)UINT16_MAX;
}

float Settings::getCarSfxVolume() const
{
    return (float)carSfxVolume / (float)UINT16_MAX;
}

float Settings::getGameSfxVolume() const
{
    return (float)gameSfxVolume / (float)UINT16_MAX;
}

float Settings::getSampleVolume(const std::string& name) const
{
    uint16_t val = sampleVolumes.at(name);
    return (float)val / (float)UINT16_MAX;
}
