/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "Car.hpp"
#include "Logger.hpp"
#include "Settings.hpp"

#include <cassert>
#include <cereal/archives/json.hpp>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;
using namespace mode7;

GlobalCarProperties Car::s_globalProperties;
mode7::Scripting Car::lua;

int Car::loadGlobalProperties(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in)
    {
        return 1;
    }

    cereal::JSONInputArchive iarchive(in);
    iarchive(s_globalProperties);

    return 0;
}

int Car::loadScript(const std::string& filename)
{
    return lua.loadScript(filename);
}

int Car::loadProperties(const std::string& filename)
{
    std::ifstream in(filename);
    if (!in)
    {
        SLOG(LG_ERROR) << "[Car] "
                         << "cannot open " << filename;
        return 1;
    }

    cereal::JSONInputArchive iarchive(in);
    iarchive(m_properties);

    mode7::AssetManager assets = mode7::AssetManager::getCurrent();

    m_accelMap.open(
        assets.assetPath("cars/newcar/" + m_properties.ACCEL_MAP_FN));
    m_brakeMap.open(
        assets.assetPath("cars/newcar/" + m_properties.BRAKE_MAP_FN));
    m_driftMap.open(
        assets.assetPath("cars/newcar/" + m_properties.DRIFT_MAP_FN));
    m_turnMap.open(
        assets.assetPath("cars/newcar/" + m_properties.STEER_MAP_FN));

    m_throttleControl.setDownRate(s_globalProperties.THROTTLE_RATE[0]);
    m_throttleControl.setUpRate(s_globalProperties.THROTTLE_RATE[1]);
    m_throttleControl.setAutoUp(true);
    m_brakeControl.setDownRate(s_globalProperties.BRAKE_RATE[0]);
    m_brakeControl.setUpRate(s_globalProperties.BRAKE_RATE[1]);
    m_brakeControl.setAutoUp(true);

    m_wheelControl.setRestoreRates(s_globalProperties.WHEEL_RATE[0],
                                   s_globalProperties.WHEEL_RATE[0],
                                   s_globalProperties.WHEEL_RATE[1]);

    return 0;
}

void Car::setFollowed(std::shared_ptr<mode7::Camera> cam)
{
    m_isFollowed = true;
    Settings& settings = Settings::getCurrent();

    mCameraBoom.setCamera(cam,
                          glm::vec3(0.0F, float(settings.getCameraHeight()),
                                    float(settings.getCameraDistance())),
                          glm::vec3(0, 0, -1));
    mCameraBoom.setCoefficients(1.0F, -1.0F, 1.0F, 20.0F,
                                float(settings.getFov()));
    addChild(&mCameraBoom);
}

int Car::create(const std::string& filename)
{
    SLOG(LG_DEBUG) << "create car " << m_name;

    SLOG(LG_DEBUG) << "load properties";
    loadProperties(filename);

    SLOG(LG_DEBUG) << "load animation";
    m_anim = std::make_shared<mode7::Animation>();
    m_anim->open("animations/test_anim.json",
                 mode7::AssetManager::getCurrent());
    m_anim->setCurrentFrame(0);
    m_sprite.setMaterial(m_anim);

    SLOG(LG_DEBUG) << "create sprite";
    Object::addChild(&m_sprite);
    m_sprite.createFromQuadFile(
        mode7::AssetManager::getCurrent().texturePath("car.png.quad"));
    m_sprite.setPositionY(-0.5f);

    SLOG(LG_DEBUG) << "create shadow";
    m_shadow.create(
        mode7::AssetManager::getCurrent().texturePath("drop_shadow.png"));
    m_shadow.setPositionY(-1.f);
    m_shadow.setPositionZ(-0.25f);
    Object::addChild(&m_shadow);

    m_sprite.scale(1.25f);

    SLOG(LG_DEBUG) << "load sound samples";
    mEngineSample.load(mode7::AssetManager::getCurrent().assetPath(
        "sound/samples/engine.ogg"));
    mEngineSample.play(true);

    mSlideSample.load(
        mode7::AssetManager::getCurrent().assetPath("sound/samples/turn.ogg"));
    mSlideSample.setGain(0.0f);
    mSlideSample.play(true);

    mRunoffSample.load(mode7::AssetManager::getCurrent().assetPath(
        "sound/samples/runoff.ogg"));
    mRunoffSample.setGain(0.0f);
    mRunoffSample.play(true);

    return 0;
}

void Car::updateControls()
{
    if (!m_controlsEnabled)
    {
        return;
    }

    if (m_zoneChangeDetected == 1)
    {
        LOG(0, LG_DEBUG) << m_name << " in zone " << m_currentZone;
        m_zoneChangeDetected = 0;

        if (IS_IN_ZONE(m_zoneType, ZONE_TRACK))
        {
            LOG(0, LG_DEBUG) << "\t" << m_name << " on track";
            // m_currentZone = ZONE_TRACK;
            //  TODO reset all changes
        }
        else if (IS_IN_ZONE(m_zoneType, ZONE_RUNOFF))
        {
            LOG(0, LG_DEBUG) << "\t" << m_name << " on runoff";
            // m_currentZone = ZONE_RUNOFF;
        }
    }

    if (!IS_IN_ZONE(m_zoneType, ZONE_TRACK) &&
        IS_IN_ZONE(m_zoneType, ZONE_RUNOFF))
    {
        // TODO apply more laggy, slippery steering profile
        // TODO apply more drift
        // TODO apply random steering oscillations
        // TODO apply random drift oscillations
        if (glm::length(getVelocity()) > 1e-4)
        {
            // float max_impulse = 0.02f;
            // float desired_impulse = glm::length(0.02f * getVelocity());
            // fflush(stdout);
            // impulse(-fmin(max_impulse, desired_impulse) *
            // glm::normalize(getVelocity()));
        }
    }

    const auto& heightData{
        m_track->getTrackData()->getHeightData()[m_currentZone]};
    const auto& quad{m_track->getTrackData()->getTrackBounds()[m_currentZone]};
    const glm::vec2 proj{getFront().x, getFront().z};
    std::vector<glm::vec3> points;
    const auto numPoints{4};
    points.resize(numPoints);
    float avgY = 0.0F;
    for (int i = 0; i < numPoints; ++i)
    {
        points[i].x = quad.getPoint(i).x;
        points[i].y = heightData.at(i);
        points[i].z = quad.getPoint(i).y;
        avgY += points[i].y * 0.25F;
    }

    //    const auto p1 = getPosition();
    //    const auto p2 = getPosition() + getFront();
    //    const auto h1{mode7::Util::arbitraryQuadBilinear(
    //        getPosition(), points[0], points[1],
    //        points[2], points[3]).y};
    //    const auto h2{mode7::Util::arbitraryQuadBilinear(
    //        getPosition() + 0.1F * getFront(), points[0], points[1],
    //        points[2], points[3]).y};
    //    const auto dx{glm::length(p2 - p1)};
    //    const auto dy{h2 - h1};
    //    const auto incline{atanf(dy/dx) * (180.0F / M_PI)};

    const float incline{mode7::Util::computeIncline(
        getFront(), points[0] - points[1], points[0] - points[2])};

    mAvgIncline.ingest(incline * 3.0F);

    mCameraBoom.setRotationX(-mAvgIncline.compute());

    m_throttleControl.update();
    m_brakeControl.update();
    m_wheelControl.update();
    float steeringPos = m_wheelControl.getPosition() * 0.5f + 0.5f;
    float max_accel = 0.1f;
    float lastZVelocity = getVelocity().z;
    // float lastXVelocity = getVelocity().x;
    assert(m_properties.MAX_SPEED > 0.0f);
    float lastZPercent = fmax(lastZVelocity, 0.0f) / m_properties.MAX_SPEED;

    float turn_amt;
    float drift_amt;
    float brake_amt;

    turn_amt = m_turnMap.calculate(steeringPos,
                                   getVelocity().z / m_properties.MAX_SPEED);

    drift_amt = m_driftMap.calculate(fabs(m_wheelControl.getPosition()),
                                     getVelocity().z / m_properties.MAX_SPEED);

    brake_amt = m_brakeMap.calculate(m_brakeControl.getPosition(),
                                     getVelocity().z / m_properties.MAX_SPEED);

    float accelLimit = 1.0f;
    if (!IS_IN_ZONE(m_zoneType, ZONE_TRACK))
    {
        accelLimit = 0.4f;
    }
    /* Calculate Z Velocity */
    float accel = max_accel *
                  m_accelMap.calculate(0.0f, lastZPercent / accelLimit) // [0,1]
                  * m_throttleControl.getPosition()                     // [0,1]
                  *
                  (1.0f - 0.2f * fabs(m_wheelControl.getPosition())); // [0,0.8]

    float brake = 0.5f * s_globalProperties.BRAKE_RATE[0] * brake_amt;

    float drift_dir =
        1.0f * (steeringPos < 0.50f) - 1.0f * (steeringPos > 0.50f); // -1 or 1

    lua_State* L = lua.getLuaState();
    assert(L != nullptr);

    lua_getglobal(L, "Move"); // function name
    /** call Move(
     * accel,
     * brake,
     * wheelPosition,
     * turnRate,
     * driftAmt,
     * driftDir,
     * vel(x,y,z),
     * maxSpeed,
     * zone
     * ) **/
    lua_pushnumber(L, ZERO_NAN(accel));
    lua_pushnumber(L, ZERO_NAN(brake));
    lua_pushnumber(L, ZERO_NAN(m_wheelControl.getPosition()));
    lua_pushnumber(L, ZERO_NAN(turn_amt + m_turnRateModifier));
    lua_pushnumber(L, ZERO_NAN(drift_amt));
    lua_pushnumber(L, ZERO_NAN(drift_dir));
    lua_pushnumber(L, ZERO_NAN(getVelocity().x));
    lua_pushnumber(L, ZERO_NAN(getVelocity().y));
    lua_pushnumber(L, ZERO_NAN(getVelocity().z));
    lua_pushnumber(L, m_properties.MAX_SPEED + (mAvgIncline.compute() * 0.01F));
    lua_pushnumber(L, m_zoneType);
    int err = lua_pcall(L, 11, 4, 0);
    if (err != LUA_OK)
    {
        SLOG(LG_ERROR) << "lua error: " << lua_tostring(L, -1);
        exit(1);
    }

    /** Move(...) return values **/
    double impulse_x = (double)lua_tonumber(L, -4);
    double impulse_y = (double)lua_tonumber(L, -3);
    double impulse_z = (double)lua_tonumber(L, -2);
    double omega_y = (double)lua_tonumber(L, -1);
    lua_pop(L, 4);

    // apply calculations to car
    impulse(float(impulse_x / accelLimit), float(impulse_y), float(impulse_z));
    rotate(0, float(-omega_y * accelLimit), 0);

    // snap small velocities to zero
    if (getVelocity().x < 1e-2f)
    {
        setVelocityX(0.0);
    }
}

void Car::update()
{
    updateControls();
    updateSprite();

    if (m_isFollowed && mCameraBoom.getCamera())
    {
        const float dive =
            mode7::Util::map(getVelocity().z, 0, m_properties.MAX_SPEED, 0, 1);

        const float slide = m_wheelControl.getPosition();

        mCameraBoom.setDive(dive);
        mCameraBoom.setFov(dive);
        mCameraBoom.applyForce(-slide * dive, 0, -dive);
    }

    Object::move();
    Object::update();
    updateTrackInfo();
    updateEffects();
}

void Car::postUpdate()
{
}

void Car::updateTrackInfo()
{
    if (!m_track)
    {
        LOG(0, LG_WARN) << "car " << getName() << " has no track!";
        return;
    }

    glm::vec2 pos(getPosition().x, getPosition().z);
    auto zones = m_track->getNearbyZones(m_currentZone);
    assert(zones.size() > 0);
    uint8_t lastZoneType = m_zoneType;
    uint8_t detectedZone;

    for (auto& e : zones)
    {
        detectedZone = (ZONE_TRACK * e.second->onTrack(pos)) |
                       (ZONE_RUNOFF * e.second->onRunoff(pos)) |
                       (ZONE_WALL * e.second->onWall(pos));

        if (detectedZone > 0)
        {
            m_zoneType = detectedZone;
            if (lastZoneType != m_zoneType)
            {
                m_zoneChangeDetected = 1;
            }

            m_currentZone = e.first;
            break;
        }

        if (m_zoneType != ZONE_TRACK)
        {
            m_zoneType = ZONE_RUNOFF;
        }
    }
}

void Car::updateEffects()
{
}

void Car::updateSound()
{
    if (m_isFollowed)
    {
        assert(sound::GetCurrentListener() != nullptr);

        sound::GetCurrentListener()->setPosition(getWorldPosition());
        sound::GetCurrentListener()->setVelocity(-getWorldVelocity());
        sound::GetCurrentListener()->setOrientation(-getFront(), getUp());
    }

    mSlideSample.setPosition(getWorldPosition());
    mSlideSample.setVelocity(-getWorldVelocity());
    mSlideSample.setDirection(-getFront());
    const float velprop = getVelocity().z / m_properties.MAX_SPEED;
    const float start = 0.9f;
    const float absctrl = fabsf(m_wheelControl.getPosition());
    const float sg = (absctrl > start && velprop > start) ? velprop : 0.0f;
    Settings& settings = Settings::getCurrent();
    const float maxSlideGain =
        settings.getCarSfxVolume() * settings.getSampleVolume("turn.ogg");
    mSlideSample.setGain(mode7::Util::map(sg, start, 1.0f, 0.0f, maxSlideGain));
    mSlideSample.update();

    const float maxRunGain =
        settings.getCarSfxVolume() * settings.getSampleVolume("runoff.ogg");
    const float rungain =
        ((m_zoneType & ZONE_TRACK) == 0)
            ? mode7::Util::map(velprop, 0.0F, 0.5F, 0.0F, maxRunGain)
            : 0.0f;
    mRunoffSample.setGain(rungain);
    mRunoffSample.setPosition(getWorldPosition());
    mRunoffSample.setVelocity(-getWorldVelocity());
    mRunoffSample.setDirection(-getFront());
    mRunoffSample.update();
    const float maxEngGain =
        settings.getCarSfxVolume() * settings.getSampleVolume("engine.ogg");
    const float minEngGain = maxEngGain * 0.5F;
    mEngineSample.setPosition(getWorldPosition());
    mEngineSample.setVelocity(-getWorldVelocity());
    mEngineSample.setDirection(-getFront());
    mEngineSample.setPitch(mode7::Util::map(velprop, 0.0F, 1.0F, 1.0F, 5.0F));
    mEngineSample.setGain(
        mode7::Util::map(velprop, 0.0F, 1.0F, maxEngGain, minEngGain));
    mEngineSample.update();
}

void Car::updateSprite()
{
    if (m_isFollowed)
    {
        return;
    }

    glm::vec3 cameraFront = Camera::getCurrent().getObject().getFront();
    glm::vec3 myFront = Object::getFront();
    float headingDot = glm::dot(cameraFront, myFront);
    float headingCross = glm::cross(cameraFront, myFront).y;

    lua_State* L = lua.getLuaState();
    assert(L != nullptr);

    lua_getglobal(L, "UpdateSprite");
    lua_pushnumber(L, headingDot);
    lua_pushnumber(L, headingCross);
    lua_call(L, 2, 1);
    auto frame{int(lua_tointeger(L, -1))};
    assert(m_anim != nullptr);
    m_anim->setCurrentFrame(frame);
    lua_pop(L, 1);
}

void Car::draw(Shader* s)
{
    s->setBlurAmount(0.0f);

    s->setParticleType(mode7::NONE);
    s->setAlphaThreshold(0.1f);
    m_shadow.draw(s);

    s->setAlwaysUp(0);
    s->setParticleType(mode7::BILLBOARD);
    s->setAlphaThreshold(0.3f);
    m_sprite.draw(s);
}

void Car::input()
{
    int left = Keyboard::isDown("left") || Keyboard::isDown("j");
    int right = Keyboard::isDown("right") || Keyboard::isDown("l");

    if (Keyboard::isDown("c"))
    {
        accelerate();
    }
    else if (Keyboard::isDown("x"))
    {
        brake();
    }

    int dir = 0;
    if (left)
    {
        dir -= 1;
    }
    if (right)
    {
        dir += 1;
    }

    turn(dir);

    m_anim->setCurrentFrame(2);
    float sc = m_wheelControl.getPosition();
    float thr0 = 0.2f;
    float thr1 = 0.7f;
    if (sc < -thr0)
    {
        m_anim->setCurrentFrame(1);
    }
    if (sc < -thr1 && getVelocity().z > 0.1f)
    {
        m_anim->setCurrentFrame(0);
    }
    if (sc > thr0)
    {
        m_anim->setCurrentFrame(3);
    }
    if (sc > thr1 && getVelocity().z > 0.1f)
    {
        m_anim->setCurrentFrame(4);
    }
}

void Car::accelerate()
{
    m_throttleControl.down();
}

void Car::brake()
{
    m_brakeControl.down();
}

void Car::turn(int32_t dir)
{
    if (dir < 0)
    {
        m_wheelControl.left();
    }
    if (dir > 0)
    {
        m_wheelControl.right();
    }
}

void Car::notifyCollision(const std::string& source)
{
    if (source == "wall")
    {
        if (getVelocity().z > 1.0f)
        {
            impulse(0, 0,
                    -getVelocity().z *
                        1.1f); // TODO debug this, divide-by-zero somewhere
            m_throttleControl.reset();
        }
    }
}
