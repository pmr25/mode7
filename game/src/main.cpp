/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef TESTS

#include "AssetManager.hpp"
#include "CameraBoom.hpp"
#include "Core.hpp"
#include "Mode7Game.hpp"
#include "RaceGameState.hpp"
#include "Settings.hpp"
#include "TitleScreen.hpp"
#include "TrackSelectionScreen.hpp"
#include "TrackViewState.hpp"
#include <memory>

static auto integrationTest() -> void
{
    std::shared_ptr<mode7::Camera> camera{std::make_shared<mode7::Camera>()};
    mode7::Object target;
    mode7::CameraBoom boom;
    boom.setCamera(camera, glm::vec3(0.0F), glm::vec3(0.0F));
    boom.update();
}

#ifdef _WIN32
#ifdef main
#undef main
#endif /* main */
#endif /* _WIN32 */
auto main(int argc, char** argv) -> int
{
    integrationTest();

    mode7::Core::init(argc, argv);

    mode7::StateBasedGame manager;

    // local scope will trigger destructors before end of main
    {

        std::ifstream in(mode7::Util::getInstallDir() +
                         "/../assets/settings.json");
        if (!in)
        {
            LOG(0, LG_ERROR) << "cannot read settings.json";
            exit(1);
        }
        cereal::JSONInputArchive iarchive(in);
        iarchive(Settings::getCurrent());
        Settings settings = Settings::getCurrent();
        mode7::GLScreen::create(
            settings.getWindowWidth(), settings.getWindowHeight(),
            settings.getResolutionWidth(), settings.getResolutionHeight(),
            "mode7", settings.getFullscreen(), settings.getVsyncEnabled(),
            settings.getDpiAware());

        std::shared_ptr<mode7::AssetManager> assets{
            mode7::AssetManager::createAndMakeCurrent()};
        assets->setBaseDirectory(mode7::Util::getInstallDir() + "/../assets");
        assets->setModelDirectory("models");
        assets->setShaderDirectory("shaders");
        assets->setTextureDirectory("textures");

        auto fonts{mode7::Core::getFontsInstance()};
        fonts->loadAllFontsJSON(assets->assetPath("fonts/index.json"));

        std::shared_ptr<Mode7Game> game = std::make_shared<Mode7Game>();
        std::shared_ptr<TitleScreen> title = std::make_shared<TitleScreen>();
        std::shared_ptr<TrackSelectionScreen> trackSelection =
            std::make_shared<TrackSelectionScreen>(
                assets->assetPath("track_data/metadata.json"));
        std::shared_ptr<RaceGameState> race = std::make_shared<RaceGameState>();

        auto trackViewState{std::make_shared<TrackViewState>()};

        manager.addState("game", game);
        manager.addState("title", title);
        manager.addState("track_select", trackSelection);
        manager.addState("race", race);
        manager.addState("track_viewer", trackViewState);
        manager.setCurrentState("title");

        manager.mainLoop(argc, argv);
    }

    return 0;
}

#endif /* TESTS */
