#include "AIMetrics.hpp"

void AIMetrics::createRayCasters()
{
    setName("AI Metrics");

    const int numRayCasters = 5;
    mRayCasters.reserve(numRayCasters);

    const double degStep = 180.0 / double(numRayCasters - 1);

    for (int i = 0; i < numRayCasters; ++i)
    {
        mRayCasters.emplace_back();
        const double theta = -90.0 + (double(i) * degStep);
        mRayCasters.back().setRotation(float(theta));
        mRayCasters.back().setName("Raycaster " + std::to_string(theta));
    }

    for (int i = 0; i < numRayCasters; ++i)
    {
        addChild(&mRayCasters[i]);
    }

    update();
}

auto AIMetrics::gatherMetrics(mode7::TrackData* data, size_t start, size_t end)
    -> std::vector<float>
{
    update();

    std::vector<Quad> quads;
    quads.reserve(end - start + 1);

    for (size_t i = start; i <= end; ++i)
    {
        quads.push_back(data->getTrackZones().at(i).getTrackRect());
    }

    return processQuads(quads);
}
auto AIMetrics::processQuads(const std::vector<Quad>& quads)
    -> std::vector<float>
{
    std::vector<float> metrics(mRayCasters.size(),
                               std::numeric_limits<float>::infinity());
    metrics.resize(mRayCasters.size());

    for (auto& quad : quads)
    {
        processQuad(quad, metrics);
    }

    return metrics;
}
void AIMetrics::processQuad(const Quad& quad, std::vector<float>& metrics)
{
    for (size_t rayIdx = 0U; rayIdx < mRayCasters.size(); ++rayIdx)
    {
        const auto& ray = mRayCasters[rayIdx];
        const float intersect = ray.intersects(quad);
        metrics[rayIdx] = fminf(metrics[rayIdx], intersect);
    }
}
