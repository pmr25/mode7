//
// Created by patrick on 9/19/22.
//

#include "KeyboardInput.hpp"
#include "Keyboard.hpp"
#ifdef _WIN32
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif

void KeyboardInput::poll()
{
    setState(UP, mode7::Keyboard::isDown(SDL_SCANCODE_UP));
    setState(DOWN, mode7::Keyboard::isDown(SDL_SCANCODE_DOWN));
    setState(LEFT, mode7::Keyboard::isDown(SDL_SCANCODE_LEFT));
    setState(RIGHT, mode7::Keyboard::isDown(SDL_SCANCODE_RIGHT));

    setState(ACCEL, mode7::Keyboard::isDown(SDL_SCANCODE_C));
    setState(BRAKE, mode7::Keyboard::isDown(SDL_SCANCODE_X));
    setState(TURN_LEFT, mode7::Keyboard::isDown(SDL_SCANCODE_LEFT));
    setState(TURN_RIGHT, mode7::Keyboard::isDown(SDL_SCANCODE_RIGHT));

    setState(CONFIRM, mode7::Keyboard::isDown(SDL_SCANCODE_C));
    setState(CANCEL, mode7::Keyboard::isDown(SDL_SCANCODE_X));
}
