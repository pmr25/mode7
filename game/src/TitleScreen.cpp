#include "TitleScreen.hpp"
#include "AssetManager.hpp"
#include "DirectPipeline.hpp"
#include "Screen.hpp"

#include <cassert>
#include <iostream>

static void startButtonCB(void* data)
{
    assert(data != nullptr);
    TitleScreen* self = (TitleScreen*)data;

    self->getManager()->setCurrentState("track_select");
}

static void closeButtonCB(void* data)
{
    assert(data != nullptr);
    TitleScreen* self = (TitleScreen*)data;

    self->getManager()->quit();
}

void TitleScreen::init()
{
}

void TitleScreen::onEnter()
{
    mode7::GLScreen::setPipeline(std::make_shared<mode7::DirectPipeline>());

    mode7::AssetManager& assets = mode7::AssetManager::getCurrent();

    mHudManager = std::make_shared<mode7::HUDManager>(
        assets.shaderPath("v2/hud_v.glsl"), assets.shaderPath("v2/hud_f.glsl"));

    float xpos = 200;
    float ypos = 200;

    mStartButton = std::make_shared<mode7::HUDButtonWidget>();
    mStartButton->setPosition(xpos, ypos);
    mStartButton->setCallback(startButtonCB, (void*)this);
    mStartButton->setText("Start");
    mHudManager->addElement(mStartButton);

    mCloseButton = std::make_shared<mode7::HUDButtonWidget>();
    mCloseButton->setPosition(xpos + 600, ypos);
    mCloseButton->setCallback(closeButtonCB, (void*)this);
    mCloseButton->setText("Quit");
    mHudManager->addElement(mCloseButton);
}

void TitleScreen::onLeave()
{
}

void TitleScreen::update()
{
    mHudManager->update();
}

void TitleScreen::drawShader(mode7::Shader* shader)
{
    if (shader == nullptr)
    {
        mHudManager->drawElements();
    }
}

void TitleScreen::destroy()
{
}
