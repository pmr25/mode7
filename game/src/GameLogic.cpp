#include "GameLogic.hpp"
#include "Clock.hpp"
#include "CountdownWidget.hpp"
#include "LapCounterWidget.hpp"

#include <cassert>
#include <iostream>

Car* CarState::getCar()
{
    return m_car.get();
}

void CarState::setLastZone(int32_t z)
{
    m_lastZone = z;
    m_lastSector = m_car->getTrack()->getTrackData()->getSectorOfZone(z);
    assert(m_lastSector > -1);
}

void CarState::update()
{
    const int32_t curZone = m_car->getCurrentZone();
    const int32_t curSector =
        m_car->getTrack()->getTrackData()->getSectorOfZone(curZone);
    assert(curSector > -1);

    const auto& quad{
        m_car->getTrack()->getTrackData()->getTrackBounds()[curZone]};
    const auto& heightData{
        m_car->getTrack()->getTrackData()->getHeightData()[curZone]};

    std::vector<glm::vec3> points;
    points.resize(4);
    for (int i = 0; i < 4; ++i)
    {
        points[i].x = quad.getPoint(i).x;
        points[i].y = heightData.at(i);
        points[i].z = quad.getPoint(i).y;
    }

    auto interpolate{mode7::Util::arbitraryQuadBilinear(
        m_car->getPosition(), points[0], points[1], points[2], points[3])};

    const auto bias{1.0F};
    const auto newY{fmaxf(0.0F, interpolate.y)};
    mHeightAverage.ingest(newY);
    m_car->setPositionY(mHeightAverage.compute() *
                            m_car->getTrack()->getScene()->getScale().y +
                        bias);

    if (curSector != m_lastSector)
    {
        if ((uint32_t)m_lastSector ==
                m_car->getTrack()->getTrackData()->getNumSectors() &&
            curSector == 1)
        {
            if (m_cleanLap)
            {
                mLapFlag = true;
                ++m_laps;
                m_lapTimes.push_back(mode7::Clock::millis());
                if (m_lapTimes.size() > 1)
                {
                    uint32_t lapTime = m_lapTimes[m_lapTimes.size() - 1] -
                                       m_lapTimes[m_lapTimes.size() - 2];
                    uint32_t sec = lapTime / 1000;
                    int32_t dec =
                        (int32_t)(((float)lapTime / 1000.0f - (float)sec) *
                                  1000.0f);
                    uint32_t min = sec / 60;
                    sec = sec % 60;
                    std::cout << "lap " << m_laps - 1 << " time " << min << ":"
                              << sec << "." << dec << std::endl;
                }
            }
            else
            {
                m_cleanLap = true;
            }
        }
        else if (curSector - m_lastSector != 1)
        {
            m_cleanLap = false;
        }
    }

    m_lastSector = curSector;
}
int CarState::getLaps()
{
    return m_laps;
}
bool CarState::lapFlag()
{
    const bool copy{mLapFlag};
    mLapFlag = false;
    return copy;
}

void GameLogic::setTrack(std::shared_ptr<Track> tr)
{
    m_track = tr;
}

void GameLogic::addCar(std::shared_ptr<Car> car, bool hudSource)
{
    m_track->placeCarOnGrid(car.get(), m_cars.size());

    CarState state(car);
    m_cars.push_back(state);
    m_cars.back().setLastZone(car->getCurrentZone());

    if (hudSource)
    {
        mHudSourceIndex = m_cars.size() - 1;
    }
}

void GameLogic::setControl(uint32_t index, bool enabled)
{
    if (index > m_cars.size())
    {
        return;
    }

    Car* car = m_cars[index].getCar();
    car->setControlsEnabled(enabled);
}

void GameLogic::setAllControls(bool enabled)
{
    for (uint32_t i = 0; i < m_cars.size(); ++i)
    {
        setControl(i, enabled);
    }
}

void GameLogic::update()
{
    if (mCountdownTimer.tick())
    {
        if (mHudManager != nullptr)
        {
            auto countdownWidget{std::static_pointer_cast<CountdownWidget>(
                mHudManager->getElementByName("countdown"))};
            countdownWidget->count();
            if (countdownWidget->isDone())
            {
                setAllControls(true);
            }
        }
    }

    auto countWidget{std::static_pointer_cast<LapCounterWidget>(
        mHudManager->getElementByName("lapcount"))};
    for (auto&& it = m_cars.begin(); it != m_cars.end(); it++)
    {
        auto& car = *it;

        if (it - m_cars.begin() == mHudSourceIndex && car.lapFlag())
        {
            countWidget->setLap(car.getLaps());
            countWidget->markDirty();
        }

        car.update();
    }
}

void GameLogic::setHudManager(std::shared_ptr<mode7::HUDManager> ptr)
{
    mHudManager = ptr;
    mode7::Clock::addTrigger(mode7::Clock::millisToTicks(5000),
                             [&] { startCountdown(); });

    auto countWidget{std::static_pointer_cast<LapCounterWidget>(
        mHudManager->getElementByName("lapcount"))};
    countWidget->setLap(1);
    countWidget->setRaceLaps(3);
}

void GameLogic::startCountdown()
{
    mCountdownTimer.create(0, true);
    mCountdownTimer.setIntervalMillis(1000);
    mCountdownTimer.start();
}
