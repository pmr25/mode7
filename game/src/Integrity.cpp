#include "Integrity.hpp"

#include <iomanip>
#include <iostream>
// #include <openssl/evp.h>
// #include <openssl/sha.h>
#include <sstream>
#include <vector>

std::string Integrity::sha3_512(const std::string& input)
{
    (void)input;
    //    uint32_t digestLen = SHA512_DIGEST_LENGTH;
    //    const EVP_MD* algo = EVP_sha3_512();
    //    uint8_t* digest = static_cast<uint8_t*>(OPENSSL_malloc(digestLen));
    //    EVP_MD_CTX* ctx = EVP_MD_CTX_new();
    //    EVP_DigestInit_ex(ctx, algo, nullptr);
    //    EVP_DigestUpdate(ctx, input.c_str(), input.size());
    //    EVP_DigestFinal_ex(ctx, digest, &digestLen);
    //    EVP_MD_CTX_destroy(ctx);
    //
    //    std::string out =
    //        bytesToHexString(std::vector<uint8_t>(digest, digest +
    //        digestLen));
    //    OPENSSL_free(digest);
    //
    //    return out;
    return "";
}

std::string Integrity::bytesToHexString(const std::vector<uint8_t>& input)
{
    std::ostringstream oss;

    for (uint8_t b : input)
    {
        oss << std::setw(2) << std::setfill('0') << std::hex
            << static_cast<int>(b);
    }

    return oss.str();
}
