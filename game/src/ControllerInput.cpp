//
// Created by patrick on 9/19/22.
//

#include "ControllerInput.hpp"

#include <iostream>

void ControllerInput::connect(int32_t index)
{
    mController = SDL_GameControllerOpen(index);
    if (!mController)
    {
        // TODO error
    }
}

void ControllerInput::poll()
{
    if (!mController)
    {
        return;
    }
}

std::vector<std::pair<int32_t, std::string>>
ControllerInput::getConnectedControllers()
{
    std::vector<std::pair<int32_t, std::string>> connectedControllers;
    const int numJoysticks = SDL_NumJoysticks();
    if (numJoysticks < 0)
    {
        // TODO handle error
        std::cout << "error: " << SDL_GetError() << std::endl;
    }
    else
    {
        for (auto i{0}; i < numJoysticks; ++i)
        {
            if (SDL_IsGameController(i))
            {
                connectedControllers.push_back(std::pair<int32_t, std::string>(
                    i, SDL_JoystickNameForIndex(i)));
            }
        }
    }

    return connectedControllers;
}
