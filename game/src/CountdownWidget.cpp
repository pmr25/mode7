#include "CountdownWidget.hpp"
#include "AssetManager.hpp"
#include "Screen.hpp"

#include <cassert>
#include <string>

static auto getFilenames() -> std::vector<std::string>
{
    std::vector<std::string> fns;
    const std::string prefix{"hud/countdown/"};

    fns.push_back(prefix + "ready.png");
    fns.push_back(prefix + "3.png");
    fns.push_back(prefix + "2.png");
    fns.push_back(prefix + "1.png");
    fns.push_back(prefix + "go.png");

    return fns;
}

void CountdownWidget::init()
{
    const auto& assets{mode7::AssetManager::getCurrent()};
    const auto& filenames{getFilenames()};
    const auto scale{mode7::GLScreen::getScale()};
    for (const auto& fn : filenames)
    {
        mCountdownBitmaps.emplace_back(assets.texturePath(fn));
        mCountdownBitmaps.back().shrink(getWidth() * scale,
                                        getHeight() * scale);
    }

    mCurrentSurface = 0;
    markDirty();

    HUDCairoElement::init();
}

void CountdownWidget::buildWidget(cairo_t* cr)
{
    const auto scale{mode7::GLScreen::getScale()};
    cairo_set_source_surface(
        cr, mCountdownBitmaps[mCurrentSurface].createCairoSurface(), 0, 0);
    assert(getWidth() > 0);
    assert(getHeight() > 0);
    cairo_rectangle(cr, 0, 0, getWidth() * scale, getHeight() * scale);
    cairo_fill(cr);
}

void CountdownWidget::update()
{
}

auto CountdownWidget::count() -> void
{

    ++mCurrentSurface;
    if (mCurrentSurface >= mCountdownBitmaps.size())
    {
        HUDElement::setVisible(false);
    }
    else
    {
        markDirty();
    }
}
auto CountdownWidget::isDone() const -> bool
{
    return mCurrentSurface == mCountdownBitmaps.size() - 1;
}

auto CountdownWidget::getCount() const -> size_t
{
    return mCurrentSurface;
}
