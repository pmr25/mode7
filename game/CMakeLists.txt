# This file is part of mode7.
# Copyright (C) 2021  Patrick Roche
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

cmake_minimum_required(VERSION 3.8)

project(game C CXX)

option(COPY_SHADERS "copy shaders" ON)
option(COPY_SCRIPTS "copy lua scripts" ON)

if (${COPY_SHADERS})
    if (WIN32)
        add_custom_target(copy_shaders
                ALL
                DEPENDS copy_assets
                COMMAND (${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tools/scripts/install.py ${CMAKE_SOURCE_DIR}/game/shaders/ ${CMAKE_BINARY_DIR}/assets/shaders/v2 --preserve)
                VERBATIM
                )
    endif ()

    if (UNIX OR MINGW)
        add_custom_target(copy_shaders
                ALL
                DEPENDS copy_assets
                COMMAND ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tools/scripts/install.py ${CMAKE_SOURCE_DIR}/game/shaders/ ${CMAKE_BINARY_DIR}/assets/shaders/v2 --preserve
                VERBATIM
                )
    endif ()
endif ()

if (${COPY_SCRIPTS})
    if (WIN32)
        add_custom_target(copy_scripts
                ALL
                DEPENDS copy_assets
                COMMAND ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tools/scripts/install.py ${CMAKE_SOURCE_DIR}/game/scripts/ ${CMAKE_BINARY_DIR}/assets/scripts/ --preserve
                VERBATIM
                )
    endif ()

    if (UNIX OR MINGW)
        add_custom_target(copy_scripts
                ALL
                DEPENDS copy_assets
                COMMAND ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/tools/scripts/install.py ${CMAKE_SOURCE_DIR}/game/scripts/ ${CMAKE_BINARY_DIR}/assets/scripts/ --preserve
                VERBATIM
                )
    endif ()
endif ()

set(incl ${incl}
        ${CMAKE_CURRENT_SOURCE_DIR}/include
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        ${CMAKE_SOURCE_DIR}/engine/include
        ${CMAKE_SOURCE_DIR}/sound/include
        ${CMAKE_SOURCE_DIR}/engine/src
        ${CMAKE_SOURCE_DIR}/trackdata/include
        ${CMAKE_SOURCE_DIR}/geometry/include
        ${CMAKE_SOURCE_DIR}/3rdparty/AppImageUpdate/include
        )

set(libs
        ${libs}
        engine
        )

set(game_sources
        ${CMAKE_SOURCE_DIR}/game/src/AICar.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Car.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Collisions.cpp
        ${CMAKE_SOURCE_DIR}/game/src/ControlMap.cpp
        ${CMAKE_SOURCE_DIR}/game/src/ControlSlider.cpp
        ${CMAKE_SOURCE_DIR}/game/src/DebugPath.cpp
        ${CMAKE_SOURCE_DIR}/game/src/DropShadow.cpp
        ${CMAKE_SOURCE_DIR}/game/src/GameLogic.cpp
        ${CMAKE_SOURCE_DIR}/game/src/GamePipeline.cpp
        ${CMAKE_SOURCE_DIR}/game/src/HelloWorldWidget.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Integrity.cpp
        ${CMAKE_SOURCE_DIR}/game/src/main.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Mode7Game.cpp
        ${CMAKE_SOURCE_DIR}/game/src/PerformanceWidget.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Properties.cpp
        ${CMAKE_SOURCE_DIR}/game/src/ResponseCurve.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Settings.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Track.cpp
        ${CMAKE_SOURCE_DIR}/game/src/TrackSelectionScreen.cpp
        ${CMAKE_SOURCE_DIR}/game/src/TitleScreen.cpp
        ${CMAKE_SOURCE_DIR}/game/src/Wall.cpp
        ${CMAKE_SOURCE_DIR}/game/src/InputSource.cpp ${CMAKE_SOURCE_DIR}/game/src/KeyboardInput.cpp ${CMAKE_SOURCE_DIR}/game/src/ControllerInput.cpp ${CMAKE_SOURCE_DIR}/game/src/RaceGameState.cpp ${CMAKE_SOURCE_DIR}/game/src/CountdownWidget.cpp ${CMAKE_SOURCE_DIR}/game/src/LapCounterWidget.cpp ${CMAKE_SOURCE_DIR}/game/src/AIMetrics.cpp ${CMAKE_SOURCE_DIR}/game/include/AIMetrics.hpp src/MultiplayerCar.cpp include/MultiplayerCar.hpp src/GameServerConnection.cpp include/GameServerConnection.hpp src/TrackViewState.cpp include/TrackViewState.hpp)

set(game_includes
        ${CMAKE_SOURCE_DIR}/game/include/AICar.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Car.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Collisions.hpp
        ${CMAKE_SOURCE_DIR}/game/include/ControlMap.hpp
        ${CMAKE_SOURCE_DIR}/game/include/ControlSlider.hpp
        ${CMAKE_SOURCE_DIR}/game/include/DebugPath.hpp
        ${CMAKE_SOURCE_DIR}/game/include/DropShadow.hpp
        ${CMAKE_SOURCE_DIR}/game/include/GameLogic.hpp
        ${CMAKE_SOURCE_DIR}/game/include/GamePipeline.hpp
        ${CMAKE_SOURCE_DIR}/game/include/HelloWorldWidget.hpp
        # ${CMAKE_SOURCE_DIR}/game/include/HUD.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Integrity.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Mode7Game.hpp
        ${CMAKE_SOURCE_DIR}/game/include/PerformanceWidget.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Properties.hpp
        ${CMAKE_SOURCE_DIR}/game/include/ResponseCurve.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Settings.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Track.hpp
        ${CMAKE_SOURCE_DIR}/game/include/TrackSelectionScreen.hpp
        # ${CMAKE_SOURCE_DIR}/game/include/Updater.hpp
        ${CMAKE_SOURCE_DIR}/game/include/Wall.hpp
        ${CMAKE_SOURCE_DIR}/game/include/InputSource.hpp ${CMAKE_SOURCE_DIR}/game/include/KeyboardInput.hpp ${CMAKE_SOURCE_DIR}/game/include/ControllerInput.hpp ${CMAKE_SOURCE_DIR}/game/include/RaceGameState.hpp
        ${CMAKE_SOURCE_DIR}/game/include/CountdownWidget.hpp ${CMAKE_SOURCE_DIR}/game/include/LapCounterWidget.hpp
        ${CEREAL_INCL}
        src/MultiplayerCar.cpp include/MultiplayerCar.hpp src/GameServerConnection.cpp include/GameServerConnection.hpp)

add_executable(${PROJECT_NAME} ${game_sources} ${game_includes})
add_dependencies(${PROJECT_NAME} copy_assets copy_scripts copy_shaders)

message(STATUS LUA_LIBS=${LUA_LIBS})

if (${USE_PCH})
    target_precompile_headers(${PROJECT_NAME} PRIVATE ${game_includes})
endif ()
target_compile_options(${PROJECT_NAME} PRIVATE ${CMAKE_CXX_FLAGS} ${flags} -DLOGURU_WITH_STREAMS ${sanitize_flags})
target_include_directories(${PROJECT_NAME} PRIVATE ${incl} ${CMAKE_SOURCE_DIR}/3rdparty/lua
        ${LOGURU_INCL}
        ${CEREAL_INCL}
        ${JSON_INCL}
        )
target_link_directories(${PROJECT_NAME} PRIVATE ${CMAKE_BINARY_DIR}/worker ${PREBUILT_LIBRARY_DIRS})
target_link_libraries(${PROJECT_NAME} PRIVATE ${libs} lua)
target_link_options(${PROJECT_NAME} PRIVATE ${sanitize_flags})

set(game_sources_ext "${game_sources}" CACHE INTERNAL "game_sources_ext")

