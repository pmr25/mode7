-- constants
local envelope = 0.7
local desired_vel = 1.2
local dist_thr = 5
local lost_thr = 30

function Command(
    position_x,
    position_y,
    velocity_x,
    velocity_y,
    centerline_normal_x,
    centerline_normal_y,
    dist_to_centerline,
    side_of_centerline,
    dist_to_wall,
    dot,
    dot_right
)
    -- controls
    local turn_dir = 0
    local accel = 0
    local brake = 0

    -- internal controls
    local skip_dot_ctrl = false
    local skip_env_ctrl = false

    if velocity_x * velocity_x + velocity_y * velocity_y < desired_vel * desired_vel
    then
        accel = 1;
    end

    if dist_to_centerline > lost_thr
    then
        --print("lost :'(")
        --accel = 0
    end

    --print(side_of_centerline, dist_to_centerline)
    if dist_to_centerline > dist_thr
    then
        if side_of_centerline > 0
        then
            turn_dir = turn_dir - 1
        elseif side_of_centerline < 0
        then
            turn_dir = turn_dir + 1
        else
            -- do nothing
        end
        skip_dot_ctrl = true
    end

    --print(dot, dot_right)

    if dot > 0 and not skip_dot_ctrl
    then
        if dot_right < 0
        then
            turn_dir = turn_dir + 1
        elseif dot_right > 0
        then
            turn_dir = turn_dir - 1
        end
    elseif dot < 0
    then
        --print("facing backward")
    end

    if dot < envelope and not skip_env_ctrl
    then
        if dot_right < 0
        then
            turn_dir = turn_dir + 1
        elseif dot_right > 0
        then
            turn_dir = turn_dir - 1
        end
    end

    return accel, brake, turn_dir
end
