-- constants
local BRAKE_RATE = 0
local COAST_RATE = 0.025
local DRIFT_BASE = 0.025
local DRIFT_DECAY = 0.075
local TURN_RATE_MULT = 0
local EPSILON = 1e-4

local FRAME_LOOKUP_TABLE_SZ = 17
local FRAME_LOOKUP = {
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    4,4,4,4,4,4,4,2,2,2,0,0,0,0,0,0,0,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2
};

local function bool_to_int(val)
    return val and 1 or 0
end

local function map(val, l1, h1, l2, h2)
    local frac = (val - l1) / (h1 - l1)
    return (h2 - l2) * frac + l2
end

local function clip(val)
    if math.abs(val) < EPSILON
    then
        return 0
    end

    return val
end

local function sign(val)
    local abs = math.abs(clip(val))
    
    if abs == 0.0
    then
        return 0
    end

    return val / abs
end

function Move(
    accel,
    brake,
    wheel_pos,
    wheel_rate,
    drift,
    drift_dir,
    vel_x,
    vel_y,
    vel_z,
    max_speed,
    zone_type
)
    --print("input", accel, brake, wheel_pos, wheel_rate, drift, drift_dir, vel_x, vel_y, vel_z, max_speed, zone_type)

    -- outputs
    local impulse_x = 0
    local impulse_y = 0
    local impulse_z = 0
    local rotation_rate = 0

    -- internal controls
    local drift_dir = 0
    local drift_impulse = 0
    local drift_decay = 0

    -- impulses
    impulse_z = impulse_z + accel;
    impulse_z = impulse_z - brake;

    if accel <= 0 and brake <= 0
    then
        local decay = sign(vel_z) * math.sqrt(math.abs(vel_z) / max_speed)
        impulse_z = impulse_z - COAST_RATE * decay
    end

    drift_dir = 0 + (1 * bool_to_int(wheel_pos < 0)) - (1 * bool_to_int(wheel_pos > 0));
    drift_impulse = clip(drift_dir * drift * DRIFT_BASE * math.abs(wheel_pos))
    drift_decay = clip(sign(vel_x) * math.abs(vel_x) * DRIFT_DECAY)

    impulse_x = drift_impulse - drift_decay

    rotation_rate = wheel_pos * wheel_rate * 3;

    -- if zone_type & 0x1 == 0
    -- then
    --     rotation_rate = rotation_rate / 2
    --     impulse_z = impulse_z - (impulse_z * 0.9)
    --     impulse_x = impulse_x * 2.00
    -- end

    return impulse_x, impulse_y, impulse_z, rotation_rate
end

function UpdateSprite(dot, cross)
    local dot_segment = map(dot, 1, -1, 0, FRAME_LOOKUP_TABLE_SZ)
    local cross_segment = map(cross, -1, 1, 0, FRAME_LOOKUP_TABLE_SZ)
    local index = math.floor(dot_segment) * FRAME_LOOKUP_TABLE_SZ + math.floor(cross_segment)
    return FRAME_LOOKUP[index + 1]
end
