/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef GAMEHUD_HPP
#define GAMEHUD_HPP

#include "Car.hpp"
#include "Mesh.hpp"
#include "Scene.hpp"
#include "Shader.hpp"

#include <memory>

class HUD
{
public:
    HUD()
        : m_car(nullptr)
        , m_healthBar(nullptr)
    {
    }

    void init();
    void update();
    void draw();

    inline void setCar(Car* c)
    {
        m_car = c;
    }

private:
    Car* m_car;

    mode7::Mesh m_health;
    mode7::Mesh* m_healthBar;
    mode7::Mesh m_gasGauge;
    mode7::Mesh m_gas;
    mode7::Mesh m_brakeGauge;
    mode7::Mesh m_brake;
    mode7::Shader m_shader;
    std::shared_ptr<mode7::Scene> scene;
};

#endif /* GAMEHUD_HPP */
