#ifndef INTEGRITY_HPP
#define INTEGRITY_HPP

#include <cstdint>
#include <string>
#include <vector>

class Integrity
{
public:
    static std::string sha3_512(const std::string&);

private:
    static std::string bytesToHexString(const std::vector<uint8_t>&);
};

#endif /* INTEGRITY_HPP */