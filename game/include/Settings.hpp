#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <cereal/archives/json.hpp>
#include <cereal/types/map.hpp>
#include <cstdint>
#include <string>

class Settings
{
public:
    Settings()
        : fullscreen(false)
        , dpiAware(false)
        , windowWidth(100)
        , windowHeight(100)
        , resolutionWidth(100)
        , resolutionHeight(100)
        , cameraFov(60)
        , cameraNear(1)
        , cameraFar(2)
        , cameraDistance(1)
        , cameraHeight(1)
        , msaaSamples(1)
        , numUpdateWorkers(1)
        , vsyncEnabled(false)
        , postResMultiplier(1)
        , musicVolume(0)
        , carSfxVolume(0)
        , gameSfxVolume(0)
    {
        sampleVolumes["test"] = 69;
    }

    ~Settings() = default;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(fullscreen), CEREAL_NVP(dpiAware),
                CEREAL_NVP(windowWidth), CEREAL_NVP(windowHeight),
                CEREAL_NVP(resolutionWidth), CEREAL_NVP(resolutionHeight),
                CEREAL_NVP(cameraFov), CEREAL_NVP(cameraNear),
                CEREAL_NVP(cameraFar), CEREAL_NVP(cameraDistance),
                CEREAL_NVP(cameraHeight), CEREAL_NVP(msaaSamples),
                CEREAL_NVP(numUpdateWorkers), CEREAL_NVP(vsyncEnabled),
                CEREAL_NVP(postResMultiplier), CEREAL_NVP(musicVolume),
                CEREAL_NVP(carSfxVolume), CEREAL_NVP(gameSfxVolume),
                CEREAL_NVP(sampleVolumes));
    }

    static Settings& getCurrent();

    bool getFullscreen() const;
    bool getDpiAware() const;
    uint32_t getWindowWidth() const;
    uint32_t getWindowHeight() const;
    uint32_t getResolutionWidth() const;
    uint32_t getResolutionHeight() const;
    double getFov() const;
    double getNear() const;
    double getFar() const;
    double getCameraDistance() const;
    double getCameraHeight() const;
    uint32_t getMSAASamples() const;
    uint32_t getNumUpdateWorkers() const;
    bool getVsyncEnabled() const;
    double getPostResMultiplier() const;
    float getMusicVolume() const;
    float getCarSfxVolume() const;
    float getGameSfxVolume() const;
    float getSampleVolume(const std::string&) const;

private:
    bool fullscreen;
    bool dpiAware;
    uint32_t windowWidth;
    uint32_t windowHeight;
    uint32_t resolutionWidth;
    uint32_t resolutionHeight;
    double cameraFov;
    double cameraNear;
    double cameraFar;
    double cameraDistance;
    double cameraHeight;
    uint32_t msaaSamples;
    uint32_t numUpdateWorkers;
    bool vsyncEnabled;
    double postResMultiplier;
    uint16_t musicVolume;
    uint16_t carSfxVolume;
    uint16_t gameSfxVolume;
    std::map<std::string, uint16_t> sampleVolumes;
};

#endif /* SETTINGS_HPP */
