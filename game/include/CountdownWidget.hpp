#ifndef MODE7_COUNTDOWNWIDGET_HPP
#define MODE7_COUNTDOWNWIDGET_HPP

#include "Bitmap.hpp"
#include "HUDCairoElement.hpp"
#include "Timer.hpp"

class CountdownWidget : public mode7::HUDCairoElement
{
public:
    CountdownWidget(uint32_t w, uint32_t h)
        : HUDCairoElement(w, h, "countdown")
    {
    }

    virtual ~CountdownWidget()
    {
    }

    auto count() -> void;
    auto isDone() const -> bool;
    auto getCount() const -> size_t;

    virtual void init();
    virtual void buildWidget(cairo_t*);
    virtual void update();

private:
    uint32_t mCurrentSurface{0};
    std::vector<mode7::Bitmap> mCountdownBitmaps;
};

#endif // MODE7_COUNTDOWNWIDGET_HPP
