#ifndef GAMELOGIC_HPP
#define GAMELOGIC_HPP

#include "Car.hpp"
#include "HUDManager.hpp"
#include "RollingAverage.hpp"
#include "Timer.hpp"
#include "Track.hpp"
#include "Types.hpp"

#include <cstdint>
#include <memory>
#include <vector>

class CarState
{
public:
    CarState(std::shared_ptr<Car> car)
        : m_car(car)
        , mHeightAverage({0.8, 0.05, 0.05, 0.05, 0.05})
    {
    }

    Car* getCar();
    void setLastZone(int32_t);
    void update();
    int getLaps();
    bool lapFlag();

private:
    std::shared_ptr<Car> m_car;
    int m_lastZone{0};
    int m_lastSector{-1};
    int m_laps{0};
    bool mLapFlag{false};
    bool m_cleanLap{true};
    std::vector<uint64_t> m_lapTimes;
    mode7::RollingAverage mHeightAverage;
};

class GameLogic
{
public:
    void setHudManager(std::shared_ptr<mode7::HUDManager>);
    void setTrack(std::shared_ptr<Track>);
    void addCar(std::shared_ptr<Car>, bool hudSource = false);

    void setControl(uint32_t, bool);
    void setAllControls(bool);
    void startCountdown();

    void update();

private:
    mode7::Timer mCountdownTimer;
    std::vector<CarState> m_cars;
    ssize_t mHudSourceIndex{-1};
    std::shared_ptr<Track> m_track;
    std::shared_ptr<mode7::HUDManager> mHudManager;
};

#endif /* GAMELOGIC_HPP */
