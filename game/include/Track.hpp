/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef TRACK_HPP
#define TRACK_HPP

#include "AssetManager.hpp"
#include "DebugPath.hpp"
#include "Line.hpp"
#include "Rect.hpp"
#include "Scene.hpp"
#include "Shader.hpp"
#include "TrackData.hpp"
#include "TrackZone.hpp"
#include "Updatable.hpp"
#include "Wall.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#define ZONE_TRACK 0x01
#define ZONE_RUNOFF 0x02
#define ZONE_WALL 0x04

#define IS_IN_ZONE(x, y) (x & y)

class Car;

class Track : public mode7::Updatable
{
public:
    Track() = default;
    virtual ~Track() = default;

    int open(const std::string& filename, const mode7::AssetManager& assets);
    void attachData(const std::string&);
    mode7::Scene* getScene();
    void placeCarOnGrid(Car*, uint32_t);
    void handleWallCollisions(Car*);
    virtual void update();
    inline virtual void update(const glm::mat4&, const glm::quat&)
    {
        update();
    }
    inline virtual void update(const glm::mat4&, const glm::quat&, bool)
    {
        update();
    }
    void draw(mode7::Shader*);
    void destroy();
    void setReversed(bool);
    void drawDebugInfo();
    uint32_t getNumZones();
    int32_t getStartFinishZone();
    std::vector<std::pair<uint32_t, mode7::TrackZone*>>
        getNearbyZones(uint32_t);
    mode7::TrackData* getTrackData();

private:
    std::vector<Line<2>> m_centerline;
    std::vector<mode7::Rect> m_trackBounds;
    std::vector<mode7::Rect> m_runoffBounds;
    std::vector<mode7::Rect> m_wallsBounds;
    std::vector<std::shared_ptr<Wall>> m_walls;
    std::unique_ptr<mode7::Scene> m_scene;
    mode7::TrackData m_data;

#ifdef _BUILD_DEBUG_TOOLS
    DebugPath m_centerLineDbg;
    DebugPath m_trackBoundDbg;
    DebugPath m_runoffBoundDbg;
    DebugPath m_wallBoundDbg;
    DebugPath m_lapBoundDbg;
#endif /* _BUILD_DEBUG_TOOLS */
};

#endif /* TRACK_HPP */
