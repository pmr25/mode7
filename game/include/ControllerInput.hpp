//
// Created by patrick on 9/19/22.
//

#ifndef MODE7_CONTROLLERINPUT_HPP
#define MODE7_CONTROLLERINPUT_HPP

#include "InputSource.hpp"

#ifdef _WIN32
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif 
#include <cstdint>
#include <string>

class ControllerInput : public InputSource
{
public:
    ControllerInput(const int32_t index)
    {
        connect(index);
    }

    virtual ~ControllerInput() = default;

    void connect(int32_t);
    virtual void poll();

    static std::vector<std::pair<int32_t, std::string>>
    getConnectedControllers();

private:
    SDL_GameController* mController;
};

#endif // MODE7_CONTROLLERINPUT_HPP
