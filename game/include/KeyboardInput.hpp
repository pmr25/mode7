//
// Created by patrick on 9/19/22.
//

#ifndef KEYBOARDINPUT_HPP
#define KEYBOARDINPUT_HPP

#include "InputSource.hpp"

class KeyboardInput : public InputSource
{
public:
    KeyboardInput()
        : InputSource()
    {
    }

    virtual ~KeyboardInput() = default;

    virtual void poll();
};

#endif // KEYBOARDINPUT_HPP
