#ifndef TRACKSELECTIONSCREEN_HPP
#define TRACKSELECTIONSCREEN_HPP

#include "GameState.hpp"
#include "HUDButtonWidget.hpp"
#include "HUDManager.hpp"
#include "StateBasedGame.hpp"

#include <cstdint>

#include <memory>
#include <ostream>
#include <string>
#include <vector>

struct TrackEntry
{
    uint32_t index;
    uint8_t row;
    uint8_t col;
    std::string modelFn;
    std::string dataFn;
    std::string thumbnailFn;
    void* data;

    friend std::ostream& operator<<(std::ostream& os, const TrackEntry& entry);
};
std::ostream& operator<<(std::ostream&, const TrackEntry&);

class TrackSelectionScreen : public mode7::GameState
{
public:
    TrackSelectionScreen(const std::string& trackIndexFn)
        : GameState()
        , numGridRows(2)
        , numGridCols(4)
        , gridPadding(10)
        , mCurrentPage(0)
        , mNumPages(0)
        , mGridOffsetX(400.0f)
        , mGridOffsetY(400.0f)
    {
        loadTrackMetadata(trackIndexFn);
    }

    virtual ~TrackSelectionScreen() = default;

    void loadTrackMetadata(const std::string&);

    virtual void init() override;
    virtual void onEnter() override;
    virtual void onLeave() override;
    virtual void update() override;
    virtual void drawShader(mode7::Shader*) override;
    virtual void destroy() override;
    virtual void draw() override
    {
    }

    std::vector<std::shared_ptr<mode7::HUDButtonWidget>>& getTrackButtons()
    {
        return mTrackButtons;
    }

    void pageLeft();
    void pageRight();
    void updateVisibility();

private:
    std::shared_ptr<mode7::HUDManager> mHudManager;
    std::shared_ptr<mode7::HUDButtonWidget> mRaceButton;
    std::shared_ptr<mode7::HUDButtonWidget> mBackButton;
    std::shared_ptr<mode7::HUDButtonWidget> mPageLeftButton;
    std::shared_ptr<mode7::HUDButtonWidget> mPageRightButton;
    std::vector<std::shared_ptr<mode7::HUDButtonWidget>> mTrackButtons;
    std::map<std::string, TrackEntry> mTrackFiles;
    uint8_t numGridRows;
    uint8_t numGridCols;
    uint8_t gridPadding;
    uint8_t mCurrentPage;
    uint8_t mNumPages;
    float mGridOffsetX;
    float mGridOffsetY;
};

#endif /* TRACKSELECTIONSCREEN_HPP */
