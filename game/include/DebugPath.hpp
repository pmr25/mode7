/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifdef _BUILD_DEBUG_TOOLS

#ifndef DEBUGPATH_HPP
#define DEBUGPATH_HPP

#include "Object.hpp"
#include "Shader.hpp"
#include "gl.hpp"
#include <vector>

class DebugPath : public mode7::Object
{
public:
    DebugPath()
        : Object()
        , m_isReady(false)
        , vao(-1)
        , vbo(-1)
        , num_lines(0)
        , m_colorUniform(0)
    {
    }

    virtual ~DebugPath() = default;

    void init();
    void createFromPoints(std::vector<glm::vec2>&, float);
    void draw();

    inline bool isReady()
    {
        return m_isReady;
    }

    inline void setColor(float r, float g, float b)
    {
        m_color = glm::vec3(r, g, b);
    }

private:
    bool m_isReady;
    GLuint vao;
    GLuint vbo;
    GLuint num_lines;
    GLuint m_colorUniform;
    glm::vec3 m_color;
    mode7::Shader shader;
};

#endif /* DEBUGPATH_HPP */

#endif /* _BUILD_DEBUG_TOOLS */