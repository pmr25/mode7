#ifndef AICAR_HPP
#define AICAR_HPP

#include "Car.hpp"

class AICar : public Car
{
public:
    explicit AICar(const std::string& name = "")
        : Car(name)
    {
    }
    ~AICar() override = default;

    void commandCar();
    void update() override;

private:
};

#endif /* AICAR_HPP */
