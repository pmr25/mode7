/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef CAR_HPP
#define CAR_HPP

#include "Animation.hpp"
#include "Billboard.hpp"
#include "Camera.hpp"
#include "CameraBoom.hpp"
#include "ControlMap.hpp"
#include "ControlSlider.hpp"
#include "DropShadow.hpp"
#include "Keyboard.hpp"
#include "Properties.hpp"
#include "RollingAverage.hpp"
#include "Scripting.hpp"
#include "Shader.hpp"
#include "Track.hpp"
#include "gl.hpp"
#include "particles.hpp"
#include "sound.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

class Track;

class Car : public mode7::Object
{
public:
    Car(const std::string& name = "")
        : Object(name)
        , m_velocityModifier(0.0F)
        , mAvgIncline(2)
    {
    }
    virtual ~Car() = default;

    virtual int create(const std::string&);
    virtual void update();
    virtual void postUpdate();
    virtual void draw(mode7::Shader*);
    virtual void notifyCollision(const std::string&);

    int loadProperties(const std::string&);
    void setFollowed(std::shared_ptr<mode7::Camera>);
    void updateSprite();
    void updateControls();
    void updateTrackInfo();
    void updateEffects();
    void updateSound();
    void input();
    void accelerate();
    void brake();
    void turn(int32_t);

    /* STATIC METHODS */
    static int loadGlobalProperties(const std::string&);
    static int loadScript(const std::string&);

    void setTrack(Track* t)
    {
        m_track = t;
    }
    Track* getTrack()
    {
        return m_track;
    }
    void setControlsEnabled(bool val)
    {
        m_controlsEnabled = val;
    }
    int32_t getCurrentZone() const
    {
        return m_currentZone;
    }

protected:
    mode7::Billboard m_sprite;
    std::shared_ptr<mode7::Animation> m_anim;
    mode7::Animation mAnimation;

    Track* m_track;

    CarProperties m_properties;

    DropShadow m_shadow;

    ControlMap m_accelMap;
    ControlMap m_brakeMap;
    ControlMap m_driftMap;
    ControlMap m_turnMap;
    ControlSlider m_throttleControl;
    ControlSlider m_brakeControl;
    ControlSlider m_wheelControl;
    glm::vec3 m_velocityModifier;
    float m_turnRateModifier{0.0F};
    mode7::RollingAverage mAvgIncline;

    int32_t m_currentZone{0};
    uint8_t m_zoneType;
    uint8_t m_zoneChangeDetected;

    bool m_controlsEnabled{true};

    // camera stuff
    bool m_isFollowed{false};
    std::shared_ptr<mode7::Camera> mCamera;
    mode7::CameraBoom mCameraBoom;

    // sound stuff
    sound::Source mEngineSample;
    sound::Source mSlideSample;
    sound::Source mRunoffSample;

    /* STATIC MEMBERS */
    static GlobalCarProperties s_globalProperties;
    static mode7::Scripting lua;

private:
};

#endif /* CAR_HPP */
