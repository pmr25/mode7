#ifndef MODE7_MULTIPLAYERCAR_HPP
#define MODE7_MULTIPLAYERCAR_HPP

#include "Car.hpp"

namespace mode7
{

class MultiplayerCar : public Car
{
public:
    explicit MultiplayerCar(const std::string& name = "")
        : Car(name)
    {
    }
    ~MultiplayerCar() override = default;

    void update() override;
};

} // namespace mode7

#endif // MODE7_MULTIPLAYERCAR_HPP
