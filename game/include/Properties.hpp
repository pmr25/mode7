/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef PROPERTIES_HPP
#define PROPERTIES_HPP

#include <cereal/archives/json.hpp>
#include <string>

class CarProperties
{
public:
    std::string ACCEL_MAP_FN;
    std::string BRAKE_MAP_FN;
    std::string DRIFT_MAP_FN;
    std::string STEER_MAP_FN;
    float DRIFT_BASE;
    float DRIFT_NORM;
    float DRIFT_LOSS;
    float MAX_SPEED;

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(ACCEL_MAP_FN), CEREAL_NVP(BRAKE_MAP_FN),
                CEREAL_NVP(DRIFT_MAP_FN), CEREAL_NVP(STEER_MAP_FN),
                CEREAL_NVP(DRIFT_BASE), CEREAL_NVP(DRIFT_NORM),
                CEREAL_NVP(DRIFT_LOSS), CEREAL_NVP(MAX_SPEED));
    }
};

class GlobalCarProperties
{
public:
    GlobalCarProperties()
        : THROTTLE_RATE{0.f, 0.f}
        , DRIFT_NORM_RATE{0.f, 0.f}
        , DRIFT_LOSS_RATE{0.f, 0.f}
        , COAST_RATE(0.f)
        , BRAKE_RATE{0.f, 0.f}
        , TURN_RATE{0.f, 0.f}
        , WHEEL_RATE{0.f, 0.f}
    {
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(THROTTLE_RATE), CEREAL_NVP(DRIFT_NORM_RATE),
                CEREAL_NVP(DRIFT_LOSS_RATE), CEREAL_NVP(COAST_RATE),
                CEREAL_NVP(BRAKE_RATE), CEREAL_NVP(TURN_RATE),
                CEREAL_NVP(WHEEL_RATE));
    }

    float THROTTLE_RATE[2];
    float DRIFT_NORM_RATE[2];
    float DRIFT_LOSS_RATE[2];
    float COAST_RATE;
    float BRAKE_RATE[2];
    float TURN_RATE[2];
    float WHEEL_RATE[2];
};

#endif /* PROPERTIES_HPP */