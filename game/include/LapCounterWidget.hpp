#ifndef MODE7_LAPCOUNTERWIDGET_HPP
#define MODE7_LAPCOUNTERWIDGET_HPP

#include "HUDCairoElement.hpp"

#ifdef _WIN32
#include <cairo/cairo.h>
#else
#include <cairo.h>
#endif

#include <cstdint>
#include <string>

class LapCounterWidget : public mode7::HUDCairoElement
{
public:
    LapCounterWidget(uint32_t w, uint32_t h)
        : HUDCairoElement(w, h, "lapcount")
    {
    }
    ~LapCounterWidget() = default;

    virtual auto buildWidget(cairo_t*) -> void;
    auto incrementLap() -> void;
    auto setLap(uint32_t) -> void;
    auto setRaceLaps(uint32_t) -> void;

private:
    auto formatLapCount() const -> std::string;

    uint32_t mCurrentLap{0};
    uint32_t mNumLaps{0};
};

#endif // MODE7_LAPCOUNTERWIDGET_HPP
