//
// Created by patrick on 9/19/22.
//

#ifndef INPUTSOURCE_HPP
#define INPUTSOURCE_HPP

#include <vector>

#define NUM_INPUTS 10

enum input_t
{
    UP = 0,
    DOWN,
    LEFT,
    RIGHT,
    CONFIRM,
    CANCEL,
    TURN_LEFT,
    TURN_RIGHT,
    ACCEL,
    BRAKE,
};

class InputSource
{
public:
    InputSource()
    {
        mPressed.resize(NUM_INPUTS);
        mPending.resize(NUM_INPUTS);
    }

    virtual ~InputSource() = default;

    bool isDown(input_t input)
    {
        return mPressed[input];
    }

    bool isClicked(input_t input)
    {
        const bool out = mPending[input] && !mPressed[input];
        mPending[input] = mPressed[input];
        return out;
    }

    virtual void poll() = 0;

protected:
    void setState(input_t input, bool val)
    {
        mPressed[input] = val;
        mPending[input] = val || mPending[input];
    }

private:
    std::vector<bool> mPressed;
    std::vector<bool> mPending;
};

#endif // INPUTSOURCE_HPP
