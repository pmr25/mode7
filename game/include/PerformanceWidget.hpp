#ifndef PERFORMANCEWIDGET_HPP
#define PERFORMANCEWIDGET_HPP

#include "FontsBackendPango.hpp"
#include "HUDCairoElement.hpp"
#include <cstdint>
#include <deque>

class PerformanceWidget : public mode7::HUDCairoElement
{
public:
    PerformanceWidget(uint32_t w = 200, uint32_t h = 120,
                      uint32_t numSamples = 200)
        : HUDCairoElement(w, h)
        , mNumSamples(numSamples)
        , mUpdateIval(5)
        , mUpdateCtr(mUpdateIval)
    {
    }

    virtual ~PerformanceWidget() = default;

    virtual void buildWidget(cairo_t*);
    virtual void update();

private:
    const uint32_t mNumSamples;
    uint32_t mUpdateIval;
    uint32_t mUpdateCtr;
    std::deque<uint32_t> mFpsBuffer;
};

#endif /* PERFORMANCEWIDGET_HPP */
