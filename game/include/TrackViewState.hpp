#ifndef MODE7_TRACKVIEWSTATE_HPP
#define MODE7_TRACKVIEWSTATE_HPP

#include "GameState.hpp"
#include "OrbitControls.hpp"
#include "RaceGameState.hpp"
#include "Shader.hpp"
#include "Track.hpp"

class TrackViewState : public mode7::GameState
{
public:
    void setConfig(RaceConfig cfg)
    {
        mConfig = cfg;
    }
    void init() override;
    void onEnter() override;
    void onLeave() override;
    void update() override;
    void drawShader(mode7::Shader*) override;
    void destroy() override;
    void draw() override
    {
    }
    void postRender() override;

private:
//    mode7::OrbitControls mControls;
    mode7::GodControls mControls;
    Track mTrack;
    std::shared_ptr<mode7::Shader> mShader;
    std::shared_ptr<mode7::Scene> mSkybox;
    RaceConfig mConfig;
};

#endif // MODE7_TRACKVIEWSTATE_HPP
