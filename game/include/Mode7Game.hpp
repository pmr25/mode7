/**
 * This file is part of mode7.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef MODE7GAME_HPP
#define MODE7GAME_HPP

#include <common.hpp>
#include <extras.hpp>
#include <hud.hpp>

#include "AICar.hpp"
#include "Car.hpp"
#include "GameLogic.hpp"
#include "PerformanceWidget.hpp"
#include "Track.hpp"

#include <memory>

class Mode7Game : public mode7::GameState
{
public:
    Mode7Game() = default;
    virtual ~Mode7Game() = default;

    virtual void init() override;
    virtual void onEnter() override;
    virtual void onLeave() override;
    virtual void update() override;
    virtual void drawShader(mode7::Shader*) override;
    virtual void destroy() override;
    virtual void draw() override
    {
    }

    void createHUD();
    void loadShaders();
    void loadModels();

private:
    // engine stuff
    mode7::AssetManager* assets;
    std::shared_ptr<mode7::TextureAtlas> atlas;
    std::shared_ptr<mode7::Shader> screenShader;
    std::shared_ptr<mode7::Shader> depthBlurShader;
    std::shared_ptr<mode7::Shader> monolithicShader;
    std::shared_ptr<mode7::Shader> postShader;

    // game items
    std::shared_ptr<Car> car;
    std::shared_ptr<AICar> aicar;
    std::shared_ptr<Track> track;
    std::shared_ptr<mode7::Camera> camera;
    std::shared_ptr<mode7::Scene> skybox;
    GameLogic logic;

    // debug things
    mode7::GodControls godControls;

    // HUD items
    std::shared_ptr<mode7::HUDManager> hudManager;
    std::shared_ptr<PerformanceWidget> perfWidget;

    // Lighting
    mode7::LightGroup mLights;
    std::shared_ptr<mode7::Light> mDirLight;
    std::shared_ptr<mode7::Light> mDirLight2;
};

#endif /* MODE7GAME_HPP */
