#ifndef WALL_HPP
#define WALL_HPP

#include "Plane.hpp"
#include "gl.hpp"
#include "glmincludes.hpp"

/* Forward Declarations */
#include "engine_forward.hpp"

class Wall
{
public:
    Wall()
        : m_width(0.0f)
        , m_height(0.0f)
        , m_margin(0.0f)
    // centered(false)
    {
        update(glm::mat4(1.0f));
    }

    void defineVectorPoint(glm::vec3, glm::vec3, glm::vec3);
    void defineThreePoints(glm::vec3, glm::vec3, glm::vec3);
    void setDimensions(float, float);
    void setMargin(float);
    float distanceFromOrigin(glm::vec3);
    float distanceTo(glm::vec3);
    bool checkCollision(glm::vec3);
    bool checkCollision(glm::vec3, glm::vec3, float);
    bool checkIfApproaching(glm::vec3, glm::vec3);
    bool handleCollision(mode7::Object*);
    void update(glm::mat4);

private:
    geom::Plane mLocalPlane;
    geom::Plane mWorldPlane;

    float m_width;
    float m_height;
    float m_margin;

    // bool centered;
};

#endif /* WALL_HPP */
