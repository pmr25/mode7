#ifndef GAMEPIPELINE_HPP
#define GAMEPIPELINE_HPP

#include "AssetManager.hpp"
#include "Framebuffer.hpp"
#include "Logger.hpp"
#include "Pipeline.hpp"
#include "Settings.hpp"

#include <iostream>
#include <memory>

class GamePipeline : public mode7::Pipeline
{
public:
    GamePipeline(Settings& settings)
        : Pipeline()
    {
        mode7::AssetManager* assets = mode7::AssetManager::getCurrentPtr();
        if (assets == nullptr)
        {
            SLOG(LG_ERROR) << "no assets are made current";
            exit(1);
        }

        mMultisampleFB = std::make_shared<mode7::Framebuffer>();
        mNoAntiAliasFB = std::make_shared<mode7::Framebuffer>();
        mDepthBloomBlurFB = std::make_shared<mode7::Framebuffer>();
        mOutputFB = std::make_shared<mode7::Framebuffer>();

        mMultisampleFB->targetMultisampledRenderBuffer(
            settings.getResolutionWidth(), settings.getResolutionHeight(),
            settings.getMSAASamples());

        mNoAntiAliasFB->targetTexture(settings.getResolutionWidth(),
                                      settings.getResolutionHeight());

        mDepthBloomBlurFB->targetTexture(settings.getResolutionWidth(),
                                         settings.getResolutionHeight());

        mDepthBloomBlurFB->setForcedShader(std::make_shared<mode7::Shader>(
            assets->shaderPath("v2/depthBlur_v.glsl"),
            assets->shaderPath("v2/depthBlur_f.glsl")));

        mOutputFB->targetTexture(settings.getResolutionWidth(),
                                 settings.getResolutionHeight(), false);

        mOutputFB->setShader(std::make_shared<mode7::Shader>(
            assets->shaderPath("v2/post_v.glsl"),
            assets->shaderPath("v2/post_f.glsl")));
        mOutputFB->appendTextureLocation("depthBloomBlurTexture");
        mOutputFB->appendTextureLocation("noAntiAliasTexture");

        uint32_t msID = addFramebuffer(mMultisampleFB);
        uint32_t naID = addFramebuffer(mNoAntiAliasFB);
        uint32_t dbID = addFramebuffer(mDepthBloomBlurFB);
        uint32_t opID = addFramebuffer(mOutputFB);

        addTarget(msID, opID);
        addTarget(opID, 0); // output to screen
        addInput(opID, dbID);
        addInput(opID, naID);

        addFlow('i', msID);
        addFlow('o', msID);
        addFlow('i', dbID);
        addFlow('i', naID);
        addFlow('o', opID);
    }

    virtual ~GamePipeline() = default;

private:
    std::shared_ptr<mode7::Framebuffer> mMultisampleFB;
    std::shared_ptr<mode7::Framebuffer> mNoAntiAliasFB;
    std::shared_ptr<mode7::Framebuffer> mDepthBloomBlurFB;
    std::shared_ptr<mode7::Framebuffer> mOutputFB;
};

#endif /* GAMEPIPELINE_HPP */
