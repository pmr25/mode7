#ifndef HELLOWORLDWIDGET_HPP
#define HELLOWORLDWIDGET_HPP

#include "HUDCairoElement.hpp"
#ifdef _WIN32
#include <cairo/cairo.h>
#else
#include <cairo/cairo.h>
#endif

class HelloWorldWidget : public mode7::HUDCairoElement
{
public:
    HelloWorldWidget()
        : HUDCairoElement(100, 100)
        , mTime(0.0)
    {
    }

    virtual ~HelloWorldWidget() = default;

    virtual void buildWidget(cairo_t*) override;
    virtual void update() override;

private:
    double mTime;
};

#endif /* HELLOWORLDWIDGET_HPP */
