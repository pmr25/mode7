//
// Created by patrick on 2/24/23.
//

#ifndef MODE7_AIMETRICS_HPP
#define MODE7_AIMETRICS_HPP

#include "Object2D.hpp"
#include "RayCaster2D.hpp"
#include "TrackData.hpp"

#include <vector>

class AIMetrics : public mode7::Object2D
{
public:
    AIMetrics()
    {
        createRayCasters();
    }
    ~AIMetrics() = default;

    void createRayCasters();
    auto gatherMetrics(mode7::TrackData* data, size_t start, size_t end)
        -> std::vector<float>;
    auto processQuads(const std::vector<Quad>& quads) -> std::vector<float>;
    void processQuad(const Quad& quad, std::vector<float>& metrics);

private:

    std::vector<mode7::RayCaster2D> mRayCasters;

    friend std::ostream& operator<<(std::ostream&, const AIMetrics&);
};

inline std::ostream& operator<<(std::ostream& os, const AIMetrics& obj)
{
    os << "AIMetrics";

    for (const auto& ray : obj.mRayCasters)
    {
        os << "\n\t" << ray;
    }

    return os;
}

#endif // MODE7_AIMETRICS_HPP
