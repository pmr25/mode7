#ifndef TITLESCREEN_HPP
#define TITLESCREEN_HPP

#include "GameState.hpp"
#include "HUDButtonWidget.hpp"
#include "HUDManager.hpp"
#include "StateBasedGame.hpp"

#include <memory>

class TitleScreen : public mode7::GameState
{
public:
    TitleScreen()
        : GameState()
    {
    }

    virtual ~TitleScreen() = default;

    virtual void init() override;
    virtual void onEnter() override;
    virtual void onLeave() override;
    virtual void update() override;
    virtual void drawShader(mode7::Shader*) override;
    virtual void destroy() override;
    virtual void draw() override
    {
    }

private:
    std::shared_ptr<mode7::HUDManager> mHudManager;
    std::shared_ptr<mode7::HUDButtonWidget> mStartButton;
    std::shared_ptr<mode7::HUDButtonWidget> mCloseButton;
};

#endif /* TITLESCREEN_HPP */
