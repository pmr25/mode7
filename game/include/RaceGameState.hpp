#ifndef MODE7_RACEGAMESTATE_HPP
#define MODE7_RACEGAMESTATE_HPP

#include "Car.hpp"
#include "GameLogic.hpp"
#include "GameState.hpp"
#include "GodControls.hpp"
#include "Light.hpp"
#include "LightGroup.hpp"

#include <memory>
#include <ostream>
#include <string>
#include <vector>

class RaceConfig
{
public:
    RaceConfig(const std::string& mFn = "", const std::string& tFn = "")
        : trackModelFn(mFn)
        , trackDataFn(tFn)
    {
    }

    std::string trackModelFn;
    std::string trackDataFn;

    friend std::ostream& operator<<(std::ostream& os, const RaceConfig& config)
    {
        os << "trackModelFn: " << config.trackModelFn
           << " trackDataFn: " << config.trackDataFn;
        return os;
    }
};

class RaceGameState : public mode7::GameState
{
public:
    RaceGameState() = default;
    virtual ~RaceGameState() = default;

    void setConfig(RaceConfig cfg)
    {
        mConfig = cfg;
    }

    void init() override;
    void onEnter() override;
    void onLeave() override;
    void update() override;
    void drawShader(mode7::Shader*) override;
    void destroy() override;
    void draw() override
    {
    }
    virtual void postRender() override;

    void loadShaders();
    void loadModels();

    template <class T>
    auto makeCar(const std::string& name,
                               const std::string& config) -> std::shared_ptr<T>
    {
        static_assert(std::is_base_of<Car, T>::value);

        auto car{std::make_shared<T>(name)};
        car->create(config);

        return car;
    }

private:
    RaceConfig mConfig;

    mode7::GodControls mGodControls;

    std::shared_ptr<mode7::Shader> screenShader;
    std::shared_ptr<mode7::Shader> depthBlurShader;
    std::shared_ptr<mode7::Shader> monolithicShader;
    std::shared_ptr<mode7::Shader> postShader;

    std::vector<std::shared_ptr<Car>> mCars;
    std::shared_ptr<mode7::Camera> mCamera;
    std::shared_ptr<mode7::Scene> mSkybox;
    std::shared_ptr<Track> mTrack;
    GameLogic mGameLogic;

    mode7::LightGroup mLights;
    std::shared_ptr<mode7::Light> mDirLight;
    std::shared_ptr<mode7::Light> mDirLight2;

    std::shared_ptr<mode7::HUDManager> mHudManager;
};

#endif // MODE7_RACEGAMESTATE_HPP
