#!/bin/bash

CC=${CMAKE_BINARY_DIR}protoc

JAVA_OUT=${WORKDIR}/../../server/src/main/java
CPP_OUT=${WORKDIR}/../${CPP_OUT}

$CC -I${WORKDIR} --cpp_out=${CPP_OUT} ${WORKDIR}/player.proto
$CC -I${WORKDIR} --cpp_out=${CPP_OUT} ${WORKDIR}/telemetry.proto
$CC -I${WORKDIR} --cpp_out=${CPP_OUT} ${WORKDIR}/uuid.proto

$CC -I${WORKDIR} --java_out=${JAVA_OUT} ${WORKDIR}/player.proto
$CC -I${WORKDIR} --java_out=${JAVA_OUT} ${WORKDIR}/telemetry.proto
$CC -I${WORKDIR} --java_out=${JAVA_OUT} ${WORKDIR}/uuid.proto
