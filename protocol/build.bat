set CC=..\..\..\out\build\x64-Debug\3rdparty\protobuf\protoc.exe

%CC% -I. --cpp_out=generated/protocpp ./player.proto
%CC% -I. --cpp_out=generated/protocpp ./telemetry.proto
%CC% -I. --cpp_out=generated/protocpp ./uuid.proto

%CC% -I. --go_out=generated/golang ./player.proto
%CC% -I. --go_out=generated/golang ./telemetry.proto
%CC% -I. --go_out=generated/golang ./uuid.proto
