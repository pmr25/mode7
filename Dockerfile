FROM ubuntu:22.10 AS base

ARG DEBIAN_FRONTEND=noninteractive


RUN apt update -y
RUN apt -y install findutils dos2unix

COPY tools/packages.deb.txt /packages.txt
RUN dos2unix packages.deb.txt

RUN xargs apt install -y < packages.deb.txt

FROM base AS install-cmake

RUN mkdir dl
WORKDIR dl
RUN wget https://github.com/Kitware/CMake/releases/download/v3.23.0-rc5/cmake-3.23.0-rc5.tar.gz
RUN tar xzvf cmake-3.23.0-rc5.tar.gz
WORKDIR cmake-3.23.0-rc5
RUN mkdir build
WORKDIR build
RUN cmake .. -DCMAKE_BUILD_TYPE=Release
RUN make -j`nproc`
RUN make install
RUN apt remove -y cmake

FROM install-cmake AS install-go

RUN mkdir -p /usr/local/
RUN wget https://go.dev/dl/go1.18.3.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.18.3.linux-amd64.tar.gz
ENV PATH "$PATH:/usr/local/go/bin"

FROM install-go AS build

WORKDIR /

#FROM dependency-stage AS copy-stage

#COPY . /mode7

#FROM copy-stage AS prebuild-stage

#RUN mkdir /mode7/3rdparty/build
#WORKDIR /mode7/3rdparty/build

#RUN cmake ..
#RUN make

#FROM prebuild-stage AS build-stage

#RUN mkdir /mode7/build
#WORKDIR /mode7/build

#RUN cmake .. -DCMAKE_BUILD_TYPE=Release -DCOPY_ASSETS=FALSE
#RUN make game
