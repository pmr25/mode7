/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#include "geometry.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define ANGLE_LIMIT 0.7 // ?? degree angle limit, cos(60 degrees)

static int ntf = 0;

int quad_connect(quad* q, vec2 a, vec2 b, vec2 c, vec2 d)
{
    q->p[0].x = a.x;
    q->p[0].y = a.y;
    q->p[1].x = b.x;
    q->p[1].y = b.y;
    q->p[2].x = c.x;
    q->p[2].y = c.y;
    q->p[3].x = d.x;
    q->p[3].y = d.y;

    int p1;
    int p2;
    for (int i = 0; i < 4; ++i)
    {
        p1 = (i + 0) % 4;
        p2 = (i + 1) % 4;
        vec2* u = q->p + p1;
        vec2* v = q->p + p2;

        line_connect(q->l + i, u->x, u->y, v->x, v->y);
        q->length[i] = line_calc_distance(q->l + i);
    }

    q->area = q->length[0] * q->length[1]; // wrong

    bool int02 = line_intersects(q->l + 0, q->l + 2);
    bool int13 = line_intersects(q->l + 1, q->l + 3);
    // assert(!(int02 && int13));

    int result = 2 * int02 + 1 * int13;
    return result;
}

void quad_print(quad* q)
{
    printf("(%.3f,%.3f)\t(%.3f,%.3f)\n(%.3f,%.3f)\t(%.3f,%.3f)\n", q->p[0].x, q->p[0].y, q->p[1].x, q->p[1].y, q->p[2].x, q->p[2].y,
           q->p[3].x, q->p[3].y);
}

int quad_orient(quad* q, int type)
{
    vec2 fixed1 = q->p[0];
    vec2 swap1;
    vec2 swap2;
    vec2 fixed2;
    int out = 0;

    switch (type)
    {
    case 2:
        swap1 = q->p[1];
        swap2 = q->p[2];
        fixed2 = q->p[3];
        out = quad_connect(q, fixed1, swap2, swap1, fixed2);
        // fixed2 = q->p[1];
        // swap1 = q->p[2];
        // swap2 = q->p[3];
        // out = quad_connect(q, fixed1, fixed2, swap2, swap1);
        break;

    case 1:
        fixed2 = q->p[1];
        swap1 = q->p[2];
        swap2 = q->p[3];
        out = quad_connect(q, fixed1, fixed2, swap2, swap1);
        break;
    }

    return out;
}

int quad_connect_raw(quad* q, float* a, float* b, float* c, float* d)
{
    vec2 va = {a[0], a[1]};
    vec2 vb = {b[0], b[1]};
    vec2 vc = {c[0], c[1]};
    vec2 vd = {d[0], d[1]};
    return quad_connect(q, va, vb, vc, vd);
}

int quad_from_buffer(quad* q, float* buf)
{
    return quad_connect_raw(q, buf, buf + 2, buf + 4, buf + 6);
}

void quad_extend(quad* dest, quad* src, line* cl, float amt)
{
    line* ortho[2];
    vec2 vecs[2];
    vec2* vp = vecs;
    vec2 a;
    vec2 b;
    line** lp = ortho;
    line* cur = NULL;

    memset(ortho, 0, 2 * sizeof(line*));

    // printf("==================== Extend Quad ====================\n");
    // line_print(cl);
    // assert(line_calc_distance(cl) > 0);

    a.x = cl->p2[0] - cl->p1[0];
    a.y = cl->p2[1] - cl->p1[1];
    vec2_normalize(&a);

    for (int i = 0; i < 4; ++i)
    {
        cur = src->l + i;
        assert(cur != NULL);
        // line_print(cur);
        assert(line_calc_distance(cur) > 0);

        vp->x = cur->p2[0] - cur->p1[0];
        vp->y = cur->p2[1] - cur->p1[1];

        vec2_normalize(vp);

        float d = fabs(vec2_dot(&a, vp));
        // printf("dot product: %f\n", d);
        if (d < ANGLE_LIMIT)
        {
            *lp = cur;
            ++lp;
            ++vp;
            if (lp - ortho == 2)
            {
                break;
            }
        }
    }
    assert(lp - ortho == 2);

    ortho[0]->p1[0] -= amt * vecs[0].x;
    ortho[0]->p1[1] -= amt * vecs[0].y;
    ortho[0]->p2[0] += amt * vecs[0].x;
    ortho[0]->p2[1] += amt * vecs[0].y;

    ortho[1]->p1[0] -= amt * vecs[1].x;
    ortho[1]->p1[1] -= amt * vecs[1].y;
    ortho[1]->p2[0] += amt * vecs[1].x;
    ortho[1]->p2[1] += amt * vecs[1].y;

    int res = quad_connect_raw(dest, ortho[0]->p1, ortho[0]->p2, ortho[1]->p1, ortho[1]->p2);
    if (res > 0)
    {
        // printf("NEED TO FIX\n");
        // quad_print(dest);
        res = quad_orient(dest, res);
        assert(res == 0);
        // printf("SUCCESS\n\n");
    }
}

float vec2_magnitude(vec2* v)
{
    return sqrtf(v->x * v->x + v->y * v->y);
}

void vec2_normalize(vec2* v)
{
    float m = vec2_magnitude(v);
    v->x /= m;
    v->y /= m;
}

void vec2_scale(vec2* v, float c)
{
    v->x *= c;
    v->y *= c;
}

float vec2_dot(vec2* a, vec2* b)
{
    return a->x * b->x + a->y * b->y;
}
