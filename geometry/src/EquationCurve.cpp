#include "EquationCurve.hpp"

namespace geom
{

auto EquationCurve::setFunction(std::function<float(float)> func) -> void
{
    mFunction = std::move(func);
}

auto EquationCurve::setDerivative(std::function<float(float)> func) -> void
{
    mDerivative = std::move(func);
}

auto EquationCurve::solve(float t) const -> float
{
    return mFunction(t);
}
auto EquationCurve::solveDerivative(float t) const -> float
{
    return mDerivative(t);
}

} // namespace geom
