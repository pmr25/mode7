#ifndef GLM_UTILS
#define GLM_UTILS

#include <cereal/archives/json.hpp>
#include <glm/glm.hpp>

namespace glm
{
template <class Archive> inline void serialize(Archive& archive, vec2& obj)
{
    archive(CEREAL_NVP(obj.x), CEREAL_NVP(obj.y));
}

template <class Archive> inline void serialize(Archive& archive, vec3& obj)
{
    archive(CEREAL_NVP(obj.x), CEREAL_NVP(obj.y), CEREAL_NVP(obj.z));
}
} // namespace glm

#endif /* GLM_UTILS */
