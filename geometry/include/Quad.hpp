#ifndef GEOM_QUAD_HPP
#define GEOM_QUAD_HPP

#include "Polygon.hpp"

class Quad : public geom::Polygon
{
public:
    Quad(glm::vec2 a = glm::vec2(0.0f), glm::vec2 b = glm::vec2(0.0f), glm::vec2 c = glm::vec2(0.0f), glm::vec2 d = glm::vec2(0.0f))
        : Polygon()
    {
        connectPoints(a, b, c, d);
    }

    virtual ~Quad() = default;

    void connectPoints(glm::vec2 a, glm::vec2 b, glm::vec2 c, glm::vec2 d)
    {
        std::vector<glm::vec2> pts;

        pts.reserve(4);
        pts.push_back(a);
        pts.push_back(b);
        pts.push_back(c);
        pts.push_back(d);
        Polygon::construct(pts);

        assert(mSides.size() == 4);
    }

    void stretchSide(uint32_t idx, float amt, float bal = 0.5f)
    {
        assert(mSides.size() == 4);
        assert(idx < 4);

        Line<2>& side = mSides[idx];
        assert(side.isSegment());
        if (glm::length(side.getDir()) == 0.0f)
        {
            return;
        }

        glm::vec2 a = side.getPoint();
        glm::vec2 b = side.getEndpoint();

        a = a - (amt * bal) * glm::normalize(side.getDir());
        b = b + (amt * (1.0f - bal)) * glm::normalize(side.getDir());

        side.updatePoints(a, b);
    }
};

#endif /* GEOM_QUAD_HPP */
