#ifndef LINE_HPP
#define LINE_HPP

#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <memory>

#include "ParametricCurve.hpp"
#include "glm_utils.hpp"

namespace geom
{
class Polygon;
}

template <int D>
class Line : public ParametricCurve<D>
{
public:
    Line()
        : ParametricCurve<D>()
    {
    }

    Line(glm::vec<D, float> a, glm::vec<D, float> b)
        : Line()
    {
        connectPoints(a, b);
    }

    auto copy() -> Line<D>
    {
        Line<D> other;

        other.mIsSegment = mIsSegment;
        assert(mPoint);
        other.mPoint = std::make_shared<glm::vec<D, float>>(*(mPoint));
        if (other.mIsSegment)
        {
            assert(mEnd);
            other.mEnd = std::make_shared<glm::vec<D, float>>(*(mEnd));
        }
        other.mDir = mDir;

        return other;
    }

    virtual ~Line() = default;

    void definePointVector(glm::vec<D, float> p, glm::vec<D, float> d)
    {
        mPoint = std::make_shared<glm::vec<D, float>>(p);

        if (glm::length(d) > 0.0F)
        {
            mDir = glm::normalize(d);
        }
    }

    void connectPoints(glm::vec<D, float> a, glm::vec<D, float> b)
    {
        mEnd = std::make_shared<glm::vec<D, float>>(b);
        mIsSegment = true;
        definePointVector(a, b - a);
    }

    [[nodiscard]] auto solve(float t) const -> glm::vec<D, float>
    {
        glm::vec<D, float> soln;

        if (mIsSegment)
        {
            soln = getPoint() + (t * (getEndpoint() - getPoint()));
        }
        else
        {
            soln = getPoint() + (t * mDir);
        }

        return soln;
    }

    [[nodiscard]] auto solveInverse(glm::vec<D, float> pt) const -> float
    {
        auto d = pt - getPoint();
        auto dir = mDir;
        if (mIsSegment)
        {
            if (glm::length(mDir) == 0.0f)
            {
                return 0.0f;
            }
            dir = glm::normalize(mDir);
        }

        float dot = glm::dot(d, dir);

        return dot;
    }

    [[nodiscard]] auto distTo(glm::vec<D, float> pt) const -> float
    {
        if (glm::length(mDir) == 0.0f)
        {
            return 0;
        }

        auto d = pt - getPoint();
        auto proj = glm::dot(d, glm::normalize(mDir)) * glm::normalize(mDir);
        return glm::length(d - proj);
    }

    [[nodiscard]] auto distToSigned(glm::vec<D, float> pt) const -> float
    {
        if (glm::length(mDir) == 0.0f)
        {
            // std::cout << "WARNING: Line length is 0!" << std::endl;
            return 0.0f;
        }
        auto d = pt - getPoint();
        auto proj = glm::dot(d, glm::normalize(mDir)) * glm::normalize(mDir);
        auto diff = d - proj;
        float sign = copysignf(1.0f, glm::dot(diff, computeNormal()));

        return sign * glm::length(diff);
    }

    [[nodiscard]] auto solveForX(float y) const -> float
    {
        const float slope = mDir.y / mDir.x;
        const float intercept = getPoint().y - slope * getPoint().x;
        return (y - intercept) / slope;
    }

    [[nodiscard]] auto solveForY(float x) const -> float
    {
        const float slope = mDir.y / mDir.x;
        const float intercept = getPoint().y - slope * getPoint().x;
        return slope * x + intercept;
    }

    //    [[nodiscard]] auto computeIntersect(Line<D> other) const -> float
    //    {
    //        const auto slope{mDir.y / mDir.x};
    //        const auto otherSlope{other.mDir.y / other.mDir.x};
    //        const auto deltaSlope{slope - otherSlope};
    //        if (deltaSlope == 0)
    //        {
    //            return std::numeric_limits<float>::infinity();
    //        }
    //
    //        const auto intercept{getPoint().y - slope * getPoint().x};
    //        const auto otherIntercept{other.getPoint().y - otherSlope *
    //        other.getPoint().x}; const auto deltaIntercept{intercept -
    //        otherIntercept};
    //
    //        return deltaIntercept / deltaSlope;
    //    }

    [[nodiscard]] auto computeIntersect(Line<D> other) const -> float
    {
        const auto a{-mDir.y};
        const auto b{mDir.x};
        const auto c{mDir.y * getPoint().x - mDir.x * getPoint().y};

        const auto otherA{-other.mDir.y};
        const auto otherB{other.mDir.x};
        const auto otherC{other.mDir.y * other.getPoint().x -
                          other.mDir.x * other.getPoint().y};

        const glm::vec3 u(a, b, c);
        const glm::vec3 v(otherA, otherB, otherC);
        const auto p = glm::cross(u, v);
        if (p.z == 0)
        {
            return std::numeric_limits<float>::infinity();
        }

        const glm::vec2 intersect(p.x / p.z, p.y / p.z);

        // ensure computed intersect falls within line segment if necessary
        const float distToStart = glm::length(intersect - other.getPoint());
        if (other.mIsSegment &&
            (distToStart > other.getLength() || distToStart < 0.0F))
        {
            return std::numeric_limits<float>::infinity();
        }

        const auto delta{intersect - getPoint()};

        return glm::dot(delta, mDir);
    }

    [[nodiscard]] auto computeNormal(glm::vec<D, float> up) const
        -> glm::vec<D, float>
    {
        return glm::cross(glm::normalize(mDir), glm::normalize(up));
    }

    [[nodiscard]] auto computeNormal() const -> glm::vec<D, float>
    {
        glm::vec<D, float> norm(-mDir.y, mDir.x);
        return glm::normalize(norm);
    }

    [[nodiscard]] auto getLength() const -> float
    {
        if (!mIsSegment)
        {
            return 0.0f;
        }

        return glm::length(getEndpoint() - getPoint());
    }

    [[nodiscard]] float estimateLength(uint32_t numSteps) const override
    {
        (void)numSteps; // unused

        return getLength();
    }

    [[nodiscard]] auto interpolate(float stepSize) const
        -> std::vector<glm::vec<D, float>> override
    {
        (void)stepSize; // unused

        std::vector<glm::vec<D, float>> points;
        if (!mIsSegment)
        {
            return points;
        }

        points.reserve(2);

        points.push_back(getPoint());
        points.push_back(getEndpoint());

        return points;
    }

    [[nodiscard]] auto stepToT(uint32_t numSteps, float stepSize) const
        -> float override
    {
        float lengthEstimate =
            estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);
        return ((float)numSteps * stepSize) / lengthEstimate;
    }

    [[nodiscard]] auto numPoints(float stepSize) const -> uint32_t override
    {
        float lengthEstimate =
            estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);
        return (uint32_t)roundf(lengthEstimate / stepSize);
    }

    [[nodiscard]] auto derivative(float t) const -> glm::vec<D, float> override
    {
        (void)t;
        return mDir;
    }

    [[nodiscard]] auto derivative2(float t) const -> glm::vec<D, float> override
    {
        (void)t;
        return glm::vec<D, float>(0.0f);
    }

    [[nodiscard]] auto curvature(float t) const -> float override
    {
        (void)t;
        return 0.0f;
    }

    [[nodiscard]] auto getAverageCurvature(float stepSize) const
        -> float override
    {
        (void)stepSize;
        return 0.0f;
    }

    [[nodiscard]] auto getPoint() const -> glm::vec<D, float>
    {
        return *mPoint;
    }

    [[nodiscard]] auto getEndpoint() const -> glm::vec<D, float>
    {
        return *mEnd;
    }

    [[nodiscard]] auto getDir() const -> glm::vec<D, float>
    {
        return mDir;
    }

    [[nodiscard]] auto isSegment() const -> bool
    {
        return mIsSegment;
    }

    template <class Archive>
    void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(mIsSegment), CEREAL_NVP(mPoint), CEREAL_NVP(mEnd),
                CEREAL_NVP(mDir));
    }

    void extend(Line<2>& from, glm::vec<D, float> to)
    {
        assert(from.isSegment());
        assert(from.mEnd);
        mPoint = from.mEnd; // link
        mEnd = std::make_shared<glm::vec<D, float>>(to);
        mDir = *mEnd - *mPoint;
        mIsSegment = true;
    }

    void updatePoints(glm::vec<D, float> a, glm::vec<D, float> b)
    {
        *mPoint = a;
        *mEnd = b;
    }

    void translate(glm::vec<D, float> delta)
    {
        updatePoints(*mPoint + delta, *mEnd);
    }

    void recomputeDir()
    {
        mDir = glm::normalize(*mEnd - *mPoint);
    }

private:
    bool mIsSegment{false};
    std::shared_ptr<glm::vec<D, float>> mPoint;
    std::shared_ptr<glm::vec<D, float>> mEnd;
    glm::vec<D, float> mDir{0.0f};

    friend class geom::Polygon;
};

template <int D>
std::ostream& operator<<(std::ostream& os, const Line<D>& obj)
{
    os << "Line{" << glm::to_string(obj.getPoint()) << " -> "
       << glm::to_string(obj.getEndpoint()) << "}";
    return os;
}

#endif /* LINE_HPP */
