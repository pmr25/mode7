/**
 * This file is part of mode7-cpp.
 * Copyright (C) 2021  Patrick Roche
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *USA.
 **/

#ifndef RECT_HPP
#define RECT_HPP

#include "GeometryUtils.hpp"
#include "Line.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <vector>

namespace mode7
{
class Rect
{
public:
    Rect()
        : a()
        , b()
        , c()
        , d()
    {
        for (int i = 0; i < 4; ++i)
        {
            p[i] = glm::vec2(0.0f);
        }
    }

    virtual ~Rect() = default;

    /**
     * Order the points like so
     * Must be counterclockwise
     *
     * pa - ld - pd
     * |         |
     * la        lc
     * |         |
     * pb - lb - pc
     */
    inline void connect(glm::vec2 pa, glm::vec2 pb, glm::vec2 pc, glm::vec2 pd)
    {
        p[0] = pa;
        p[1] = pb;
        p[2] = pc;
        p[3] = pd;

        a.connectPoints(pa, pb);
        b.connectPoints(pb, pc);
        c.connectPoints(pc, pd);
        d.connectPoints(pd, pa);
    }

    inline bool checkIntersect(glm::vec2 pt)
    {
        float da = a.distToSigned(pt);
        float db = b.distToSigned(pt);
        float dc = c.distToSigned(pt);
        float dd = d.distToSigned(pt);

        bool status = da >= 0 && db >= 0 && dc >= 0 && dd >= 0;

        return status;
    }

    inline bool checkIntersect(Rect& r)
    {
        auto a = getLines();
        auto b = r.getLines();
        return GeometryUtils::sat(a, b);
    }

    inline std::vector<Line<2>*> getLines()
    {
        std::vector<Line<2>*> out;

        out.reserve(4);
        out.push_back(&a);
        out.push_back(&b);
        out.push_back(&c);
        out.push_back(&d);

        return out;
    }

    Line<2> a;
    Line<2> b;
    Line<2> c;
    Line<2> d;
    glm::vec2 p[4];
};
} // namespace mode7
#endif /* RECT_HPP */
