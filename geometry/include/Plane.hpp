#ifndef GEOM_PLANE_HPP
#define GEOM_PLANE_HPP

#include "glm_utils.hpp"
#include <cereal/types/vector.hpp>
#include <glm/glm.hpp>
#include <vector>

namespace geom
{
class Plane
{
public:
    Plane()
        : mPoint(0.0f)
        , mNormal(0.0f)
        , mU(0.0f)
        , mV(0.0f)
        , mHasDimensions(false)
        , mLength(0.0f)
        , mWidth(0.0f)
    {
    }

    Plane(const Plane& other)
        : mPoint(other.mPoint)
        , mSupports(other.mSupports)
        , mNormal(other.mNormal)
        , mU(other.mU)
        , mV(other.mV)
        , mHasDimensions(other.mHasDimensions)
        , mLength(other.mLength)
        , mWidth(other.mWidth)
    {
    }

    Plane& operator=(const Plane& other)
    {
        mPoint = other.mPoint;
        mNormal = other.mNormal;
        mU = other.mU;
        mV = other.mV;
        mHasDimensions = other.mHasDimensions;
        mLength = other.mLength;
        mWidth = other.mWidth;
        mSupports = other.mSupports;
        return *this;
    }

    virtual ~Plane() = default;

    inline void definePointNormal(glm::vec3 point, glm::vec3 norm)
    {
        mHasDimensions = false;
        mPoint = point;
        mNormal = glm::normalize(norm);
    }

    inline void definePointNormal(glm::vec3 point, glm::vec3 norm, glm::vec3 up)
    {
        definePointNormal(point, norm);

        mU = glm::normalize(glm::cross(norm, up));
        mV = glm::normalize(glm::cross(mU, norm));
    }

    inline void definePoints(glm::vec3 a, glm::vec3 b, glm::vec3 c,
                             bool center = false)
    {
        glm::vec3 ctr = b;
        if (center)
        {
            ctr = a + (glm::normalize(b - a) * glm::length(b - a) / 2.0f);
        }

        mSupports.push_back(a);
        mSupports.push_back(b);
        mSupports.push_back(c);

        mU = b - a;
        mV = c - b;
        mLength = glm::length(mU);
        mWidth = glm::length(mV);

        mU = glm::normalize(mU);
        mV = glm::normalize(mV);
        assert(glm::dot(mV, mU) < 1.0f);

        definePointNormal(ctr, glm::cross(mU, mV));
        mHasDimensions = true;
    }

    inline void setDimensions(float length, float width)
    {
        mLength = length;
        mWidth = width;
    }

    inline void transform(glm::mat4 matrix)
    {
        mPoint = glm::vec3(matrix * glm::vec4(mPoint, 1.0f));
        mNormal = glm::vec3(matrix * glm::vec4(mNormal, 0.0f));
        mU = glm::vec3(matrix * glm::vec4(mU, 0.0f));
        mV = glm::vec3(matrix * glm::vec4(mV, 0.0f));
        for (auto& e : mSupports)
        {
            e = glm::vec3(matrix * glm::vec4(e, 1.0f));
        }
    }

    inline float distanceTo(const glm::vec3 point) const
    {
        const glm::vec3 d = point - mPoint;
        return glm::dot(mNormal, d);
    }

    inline float distanceFromOrigin(glm::vec3 point) const
    {
        point.y = 0;
        glm::vec3 d = point - mPoint;
        float dot = glm::dot(d, mNormal);
        d = d - (dot * mNormal);
        return glm::length(d);
    }

    inline glm::vec3 getPoint() const
    {
        return mPoint;
    }

    inline glm::vec3 getNormal() const
    {
        return mNormal;
    }

    inline std::pair<glm::vec3, glm::vec3> getBasisVectors() const
    {
        return std::pair<glm::vec3, glm::vec3>(mU, mV);
    }

    inline const std::vector<glm::vec3>& getSupports() const
    {
        return mSupports;
    }

    template <class Archive>
    inline void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(mHasDimensions), CEREAL_NVP(mPoint),
                CEREAL_NVP(mNormal), CEREAL_NVP(mSupports));
    }

private:
    glm::vec3 mPoint;
    std::vector<glm::vec3> mSupports;
    glm::vec3 mNormal;
    glm::vec3 mU;
    glm::vec3 mV;
    bool mHasDimensions;
    float mLength;
    float mWidth;
};
} // namespace geom

#endif /* GEOM_PLANE_HPP */