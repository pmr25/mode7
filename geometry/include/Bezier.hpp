#ifndef BEZIER_HPP
#define BEZIER_HPP

#include "ParametricCurve.hpp"
#include <cstdint>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <vector>

template <int D> class Bezier : public ParametricCurve<D>
{
public:
    inline void createOneControl(glm::vec<D, float> start, glm::vec<D, float> end, glm::vec<D, float> c1)
    {
        setStartPoint(start);
        setEndPoint(end);
        addControlPoint(c1);
    }

    inline void createTwoControl(glm::vec<D, float> start, glm::vec<D, float> end, glm::vec<D, float> c1, glm::vec<D, float> c2)
    {
        setStartPoint(start);
        setEndPoint(end);
        addControlPoint(c1);
        addControlPoint(c2);
    }

    inline void setStartPoint(glm::vec<D, float> pt)
    {
        reset();
        mStartPoint = pt;
    }

    inline void setEndPoint(glm::vec<D, float> pt)
    {
        mEndPoint = pt;
    }

    inline void addControlPoint(glm::vec<D, float> pt)
    {
        mControlPoints.push_back(pt);
    }

    inline void reset()
    {
        mControlPoints.clear();
    }

    inline auto solve(float t) const -> glm::vec<D, float>
    {
        if (mControlPoints.size() == 1)
        {
            return (1.0f - t) * ((1.0f - t) * mStartPoint + t * mControlPoints[0]) +
                   t * ((1.0f - t) * mControlPoints[0] + t * mEndPoint);
        }
        if (mControlPoints.size() == 2)
        {
            return powf(1.0f - t, 3.0f) * mStartPoint + 3.0f * powf(1.0f - t, 2.0f) * t * mControlPoints[0] +
                   3.0f * (1.0f - t) * t * t * mControlPoints[1] + powf(t, 3.0f) * mEndPoint;
        }

        assert(0); // error
        return glm::vec<D, float>(0.0f);
    }

    [[nodiscard]] inline virtual float estimateLength(uint32_t numSteps) const
    {
        float sum = 0.0f;

        for (uint32_t i = 0; i <= numSteps; ++i)
        {
            const float u = (float)i / (float)numSteps;
            const float v = (float)(i + 1) / (float)numSteps;
            sum += glm::length(solve(u) - solve(v));
        }

        return sum;
    }

    inline virtual auto interpolate(float stepSize) const -> std::vector<glm::vec<D, float>>
    {
        float lengthEst = estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);

        uint32_t numPoints = (uint32_t)roundf(lengthEst / stepSize);
        std::vector<glm::vec<D, float>> points;
        points.reserve(numPoints);

        for (uint32_t i = 0; i <= numPoints; ++i)
        {
            const float t = (float)i / (float)numPoints;
            points.push_back(solve(t));
        }

        return points;
    }

    [[nodiscard]] inline virtual auto stepToT(uint32_t numSteps, float stepSize) const -> float
    {
        float lengthEstimate = estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);
        return ((float)numSteps * stepSize) / lengthEstimate;
    }

    [[nodiscard]] inline virtual auto numPoints(float stepSize) const -> uint32_t
    {
        float lengthEstimate = estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);
        return (uint32_t)roundf(lengthEstimate / stepSize);
    }

    inline virtual auto derivative(float t) const -> glm::vec<D, float>
    {
        if (mControlPoints.size() == 1)
        {
            return 2.0f * (1.0f - t) * (mControlPoints[0] - mStartPoint) + 2.0f * t * (mEndPoint - mControlPoints[0]);
        }
        if (mControlPoints.size() == 2)
        {
            return 3.0f * (1.0f - t) * (1.0f - t) * (mControlPoints[0] - mStartPoint) +
                   6.0f * (1.0f - t) * t * (mControlPoints[1] - mControlPoints[0]) + 3.0f * t * t * (mEndPoint - mControlPoints[1]);
        }

        assert(0); // error
        return glm::vec<D, float>();
    }

    inline virtual auto derivative2(float t) const -> glm::vec<D, float>
    {
        if (mControlPoints.size() == 1)
        {
            return 2.0f * mEndPoint - 2.0f * mControlPoints[0] + mStartPoint;
        }
        if (mControlPoints.size() == 2)
        {
            return 6.0f * (1.0f - t) * (mControlPoints[1] - 2.0f * mControlPoints[0] + mStartPoint) +
                   6.0f * t * (mEndPoint - 2.0f * mControlPoints[1] + mControlPoints[0]);
        }

        assert(0); // error;
        return glm::vec<D, float>();
    }

    [[nodiscard]] inline virtual float curvature(float t) const
    {
        // curvature k = ||r'(t) x r''(t)|| / ||r'(t)||^3

        const glm::vec<D, float> deriv{derivative(t)};
        const glm::vec<D, float> deriv2{derivative2(t)};
        float cross;
        if constexpr (D == 3)
        {
            cross = glm::length(glm::cross(deriv, deriv2));
        }
        else if constexpr (D == 2)
        {
            cross = deriv.x * deriv2.y - deriv.y * deriv2.x; // 2d cross product
        }
        const float lenCubed = powf(glm::length(deriv), 3.0f);

        return cross / lenCubed;
    }

    [[nodiscard]] inline virtual auto getAverageCurvature(float stepSize) const -> float
    {
        const float lengthEst = estimateLength(GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION);
        const uint32_t numPoints = (uint32_t)roundf(lengthEst / stepSize);
        float sum = 0;

        for (uint32_t i = 0; i <= numPoints; ++i)
        {
            const float t = (float)i / (float)numPoints;
            sum += curvature(t);
        }

        return sum / lengthEst;
    }

    inline auto getStartPoint() const -> glm::vec<D, float>
    {
        return mStartPoint;
    }

    inline auto getEndPoint() const -> glm::vec<D, float>
    {
        return mEndPoint;
    }

    inline auto getControlPoint(uint32_t idx) const -> glm::vec<D, float>
    {
        glm::vec<D, float> out(0.0f);

        if (idx < mControlPoints.size())
        {
            out = mControlPoints[idx];
        }

        return out;
    }

    inline auto getControlPoints() const -> const std::vector<glm::vec<D, float>>&
    {
        return mControlPoints;
    }

private:
    glm::vec<D, float> mStartPoint;
    glm::vec<D, float> mEndPoint;
    std::vector<glm::vec<D, float>> mControlPoints;
};

template <int D> inline std::ostream& operator<<(std::ostream& os, const Bezier<D>& obj)
{
    os << "Bezier{";
    os << "s:" << glm::to_string(obj.getStartPoint()) << ", ";
    os << "e:" << glm::to_string(obj.getEndPoint()) << ", ";
    for (auto& e : obj.getControlPoints())
    {
        os << "c:" << glm::to_string(e) << ", ";
    }
    os << "}";

    return os;
}

#endif /* BEZIER_HPP */
