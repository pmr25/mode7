#ifndef MODE7_EQUATIONCURVE_HPP
#define MODE7_EQUATIONCURVE_HPP

#include <functional>
#include <utility>

#define SCHEMA float(float)
#define ZERO [](float) -> float { return 0.0F; }

namespace geom
{

class EquationCurve
{
public:
    EquationCurve(std::function<SCHEMA> s = ZERO, std::function<SCHEMA> ds = ZERO)
        : mFunction(std::move(s))
        , mDerivative(std::move(ds))
    {
    }
    ~EquationCurve() = default;

    auto setFunction(std::function<SCHEMA>) -> void;
    auto setDerivative(std::function<SCHEMA>) -> void;
    [[nodiscard]] auto solve(float) const -> float;
    [[nodiscard]] auto solveDerivative(float) const -> float;

private:
    std::function<SCHEMA> mFunction;
    std::function<SCHEMA> mDerivative;
};

} // namespace geom

#endif // MODE7_EQUATIONCURVE_HPP
