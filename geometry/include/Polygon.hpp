#ifndef GEOM_POLYGON_HPP
#define GEOM_POLYGON_HPP

#include "GeometryUtils.hpp"
#include "Line.hpp"
#include "glm_utils.hpp"
#include <cassert>
#include <cereal/types/vector.hpp>
#include <cstdint>
#include <iostream>
#include <vector>

namespace geom
{
class Polygon
{
public:
    Polygon() = default;

    Polygon(const Polygon& other)
    {
        mSides = std::vector<Line<2>>(other.mSides);
    }

    auto operator=(const Polygon& other) -> Polygon&
    {
        mSides = other.mSides;
        return *this;
    }

    virtual ~Polygon() = default;

    virtual void copy(const Polygon& other)
    {
        std::vector<glm::vec2> pts;

        for (auto& e : other.mSides)
        {
            pts.push_back(e.getPoint());
        }

        construct(pts);
    }

    void construct(std::vector<glm::vec2>& pts)
    {
        reset();
        Line<2> ln;

        assert(pts.size() > 2);
        ln.connectPoints(pts[0], pts[1]);
        mSides.push_back(ln);

        for (uint32_t i = 1; i < pts.size(); ++i)
        {
            assert(mSides.size() > 0);
            mSides.push_back(ln);
            mSides[i].extend(mSides[i - 1], pts[(i + 1) % pts.size()]);
        }

        mSides.back().mEnd = mSides.front().mPoint;
    }

    void reconstruct()
    {
        std::vector<glm::vec2> pts;
        for (Line<2>& e : mSides)
        {
            pts.push_back(e.getPoint());
        }
        std::cout << std::endl;

        construct(pts);
    }

    bool checkIntersect(const Polygon& other) const
    {
        return GeometryUtils::sat<2>(mSides, other.mSides);
    }

    bool checkIntersect(glm::vec2 pt) const
    {
        for (auto& e : mSides)
        {
            if (e.distToSigned(pt) < 0)
            {
                return false;
            }
        }

        return true;
    }

    uint32_t getNumSides() const
    {
        return uint32_t(mSides.size());
    }

    const std::vector<Line<2>>& getSides() const
    {
        return mSides;
    }

    glm::vec2 getPoint(uint32_t idx) const
    {
        if (idx >= mSides.size())
        {
            assert(0);
        }

        return mSides[idx].getPoint();
    }

    void reset()
    {
        mSides.clear();
    }

    void translate(glm::vec2 delta)
    {
        for (auto& line : mSides)
        {
            line.translate(delta);
        }
        for (auto& line : mSides)
        {
            line.recomputeDir();
        }
    }

    template <class Archive> void serialize(Archive& archive)
    {
        archive(CEREAL_NVP(mSides));
    }

protected:
    std::vector<Line<2>> mSides;
};
} // namespace geom

inline std::ostream& operator<<(std::ostream& os, const geom::Polygon& obj)
{
    os << "Polygon(" << obj.getSides().size() << ")";

    for (auto& e : obj.getSides())
    {
        os << "\n\t" << glm::to_string(e.getPoint()) << " -> "
           << glm::to_string(e.getEndpoint());
    }

    return os;
}

#endif /* GEOM_POLYGON_HPP */
