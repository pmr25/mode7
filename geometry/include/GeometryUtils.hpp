#ifndef GEOMETRYUTILS_HPP
#define GEOMETRYUTILS_HPP

#include "Line.hpp"

#include <cassert>
#include <vector>

class GeometryUtils
{
public:
    template <int D>
    static glm::vec<D, float> project(const std::vector<Line<D>*>& shape,
                                      Line<D>* axis)
    {
        glm::vec<D, float> a;
        glm::vec<D, float> b;
        glm::vec<D, float> origin = axis->getPoint();
        glm::vec<D, float> dir = axis->getDir();
        glm::vec2 proj(FLT_MAX, FLT_MIN);

        for (auto& e : shape)
        {
            assert(e->isSegment());

            a = e->getPoint() - origin;
            b = e->getEndpoint() - origin;

            proj.x = fmin(proj.x, glm::dot(a, dir));
            proj.y = fmax(proj.y, glm::dot(b, dir));
        }

        return proj;
    }

    template <int D>
    static glm::vec<D, float> project(const std::vector<Line<D>>& shape,
                                      Line<D> axis)
    {
        glm::vec<D, float> a;
        glm::vec<D, float> b;
        glm::vec<D, float> origin = axis.getPoint();
        glm::vec<D, float> dir = axis.getDir();
        glm::vec2 proj(FLT_MAX, FLT_MIN);

        for (auto& e : shape)
        {
            assert(e.isSegment());

            a = e.getPoint() - origin;
            b = e.getEndpoint() - origin;

            proj.x = fmin(proj.x, glm::dot(a, dir));
            proj.y = fmax(proj.y, glm::dot(b, dir));
        }

        return proj;
    }

    template <int D>
    static bool check_overlap(glm::vec<D, float> a, glm::vec<D, float> b)
    {
        return ((a.x >= b.x) && (a.x <= b.y)) ||
               ((a.y >= b.x) && (a.y <= b.y)) ||
               ((b.x >= a.x) && (b.x <= a.y)) || ((b.y >= a.x) && (b.y <= a.y));
    }

    template <int D>
    static bool sat(const std::vector<Line<D>*>& a,
                    const std::vector<Line<D>*>& b)
    {
        glm::vec<D, float> axis;
        glm::vec<D, float> proj_a;
        glm::vec<D, float> proj_b;

        for (auto& e : a)
        {
            axis = glm::normalize(e->getDir());

            proj_a = project<D>(a, e);
            proj_b = project<D>(b, e);

            if (!check_overlap<D>(proj_a, proj_b))
            {
                return false;
            }
        }

        for (auto& e : b)
        {
            axis = glm::normalize(e->getDir());

            proj_a = project<D>(a, e);
            proj_b = project<D>(b, e);

            if (!check_overlap<D>(proj_a, proj_b))
            {
                return false;
            }
        }

        return true;
    }

    template <int D>
    static bool sat(const std::vector<Line<D>>& a,
                    const std::vector<Line<D>>& b)
    {
        glm::vec<D, float> axis;
        glm::vec<D, float> proj_a;
        glm::vec<D, float> proj_b;

        for (auto& e : a)
        {
            axis = glm::normalize(e.getDir());

            proj_a = project<D>(a, e);
            proj_b = project<D>(b, e);

            if (!check_overlap<D>(proj_a, proj_b))
            {
                return false;
            }
        }

        for (auto& e : b)
        {
            axis = glm::normalize(e.getDir());

            proj_a = project<D>(a, e);
            proj_b = project<D>(b, e);

            if (!check_overlap<D>(proj_a, proj_b))
            {
                return false;
            }
        }

        return true;
    }

    template <int D>
    static auto pointsToSegments(const std::vector<glm::vec<D, float>>& points)
        -> std::vector<Line<D>>
    {
        std::vector<Line<D>> lines;

        for (auto it{points.begin() + 1}; it != points.end(); ++it)
        {
            lines.emplace_back(*(it - 1), *(it - 0));
        }

        return lines;
    }
};

#endif /* GEOMETRYUTILS_HPP */