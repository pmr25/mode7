#ifndef PARAMETRICCURVE_HPP
#define PARAMETRICCURVE_HPP

#include <concepts>
#include <cstdint>
#include <glm/glm.hpp>
#include <vector>

#define GEOM_PARAMETRIC_CURVE_DEFAULT_RESOLUTION 1000

template <int D>
class ParametricCurve
{
public:
    ParametricCurve() = default;
    virtual ~ParametricCurve() = default;

    virtual float estimateLength(uint32_t) const = 0;
    virtual std::vector<glm::vec<D, float>> interpolate(float) const = 0;
    virtual float stepToT(uint32_t, float) const = 0;
    virtual uint32_t numPoints(float) const = 0;
    virtual glm::vec<D, float> derivative(float) const = 0;
    virtual glm::vec<D, float> derivative2(float) const = 0;
    virtual float curvature(float) const = 0;
    virtual float getAverageCurvature(float stepSize) const = 0;
};

#endif /* PARAMETRICCUREVE_HPP */
